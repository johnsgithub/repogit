package com.jpw.raptor.cmdline.scrapeweb;


import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;

import com.jpw.raptor.model.EquityToScrape;
import com.jpw.raptor.model.Etf;
import com.jpw.raptor.model.Stock;

import com.jpw.raptor.scrape.Scrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.io.IOException;

/**
 * Created by john on 4/3/18.
 */
@Service
public class ScrapeWorker {

    //Logger log = LoggerFactory.getLogger(this.getClass().getName());
    static final  long SLEEP_TIME = 500L;

    static final String pattern = "HH:mm:ss:SSS";

    protected BlockingQueue<EquityToScrape> queue;
    protected BlockingQueue<EquityToScrape> errorQ;

    @Autowired
    public EtfDAO etfTbl;

    @Autowired
    public StockDAO stockTbl;


    @Async
    public Future<String> process(BlockingQueue<EquityToScrape> blockingQ, BlockingQueue<EquityToScrape> errors) throws IOException, InterruptedException {

        this.queue  = blockingQ;
        this.errorQ = errors;

        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        String          threadName  = Thread.currentThread().getName();


        boolean dataFound = true;

        while ( dataFound ) {

            EquityToScrape equity = queue.poll(1l, TimeUnit.SECONDS);

            if ( equity != null ) {

                System.out.println("### " + equity.getSymbol() + " begin   "  + " " + threadName);

                if ( equity.getType().equalsIgnoreCase("etf") || equity.getType().equalsIgnoreCase("fund") ) {
                    if ( !processEtfFund(equity, threadName) ) {

                        System.out.println("### " + equity.getSymbol() + " re-queue  " +  threadName);
                        errorQ.put(equity);
                    } else {
                        System.out.println("### " + equity.getSymbol() + " finish    "  + " " + threadName);
                    }
                } else if ( equity.getType().equalsIgnoreCase("stock") ) {
                    if ( !processStock(equity, threadName) ) {

                        System.out.println("### " + equity.getSymbol() + " re-queue  " +  threadName);
                        errorQ.put(equity);
                    } else {
                        System.out.println("### " + equity.getSymbol() + " finish    "  + " " + threadName);
                    }
                } else {
                    //log.info( "### Unknown equity type " + equity.getType() + " " + threadName );
                    System.out.println("### " + equity.getSymbol() + " Unknown equity type   "  +  threadName);
                    errorQ.put(equity);
                }


                TimeUnit.MILLISECONDS.sleep(SLEEP_TIME);
            } else {
                dataFound = false;
                //log.info("### equity queue empty " + threadName );
                System.out.println( sdf.format(new Date()) + " " + threadName + " equity queue empty " );
            }
        }



        //close chrome
        //driver.close();

        String processInfo = String.format("### Processing is Done with Thread id= %d", Thread.currentThread().getId());
        return new AsyncResult<>(processInfo);
    }


    public boolean processStock(EquityToScrape  equity, String threadName) throws IOException  {

        //log.info("### process start  " + equity.getSymbol() + " " + threadName );

        boolean result = true;

        // Read record from database
        Stock stockRecord = stockTbl.get(equity.getSymbol());

        // Scrape data
        Scrapper scrapper = new Scrapper();
        Stock updatedStockRecord = scrapper.scrapeStock(stockRecord);

        // Validate the scrape
        if ( updatedStockRecord == null ) {
            // Error occurred requeue the equity

            //System.out.println("### " + equity.getSymbol() + " page read fail "  + threadName );
            result = false;
        } else {
            // Update the database
            stockTbl.update(updatedStockRecord);
        }


        return result;
    }


    public boolean processEtfFund(EquityToScrape  equity, String threadName) throws IOException, InterruptedException {

        boolean result = true;

        // Read record from database
        Etf etfRecord = etfTbl.get(equity.getSymbol());

        // Scrape data
        Scrapper scrapper = new Scrapper();
        Etf updatedEtfRecord = scrapper.scrapeEtf(etfRecord);

        // Validate the scrape
        if ( updatedEtfRecord == null ) {
            // Error occurred requeue the equity

            //System.out.println("### " + equity.getSymbol() + " page read fail "  + threadName );
            result = false;
        } else {
            // Update the database
            etfTbl.update(updatedEtfRecord);
        }

        return result;
    }

}

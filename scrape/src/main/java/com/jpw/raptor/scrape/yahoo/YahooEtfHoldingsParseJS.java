package com.jpw.raptor.scrape.yahoo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class YahooEtfHoldingsParseJS {

    static final String SPAN_START = "<span";
    static final String SPAN_END = "</span>";
    static final String TBODY_START = "<tbody>";
    static final String TBODY_END = "</tbody>";
    static final String TR_START = "<tr ";
    static final String TR_END = "</tr>";
    static final String TD_START = "<td ";
    static final String TD_END = "</td>";
    static final String HREF_START = "<a href";
    static final String HREF_END = "</a>";
    static final String GREATER_THAN = ">";
    static final String LESS_THAN = "<";



    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());




    public void parse ( String page, YahooEtfHoldings h ) {

        parseOverallPortfolio (  page,  h );
        parseSectorWeightings (  page,  h );
        parseEquityHoldings (  page,  h );
        parseBondRatings (  page,  h );
        parseTopHoldings( page, h );
    }

    public void parseOverallPortfolio ( String page, YahooEtfHoldings h ) {

        ValueObject vo = new ValueObject();

        //
        // Overall Portfolio Composition
        //
        int overallStart = page.indexOf(">Overall Portfolio Composition");
        if ( overallStart == -1 ) return;

        //  Stock percentage
        int stocksLabelStart = page.indexOf(">Stocks<", overallStart);
        if ( stocksLabelStart > 0 ) {
            int stocksTDStart = page.indexOf(TD_START, stocksLabelStart) + 1;
            int stocksStartData = page.indexOf(GREATER_THAN, stocksTDStart) + 1;
            int stocksEndData = page.indexOf(LESS_THAN, stocksStartData);
            h.setStocks(vo.getPercent(page.substring(stocksStartData, stocksEndData)));
        }

        //  Bonds percentage
        int bondsLabelStart = page.indexOf(">Bonds<", overallStart);
        if ( bondsLabelStart > 0 ) {
            int bondsTDStart = page.indexOf(TD_START, bondsLabelStart) + 1;
            int bondsStartData = page.indexOf(GREATER_THAN, bondsTDStart) + 1;
            int bondsEndData = page.indexOf(LESS_THAN, bondsStartData);
            h.setBonds(vo.getPercent(page.substring(bondsStartData, bondsEndData)));
        }
    }

    public void parseSectorWeightings ( String page, YahooEtfHoldings h ) {

        ValueObject vo = new ValueObject();

        //
        // Sector weightings
        //
        int sectorsStart = page.indexOf(">Sector Weightings<");
        if ( sectorsStart == -1 ) return;
        if ( page.contains("Sector Weightings Information Not Available") ) return;

        //  Technology
        int technologyLabelStart = page.indexOf(">Technology", sectorsStart);
        if ( technologyLabelStart > 0 ) {
            int technologySpan1 = page.indexOf(SPAN_START, technologyLabelStart) + 1;
            int technologySpan2 = page.indexOf(SPAN_START, technologySpan1) + 1;
            int technologyStartData = page.indexOf(GREATER_THAN, technologySpan2) + 1;
            int technologyEndData = page.indexOf(LESS_THAN, technologyStartData);
            h.setTechnology(vo.getPercent(page.substring(technologyStartData, technologyEndData)));
        }

        //  Healthcare
        int healthcareLabelStart = page.indexOf(">Healthcare", sectorsStart);
        if ( healthcareLabelStart > 0 ) {
            int healthcareSpan1 = page.indexOf(SPAN_START, healthcareLabelStart) + 1;
            int healthcareSpan2 = page.indexOf(SPAN_START, healthcareSpan1) + 1;
            int healthcareStartData = page.indexOf(GREATER_THAN, healthcareSpan2) + 1;
            int healthcareEndData = page.indexOf(LESS_THAN, healthcareStartData);
            h.setHealthcare(vo.getPercent(page.substring(healthcareStartData, healthcareEndData)));
        }

        //  Financial Services
        int financialServicesLabelStart = page.indexOf(">Financial Services", sectorsStart);
        if ( financialServicesLabelStart > 0 ) {
            int financialServicesSpan1 = page.indexOf(SPAN_START, financialServicesLabelStart) + 1;
            int financialServicesSpan2 = page.indexOf(SPAN_START, financialServicesSpan1) + 1;
            int financialServicesStartData = page.indexOf(GREATER_THAN, financialServicesSpan2) + 1;
            int financialServicesEndData = page.indexOf(LESS_THAN, financialServicesStartData);
            h.setFinancialServices(vo.getPercent(page.substring(financialServicesStartData, financialServicesEndData)));
        }

        //  Consumer Cyclical
        int consumerCyclicalLabelStart = page.indexOf(">Consumer Cyclical", sectorsStart);
        if ( consumerCyclicalLabelStart > 0 ) {
            int consumerCyclicalSpan1 = page.indexOf(SPAN_START, consumerCyclicalLabelStart) + 1;
            int consumerCyclicalSpan2 = page.indexOf(SPAN_START, consumerCyclicalSpan1) + 1;
            int consumerCyclicalStartData = page.indexOf(GREATER_THAN, consumerCyclicalSpan2) + 1;
            int consumerCyclicalEndData = page.indexOf(LESS_THAN, consumerCyclicalStartData);
            h.setConsumerCyclical(vo.getPercent(page.substring(consumerCyclicalStartData, consumerCyclicalEndData)));
        }

        //  Communication Services
        int communicationServicesLabelStart = page.indexOf(">Communication Services", sectorsStart);
        if ( communicationServicesLabelStart > 0 ) {
            int communicationServicesSpan1 = page.indexOf(SPAN_START, communicationServicesLabelStart) + 1;
            int communicationServicesSpan2 = page.indexOf(SPAN_START, communicationServicesSpan1) + 1;
            int communicationServicesStartData = page.indexOf(GREATER_THAN, communicationServicesSpan2) + 1;
            int communicationServicesEndData = page.indexOf(LESS_THAN, communicationServicesStartData);
            h.setCommunicationServices(vo.getPercent(page.substring(communicationServicesStartData, communicationServicesEndData)));
        }

        //  Industrials
        int industrialsLabelStart = page.indexOf(">Industrials", sectorsStart);
        if ( industrialsLabelStart > 0 ) {
            int industrialsSpan1 = page.indexOf(SPAN_START, industrialsLabelStart) + 1;
            int industrialsSpan2 = page.indexOf(SPAN_START, industrialsSpan1) + 1;
            int industrialsStartData = page.indexOf(GREATER_THAN, industrialsSpan2) + 1;
            int industrialsEndData = page.indexOf(LESS_THAN, industrialsStartData);
            h.setIndustrials(vo.getPercent(page.substring(industrialsStartData, industrialsEndData)));
        }

        //  Consumer Defensive
        int consumerDefensiveLabelStart = page.indexOf(">Consumer Defensive", sectorsStart);
        if ( consumerDefensiveLabelStart > 0 ) {
            int consumerDefensiveSpan1 = page.indexOf(SPAN_START, consumerDefensiveLabelStart) + 1;
            int consumerDefensiveSpan2 = page.indexOf(SPAN_START, consumerDefensiveSpan1) + 1;
            int consumerDefensiveStartData = page.indexOf(GREATER_THAN, consumerDefensiveSpan2) + 1;
            int consumerDefensiveEndData = page.indexOf(LESS_THAN, consumerDefensiveStartData);
            h.setConsumerDefensive(vo.getPercent(page.substring(consumerDefensiveStartData, consumerDefensiveEndData)));
        }

        //  Energy
        int energyLabelStart = page.indexOf(">Energy", sectorsStart);
        if ( energyLabelStart > 0 ) {
            int energySpan1 = page.indexOf(SPAN_START, energyLabelStart) + 1;
            int energySpan2 = page.indexOf(SPAN_START, energySpan1) + 1;
            int energyStartData = page.indexOf(GREATER_THAN, energySpan2) + 1;
            int energyEndData = page.indexOf(LESS_THAN, energyStartData);
            h.setEnergy(vo.getPercent(page.substring(energyStartData, energyEndData)));
        }

        //  Real Estate
        int realEstateLabelStart = page.indexOf(">Real Estate", sectorsStart);
        if ( realEstateLabelStart > 0 ) {
            int realEstateSpan1 = page.indexOf(SPAN_START, realEstateLabelStart) + 1;
            int realEstateSpan2 = page.indexOf(SPAN_START, realEstateSpan1) + 1;
            int realEstateStartData = page.indexOf(GREATER_THAN, realEstateSpan2) + 1;
            int realEstateEndData = page.indexOf(LESS_THAN, realEstateStartData);
            h.setRealEstate(vo.getPercent(page.substring(realEstateStartData, realEstateEndData)));
        }

        //  Utilities
        int utilitiesLabelStart = page.indexOf(">Utilities", sectorsStart);
        if ( utilitiesLabelStart > 0 ) {
            int utilitiesSpan1 = page.indexOf(SPAN_START, utilitiesLabelStart) + 1;
            int utilitiesSpan2 = page.indexOf(SPAN_START, utilitiesSpan1) + 1;
            int utilitiesStartData = page.indexOf(GREATER_THAN, utilitiesSpan2) + 1;
            int utilitiesEndData = page.indexOf(LESS_THAN, utilitiesStartData);
            h.setUtilities(vo.getPercent(page.substring(utilitiesStartData, utilitiesEndData)));
        }

        //  Basic Materials
        int basicMaterialsLabelStart = page.indexOf(">Basic Materials", sectorsStart);
        if ( basicMaterialsLabelStart > 0 ) {
            int basicMaterialsSpan1 = page.indexOf(SPAN_START, basicMaterialsLabelStart) + 1;
            int basicMaterialsSpan2 = page.indexOf(SPAN_START, basicMaterialsSpan1) + 1;
            int basicMaterialsStartData = page.indexOf(GREATER_THAN, basicMaterialsSpan2) + 1;
            int basicMaterialsEndData = page.indexOf(LESS_THAN, basicMaterialsStartData);
            h.setBasicMaterials(vo.getPercent(page.substring(basicMaterialsStartData, basicMaterialsEndData)));
        }
    }

    public void parseEquityHoldings ( String page, YahooEtfHoldings h ) {

        ValueObject vo = new ValueObject();
        //
        // Equity Holdings
        //

        int equityHoldingsStart = page.indexOf(">Equity Holdings");
        if ( equityHoldingsStart == -1 ) return;

        //  Price/Earnings
        int priceEarningsLabelStart = page.indexOf(">Price/Earnings", equityHoldingsStart);
        if ( priceEarningsLabelStart > 0 ) {
            int priceEarningsTDStart = page.indexOf(TD_START, priceEarningsLabelStart) + 1;
            int priceEarningsStartData = page.indexOf(GREATER_THAN, priceEarningsTDStart) + 1;
            int priceEarningsEndData = page.indexOf(LESS_THAN, priceEarningsStartData);
            h.setPriceEarnings(vo.getDouble(page.substring(priceEarningsStartData, priceEarningsEndData)));
        }

        //  Price/Book
        int priceBookLabelStart = page.indexOf(">Price/Book", equityHoldingsStart);
        if ( priceBookLabelStart > 0 ) {
            int priceBookTDStart = page.indexOf(TD_START, priceBookLabelStart);
            int priceBookStartData = page.indexOf(GREATER_THAN, priceBookTDStart) + 1;
            int priceBookEndData = page.indexOf(LESS_THAN, priceBookStartData);
            h.setPriceBook(vo.getDouble(page.substring(priceBookStartData, priceBookEndData)));
        }

        //  Price/Sales
        int priceSalesLabelStart = page.indexOf(">Price/Sales", equityHoldingsStart);
        if ( priceSalesLabelStart > 0 ) {
            int priceSalesTDStart = page.indexOf(TD_START, priceSalesLabelStart) + 1;
            int priceSalesStartData = page.indexOf(GREATER_THAN, priceSalesTDStart) + 1;
            int priceSalesEndData = page.indexOf(LESS_THAN, priceSalesStartData);
            h.setPriceSales(vo.getDouble(page.substring(priceSalesStartData, priceSalesEndData)));
        }

        //  Price/Cashflow
        int priceCashFlowLabelStart = page.indexOf(">Price/Cashflow", equityHoldingsStart);
        if ( priceCashFlowLabelStart > 0 ) {
            int priceCashFlowTDStart = page.indexOf(TD_START, priceCashFlowLabelStart) + 1;
            int priceCashFlowStartData = page.indexOf(GREATER_THAN, priceCashFlowTDStart) + 1;
            int priceCashFlowEndData = page.indexOf(LESS_THAN, priceCashFlowStartData);
            h.setPriceCashFlow(vo.getDouble(page.substring(priceCashFlowStartData, priceCashFlowEndData)));
        }

        //  Median Market Cap
        int marketCapLabelStart = page.indexOf(">Median Market Cap", equityHoldingsStart);
        if ( marketCapLabelStart > 0 ) {
            int marketCapTDStart = page.indexOf(TD_START, marketCapLabelStart) + 1;
            int marketCapStartData = page.indexOf(GREATER_THAN, marketCapTDStart) + 1;
            int marketCapEndData = page.indexOf(LESS_THAN, marketCapStartData);
            h.setMarketCap(vo.getDouble(page.substring(marketCapStartData, marketCapEndData)));
        }
    }

    public void parseBondRatings ( String page, YahooEtfHoldings h ) {

        ValueObject vo = new ValueObject();
        //
        // Bond ratings
        //
        int bondRatingsStart     = page.indexOf(">Bond Ratings");
        if ( bondRatingsStart == -1 ) return;

        //  US Government
        int bondUsLabelStart     = page.indexOf(">US Government", bondRatingsStart);
        if ( bondUsLabelStart > 0 ) {
            int bondUsTDStart = page.indexOf(TD_START, bondUsLabelStart) + 1;
            int bondUsStartData = page.indexOf(GREATER_THAN, bondUsTDStart) + 1;
            int bondUsEndData = page.indexOf(LESS_THAN, bondUsStartData);
            h.setBondUs(vo.getPercent(page.substring(bondUsStartData, bondUsEndData)));
        }

        //  AAA
        int bondAaaLabelStart     = page.indexOf(">AAA<", bondRatingsStart);
        if ( bondAaaLabelStart > 0 ) {
            int bondAaaTDStart = page.indexOf(TD_START, bondAaaLabelStart) + 1;
            int bondAaaStartData = page.indexOf(GREATER_THAN, bondAaaTDStart) + 1;
            int bondAaaEndData = page.indexOf(LESS_THAN, bondAaaStartData);
            h.setBondAaa(vo.getPercent(page.substring(bondAaaStartData, bondAaaEndData)));
        }

        //  AA
        int bondAaLabelStart     = page.indexOf(">AA<", bondRatingsStart);
        if ( bondAaLabelStart > 0 ) {
            int bondAaTDStart = page.indexOf(TD_START, bondAaLabelStart) + 1;
            int bondAaStartData = page.indexOf(GREATER_THAN, bondAaTDStart) + 1;
            int bondAaEndData = page.indexOf(LESS_THAN, bondAaStartData);
            h.setBondAa(vo.getPercent(page.substring(bondAaStartData, bondAaEndData)));
        }

        //  A
        int bondALabelStart     = page.indexOf(">A<", bondRatingsStart);
        if ( bondALabelStart > 0 ) {
            int bondATDStart = page.indexOf(TD_START, bondALabelStart) + 1;
            int bondAStartData = page.indexOf(GREATER_THAN, bondATDStart) + 1;
            int bondAEndData = page.indexOf(LESS_THAN, bondAStartData);
            h.setBondA(vo.getPercent(page.substring(bondAStartData, bondAEndData)));
        }

        //  BBB
        int bondBbbLabelStart     = page.indexOf(">BBB<", bondRatingsStart);
        if ( bondBbbLabelStart > 0 ) {
            int bondBbbTDStart = page.indexOf(TD_START, bondBbbLabelStart) + 1;
            int bondBbbStartData = page.indexOf(GREATER_THAN, bondBbbTDStart) + 1;
            int bondBbbEndData = page.indexOf(LESS_THAN, bondBbbStartData);
            h.setBondBbb(vo.getPercent(page.substring(bondBbbStartData, bondBbbEndData)));
        }

        //  BB
        int bondBbLabelStart     = page.indexOf(">BB<", bondRatingsStart);
        if ( bondBbLabelStart > 0 ) {
            int bondBbTDStart = page.indexOf(TD_START, bondBbLabelStart) + 1;
            int bondBbStartData = page.indexOf(GREATER_THAN, bondBbTDStart) + 1;
            int bondBbEndData = page.indexOf(LESS_THAN, bondBbStartData);
            h.setBondBb(vo.getPercent(page.substring(bondBbStartData, bondBbEndData)));
        }

        //  B
        int bondBLabelStart     = page.indexOf(">B<", bondRatingsStart);
        if ( bondBLabelStart > 0 ) {
            int bondBTDStart = page.indexOf(TD_START, bondBLabelStart) + 1;
            int bondBStartData = page.indexOf(GREATER_THAN, bondBTDStart) + 1;
            int bondBEndData = page.indexOf(LESS_THAN, bondBStartData);
            h.setBondB(vo.getPercent(page.substring(bondBStartData, bondBEndData)));
        }

        //  Below B
        int bondBelowBLabelStart     = page.indexOf(">Below B", bondRatingsStart);
        if ( bondBelowBLabelStart > 0 ) {
            int bondBelowBTDStart = page.indexOf(TD_START, bondBelowBLabelStart) + 1;
            int bondBelowBStartData = page.indexOf(GREATER_THAN, bondBelowBTDStart) + 1;
            int bondBelowBEndData = page.indexOf(LESS_THAN, bondBelowBStartData);
            h.setBondBelowB(vo.getPercent(page.substring(bondBelowBStartData, bondBelowBEndData)));
        }

        //  Others
        int bondOthersLabelStart     = page.indexOf(">Other", bondRatingsStart);
        if ( bondOthersLabelStart > 0 ) {
            int bondOthersTDStart = page.indexOf(TD_START, bondOthersLabelStart) + 1;
            int bondOthersStartData = page.indexOf(GREATER_THAN, bondOthersTDStart) + 1;
            int bondOthersEndData = page.indexOf(LESS_THAN, bondOthersStartData);
            h.setBondOthers(vo.getPercent(page.substring(bondOthersStartData, bondOthersEndData)));
        };
    }

    public void parseTopHoldings( String page, YahooEtfHoldings h ) {

        String json = "[";
        //
        // Find the start of the table
        int tableStart      = page.indexOf(">Top 10 Holdings ");
        if ( tableStart == -1 ) {
            return;
        }
        
        int tableEnd        = page.indexOf("<div class=\"footer", tableStart);

        // get first row
        int i = rowAvailable(page, tableStart, tableEnd);
        if ( i > 0 ) {
            json += getRow(page, i);
            // update i for next row
            i += 4;
        }

        // process the rest of the rows
        while ( i > 0 ) {

            // Start of next row
            i = rowAvailable(page, i, tableEnd);
            if ( i > 0 ) {
                // another row found
                json += ",";
                json += getRow(page, i);
                // update i for next row
                i += 4;
            }
        }

        // close out the json string
        json += "]";

        h.setHoldings(json);
    }

    public int rowAvailable(String page, int start, int end) {

        // find the start of the row
        int rowLoc = page.indexOf("<div class=\"content", start);
        if ( rowLoc < 0 || rowLoc > end )
            return 0;
        else
            return rowLoc;
    }

    public String getRow(String page, int start) {

        // find the start of the row
        int rowLoc = page.indexOf("<div class=\"content", start);

        int span1Start    = page.indexOf(SPAN_START, rowLoc) + 1;
        int symbolStart   = page.indexOf(GREATER_THAN, span1Start) + 1;
        int symbolEnd     = page.indexOf(LESS_THAN, symbolStart);
        String symbol     = page.substring(symbolStart, symbolEnd);

        int span2Start    = page.indexOf(SPAN_START, symbolEnd);
        int nameStart     = page.indexOf(GREATER_THAN, span2Start) + 1;
        int nameEnd       = page.indexOf(LESS_THAN, nameStart);
        String name       = page.substring(nameStart, nameEnd);


        int span3Start    = page.indexOf(SPAN_START, nameEnd);
        int percentStart  = page.indexOf(GREATER_THAN, span3Start) + 1;
        int percentEnd    = page.indexOf(LESS_THAN, percentStart);
        double percent    = getPercent( page.substring(percentStart, percentEnd) );

        String json =  "{\"symbol\":\"" + symbol +
                "\",\"holdingName\":\"" + name +
                "\",\"holdingPercent\":" + percent +
                "}";

        //System.out.println(json);
        return json;

    }

    private double getPercent(String buf) {
        String stripped = buf.replace(",","").replace('%',' ').trim();
        double tempVal =  (Double.parseDouble( stripped ) / 100.0);

        // Round to 4 decimal places
        return Math.round(tempVal * 10000.0) / 10000.0;
    }

    boolean rowFound ( String page, int start, int end ) {
        return page.substring(start, end).contains(TR_START);
    }


}

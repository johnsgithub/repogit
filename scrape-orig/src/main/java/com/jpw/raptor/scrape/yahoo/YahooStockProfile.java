package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

@Getter
@Setter
public class YahooStockProfile {

    static final String GREATER_THAN = ">";
    static final String SPAN_END = "</span>";
    static final String P_START = "<p ";
    static final String P_END = "</p>";

    private String  page;
    private String  sector;
    private String  industry;
    private String  description;

    private boolean readFailed;


    public boolean readPage(String symbol) throws IOException {

        readFailed = false;

        // Yahoo replaces . with -
        String ticker = symbol.toUpperCase().replace('.', '-');
        String baseUrl = "https://finance.yahoo.com/quote/" + ticker + "/profile?p=" + ticker;
        try {
            Document doc = Jsoup.connect(baseUrl).get();
            page = doc.toString();
        } catch (org.jsoup.HttpStatusException ex) {
            readFailed = true;
        }

        // Validate page was found
        if (readFailed || page.contains("<span>No results for") || page.contains("<span>Symbols similar to")) {
            return false;
        }

        //  Sector
        int sectorStartTag  = page.indexOf("<span>Sector(s)</span>");

        if ( sectorStartTag > 0 ) {
            int sectorStartData = page.indexOf(GREATER_THAN, sectorStartTag+23) + 1;
            int sectorEndData = page.indexOf(SPAN_END, sectorStartData);
            sector = Jsoup.parse(page.substring(sectorStartData, sectorEndData)).text();
        } else {
            return false;
        }

        //  Industry
        int industryStartTag  = page.indexOf("<span>Industry</span>");

        if ( industryStartTag > 0 ) {
            int industryStartData = page.indexOf(GREATER_THAN, industryStartTag+22) + 1;
            int industryEndData = page.indexOf(SPAN_END, industryStartData);
            industry = Jsoup.parse(page.substring(industryStartData, industryEndData)).text();
        } else {
            return false;
        }

        //  description
        int descriptionStartTag  = page.indexOf("<span>Description</span>");

        if ( descriptionStartTag > 0 ) {
            int paragraphStart = page.indexOf(GREATER_THAN, descriptionStartTag);
            int descriptionStartData = page.indexOf(GREATER_THAN, paragraphStart) + 1;
            int descriptionEndData = page.indexOf(P_END, descriptionStartData);
            description = Jsoup.parse(page.substring(descriptionStartData, descriptionEndData)).text();
        } else {
            return false;
        }

        return true;
    }

    public void print() {
        System.out.println("sector      " + sector);
        System.out.println("industry    " + industry);
        System.out.println("description " + description);
    }
}

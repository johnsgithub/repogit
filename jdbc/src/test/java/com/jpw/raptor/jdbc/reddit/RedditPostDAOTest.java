package com.jpw.raptor.jdbc.reddit;

import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAOTest;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.RedditPost;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.core.io.ClassPathResource;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RedditPostDAOTest {

    RedditPostDAO tbl;

    RedditPost  testRec;
    RedditPost  r1;
    RedditPost  r2;
    RedditPost  r3;
    RedditPost  r4;


    Date            date;
    Date            d1;
    Date            d2;
    Date            d3;
    Date            d4;

    String          dateString;

    static class PostgresResource  {

        // postgres:9.6.8

        PostgreSQLContainer postgreSQLContainer;
        PGSimpleDataSource dataSource;

        public PostgresResource() {
            postgreSQLContainer = new PostgreSQLContainer("postgres:10.11")
                    .withDatabaseName("test-jdbc")
                    .withUsername("sa")
                    .withPassword("sa");

            postgreSQLContainer.start();

            dataSource = new PGSimpleDataSource();
            dataSource.setDatabaseName(postgreSQLContainer.getDatabaseName());
            dataSource.setServerName(postgreSQLContainer.getContainerIpAddress());
            dataSource.setPortNumber(postgreSQLContainer.getMappedPort(5432));
            dataSource.setUser(postgreSQLContainer.getUsername());
            dataSource.setPassword(postgreSQLContainer.getPassword());

        }

        public void stop() { postgreSQLContainer.stop(); }

        public PGSimpleDataSource getDataSource () {return dataSource;}

    }

    private static RedditPostDAOTest.PostgresResource myPostgresResource;

    @BeforeClass
    public static void setUp() throws java.text.ParseException {
        myPostgresResource = new RedditPostDAOTest.PostgresResource();
    }

    @Before
    public void setUpTest() throws java.text.ParseException {

        // Create test fields
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        d1 = sdf.parse("20171101");
        d2 = sdf.parse("20171230");
        d3 = sdf.parse("20180102");
        d4 = sdf.parse("20180222");

        // Create 4 test records
        r1 = new RedditPost(d1,"stocka","communityz","post_id01","author1","title-1", "P", "contentsa");
        r2 = new RedditPost(d2,"stock","community","post_id","author","title", "C", "contents");
        r3 = new RedditPost(d3,"stock","community","post_id","author","title", "P", "contents");
        r4 = new RedditPost(d4,"stock","community","post_id","author","title", "C", "contents");
    }

    @AfterClass
    public static void tearDown(){
        myPostgresResource.stop();
    }


    @Test
    public void test01() throws Exception {

        System.out.println("test01");

        // Create connection to docker database
        tbl = new RedditPostDAO();
        tbl.setDataSource(myPostgresResource.getDataSource());
        tbl.postConstruct();

        // Create table for testing
        File   resource;
        String cmds;

        resource = new ClassPathResource("Reddit_post.sql").getFile();
        cmds = new String(Files.readAllBytes(resource.toPath()));
        tbl.sqlScript(cmds);

        // Write 1 record
        tbl.add(r1);

        // read all records only 1 should be found
        List<RedditPost> recs = tbl.getAll();

        assertEquals(1,recs.size());

        RedditPost read1 = recs.get(0);
        assertEquals(d1.compareTo(read1.getDate()),0);
        assertTrue(r1.getStock().equalsIgnoreCase(read1.getStock()));
        assertTrue(r1.getCommunity().equalsIgnoreCase(read1.getCommunity()));
        assertTrue(r1.getPostId().equalsIgnoreCase(read1.getPostId()));
        assertTrue(r1.getAuthor().equalsIgnoreCase(read1.getAuthor()));
        assertTrue(r1.getTitle().equalsIgnoreCase(read1.getTitle()));
        assertTrue(r1.getCType().equalsIgnoreCase(read1.getCType()));
        assertTrue(r1.getContents().equalsIgnoreCase(read1.getContents()));
    }

}

package com.jpw.raptor.algorithm.signals;

import com.jpw.raptor.model.AveragesModel;

import java.text.DecimalFormat;

/**
 * Created by john on 7/4/18.
 */
public class MovingAverageSignalFactory {

    public MovingAverageSignalFactory() {
        /* Constructor */
    }

    public String getSimpleMaSignal(AveragesModel rec) {
        // Get averages and close

        double avg200 = rec.getSimple200();
        double avg50  = rec.getSimple50();
        double avg20  = rec.getSimple20();

        if      (avg20 >= avg50 && avg50 >= avg200)
            return get_20_50_200(rec);
        else if (avg50 >= avg20 && avg50 >= avg200)
            return get_50_20_200(rec);
        else if ( avg50 >= avg200 && avg200 >= avg20 )
            return get_50_200_20(rec);
        else if (avg20 >= avg200 && avg200 >= avg50)
            return get_20_200_50(rec);
        else if (avg200 >= avg20 && avg20 >= avg50)
            return get_200_20_50(rec);
        else
            return get_200_50_20(rec);
    }

    public String get_20_50_200(AveragesModel rec) {

        String  signal;

        // Get averages and close
        double close  = rec.getClose();
        double avg200 = rec.getSimple200();
        double avg50  = rec.getSimple50();
        double avg20  = rec.getSimple20();
        double avg10  = rec.getSimple10();
        double avg5   = rec.getSimple5();

        // when entering this routine 20 >= 50 >= 200
        if ( close >= avg20 ) {
            if ( avg5 >= avg10 ) {
                signal = "96: C > 20 > 50 > 200";
            } else {
                signal = "94: C > 20 > 50 > 200)";
            }
        } else if ( close >= avg50 ) {
            signal = "92: 20 > C > 50 > 200";
        } else if ( close >= avg200 ) {
            signal = "88: 20 > 50 > C > 200";
        } else {
            // close below 200
            signal = "84: 20 > 50 > 200 > C";
        }

        return signal;
    }

    public String get_50_20_200(AveragesModel rec) {

        String  signal;

        // Get averages and close
        double close  = rec.getClose();
        double avg200 = rec.getSimple200();
        double avg50  = rec.getSimple50();
        double avg20  = rec.getSimple20();

        // when entering this routine 50 >= 20 >= 200
        if ( close >= avg50 ) {
            signal = "80: C > 50 > 20 > 200";
        } else if ( close >= avg20 ) {
            signal = "76: 50 > C > 20 > 200";
        } else if ( close >= avg200 ) {
            signal = "72: 50 > 20 > C > 200";
        } else {
            // close below 200
            signal = "68: 50 > 20 > 200 > C";
        }

        return signal;
    }

    public String get_50_200_20(AveragesModel rec) {

        String  signal;

        // Get averages and close
        double close  = rec.getClose();
        double avg200 = rec.getSimple200();
        double avg50  = rec.getSimple50();
        double avg20  = rec.getSimple20();

        // when entering this routine 50 >= 200 >= 20
        if ( close >= avg50 ) {
            signal = "64: C > 50 > 200 > 20";
        } else if ( close >= avg200 ) {
            signal = "60: 50 > C > 200 > 20";
        } else if ( close >= avg20 ) {
            signal = "56: 50 > 200 > C > 20";
        } else {
            // close below 20
            signal = "52: 50 > 200 > 20 > C";
        }

        return signal;
    }

    public String get_20_200_50(AveragesModel rec) {

        String  signal;

        // Get averages and close
        double close  = rec.getClose();
        double avg200 = rec.getSimple200();
        double avg50  = rec.getSimple50();
        double avg20  = rec.getSimple20();

        // when entering this routine 20 >= 200 >= 50
        if ( close >= avg20 ) {
            signal = "48: C > 20 > 200 > 50";
        } else if ( close >= avg200 ) {
            signal = "44: 20 > C > 200 > 50";
        } else if ( close >= avg50 ) {
            signal = "40: 20 > 200 > C > 50";
        } else {
            // close below 50
            signal = "36: 20 > 200 > 50 > C";
        }

        return signal;
    }

    public String get_200_20_50(AveragesModel rec) {

        String  signal;

        // Get averages and close
        double close  = rec.getClose();
        double avg200 = rec.getSimple200();
        double avg50  = rec.getSimple50();
        double avg20  = rec.getSimple20();

        // when entering this routine 200 >= 20 >= 50
        if ( close >= avg200 ) {
            signal = "32: C > 200 > 20 > 50";
        } else if ( close >= avg20 ) {
            signal = "28: 200 > C 20 > 50";
        } else if ( close >= avg50 ) {
            signal = "24: 200 > 20 > C > 50";
        } else {
            // close below 50
            signal = "20: 200 > 20 > 50 > C";
        }

        return signal;
    }

    public String get_200_50_20(AveragesModel rec) {

        String  signal;

        // Get averages and close
        double close  = rec.getClose();
        double avg200 = rec.getSimple200();
        double avg50  = rec.getSimple50();
        double avg20  = rec.getSimple20();

        // when entering this routine 200 >= 50 >= 20
        if ( close >= avg200 ) {
            signal = "16: C > 200 > 50 > 20";
        } else if ( close >= avg50 ) {
            signal = "12: 200 > C > 50 > 20";
        } else if ( close >= avg20 ) {
            signal = "8: 200 > 50 > C > 20";
        } else {
            // close below 20
            signal = "4: 200 > 50 > 20 > C";
        }

        return signal;
    }

    /*************************/

    public String getSimpleMaSignalShort(AveragesModel rec) {
        // Get averages and close

        double avg50  = rec.getSimple50();
        double avg20  = rec.getSimple20();

        if      (avg20 >= avg50 )
            return get_20_50(rec);
        else
            return get_50_20(rec);
    }

    public String get_20_50(AveragesModel rec) {

        String  signal;

        // Get averages and close
        double close  = rec.getClose();
        double avg50  = rec.getSimple50();
        double avg20  = rec.getSimple20();
        double avg10  = rec.getSimple10();
        double avg5   = rec.getSimple5();

        // when entering this routine 20 >= 50 >= 200
        if ( close >= avg20 ) {
            if ( avg5 >= avg10 ) {
                signal = "90: C > 20 > 50";
            } else {
                signal = "80: C > 20 > 50)";
            }
        } else if ( close >= avg50 ) {
            signal = "70: 20 > C > 50";
        } else {
            // close below 50
            signal = "60: 20 > 50 > C";
        }

        return signal;
    }

    public String get_50_20(AveragesModel rec) {

        String  signal;

        // Get averages and close
        double close  = rec.getClose();
        double avg50  = rec.getSimple50();
        double avg20  = rec.getSimple20();

        // when entering this routine 50 >= 20
        if ( close >= avg50 ) {
            signal = "50: C > 50 > 20";
        } else if ( close >= avg20 ) {
            signal = "76: 50 > C > 20";
        } else {
            // close below 20
            signal = "68: 50 > 20 > C";
        }

        return signal;
    }
}

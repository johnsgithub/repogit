package com.jpw.raptor.cmdline.reddit;

import com.aliasi.dict.DictionaryEntry;
import com.aliasi.dict.MapDictionary;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class RedditDictionary {

    static final double CHUNK_SCORE = 1.0;

    public  MapDictionary<String> getDictionary() throws java.io.IOException  {

        String                line;
        MapDictionary<String> dictionary = new MapDictionary<String>();

        // The class loader that loaded the class
        ClassLoader classLoader = getClass().getClassLoader();

        // Process list of stocks stocks
        InputStream       stream       = classLoader.getResourceAsStream("STOCKS.txt");
        InputStreamReader streamReader = new InputStreamReader(stream, StandardCharsets.UTF_8);
        BufferedReader    reader       = new BufferedReader(streamReader);

        while ((line = reader.readLine()) != null) {
            String[] parsed = line.split("\t");
            if ( parsed.length > 1 ) {
                // Only process stocks with names
                dictionary.addEntry(new DictionaryEntry<String>(parsed[0],"STOCK",CHUNK_SCORE));
            }
        }

        reader.close();
        return dictionary;
    }


    public  TreeMap<String, String> getStockMap() throws java.io.IOException  {

        String                  line;
        TreeMap<String, String> map = new TreeMap<>();

        // The class loader that loaded the class
        ClassLoader classLoader = getClass().getClassLoader();

        // Process list of stocks stocks
        InputStream       stream       = classLoader.getResourceAsStream("STOCKS.txt");
        InputStreamReader streamReader = new InputStreamReader(stream, StandardCharsets.UTF_8);
        BufferedReader    reader       = new BufferedReader(streamReader);

        while ((line = reader.readLine()) != null) {
            String[] parsed = line.split("\t");
            if ( parsed.length > 1 ) {
                // Only process stocks with names
                map.put(parsed[0].toUpperCase(Locale.ROOT),parsed[1]);
            }
        }

        reader.close();
        return map;
    }

    public void buildStocksFile() throws java.io.IOException {

        String                  line;
        TreeMap<String, String> map = new TreeMap<>();

        // The class loader that loaded the class
        ClassLoader classLoader = getClass().getClassLoader();

        // Process list of NYSE stocks
        InputStream       nyseStream       = classLoader.getResourceAsStream("NYSE.txt");
        InputStreamReader nyseStreamReader = new InputStreamReader(nyseStream, StandardCharsets.UTF_8);
        BufferedReader    nyseReader       = new BufferedReader(nyseStreamReader);

        while ((line = nyseReader.readLine()) != null) {
            String[] parsed = line.split("\t");
            if ( parsed.length > 1 ) {
                // Only process stocks with names
                if (parsed[1].contains("ETF") || parsed[1].contains("ETN")) {
                    // Ignore etfs and etns
                } else {
                    map.put(parsed[0], parsed[1]);
                }
            }

        }

        // Process list of Nasdaq stocks
        InputStream       nasdaqStream       = classLoader.getResourceAsStream("NASDAQ.txt");
        InputStreamReader nasdaqStreamReader = new InputStreamReader(nasdaqStream, StandardCharsets.UTF_8);
        BufferedReader    nasdaqReader       = new BufferedReader(nasdaqStreamReader);

        while ((line = nasdaqReader.readLine()) != null) {
            String[] parsed = line.split("\t");
            if ( parsed.length > 1 ) {
                // Only process stocks with names
                if (parsed[1].contains("ETF") || parsed[1].contains("ETN")) {
                    // Ignore etfs and etns
                } else {
                    map.put(parsed[0], parsed[1]);
                }
            }

        }

        // Write stocks to STOCK File
        BufferedWriter writer = new BufferedWriter(new FileWriter("/home/finance/runtime/files/STOCKS.txt"));
        for (Map.Entry<String, String> entrySetVal : map.entrySet()) {
            String key   = entrySetVal.getKey();
            String value = entrySetVal.getValue();
            writer.write(key + "\t" + value + "\n");
        }
        writer.close();
    }
}

package com.jpw.raptor.cmdline.split;


import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;


/**
 * Created by john on 8/14/18.
 */
public class TypeValidator implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {

        if ( value.equalsIgnoreCase("etf")
                || value.equalsIgnoreCase("fund")
                || value.equalsIgnoreCase("stock")
        ) {
            // good to go
        } else {
            throw new ParameterException("Type must be etf or fund");
        }

    }

}

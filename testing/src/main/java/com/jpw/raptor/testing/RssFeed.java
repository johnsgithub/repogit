package com.jpw.raptor.testing;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import java.net.URL;
import java.util.Iterator;

import java.io.InputStreamReader;


public class RssFeed {

    public void doit() throws Exception {

        // comments for a post
        // taken from SyndEntryImpl.link add .rss to string
        URL url = new URL("https://www.reddit.com/r/StockMarket/comments/mpn2qm/here_is_a_market_recap_for_today_monday_april_12/.rss");

        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feeder = input.build(new XmlReader(url));

        System.out.println("Title Value " + feeder.getAuthor());

        for (Iterator iterator = feeder.getEntries().iterator(); iterator.hasNext();) {
            SyndEntry syndEntry = (SyndEntry) iterator.next();
            //System.out.println(syndEntry.getTitle());
            System.out.println(syndEntry.getContents());
        }


    }

    public void doit3() throws Exception {

        // comments for a post
        // taken from SyndEntryImpl.link add .rss to string
        URL url = new URL("https://www.reddit.com/r/StockMarket/comments/mpp1qx/sp_500_visual_summary_april_12/.rss");

        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feeder = input.build(new XmlReader(url));

        System.out.println("Title Value " + feeder.getAuthor());

        for (Iterator iterator = feeder.getEntries().iterator(); iterator.hasNext();) {
            SyndEntry syndEntry = (SyndEntry) iterator.next();
            //System.out.println(syndEntry.getTitle());
            System.out.println(syndEntry.getContents());
        }


    }

    public void doit2() throws Exception {

        URL url = new URL("https://www.reddit.com/r/StockMarket.rss");

        // get posts
        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feeder = input.build(new XmlReader(url));

        System.out.println("Title Value " + feeder.getAuthor());

        for (Iterator iterator = feeder.getEntries().iterator(); iterator.hasNext();) {
            SyndEntry syndEntry = (SyndEntry) iterator.next();
            //System.out.println(syndEntry.getTitle());
            //System.out.println(syndEntry.getAuthor());
            //System.out.println(syndEntry.getUpdatedDate());
            //System.out.println(syndEntry.getLink());
            //System.out.println();
            System.out.println(syndEntry);
        }


    }

    public void doit1() throws Exception {

        URL feedUrl = new URL("https://www.reddit.com/r/StockMarket.rss");

        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feed = input.build(new XmlReader(feedUrl));

        System.out.println(feed);

    }
}

package com.jpw.raptor.scrape.yahoo;

import java.io.IOException;

public class YahooStockStatParseJS {

    static final String GREATER_THAN = ">";
    static final String LESS_THAN = "<";
    static final String TABLE_START = "<table";
    static final String TABLE_END = "</table>";
    static final String TBODY_START = "<tbody";
    static final String TBODY_END = "</tbody>";
    static final String TR_START = "<tr";
    static final String TR_END = "</tr>";
    static final String TD_START = "<td ";
    static final String TD_END = "</td>";


    public void parse(String page, YahooStockStatistics s) {

        // Valuation Measures
        valuationMeasuresTable(page, s);

        // Stock Price History
        stockPriceHistoryTable(page, s);

        // Share Statistics
        shareStatisticsTable(page, s);

        // Dividend and Splits
        dividendAndSplitsTable(page, s);

        // Financial Highlights

        // Profitability
        profitabilityTable(page, s);

        // Management Effectiveness
        managementEffectivenessTable(page, s);

        // Income Statement
        incomeStatementTable(page, s);

        // Balance Sheet
        balanceSheetTable(page, s);

        // Cash Flow
        cashFlowTable(page, s);
    }

    public void valuationMeasuresTable(String page, YahooStockStatistics s) {

        ValueObject vo = new ValueObject();

        //  Market Cap
        // Start and end of row
        int marketCapStartTag  = page.indexOf(">Market Cap<");

        if ( marketCapStartTag > 0 ) {
            // First data column value
            int marketCapStartTd = page.indexOf(TD_START, marketCapStartTag);
            int marketCapStartData = page.indexOf(GREATER_THAN, marketCapStartTd) + 1;
            int marketCapEndData = page.indexOf(LESS_THAN, marketCapStartData);
            s.setMarketCap( vo.getLong(page.substring(marketCapStartData, marketCapEndData)) );

            if (page.substring(marketCapStartData, marketCapEndData).contains("N/A")) {
                // First column has no data check the second column
                marketCapStartTd = page.indexOf(TD_START, marketCapEndData);
                // make sure there is a second column in the row
                if (marketCapStartTd > 0 ) {
                    marketCapStartData = page.indexOf(GREATER_THAN, marketCapStartTd) + 1;
                    marketCapEndData = page.indexOf(LESS_THAN, marketCapStartData);
                    s.setMarketCap( vo.getLong(page.substring(marketCapStartData, marketCapEndData)) );
                }
            }
        } else {
            s.setMarketCap( vo.getLong("N/A") );
        }

        // Enterprise Value
        // start and end row
        int enterpriseValueStartTag  = page.indexOf(">Enterprise Value<");

        if ( enterpriseValueStartTag > 0 ) {
            // First data column value
            int enterpriseValueStartTd = page.indexOf(TD_START, enterpriseValueStartTag);
            int enterpriseValueStartData = page.indexOf(GREATER_THAN, enterpriseValueStartTd) + 1;
            int enterpriseValueEndData = page.indexOf(LESS_THAN, enterpriseValueStartData);
            s.setEnterpriseValue( vo.getLong(page.substring(enterpriseValueStartData, enterpriseValueEndData)) );

            if (page.substring(enterpriseValueStartData, enterpriseValueEndData).contains("N/A")) {
                // First column has no data check the second column
                enterpriseValueStartTd = page.indexOf(TD_START, enterpriseValueEndData);
                // make sure there is a second column in the row
                if (enterpriseValueStartTd > 0 ) {
                    enterpriseValueStartData = page.indexOf(GREATER_THAN, enterpriseValueStartTd) + 1;
                    enterpriseValueEndData = page.indexOf(LESS_THAN, enterpriseValueStartData);
                    s.setEnterpriseValue( vo.getLong(page.substring(enterpriseValueStartData, enterpriseValueEndData)) );
                }
            }
        } else {
            s.setEnterpriseValue( vo.getLong("N/A") );
        }


        // Trailing P/E
        // start and end row
        int trailingPEStartTag  = page.indexOf(">Trailing P/E<");


        if ( trailingPEStartTag > 0 ) {
            // First data column value
            int trailingPEStartTd   = page.indexOf(TD_START, trailingPEStartTag);
            int trailingPEStartData = page.indexOf(GREATER_THAN, trailingPEStartTd) + 1;
            int trailingPEEndData   = page.indexOf(LESS_THAN, trailingPEStartData);
            s.setTrailingPE( vo.getDouble( page.substring(trailingPEStartData, trailingPEEndData) ) );

            if ( page.substring(trailingPEStartData, trailingPEEndData).contains("N/A") ) {
                // First column has no data check the second column
                trailingPEStartTd   = page.indexOf(TD_START, trailingPEEndData);
                // make sure there is a second column in the row
                if ( trailingPEStartTd > 0 ) {
                    trailingPEStartData = page.indexOf(GREATER_THAN, trailingPEStartTd) + 1;
                    trailingPEEndData   = page.indexOf(LESS_THAN, trailingPEStartData);
                    s.setTrailingPE( vo.getDouble( page.substring(trailingPEStartData, trailingPEEndData) ) );
                }
            }
        } else {
            s.setTrailingPE( vo.getDouble( "N/A" ) );
        }

        // Forward P/E
        // start and end row
        int forwardPEStartTag  = page.indexOf(">Forward P/E<");

        if ( forwardPEStartTag > 0 ) {
            // First data column value
            int forwardPEStartTd = page.indexOf(TD_START, forwardPEStartTag);
            int forwardPEStartData = page.indexOf(GREATER_THAN, forwardPEStartTd) + 1;
            int forwardPEEndData = page.indexOf(LESS_THAN, forwardPEStartData);
            s.setForwardPE( vo.getDouble(page.substring(forwardPEStartData, forwardPEEndData)) );

            if (page.substring(forwardPEStartData, forwardPEEndData).contains("N/A")) {
                // First column has no data check the second column
                forwardPEStartTd = page.indexOf(TD_START, forwardPEEndData);
                // make sure there is a second column in the row
                if (forwardPEStartTd > 0 ) {
                    forwardPEStartData = page.indexOf(GREATER_THAN, forwardPEStartTd) + 1;
                    forwardPEEndData = page.indexOf(LESS_THAN, forwardPEStartData);
                    s.setForwardPE( vo.getDouble(page.substring(forwardPEStartData, forwardPEEndData)) );
                }
            }
        } else {
            s.setForwardPE( vo.getDouble("N/A") );
        }


        // PEG Ratio
        // start and end row
        int pegRatioStartTag  = page.indexOf(">PEG Ratio (5yr expected)<");

        if ( pegRatioStartTag > 0 ) {
            // First data column value
            int pegRatioStartTd = page.indexOf(TD_START, pegRatioStartTag);
            int pegRatioStartData = page.indexOf(GREATER_THAN, pegRatioStartTd) + 1;
            int pegRatioEndData = page.indexOf(LESS_THAN, pegRatioStartData);
            s.setPegRatio( vo.getDouble(page.substring(pegRatioStartData, pegRatioEndData)) );

            if (page.substring(pegRatioStartData, pegRatioEndData).contains("N/A")) {
                // First column has no data check the second column
                pegRatioStartTd = page.indexOf(TD_START, pegRatioEndData);
                // make sure there is a second column in the row
                if (pegRatioStartTd > 0) {
                    pegRatioStartData = page.indexOf(GREATER_THAN, pegRatioStartTd) + 1;
                    pegRatioEndData = page.indexOf(LESS_THAN, pegRatioStartData);
                    s.setPegRatio( vo.getDouble(page.substring(pegRatioStartData, pegRatioEndData)) );
                }
            }
        } else {
            s.setPegRatio( vo.getDouble("N/A") );
        }


        // Price/Sales
        // start and end row
        int priceSalesStartTag  = page.indexOf(">Price/Sales<");

        if ( priceSalesStartTag > 0 ) {
            // First data column value
            int priceSalesStartTd = page.indexOf(TD_START, priceSalesStartTag);
            int priceSalesStartData = page.indexOf(GREATER_THAN, priceSalesStartTd) + 1;
            int priceSalesEndData = page.indexOf(LESS_THAN, priceSalesStartData);
            s.setPriceSales( vo.getDouble(page.substring(priceSalesStartData, priceSalesEndData)) );

            if (page.substring(priceSalesStartData, priceSalesEndData).contains("N/A")) {
                // First column has no data check the second column
                priceSalesStartTd = page.indexOf(TD_START, priceSalesEndData);
                if (priceSalesStartTd > 0) {
                    priceSalesStartData = page.indexOf(GREATER_THAN, priceSalesStartTd) + 1;
                    priceSalesEndData = page.indexOf(LESS_THAN, priceSalesStartData);
                    s.setPriceSales( vo.getDouble(page.substring(priceSalesStartData, priceSalesEndData)) );
                }
            }
        } else {
            s.setPriceSales( vo.getDouble("N/A") );
        }


        // Price/Book
        // start and end row
        int priceBookStartTag  = page.indexOf(">Price/Book<");
        int priceBookRowEnd    = page.indexOf(TR_END, priceBookStartTag);

        if ( priceBookStartTag > 0 ) {
            // First data column value
            int priceBookStartTd = page.indexOf(TD_START, priceBookStartTag);
            int priceBookStartData = page.indexOf(GREATER_THAN, priceBookStartTd) + 1;
            int priceBookEndData = page.indexOf(LESS_THAN, priceBookStartData);
            s.setPriceBook( vo.getDouble(page.substring(priceBookStartData, priceBookEndData)) );

            if (page.substring(priceBookStartData, priceBookEndData).contains("N/A")) {
                // First column has no data check the second column
                priceBookStartTd = page.indexOf(TD_START, priceBookEndData);
                if (priceBookStartTd > 0) {
                    priceBookStartData = page.indexOf(GREATER_THAN, priceBookStartTd) + 1;
                    priceBookEndData = page.indexOf(LESS_THAN, priceBookStartData);
                    s.setPriceBook( vo.getDouble(page.substring(priceBookStartData, priceBookEndData)) );
                }
            }
        } else {
            s.setPriceBook( vo.getDouble("N/A") );
        }


        // Enterprise Value/Revenue
        // start and end row
        int evRevenueStartTag  = page.indexOf(">Enterprise Value/Revenue<");

        if ( evRevenueStartTag > 0 ) {
            // First data column value
            int evRevenueStartTd = page.indexOf(TD_START, evRevenueStartTag);
            int evRevenueStartData = page.indexOf(GREATER_THAN, evRevenueStartTd) + 1;
            int evRevenueEndData = page.indexOf(LESS_THAN, evRevenueStartData);
            s.setEvRevenue( vo.getDouble(page.substring(evRevenueStartData, evRevenueEndData)) );

            if (page.substring(evRevenueStartData, evRevenueEndData).contains("N/A")) {
                // First column has no data check the second column
                evRevenueStartTd = page.indexOf(TD_START, evRevenueEndData);
                if (evRevenueStartTd > 0 ) {
                    evRevenueStartData = page.indexOf(GREATER_THAN, evRevenueStartTd) + 1;
                    evRevenueEndData = page.indexOf(LESS_THAN, evRevenueStartData);
                    s.setEvRevenue( vo.getDouble(page.substring(evRevenueStartData, evRevenueEndData)) );
                }
            }
        } else {
            s.setEvRevenue( vo.getDouble("N/A") );
        }


        // Enterprise Value/EBITDA
        // start and end row
        int evEbitdaStartTag  = page.indexOf(">Enterprise Value/EBITDA<");

        if ( evEbitdaStartTag > 0 ) {
            // First data column value
            int evEbitdaStartTd = page.indexOf(TD_START, evEbitdaStartTag);
            int evEbitdaStartData = page.indexOf(GREATER_THAN, evEbitdaStartTd) + 1;
            int evEbitdaEndData = page.indexOf(LESS_THAN, evEbitdaStartData);
            s.setEvEbitda( vo.getDouble(page.substring(evEbitdaStartData, evEbitdaEndData)) );

            if (page.substring(evEbitdaStartData, evEbitdaEndData).contains("N/A")) {
                // First column has no data check the second column
                evEbitdaStartTd = page.indexOf(TD_START, evEbitdaEndData);
                if (evEbitdaStartTd > 0) {
                    evEbitdaStartData = page.indexOf(GREATER_THAN, evEbitdaStartTd) + 1;
                    evEbitdaEndData = page.indexOf(LESS_THAN, evEbitdaStartData);
                    s.setEvEbitda( vo.getDouble(page.substring(evEbitdaStartData, evEbitdaEndData)) );
                }
            }
        } else {
            s.setEvEbitda( vo.getDouble("N/A") );
        }
    }


    public void profitabilityTable(String page, YahooStockStatistics s) {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf(">Profitability<");

        // Profit Margin
        int profitMarginTag       = page.indexOf(">Profit Margin", tableLabelStart);
        int profitMarginTd        = page.indexOf(TD_START, profitMarginTag);
        int profitMarginStartData = page.indexOf(GREATER_THAN, profitMarginTd) + 1;
        int profitMarginEndData   = page.indexOf(LESS_THAN,profitMarginStartData);
        s.setProfitMargin( vo.getPercent( page.substring(profitMarginStartData, profitMarginEndData) ) );

        // Operating Margin
        int operatingMarginTag       = page.indexOf(">Operating Margin (ttm)<", tableLabelStart);
        int operatingMarginTd        = page.indexOf(TD_START, operatingMarginTag);
        int operatingMarginStartData = page.indexOf(GREATER_THAN, operatingMarginTd) + 1;
        int operatingMarginEndData   = page.indexOf(LESS_THAN,operatingMarginStartData);
        s.setOperatingMargin( vo.getPercent( page.substring(operatingMarginStartData, operatingMarginEndData) ) );

    }

    public void managementEffectivenessTable(String page, YahooStockStatistics s) {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf(">Management Effectiveness<");

        // Return On Assets
        int returnOnAssetsTag       = page.indexOf(">Return on Assets (ttm)<", tableLabelStart);
        int returnOnAssetsTd        = page.indexOf(TD_START, returnOnAssetsTag);
        int returnOnAssetsStartData = page.indexOf(GREATER_THAN, returnOnAssetsTd) + 1;
        int returnOnAssetsEndData   = page.indexOf(LESS_THAN,returnOnAssetsStartData);
        s.setReturnOnAssets( vo.getPercent( page.substring(returnOnAssetsStartData, returnOnAssetsEndData) ) );

        // Return On Equity
        int returnOnEquityTag       = page.indexOf(">Return on Equity (ttm)<", tableLabelStart);
        int returnOnEquityTd        = page.indexOf(TD_START, returnOnEquityTag);
        int returnOnEquityStartData = page.indexOf(GREATER_THAN, returnOnEquityTd) + 1;
        int returnOnEquityEndData   = page.indexOf(LESS_THAN,returnOnEquityStartData);
        s.setReturnOnEquity( vo.getPercent( page.substring(returnOnEquityStartData, returnOnEquityEndData) ) );

    }


    public void incomeStatementTable(String page, YahooStockStatistics s) {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf(">Income Statement<");

        // Revenue
        int revenueTag       = page.indexOf(">Revenue (ttm)<", tableLabelStart);
        int revenueTd        = page.indexOf(TD_START, revenueTag);
        int revenueStartData = page.indexOf(GREATER_THAN, revenueTd) + 1;
        int revenueEndData   = page.indexOf(LESS_THAN,revenueStartData);
        s.setRevenue( vo.getLong( page.substring(revenueStartData, revenueEndData) ) );

        // Quarterly Revenue Growth
        int revenueGrowthTag       = page.indexOf(">Quarterly Revenue Growth (yoy)<", tableLabelStart);
        int revenueGrowthTd        = page.indexOf(TD_START, revenueGrowthTag);
        int revenueGrowthStartData = page.indexOf(GREATER_THAN, revenueGrowthTd) + 1;
        int revenueGrowthEndData   = page.indexOf(LESS_THAN,revenueGrowthStartData);
        s.setRevenueGrowth( vo.getPercent( page.substring(revenueGrowthStartData, revenueGrowthEndData) ) );

        // EBITDA
        int ebitdaTag       = page.indexOf(">EBITDA", revenueGrowthEndData);
        int ebitdaTd        = page.indexOf(TD_START, ebitdaTag);
        int ebitdaStartData = page.indexOf(GREATER_THAN, ebitdaTd) + 1;
        int ebitdaEndData   = page.indexOf(LESS_THAN,ebitdaStartData);
        s.setEbitda( vo.getLong( page.substring(ebitdaStartData, ebitdaEndData) ) );

        // Diluted EPS
        int dilutedEpsTag       = page.indexOf(">Diluted EPS (ttm)<", tableLabelStart);
        int dilutedEpsTd        = page.indexOf(TD_START, dilutedEpsTag);
        int dilutedEpsStartData = page.indexOf(GREATER_THAN, dilutedEpsTd) + 1;
        int dilutedEpsEndData   = page.indexOf(LESS_THAN,dilutedEpsStartData);
        s.setDilutedEps( vo.getDouble( page.substring(dilutedEpsStartData, dilutedEpsEndData) ) );

        // Quarterly Earnings Growth
        int earningsGrowthTag       = page.indexOf(">Quarterly Earnings Growth (yoy)<", tableLabelStart);
        int earningsGrowthTd        = page.indexOf(TD_START, earningsGrowthTag);
        int earningsGrowthStartData = page.indexOf(GREATER_THAN, earningsGrowthTd) + 1;
        int earningsGrowthEndData   = page.indexOf(LESS_THAN,earningsGrowthStartData);
        s.setEarningsGrowth( vo.getPercent( page.substring(earningsGrowthStartData, earningsGrowthEndData) ) );

    }


    public void balanceSheetTable(String page, YahooStockStatistics s) {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf(">Balance Sheet<");

        // Total Cash
        int totalCashTag       = page.indexOf(">Total Cash (mrq)<", tableLabelStart);
        int totalCashTd        = page.indexOf(TD_START, totalCashTag);
        int totalCashStartData = page.indexOf(GREATER_THAN, totalCashTd) + 1;
        int totalCashEndData   = page.indexOf(LESS_THAN,totalCashStartData);
        s.setTotalCash( vo.getLong( page.substring(totalCashStartData, totalCashEndData) ) );

        // Total Cash Per Share
        int totalCashPerShareTag       = page.indexOf(">Total Cash Per Share (mrq)<", tableLabelStart);
        int totalCashPerShareTd        = page.indexOf(TD_START, totalCashPerShareTag);
        int totalCashPerShareStartData = page.indexOf(GREATER_THAN, totalCashPerShareTd) + 1;
        int totalCashPerShareEndData   = page.indexOf(LESS_THAN,totalCashPerShareStartData);
        s.setTotalCashPerShare( vo.getDouble( page.substring(totalCashPerShareStartData, totalCashPerShareEndData) ) );

        // Total Debt
        int totalDebtTag       = page.indexOf(">Total Debt (mrq)<", tableLabelStart);
        int totalDebtTd        = page.indexOf(TD_START, totalDebtTag);
        int totalDebtStartData = page.indexOf(GREATER_THAN, totalDebtTd) + 1;
        int totalDebtEndData   = page.indexOf(LESS_THAN,totalDebtStartData);
        s.setTotalDebt( vo.getLong( page.substring(totalDebtStartData, totalDebtEndData) ) );

        // Current Ratio
        int currentRatioTag       = page.indexOf(">Current Ratio (mrq)<", tableLabelStart);
        int currentRatioTd        = page.indexOf(TD_START, currentRatioTag);
        int currentRatioStartData = page.indexOf(GREATER_THAN, currentRatioTd) + 1;
        int currentRatioEndData   = page.indexOf(LESS_THAN,currentRatioStartData);
        s.setCurrentRatio( vo.getDouble( page.substring(currentRatioStartData, currentRatioEndData) ) );
    }

    public void cashFlowTable(String page, YahooStockStatistics s) {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf(">Cash Flow Statement<");

        // Operating Cash Flow
        int operatingCashFlowTag       = page.indexOf(">Operating Cash Flow (ttm)<", tableLabelStart);
        int operatingCashFlowTd        = page.indexOf(TD_START, operatingCashFlowTag);
        int operatingCashFlowStartData = page.indexOf(GREATER_THAN, operatingCashFlowTd) + 1;
        int operatingCashFlowEndData   = page.indexOf(LESS_THAN,operatingCashFlowStartData);
        s.setOperatingCashFlow( vo.getLong( page.substring(operatingCashFlowStartData, operatingCashFlowEndData) ) );

        // Free Cash Flow
        int freeCashFlowTag       = page.indexOf(">Levered Free Cash Flow (ttm)<", tableLabelStart);
        int freeCashFlowTd        = page.indexOf(TD_START, freeCashFlowTag);
        int freeCashFlowStartData = page.indexOf(GREATER_THAN, freeCashFlowTd) + 1;
        int freeCashFlowEndData   = page.indexOf(LESS_THAN,freeCashFlowStartData);
        s.setFreeCashFlow( vo.getLong( page.substring(freeCashFlowStartData, freeCashFlowEndData) ) );
    }

    public void stockPriceHistoryTable(String page, YahooStockStatistics s) {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf(">Stock Price History<");

        // Beta (5Y Monthly)
        int betaStartTag  = page.indexOf(">Beta (5Y Monthly)", tableLabelStart);
        int betaStartTd   = page.indexOf(TD_START, betaStartTag);
        int betaStartData = page.indexOf(GREATER_THAN, betaStartTd) + 1;
        int betaEndData   = page.indexOf(LESS_THAN, betaStartData);
        s.setBeta( vo.getDouble(page.substring(betaStartData, betaEndData)) );
    }

    public void shareStatisticsTable(String page, YahooStockStatistics s) {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf(">Share Statistics<");

        // AvgVol3Month
        int avgVol3MonthTag       = page.indexOf(">Avg Vol (3 month)", tableLabelStart);
        int avgVol3MonthTd        = page.indexOf(TD_START, avgVol3MonthTag);
        int avgVol3MonthStartData = page.indexOf(GREATER_THAN, avgVol3MonthTd) + 1;
        int avgVol3MonthEndData   = page.indexOf(LESS_THAN,avgVol3MonthStartData);
        s.setAvgVol3Month( vo.getLong( page.substring(avgVol3MonthStartData, avgVol3MonthEndData) ) );

        // AvgVol10Day
        int avgVol10DayTag       = page.indexOf(">Avg Vol (10 day)", tableLabelStart);
        int avgVol10DayTd        = page.indexOf(TD_START, avgVol10DayTag);
        int avgVol10DayStartData = page.indexOf(GREATER_THAN, avgVol10DayTd) + 1;
        int avgVol10DayEndData   = page.indexOf(LESS_THAN,avgVol10DayStartData);
        s.setAvgVol10Day( vo.getLong( page.substring(avgVol10DayStartData, avgVol10DayEndData) ) );

        // ShortRatio
        int shortRatioTag       = page.indexOf(">Short Ratio ", tableLabelStart);
        int shortRatioTd        = page.indexOf(TD_START, shortRatioTag);
        int shortRatioStartData = page.indexOf(GREATER_THAN, shortRatioTd) + 1;
        int shortRatioEndData   = page.indexOf(LESS_THAN,shortRatioStartData);
        s.setShortRatio( vo.getDouble( page.substring(shortRatioStartData, shortRatioEndData) ) );
    }

    public void dividendAndSplitsTable(String page, YahooStockStatistics s) {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf(">Dividends &amp; Splits<");

        // Forward Annual Dividend Rate
        int divRateForwardTag       = page.indexOf(">Forward Annual Dividend Rate", tableLabelStart);
        int divRateForwardTd        = page.indexOf(TD_START, divRateForwardTag);
        int divRateForwardStartData = page.indexOf(GREATER_THAN, divRateForwardTd) + 1;
        int divRateForwardEndData   = page.indexOf(LESS_THAN,divRateForwardStartData);
        s.setDivRateForward( vo.getPercent( page.substring(divRateForwardStartData, divRateForwardEndData) ) );

        // Forward Annual Dividend Yield
        int divYieldForwardTag       = page.indexOf(">Forward Annual Dividend Yield", tableLabelStart);
        int divYieldForwardTd        = page.indexOf(TD_START, divYieldForwardTag);
        int divYieldForwardStartData = page.indexOf(GREATER_THAN, divYieldForwardTd) + 1;
        int divYieldForwardEndData   = page.indexOf(LESS_THAN,divYieldForwardStartData);
        s.setDivYieldForward( vo.getPercent( page.substring(divYieldForwardStartData, divYieldForwardEndData) ) );

    }


}

package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.JdkRatio;
import com.jpw.raptor.model.JdkRelativeRotation;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Rp;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class RelativeRotation {

    public List<JdkRelativeRotation> computeJdkRatio(List<Quote> refQuotes, List<Quote> equityQuotes) {

        List<JdkRelativeRotation>      result       = new ArrayList<>();

        // Step one compute relative performance equity quotes / reference quotes
        RelativePerformance relPerfGen   = new RelativePerformance();
        List<Rp>            tempList     = relPerfGen.computeRelativePerformance(refQuotes, equityQuotes);

        // Create new list with one years worth of data
        int                  maxEntries  = tempList.size() > 252 ? 252 : tempList.size();
        List<Rp>             relPerfList = new ArrayList<>(tempList.subList(0, maxEntries));

        // Step two compute the mean
        Mean                meanGen      = new Mean();
        double              relPerfMean  = meanGen.rp(relPerfList, 0, maxEntries);

        //Step three compute the standard deviation
        StandardDeviation   stdDevGen    = new StandardDeviation();
        double              relPerfStd   = stdDevGen.rp(relPerfList, 0, maxEntries);

        // generate a list of JDK ratios
        for ( Rp relPerf : relPerfList ) {
            JdkRelativeRotation jdk = new JdkRelativeRotation();
            jdk.setDate(relPerf.getDate());
            jdk.setSymbol(relPerf.getSymbol());
            double temp = 100.0 + ( ((relPerf.getVal() - relPerfMean) / relPerfStd) + 1.0);
            jdk.setRatio(Math.round(temp * 100.0) / 100.0);
            result.add(jdk);
        }

        return result;
    }

}

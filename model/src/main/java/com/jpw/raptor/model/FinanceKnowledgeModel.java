package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class FinanceKnowledgeModel {

    private String          id;
    private String          title;
    private String          body;
    private String          tag;
    private String          url;
    private String          loc;
    private double          score;

    // constructor
    public FinanceKnowledgeModel() {
        id    = null;
        title = null;
        url   = null;
        loc   = null;
        tag   = null;
        body  = null;
        score = 0.0;
    }

}

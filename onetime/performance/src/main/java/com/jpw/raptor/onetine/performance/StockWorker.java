package com.jpw.raptor.onetine.performance;

import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.model.Etf;
import com.jpw.raptor.model.Stock;
import javafx.util.Pair;

import java.util.List;

public class StockWorker {

    public void doit(EtfDAO etfTbl, StockDAO stockTbl, QuoteDAO quoteTbl) {

        // Get the reference equity
        Etf etf = etfTbl.get("SPY");

        // Get the Stocks to display
        List<Stock> equities = stockTbl.getAll();

        // Generate stock signals
        StockSignal worker = new StockSignal(etf, equities, quoteTbl);

        // Get generated stock signals
        List<EquitySignal> signals = worker.getSignals();

        // print the signals
        System.out.println("");
        System.out.println("Stock Signals");
        System.out.println("");
        for ( EquitySignal l : signals ) {
            System.out.println(l.getJdk() + " :  " + l.getLine());
        }

        // Get stock sectors
        SectorSummary stockSectors = worker.getStockSectors();

        // print stock sectors
        Double stockSectorCount = Double.valueOf( stockSectors.getSectorCount() );
        List<Pair<String,Integer>> stockSectorList = stockSectors.getSectors();

        System.out.println("");
        System.out.println("Stock Sectors");
        System.out.println("");
        for ( Pair<String,Integer> p : stockSectorList ) {
            Double percent = Double.valueOf( p.getValue() ) / stockSectorCount;
            System.out.println(p.getKey() + " :  " + p.getValue() + " : " +
                    stockSectors.formatDouble(percent));
        }

        // Get signal sectors
        SectorSummary signalSectors = worker.getSignalSectors();

        // print signal sectors
        Double signalSectorCount = Double.valueOf( signalSectors.getSectorCount() );
        List<Pair<String,Integer>> signalSectorList = signalSectors.getSectors();

        System.out.println("");
        System.out.println("Signal sectors");
        System.out.println("");
        for ( Pair<String,Integer> p : signalSectorList ) {
            Double percent = Double.valueOf( p.getValue() ) / signalSectorCount;
            System.out.println(p.getKey() + " :  " + p.getValue() + " : " +
                    signalSectors.formatDouble(percent));
        }

        // Print combined sector summary
        List<Pair<String,Pair<String,String>>> combined = stockSectors.combineSummaries(stockSectors, signalSectors);
        for ( Pair<String,Pair<String,String>> item : combined ) {
            System.out.println(item.getKey() + " :  " +
                            item.getValue().getKey() + " : " +
                    item.getValue().getValue()
                    );
        }

    }

}

package com.jpw.raptor.algorithm;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.jpw.raptor.model.Quote;

public class WeeksFactoryTest {

    @Test
    public void test() throws ParseException {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

        List<Quote> data = new ArrayList<>(Arrays.asList(
                new Quote("wk1", fmt.parse("2020-02-14"), 0, 0, 0, 106.07, 0),
                new Quote("RSP", fmt.parse("2020-02-12"), 0, 0, 0, 107.33, 0),
                new Quote("wk2", fmt.parse("2020-02-06"), 0, 0, 0, 108.56, 0),
                new Quote("RSP", fmt.parse("2020-02-05"), 0, 0, 0, 109.29, 0),
                new Quote("wk3", fmt.parse("2020-01-31"), 0, 0, 0, 105.79, 0),
                new Quote("RSP", fmt.parse("2020-01-30"), 0, 0, 0, 105.23, 0),
                new Quote("wk4", fmt.parse("2020-01-23"), 0, 0, 0, 104.4, 0),
                new Quote("RSP", fmt.parse("2020-01-22"), 0, 0, 0, 102.22, 0)
        ));

        WeeksFactory wf   = new WeeksFactory();
        List<Quote> list = wf.quote(data);

        Assert.assertEquals( list.size(), 4);
        Assert.assertTrue( list.get(0).getSymbol().equalsIgnoreCase("wk1") );
        Assert.assertTrue( list.get(1).getSymbol().equalsIgnoreCase("wk2") );
        Assert.assertTrue( list.get(2).getSymbol().equalsIgnoreCase("wk3") );
        Assert.assertTrue( list.get(3).getSymbol().equalsIgnoreCase("wk4") );
    }
}

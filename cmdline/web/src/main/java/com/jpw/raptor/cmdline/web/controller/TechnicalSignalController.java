package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.algorithm.TechnicalSummaryFactory;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;

import com.jpw.raptor.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TechnicalSignalController {

    @Autowired
    public EtfDAO etfTbl;

    @Autowired
    public QuoteDAO quoteTbl;

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    protected static final  String RECORDS = "tech_model";
    protected static final  String PAGE_TITLE = "pagetitle";
    protected static final  String SIGNAL_PAGE = "signals";
    protected static final String DEFAULT_DAYS = "220";

    @RequestMapping("/sector-signal")
    public String sectorSummaryReq (
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model ) {

        //logger.debug("sectorSummaryReq ");

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc("SPY", days);

        // get etf records
        String[] equities = new String[] {"SPY", "XLE", "XLF", "XLU", "XLI", "XLK", "XLV", "XLY", "XLP", "XLB", "IYR"};
        List<Etf> etfs = new ArrayList<>(equities.length);
        for ( String symbol : equities ) {
            etfs.add( etfTbl.get(symbol) );
        }

        List<TechnicalSummaryModel> array = new ArrayList<>(equities.length);
        for ( Etf etf : etfs ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(etf.getSymbol(), days);
            array.add(summaryService.generate(refQuotes, equityQuotes, etf.getSymbol(), etf.getName()));
        }

        model.addAttribute(RECORDS, array);
        model.addAttribute(PAGE_TITLE, "SECTOR");

        return SIGNAL_PAGE;
    }

}

package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.JdkRatio;
import com.jpw.raptor.model.JdkRelativeRotation;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Rp;
import org.junit.Assert;
import org.junit.Test;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class RelativeRotationTest {

    public ArrayList<Quote> refList;
    public ArrayList<Quote> equityList;


    @Test
    public void test01() throws ParseException {

        System.out.println("test 01 started");
/*
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

        Quote[] refData = {
                new Quote("IVV", fmt.parse("2019-01-09"), 259.09, 260.48, 257.72, 259.49, 4096900),
                new Quote("IVV", fmt.parse("2019-01-08"), 258.38, 258.82, 255.52, 258.29, 5340600),
                new Quote("IVV", fmt.parse("2019-01-07"), 254.24, 257.46, 253.21, 255.77, 5961300),
                new Quote("IVV", fmt.parse("2019-01-04"), 249.06, 254.59, 248.64, 254.06, 6430100),
                new Quote("IVV", fmt.parse("2019-01-03"), 249.79, 250.03, 245.08, 245.43, 5828300),
                new Quote("IVV", fmt.parse("2019-01-02"), 247.54, 252.7, 247.42, 251.72, 5978400),
                new Quote("IVV", fmt.parse("2018-12-31"), 251.04, 251.63, 248.99, 251.61, 10117800),
                new Quote("IVV", fmt.parse("2018-12-28"), 251.03, 252.88, 247.95, 249.33, 10856300)
        };

        Quote[] equityData = {
                new Quote("USMV", fmt.parse("2019-01-09"), 0.0, 0.0, 0.0, 52.93, 0),
                new Quote("USMV", fmt.parse("2019-01-08"), 0.0, 0.0, 0.0, 52.92, 0),
                new Quote("USMV", fmt.parse("2019-01-07"), 0.0, 0.0, 0.0, 52.47, 0),
                new Quote("USMV", fmt.parse("2019-01-04"), 0.0, 0.0, 0.0, 52.3, 0),
                new Quote("USMV", fmt.parse("2019-01-03"), 0.0, 0.0, 0.0, 51.18, 0),
                new Quote("USMV", fmt.parse("2019-01-02"), 0.0, 0.0, 0.0, 51.94, 0),
                new Quote("USMV", fmt.parse("2018-12-31"), 0.0, 0.0, 0.0, 52.4, 0),
                new Quote("USMV", fmt.parse("2018-12-28"), 0.0, 0.0, 0.0, 51.94, 0)
        };


        // Validate relative performance
        List<Quote>         refList     = Arrays.asList(refData);
        List<Quote>         equityList  = Arrays.asList(equityData);

        RelativePerformance relPerfGen   = new RelativePerformance();
        List<Rp>            tempList     = relPerfGen.computeRelativePerformance(refList, equityList);

        double d0 = Math.round((52.93 / 259.49) * 100.0) / 100.0;
        double d1 = Math.round((52.92 / 258.29) * 100.0) / 100.0;
        double d2 = Math.round((52.47 / 255.77) * 100.0) / 100.0;
        double d3 = Math.round((52.3 / 254.06) * 100.0) / 100.0;
        double d4 = Math.round((51.18 / 245.43) * 100.0) / 100.0;
        double d5 = Math.round((51.94 / 251.72) * 100.0) / 100.0;
        double d6 = Math.round((52.4 / 251.61) * 100.0) / 100.0;
        double d7 = Math.round((51.94 / 249.33) * 100.0) / 100.0;

        Assert.assertEquals(d0, tempList.get(0).getVal(), 0.01);
        Assert.assertEquals(d1, tempList.get(1).getVal(), 0.01);
        Assert.assertEquals(d2, tempList.get(2).getVal(), 0.01);
        Assert.assertEquals(d3, tempList.get(3).getVal(), 0.01);
        Assert.assertEquals(d4, tempList.get(4).getVal(), 0.01);
        Assert.assertEquals(d5, tempList.get(5).getVal(), 0.01);
        Assert.assertEquals(d6, tempList.get(6).getVal(), 0.01);
        Assert.assertEquals(d7, tempList.get(7).getVal(), 0.01);

        // get the mean and standard deviation
        Mean                        meanGen     = new Mean();
        double                      mean        = meanGen.rp(tempList, 0, 8);
        StandardDeviation           stdDevGen   = new StandardDeviation();
        double                      stdDev      = stdDevGen.rp(tempList, 0, 8);

        // generate jdk ratio
        RelativeRotation            rr          = new RelativeRotation();
        List<JdkRelativeRotation>   ratioList   = rr.computeJdkRatio(refList, equityList);

        // validate ratio
        double v0 = 100.0 + ( ((d0 - mean) / stdDev) + 1.0);
        double v1 = 100.0 + ( ((d1 - mean) / stdDev) + 1.0);
        double v2 = 100.0 + ( ((d2 - mean) / stdDev) + 1.0);
        double v3 = 100.0 + ( ((d3 - mean) / stdDev) + 1.0);
        double v4 = 100.0 + ( ((d4 - mean) / stdDev) + 1.0);
        double v5 = 100.0 + ( ((d5 - mean) / stdDev) + 1.0);
        double v6 = 100.0 + ( ((d6 - mean) / stdDev) + 1.0);
        double v7 = 100.0 + ( ((d7 - mean) / stdDev) + 1.0);

        double r0 = Math.round(v0 * 100.0) / 100.0;
        double r1 = Math.round(v1 * 100.0) / 100.0;
        double r2 = Math.round(v2 * 100.0) / 100.0;
        double r3 = Math.round(v3 * 100.0) / 100.0;
        double r4 = Math.round(v4 * 100.0) / 100.0;
        double r5 = Math.round(v5 * 100.0) / 100.0;
        double r6 = Math.round(v6 * 100.0) / 100.0;
        double r7 = Math.round(v7 * 100.0) / 100.0;

        assertEquals(r0, ratioList.get(0).getRatio(), 0.01);
        assertEquals(r1, ratioList.get(1).getRatio(), 0.01);
        assertEquals(r2, ratioList.get(2).getRatio(), 0.01);
        assertEquals(r3, ratioList.get(3).getRatio(), 0.01);
        assertEquals(r4, ratioList.get(4).getRatio(), 0.01);
        assertEquals(r5, ratioList.get(5).getRatio(), 0.01);
        assertEquals(r6, ratioList.get(6).getRatio(), 0.01);
        assertEquals(r7, ratioList.get(7).getRatio(), 0.01);

 */
    }

}

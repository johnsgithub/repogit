package com.jpw.raptor.scrape;

import com.jpw.raptor.model.Etf;
import com.jpw.raptor.model.Stock;
import com.jpw.raptor.scrape.yahoo.YahooEtfScrape;
import com.jpw.raptor.scrape.yahoo.YahooStockScrape;

import java.io.IOException;

public class Scrapper {

    public Etf scrapeEtf(Etf etf) throws IOException, InterruptedException {
        YahooEtfScrape etfScraper = new YahooEtfScrape();
        return etfScraper.scrape(etf);
    }

    public Stock scrapeStock(Stock stock) throws IOException {
        YahooStockScrape stockScraper = new YahooStockScrape();
        return stockScraper.scrape(stock);
    }

}

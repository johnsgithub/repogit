package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Jdk;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Rp;
import com.jpw.raptor.model.ZScore;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class JdkFactoryTest {

   @Test
   public void test01() throws ParseException {

       TestData td = new TestData();

       List<Quote> refQuotes = td.getRefData();
       List<Quote> equityQuotes = td.getEquityData();

       List<Double> rpValues    = td.getRpValues();

       // generate relative performance for equity and reference
       RelativePerformance rpFactory = new RelativePerformance();
       List<Rp>            rpList    = rpFactory.computeRelativePerformance(refQuotes, equityQuotes);

       // generate z scores ie normalize the relative performance values
       ZScoreFactory zScrFactory = new ZScoreFactory();
       List<ZScore> zScrList     = zScrFactory.rp(rpList, 0, rpList.size());

       double magic = 2.0;
       double ratio0 = 100.0 + (zScrList.get(0).getScore() * magic);
       double ratio5 = 100.0 + (zScrList.get(5).getScore() * magic);
       double ratio9 = 100.0 + (zScrList.get(9).getScore() * magic);

       JDKFactory factory = new JDKFactory();
       List<Jdk> list = factory.generate(refQuotes, equityQuotes);

       assertEquals(ratio0, list.get(0).getRsRatio(), 0.001);
       assertEquals(ratio5, list.get(5).getRsRatio(), 0.001);
       assertEquals(ratio9, list.get(9).getRsRatio(), 0.001);

     }


}

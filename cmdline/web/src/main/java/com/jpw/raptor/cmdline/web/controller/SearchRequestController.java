package com.jpw.raptor.cmdline.web.controller;

import com.google.common.collect.Lists;
import com.jpw.raptor.jdbc.event.EventDAO;
import com.jpw.raptor.model.Event;

import com.jpw.raptor.model.FinanceKnowledge;
import com.jpw.raptor.search.finance.KnowledgeSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by john on 5/11/18.
 */
@Controller
public class SearchRequestController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());


    @Autowired
    EventDAO eventTbl;


    @Autowired
    KnowledgeSearch fd;
    public static final int SLOP = 2;
    public static final String KNOWLEDGE_INDEX = "index-know";
    protected static final  String EVENT_RECORDS = "event_model";
    protected static final  String PAGE_TITLE = "pagetitle";
    protected static final  String ENCODING = "UTF-8";

    @RequestMapping("/event-req")
    public String eventReq(
            @RequestParam(value="start", required=true) String start,
            @RequestParam(value="end", required=true) String end,
            Model model) {

        Date startDate  = null;
        Date endDate    = null;

        // validate date
        try {
            // Convert string to date object
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            startDate = format.parse(start);
            endDate   = format.parse(end);
        }
        catch(Exception e) {
            return "error";
        }

        List<Event> recs = eventTbl.getByDate(startDate, endDate);

        model.addAttribute(EVENT_RECORDS, recs);
        model.addAttribute(PAGE_TITLE, "Events from " + start + " to " + end);

        return "event-list";
    }


    @RequestMapping("/all-document-req")
    public String AllDocumentReq(Model model) throws IOException {

        fd.openIndex(KNOWLEDGE_INDEX);

        //System.out.println( "***********");
        //System.out.println( "alldocumentreq");
        //System.out.println( "***********");
        model.addAttribute("pagetitle", "All Documents");

        try {
            List<FinanceKnowledge> docs = fd.getAll();
            model.addAttribute("doc_list", docs);

        } catch (IOException ex) {
            model.addAttribute("doc_list", null);
        }

        fd.closeIndex();

        return "document-list";


    }


    @RequestMapping("/document-req")
    public String documentReq(
            @RequestParam(value="type", required=true) String type,
            @RequestParam(value="text", required=true) String text,
            Model model) throws IOException {

        fd.openIndex(KNOWLEDGE_INDEX);

        String textClean     = null;

        try {
            textClean  = java.net.URLDecoder.decode(text, "UTF-8");
        } catch ( UnsupportedEncodingException ex) {};

        //System.out.println( "***********");
        //System.out.println( "documentreq " + type + " " + text);
        //System.out.println( "***********");


        model.addAttribute("pagetitle", "Search for: " + text);

        List<FinanceKnowledge> docs = null;

        try {
            if ( type.equalsIgnoreCase("phrase") ) {
                docs = fd.phraseQuery(text, SLOP);
            } else if ( type.equalsIgnoreCase("allwords") ) {
                docs = fd.allWordsQuery(text);
            } else if ( type.equalsIgnoreCase("anyword") ) {
                docs = fd.wordsQuery(text);
            } else {

            }

            model.addAttribute("doc_list", docs);
        } catch (IOException ex) {
            model.addAttribute("doc_list", null);
        }

        fd.closeIndex();

        return "document-list";

    }


    @RequestMapping("/doc-detail-req")
    public String knowledgeDoc(
            @RequestParam(value="id", required=true) String id,
            Model model) throws IOException {


        fd.openIndex(KNOWLEDGE_INDEX);

        //System.out.println( "***********");
        //System.out.println( "docdetailreq " + id);
        //System.out.println( "***********");

        try {
            FinanceKnowledge rec = fd.getDocument(id);
            rec.setBody(rec.getBody().replaceAll("\n","<br/>"));
            model.addAttribute("knowdoc", rec);

        } catch (IOException ex) {
            model.addAttribute("knowdoc", null);
        }

        fd.closeIndex();
        return "doc-detail";

    }

    @RequestMapping("/searchequity")
    public String searchEquityReq(
            @RequestParam(value="type", required=true) String equityType,
            @RequestParam(value="class", required=true) String equityClass,
            @RequestParam(value="fundtype", required=true) String fundType,
            Model model) {

        return "under-construction";
    }

}

package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TechnicalSummaryModel {

    String symbol;
    String name;
    Date   date;
    double jdkSignal;
    String adxSignal;
    String macdSignal;
    String mAvgSignal;
    String rsiSignal;
    double atrSignal;

    public TechnicalSummaryModel() {
        symbol        = null;
        name          = null;
        date          = null;
        jdkSignal     = 0.0;
        adxSignal     = null;
        macdSignal    = null;
        mAvgSignal    = null;
        rsiSignal     = null;
        atrSignal     = 0.0;
    }
}

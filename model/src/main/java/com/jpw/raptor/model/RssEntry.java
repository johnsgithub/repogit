package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RssEntry {

    protected String  hash;
    protected Date datePosted;
    protected String  title;
    protected String  url;
    protected String  uri;

    // Sat Aug 06 12:15:01 EDT 2022

    public RssEntry() {}

    public RssEntry(String hash, Date datePosted, String title, String url, String uri) {
        this.hash       = hash;
        this.datePosted = datePosted;
        this.title      = title;
        this.url        = url;
        this.uri        = uri;
    }
}

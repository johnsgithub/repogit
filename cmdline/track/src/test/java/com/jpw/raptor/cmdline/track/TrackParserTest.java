package com.jpw.raptor.cmdline.track;

import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.TrackRec;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class TrackParserTest {

    @Test
    public void test01() throws java.text.ParseException {
        System.out.println("test 01 started");

        SimpleDateFormat simpleDateFormat    = new SimpleDateFormat("yyyyMMdd");
        Date             validDate           = simpleDateFormat.parse("20160212");

        String str = "abc,name,sector";

        TrackParser parser = new TrackParser();

        TrackRec r = parser.parseCsvLine("analyst", validDate, str);

        assertTrue(r.getSymbol().equalsIgnoreCase("abc"));
        assertTrue(r.getAnalyst().equalsIgnoreCase("analyst"));
        assertTrue(r.getName().equalsIgnoreCase("name"));
        assertTrue(r.getSector().equalsIgnoreCase("sector"));
        assertEquals(0, r.getDate().compareTo(validDate));
        assertEquals(0.0, r.getDelta(), 0.01);

    }

}

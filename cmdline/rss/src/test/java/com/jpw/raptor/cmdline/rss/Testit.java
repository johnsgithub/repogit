package com.jpw.raptor.cmdline.rss;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Testit {

    @Test
    public void test01() throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateAsString = sdf.format(new Date());
        Date formattedDate  = sdf.parse(dateAsString);

        System.out.println("Date " + formattedDate);

    }

}

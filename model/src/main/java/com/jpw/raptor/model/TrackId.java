package com.jpw.raptor.model;

import lombok.*;

import java.io.Serializable;

/**
 * Created by john on 4/2/17.
 */
@Data
@NoArgsConstructor
public class TrackId implements Serializable {

    private String  symbol;
    private String  analyst;

}

package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

@Getter
@Setter
public class YahooEtfHoldings {

    static final String SPAN_START = "<span";
    static final String SPAN_END = "</span>";
    static final String TBODY_START = "<tbody>";
    static final String TBODY_END = "</tbody>";
    static final String TR_START = "<tr ";
    static final String TR_END = "</tr>";
    static final String TD_START = "<td ";
    static final String TD_END = "</td>";
    static final String HREF_START = "<a href";
    static final String HREF_END = "</a>";
    static final String GREATER_THAN = ">";
    static final String LESS_THAN = "<";

    private double  stocks;
    private double  bonds;
    private double  basicMaterials;
    private double  consumerCyclical;
    private double  financialServices;
    private double  realEstate;
    private double  consumerDefensive;
    private double  healthcare;
    private double  utilities;
    private double  industrials;
    private double  communicationServices;
    private double  energy;
    private double  technology;
    private double  priceEarnings;
    private double  priceBook;
    private double  priceSales;
    private double  priceCashFlow;
    private double  marketCap;
    private double  bondUs;
    private double  bondAaa;
    private double  bondAa;
    private double  bondA;
    private double  bondBbb;
    private double  bondBb;
    private double  bondB;
    private double  bondBelowB;
    private double  bondOthers;
    private String holdings;
    private String  page;
    private boolean readFailed;

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public boolean readPage(String symbol) throws IOException {

        readFailed = false;

        // Yahoo replaces . with -
        String ticker = symbol.toUpperCase().replace('.', '-');
        String baseUrl = "https://finance.yahoo.com/quote/" + ticker + "/holdings?p=" + ticker;
        try {
            Document doc = Jsoup.connect(baseUrl).get();
            page = doc.toString();
        } catch ( org.jsoup.HttpStatusException ex ) {
            readFailed = true;
        }

        // Validate page was found
        if (readFailed || page.contains("<span>No results for")  || page.contains("<span>Symbols similar to")) {
            return false;
        }

        ValueObject vo = new ValueObject();

        //  Stock percentage
        int stocksLabelStart     = page.indexOf("<span>Stocks") + 1;
        int stocksStartDataSpan  = page.indexOf(SPAN_START, stocksLabelStart) + 1;
        int stocksStartData      = page.indexOf(GREATER_THAN, stocksStartDataSpan) + 1;
        int stocksEndData        = page.indexOf(SPAN_END, stocksStartData);
        stocks = vo.getPercent( page.substring(stocksStartData, stocksEndData) );

        //  Bonds percentage
        int bondsLabelStart     = page.indexOf("<span>Bonds", stocksEndData) + 1;
        int bondsStartDataSpan  = page.indexOf(SPAN_START, bondsLabelStart) + 1;
        int bondsStartData      = page.indexOf(GREATER_THAN, bondsStartDataSpan) + 1;
        int bondsEndData        = page.indexOf(SPAN_END, bondsStartData);
        bonds = vo.getPercent( page.substring(bondsStartData, bondsEndData) );

        //  Basic Materials
        int basicMaterialsLabelStart     = page.indexOf("<span>Basic Materials", bondsEndData) + 1;
        int basicMaterialsGraphicSpan    = page.indexOf(SPAN_START, basicMaterialsLabelStart) + 1;
        int basicMaterialsStartDataSpan  = page.indexOf(SPAN_START, basicMaterialsGraphicSpan) + 1;
        int basicMaterialsStartData      = page.indexOf(GREATER_THAN, basicMaterialsStartDataSpan) + 1;
        int basicMaterialsEndData        = page.indexOf(SPAN_END, basicMaterialsStartData);
        basicMaterials = vo.getPercent( page.substring(basicMaterialsStartData, basicMaterialsEndData) );

        //  Consumer Cyclical
        int consumerCyclicalLabelStart     = page.indexOf("<span>Consumer Cyclical", basicMaterialsEndData) + 1;
        int consumerCyclicalGraphicSpan    = page.indexOf(SPAN_START, consumerCyclicalLabelStart) + 1;
        int consumerCyclicalStartDataSpan  = page.indexOf(SPAN_START, consumerCyclicalGraphicSpan) + 1;
        int consumerCyclicalStartData      = page.indexOf(GREATER_THAN, consumerCyclicalStartDataSpan) + 1;
        int consumerCyclicalEndData        = page.indexOf(SPAN_END, consumerCyclicalStartData);
        consumerCyclical = vo.getPercent( page.substring(consumerCyclicalStartData, consumerCyclicalEndData) );

        //  Financial Services
        int financialServicesLabelStart     = page.indexOf("<span>Financial Services", consumerCyclicalEndData) + 1;
        int financialServicesGraphicSpan    = page.indexOf(SPAN_START, financialServicesLabelStart) + 1;
        int financialServicesStartDataSpan  = page.indexOf(SPAN_START, financialServicesGraphicSpan) + 1;
        int financialServicesStartData      = page.indexOf(GREATER_THAN, financialServicesStartDataSpan) + 1;
        int financialServicesEndData        = page.indexOf(SPAN_END, financialServicesStartData);
        financialServices = vo.getPercent( page.substring(financialServicesStartData, financialServicesEndData) );

        //  Real Estate
        int realEstateLabelStart     = page.indexOf("<span>Real Estate", financialServicesEndData) + 1;
        int realEstateGraphicSpan    = page.indexOf(SPAN_START, realEstateLabelStart) + 1;
        int realEstateStartDataSpan  = page.indexOf(SPAN_START, realEstateGraphicSpan) + 1;
        int realEstateStartData      = page.indexOf(GREATER_THAN, realEstateStartDataSpan) + 1;
        int realEstateEndData        = page.indexOf(SPAN_END, realEstateStartData);
        realEstate = vo.getPercent( page.substring(realEstateStartData, realEstateEndData) );

        //  Consumer Defensive
        int consumerDefensiveLabelStart     = page.indexOf("<span>Consumer Defensive", realEstateEndData) + 1;
        int consumerDefensiveGraphicSpan    = page.indexOf(SPAN_START, consumerDefensiveLabelStart) + 1;
        int consumerDefensiveStartDataSpan  = page.indexOf(SPAN_START, consumerDefensiveGraphicSpan) + 1;
        int consumerDefensiveStartData      = page.indexOf(GREATER_THAN, consumerDefensiveStartDataSpan) + 1;
        int consumerDefensiveEndData        = page.indexOf(SPAN_END, consumerDefensiveStartData);
        consumerDefensive = vo.getPercent( page.substring(consumerDefensiveStartData, consumerDefensiveEndData) );

        //  Healthcare
        int healthcareLabelStart     = page.indexOf("<span>Healthcare", consumerDefensiveEndData) + 1;
        int healthcareGraphicSpan    = page.indexOf(SPAN_START, healthcareLabelStart) + 1;
        int healthcareStartDataSpan  = page.indexOf(SPAN_START, healthcareGraphicSpan) + 1;
        int healthcareStartData      = page.indexOf(GREATER_THAN, healthcareStartDataSpan) + 1;
        int healthcareEndData        = page.indexOf(SPAN_END, healthcareStartData);
        healthcare = vo.getPercent( page.substring(healthcareStartData, healthcareEndData) );

        //  Utilities
        int utilitiesLabelStart     = page.indexOf("<span>Utilities", healthcareEndData) + 1;
        int utilitiesGraphicSpan    = page.indexOf(SPAN_START, utilitiesLabelStart) + 1;
        int utilitiesStartDataSpan  = page.indexOf(SPAN_START, utilitiesGraphicSpan) + 1;
        int utilitiesStartData      = page.indexOf(GREATER_THAN, utilitiesStartDataSpan) + 1;
        int utilitiesEndData        = page.indexOf(SPAN_END, utilitiesStartData);
        utilities = vo.getPercent( page.substring(utilitiesStartData, utilitiesEndData) );

        //  Communication Services
        int communicationServicesLabelStart     = page.indexOf("<span>Communication Services", utilitiesEndData) + 1;
        int communicationServicesGraphicSpan    = page.indexOf(SPAN_START, communicationServicesLabelStart) + 1;
        int communicationServicesStartDataSpan  = page.indexOf(SPAN_START, communicationServicesGraphicSpan) + 1;
        int communicationServicesStartData      = page.indexOf(GREATER_THAN, communicationServicesStartDataSpan) + 1;
        int communicationServicesEndData        = page.indexOf(SPAN_END, communicationServicesStartData);
        communicationServices = vo.getPercent( page.substring(communicationServicesStartData, communicationServicesEndData) );

        //  Energy
        int energyLabelStart     = page.indexOf("<span>Energy", communicationServicesEndData) + 1;
        int energyGraphicSpan    = page.indexOf(SPAN_START, energyLabelStart) + 1;
        int energyStartDataSpan  = page.indexOf(SPAN_START, energyGraphicSpan) + 1;
        int energyStartData      = page.indexOf(GREATER_THAN, energyStartDataSpan) + 1;
        int energyEndData        = page.indexOf(SPAN_END, energyStartData);
        energy = vo.getPercent( page.substring(energyStartData, energyEndData) );

        //  Industrials
        int industrialsLabelStart     = page.indexOf("<span>Industrials", energyEndData) + 1;
        int industrialsGraphicSpan    = page.indexOf(SPAN_START, industrialsLabelStart) + 1;
        int industrialsStartDataSpan  = page.indexOf(SPAN_START, industrialsGraphicSpan) + 1;
        int industrialsStartData      = page.indexOf(GREATER_THAN, industrialsStartDataSpan) + 1;
        int industrialsEndData        = page.indexOf(SPAN_END, industrialsStartData);
        industrials = vo.getPercent( page.substring(industrialsStartData, industrialsEndData) );

        //  Technology
        int technologyLabelStart     = page.indexOf("<span>Technology", industrialsEndData) + 1;
        int technologyGraphicSpan    = page.indexOf(SPAN_START, technologyLabelStart) + 1;
        int technologyStartDataSpan  = page.indexOf(SPAN_START, technologyGraphicSpan) + 1;
        int technologyStartData      = page.indexOf(GREATER_THAN, technologyStartDataSpan) + 1;
        int technologyEndData        = page.indexOf(SPAN_END, technologyStartData);
        technology = vo.getPercent( page.substring(technologyStartData, technologyEndData) );

        //  Price/Earnings
        int priceEarningsLabelStart     = page.indexOf("<span>Price/Earnings", technologyEndData) + 1;
        int priceEarningsStartDataSpan  = page.indexOf(SPAN_START, priceEarningsLabelStart) + 1;
        int priceEarningsStartData      = page.indexOf(GREATER_THAN, priceEarningsStartDataSpan) + 1;
        int priceEarningsEndData        = page.indexOf(SPAN_END, priceEarningsStartData);
        priceEarnings = vo.getDouble( page.substring(priceEarningsStartData, priceEarningsEndData) );

        //  Price/Book
        int priceBookLabelStart     = page.indexOf("<span>Price/Book", priceEarningsEndData) + 1;
        int priceBookStartDataSpan  = page.indexOf(SPAN_START, priceBookLabelStart) + 1;
        int priceBookStartData      = page.indexOf(GREATER_THAN, priceBookStartDataSpan) + 1;
        int priceBookEndData        = page.indexOf(SPAN_END, priceBookStartData);
        priceBook = vo.getDouble( page.substring(priceBookStartData, priceBookEndData) );

        //  Price/Sales
        int priceSalesLabelStart     = page.indexOf("<span>Price/Sales", priceBookEndData) + 1;
        int priceSalesStartDataSpan  = page.indexOf(SPAN_START, priceSalesLabelStart) + 1;
        int priceSalesStartData      = page.indexOf(GREATER_THAN, priceSalesStartDataSpan) + 1;
        int priceSalesEndData        = page.indexOf(SPAN_END, priceSalesStartData);
        priceSales = vo.getDouble( page.substring(priceSalesStartData, priceSalesEndData) );

        //  Price/Cashflow
        int priceCashFlowLabelStart     = page.indexOf("<span>Price/Cashflow", priceSalesEndData) + 1;
        int priceCashFlowStartDataSpan  = page.indexOf(SPAN_START, priceCashFlowLabelStart) + 1;
        int priceCashFlowStartData      = page.indexOf(GREATER_THAN, priceCashFlowStartDataSpan) + 1;
        int priceCashFlowEndData        = page.indexOf(SPAN_END, priceCashFlowStartData);
        priceCashFlow = vo.getDouble( page.substring(priceCashFlowStartData, priceCashFlowEndData) );

        //  Median Market Cap
        int marketCapLabelStart     = page.indexOf("<span>Median Market Cap", priceCashFlowEndData) + 1;
        int marketCapStartDataSpan  = page.indexOf(SPAN_START, marketCapLabelStart) + 1;
        int marketCapStartData      = page.indexOf(GREATER_THAN, marketCapStartDataSpan) + 1;
        int marketCapEndData        = page.indexOf(SPAN_END, marketCapStartData);
        marketCap = vo.getDouble( page.substring(marketCapStartData, marketCapEndData) );

        //  US Government
        int bondUsLabelStart     = page.indexOf("<span>US Government", marketCapEndData) + 1;
        int bondUsStartDataSpan  = page.indexOf(SPAN_START, bondUsLabelStart) + 1;
        int bondUsStartData      = page.indexOf(GREATER_THAN, bondUsStartDataSpan) + 1;
        int bondUsEndData        = page.indexOf(SPAN_END, bondUsStartData);
        bondUs = vo.getPercent( page.substring(bondUsStartData, bondUsEndData) );

        //  AAA
        int bondAaaLabelStart     = page.indexOf("<span>AAA", bondUsEndData) + 1;
        int bondAaaStartDataSpan  = page.indexOf(SPAN_START, bondAaaLabelStart) + 1;
        int bondAaaStartData      = page.indexOf(GREATER_THAN, bondAaaStartDataSpan) + 1;
        int bondAaaEndData        = page.indexOf(SPAN_END, bondAaaStartData);
        bondAaa = vo.getPercent( page.substring(bondAaaStartData, bondAaaEndData) );

        //  AA
        int bondAaLabelStart     = page.indexOf("<span>AA", bondAaaEndData) + 1;
        int bondAaStartDataSpan  = page.indexOf(SPAN_START, bondAaLabelStart) + 1;
        int bondAaStartData      = page.indexOf(GREATER_THAN, bondAaStartDataSpan) + 1;
        int bondAaEndData        = page.indexOf(SPAN_END, bondAaStartData);
        bondAa = vo.getPercent( page.substring(bondAaStartData, bondAaEndData) );

        //  A
        int bondALabelStart     = page.indexOf("<span>A", bondAaEndData) + 1;
        int bondAStartDataSpan  = page.indexOf(SPAN_START, bondALabelStart) + 1;
        int bondAStartData      = page.indexOf(GREATER_THAN, bondAStartDataSpan) + 1;
        int bondAEndData        = page.indexOf(SPAN_END, bondAStartData);
        bondA = vo.getPercent( page.substring(bondAStartData, bondAEndData) );

        //  BBB
        int bondBbbLabelStart     = page.indexOf("<span>BBB", bondAEndData) + 1;
        int bondBbbStartDataSpan  = page.indexOf(SPAN_START, bondBbbLabelStart) + 1;
        int bondBbbStartData      = page.indexOf(GREATER_THAN, bondBbbStartDataSpan) + 1;
        int bondBbbEndData        = page.indexOf(SPAN_END, bondBbbStartData);
        bondBbb = vo.getPercent( page.substring(bondBbbStartData, bondBbbEndData) );

        //  BB
        int bondBbLabelStart     = page.indexOf("<span>BB", bondBbbEndData) + 1;
        int bondBbStartDataSpan  = page.indexOf(SPAN_START, bondBbLabelStart) + 1;
        int bondBbStartData      = page.indexOf(GREATER_THAN, bondBbStartDataSpan) + 1;
        int bondBbEndData        = page.indexOf(SPAN_END, bondBbStartData);
        bondBb = vo.getPercent( page.substring(bondBbStartData, bondBbEndData) );

        //  B
        int bondBLabelStart     = page.indexOf("<span>B", bondBbEndData) + 1;
        int bondBStartDataSpan  = page.indexOf(SPAN_START, bondBLabelStart) + 1;
        int bondBStartData      = page.indexOf(GREATER_THAN, bondBStartDataSpan) + 1;
        int bondBEndData        = page.indexOf(SPAN_END, bondBStartData);
        bondB = vo.getPercent( page.substring(bondBStartData, bondBEndData) );

        //  Below B
        int bondBelowBLabelStart     = page.indexOf("<span>Below B", bondBEndData) + 1;
        int bondBelowBStartDataSpan  = page.indexOf(SPAN_START, bondBelowBLabelStart) + 1;
        int bondBelowBStartData      = page.indexOf(GREATER_THAN, bondBelowBStartDataSpan) + 1;
        int bondBelowBEndData        = page.indexOf(SPAN_END, bondBelowBStartData);
        bondBelowB = vo.getPercent( page.substring(bondBelowBStartData, bondBelowBEndData) );

        //  Others
        int bondOthersLabelStart     = page.indexOf("<span>Others", bondBelowBEndData) + 1;
        int bondOthersStartDataSpan  = page.indexOf(SPAN_START, bondOthersLabelStart) + 1;
        int bondOthersStartData      = page.indexOf(GREATER_THAN, bondOthersStartDataSpan) + 1;
        int bondOthersEndData        = page.indexOf(SPAN_END, bondOthersStartData);
        bondOthers = vo.getPercent( page.substring(bondOthersStartData, bondOthersEndData) );

        holdings = parseTopHoldings(page);

        return true;
    }

    public String parseTopHoldings(String page) {

        // Holdings store as json string
        //[
        //    {"symbol":"NPN.JO","holdingName":"Naspers Ltd Class N","holdingPercent":0.080699995},
        //    ...
        //    {"symbol":"IVN.TO","holdingName":"Ivanhoe Mines Ltd A","holdingPercent":0.0345}
        //]

        int titleStart = page.indexOf("<span>Top") + 1;
        int bodyStart  = page.indexOf(TBODY_START, titleStart) + 1;
        int bodyEnd    = page.indexOf(TBODY_END, bodyStart) + 1;

        if ( titleStart <= 0  ) {
            return "[]";
        }

        // Parse the first row
        String              jsonValue = "[";
        boolean             loopAgain = true;
        YahooEtfTableRow    row       = new YahooEtfTableRow();

        row.parseRow (page, bodyStart, bodyEnd);
        jsonValue += "{\"symbol\":\"" + row.getColumn2() +
                "\",\"holdingName\":\"" + Jsoup.parse(row.getColumn1()).text() +
                "\",\"holdingPercent\":" + getPercent(row.getColumn3()) +
                "}";

        while (loopAgain ) {
            row.parseRow(page, row.getRowEnd(), bodyEnd);
            if ( row.isFound() ) {
                jsonValue += ",{\"symbol\":\"" + row.getColumn2() +
                        "\",\"holdingName\":\"" + Jsoup.parse(row.getColumn1()).text() +
                        "\",\"holdingPercent\":" + getPercent(row.getColumn3()) +
                        "}";
            } else {
                loopAgain = false;
            }
        }

        return jsonValue + "]";
    }

    private double getPercent(String buf) {
        String stripped = buf.replace(",","").replace('%',' ').trim();
        double tempVal =  (Double.parseDouble( stripped ) / 100.0);

        // Round to 4 decimal places
        return Math.round(tempVal * 10000.0) / 10000.0;
    }

    boolean rowFound ( String page, int start, int end ) {
        return page.substring(start, end).contains(TR_START);
    }


    public void print() {
        System.out.println("stocks                  " + stocks);
        System.out.println("bonds                   " + bonds);

        System.out.println("basicMaterials          " + basicMaterials);
        System.out.println("consumerCyclical        " + consumerCyclical);
        System.out.println("financialServices       " + financialServices);
        System.out.println("realEstate              " + realEstate);
        System.out.println("consumerDefensive       " + consumerDefensive);
        System.out.println("healthcare              " + healthcare);
        System.out.println("utilities               " + utilities);
        System.out.println("communicationServices   " + communicationServices);
        System.out.println("energy                  " + energy);
        System.out.println("industrials             " + industrials);
        System.out.println("technology              " + technology);

        System.out.println("priceEarnings           " + priceEarnings);
        System.out.println("priceBook               " + priceBook);
        System.out.println("priceSales              " + priceSales);
        System.out.println("priceCashFlow           " + priceCashFlow);
        System.out.println("marketCap               " + marketCap);

        System.out.println("bondUs                  " + bondUs);
        System.out.println("bondAaa                 " + bondAaa);
        System.out.println("bondAa                  " + bondAa);
        System.out.println("bondA                   " + bondA);
        System.out.println("bondBbb                 " + bondBbb);
        System.out.println("bondBb                  " + bondBb);
        System.out.println("bondB                   " + bondB);
        System.out.println("bondBelowB              " + bondBelowB);
        System.out.println("bondOthers              " + bondOthers);

        System.out.println(holdings);
    }

}

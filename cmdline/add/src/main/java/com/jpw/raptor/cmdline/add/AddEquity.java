package com.jpw.raptor.cmdline.add;

import com.jpw.raptor.algorithm.EquityPerformance;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.index.IndexDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.model.Etf;
import com.jpw.raptor.model.Performance;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Stock;
import com.jpw.raptor.model.Index;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class AddEquity {

    @Autowired
    public IndexDAO indexTbl;

    @Autowired
    public EtfDAO etfTbl;

    @Autowired
    public QuoteDAO quoteTbl;

    @Autowired
    public StockDAO stockTbl;

    public void addEtf(String exchange, String symbol, String name) throws IOException {
        QuotesFromFiles worker = new QuotesFromFiles();

        // Create dummy record and add to database
        //etfTbl.addEmpty(symbol.toUpperCase(), name);

        // Add quotes
        List<Quote> quotes = worker.getQuotes(exchange, symbol);
        for ( Quote quote : quotes ) {
            quoteTbl.add(quote);
        }

        // Worker to compute performance
        EquityPerformance ep = new EquityPerformance();

        // get equity record to update
        Etf equity = etfTbl.get(symbol.toUpperCase());

        // Get quotes for computing performance
        quotes = quoteTbl.getYearsWorthDesc(symbol.toUpperCase());

        // Compute performance
        Performance rec = ep.computePerformance(symbol.toUpperCase(),  quotes);

        // Update equity
        equity.updatePerformance(rec);
        etfTbl.update(equity);
    }


    public void addStock(String exchange, String symbol, String name) throws IOException {
        QuotesFromFiles worker = new QuotesFromFiles();

        // Create dummy record and add to database
        // symbol, name, sp, dow, russell
        //stockTbl.addEmpty(symbol.toUpperCase(), name, "", "", "");

        // Add quotes
        List<Quote> quotes = worker.getQuotes(exchange, symbol);
        for ( Quote quote : quotes ) {
            quoteTbl.add(quote);
        }

        // Worker to compute performance
        EquityPerformance ep = new EquityPerformance();

        // get equity record to update
        Stock equity = stockTbl.get(symbol.toUpperCase());

        // Get quotes for computing performance
        quotes = quoteTbl.getYearsWorthDesc(symbol.toUpperCase());

        // Compute performance
        Performance rec = ep.computePerformance(symbol.toUpperCase(),  quotes);

        // Update equity
        equity.updatePerformance(rec);
        stockTbl.update(equity);
    }


    public void addIndex(String exchange, String symbol, String name) throws IOException {
        QuotesFromFiles worker = new QuotesFromFiles();

        // Create dummy record and add to database
        //indexTbl.addEmpty(symbol.toUpperCase(), name);

        // Add quotes
        List<Quote> quotes = worker.getQuotes(exchange, symbol);
        for ( Quote quote : quotes ) {
            quoteTbl.add(quote);
        }

        // Worker to compute performance
        EquityPerformance ep = new EquityPerformance();

        // get equity record to update
        Index equity = indexTbl.get(symbol.toUpperCase());

        // Get quotes for computing performance
        quotes = quoteTbl.getYearsWorthDesc(symbol.toUpperCase());

        // Compute performance
        Performance rec = ep.computePerformance(symbol.toUpperCase(),  quotes);

        // Update equity
        equity.updatePerformance(rec);
        indexTbl.update(equity);
    }
}

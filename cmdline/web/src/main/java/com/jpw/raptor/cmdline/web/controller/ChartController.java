package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.algorithm.JDKFactory;
import com.jpw.raptor.jdbc.index.IndexDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.model.Index;
import com.jpw.raptor.model.Jdk;
import com.jpw.raptor.model.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;

@Controller
public class ChartController {

    @Autowired
    private IndexDAO indexTbl;

    @Autowired
    private QuoteDAO quoteTbl;

    @RequestMapping("/chart")
    public String eventReq(
            @RequestParam(value="symbol", required=false) String symbol,
            Model model){

        model.addAttribute("symbol", "VIX");

        // Obtain details
        Index rec = indexTbl.get("VIX");
        model.addAttribute("index_info", rec);

        // Get Quotes
        List<Quote> ivvList = quoteTbl.getYearsWorthDesc("IVV");
        List<Quote> usmvList = quoteTbl.getYearsWorthDesc("USMV");

        JDKFactory jdkFactory = new JDKFactory();
        List<Jdk> jdkValues  = jdkFactory.generate(ivvList, usmvList);

        // Compute the moving averages
        Collections.reverse(jdkValues);
        model.addAttribute("data", jdkValues);

        return "chart";
    }

}

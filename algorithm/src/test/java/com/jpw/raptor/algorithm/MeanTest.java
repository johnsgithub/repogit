package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Quote;
import org.junit.Assert;
import org.junit.Test;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MeanTest {

    @Test
    public void test01() throws ParseException {
        System.out.println("mean test01 started");

        SimpleDateFormat sdf    = new SimpleDateFormat("yyyyMMdd");
        DecimalFormat df     = new DecimalFormat("#.##");
        ArrayList<Quote> data   = new ArrayList(253);

        data.add(new Quote("SPY", sdf.parse("20180323"), 0.0, 0.0, 0.0, 258.05, 0));
        data.add(new Quote("SPY", sdf.parse("20180322"), 0.0, 0.0, 0.0, 263.67, 0));
        data.add(new Quote("SPY", sdf.parse("20180321"), 0.0, 0.0, 0.0, 270.43, 0));
        data.add(new Quote("SPY", sdf.parse("20180320"), 0.0, 0.0, 0.0, 270.95, 0));
        data.add(new Quote("SPY", sdf.parse("20180319"), 0.0, 0.0, 0.0, 270.49, 0));
        Mean doit = new Mean();

        double result;

        // 0 start 3 days
        double test1 = doit.quote(data, 0, 3);
        result = data.get(0).getClose() + data.get(1).getClose() + data.get(2).getClose();
        result = result / 3.0;
        Assert.assertEquals(Double.parseDouble(df.format(result)), test1, 0.001);

        // 2 start 3 days
        double test2 = doit.quote(data, 2, 3);
        result = data.get(2).getClose() + data.get(3).getClose() + data.get(4).getClose();
        result = result / 3.0;
        Assert.assertEquals(Double.parseDouble(df.format(result)), test2, 0.001);

        // 3 start 3 days
        double test3 = doit.quote(data, 3, 3);
        Assert.assertEquals(270.72, test3, 0.001);

        System.out.println("mean  test finished");
    }


    @Test
    public void test02() throws ParseException {
        System.out.println("mean test02 started");


        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        DecimalFormat df = new DecimalFormat("#.##");

        Quote[] refData = {
                new Quote("IVV", fmt.parse("2019-01-09"), 259.09, 260.48, 257.72, 259.49, 4096900),
                new Quote("IVV", fmt.parse("2019-01-08"), 258.38, 258.82, 255.52, 258.29, 5340600),
                new Quote("IVV", fmt.parse("2019-01-07"), 254.24, 257.46, 253.21, 255.77, 5961300),
                new Quote("IVV", fmt.parse("2019-01-04"), 249.06, 254.59, 248.64, 254.06, 6430100),
                new Quote("IVV", fmt.parse("2019-01-03"), 249.79, 250.03, 245.08, 245.43, 5828300),
                new Quote("IVV", fmt.parse("2019-01-02"), 247.54, 252.7, 247.42, 251.72, 5978400),
                new Quote("IVV", fmt.parse("2018-12-31"), 251.04, 251.63, 248.99, 251.61, 10117800),
                new Quote("IVV", fmt.parse("2018-12-28"), 251.03, 252.88, 247.95, 249.33, 10856300)
        };
        List<Quote> refList = Arrays.asList(refData);

        double result = (259.49 +  258.29 + 255.77 + 254.06 + 245.43 + 251.72 + 251.61 + 249.33) / 8.0;

        Mean doit = new Mean();
        double test = doit.quote(refList, 0, 8);
        Assert.assertEquals(Double.parseDouble(df.format(result)), test, 0.001);
    }
}

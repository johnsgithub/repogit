package com.jpw.raptor.cmdline.reports.track;

import com.jpw.raptor.lib.properties.FinanceProperties;
import com.jpw.raptor.model.FileQualified;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import java.util.Date;

public class TrackReportUtil {

    Writer fileWriter;

    public void openResultsFile(String analyst, Date date) throws java.io.IOException {

        // import directory
        FinanceProperties fp   = new FinanceProperties();
        Properties prop = fp.get();

        String dir  = prop.getProperty("reports_dir");
        if ( dir == null )
        {
            System.out.println("Track directory not specified");
            return;
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String strDate = dateFormat.format(date);
        if ( StringUtils.isNotEmpty(analyst) )
            fileWriter = new FileWriter(dir + "/" + "track-" + analyst + "-" + strDate + ".csv");
        else
            fileWriter = new FileWriter(dir + "/" + "track-" + "all" + "-" + strDate + ".csv");
    }

    public void closeResultsFile() throws java.io.IOException {
        fileWriter.flush();
        fileWriter.close();
    }

    public void writeResultsFile(String line) throws java.io.IOException {
        fileWriter.write(line + "\n");
    }
}

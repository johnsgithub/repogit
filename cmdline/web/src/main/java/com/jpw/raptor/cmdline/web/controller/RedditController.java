package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.reddit.RedditPostDAO;
import com.jpw.raptor.jdbc.reddit.RedditSummDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

@Controller
public class RedditController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    protected RedditSummDAO summTbl;

    @Autowired
    protected RedditPostDAO postTbl;

    @Autowired
    protected StockDAO stockTbl;

    protected final static String REDDIT_MODEL = "reddit_model";
    protected final static String REDDIT_SUMMARY_MODEL = "reddit_summ";
    protected final static String PAGE_TITLE = "pagetitle";
    protected final static String REDDIT_PAGE = "reddit";
    protected final static String REDDIT_SUMM_PAGE = "reddit-summary";

    @RequestMapping("/reddit-summary")
    public String redditSummaryReq(Model model)
    {

        // Get the records to display
        List<RedditSumm> recs = summTbl.getAllAsc();

        model.addAttribute(REDDIT_SUMMARY_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "Reddit Stock Reference Summary" );

        return REDDIT_SUMM_PAGE;
    }

    @RequestMapping("/reddit")
    public String redditDetailReq(
            @RequestParam(value="symbol", required=true) String symbol,
            Model model)
    {

        // Get the records to display
        List<RedditPost> recs = postTbl.getStock(symbol.toUpperCase(Locale.ROOT));
        model.addAttribute(REDDIT_MODEL, recs);

        // Get stock name
        Stock stock = stockTbl.get(symbol);
        if ( stock != null )
            model.addAttribute(PAGE_TITLE, symbol.toUpperCase(Locale.ROOT) + " - " + stock.getName());
        else
            model.addAttribute(PAGE_TITLE, symbol.toUpperCase(Locale.ROOT) );

        return REDDIT_PAGE;
    }

}

package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.ZScore;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.jpw.raptor.model.Quote;

public class ZScoreFactoryTest {

    @Test
    public void test1() throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        // Create list of quotes
        List<Quote> data = new ArrayList<>(Arrays.asList(
            new Quote("SPY", sdf.parse("20160211"), 182.34, 184.1, 181.09, 258.05, 218437700),
            new Quote("SPY", sdf.parse("20160210"), 186.41, 188.34, 185.12, 263.67, 148214300),
            new Quote("SPY", sdf.parse("20160209"), 183.36, 186.94, 183.2, 270.43, 184512600),
            new Quote("SPY", sdf.parse("20160208"), 185.77, 186.12, 182.8, 270.95, 191526500),
            new Quote("SPY", sdf.parse("20160205"), 190.99, 191.67, 187.2, 270.49, 180788300)
        ));

        // compute data set mean
        Mean meanFactory = new Mean();
        double mean        = meanFactory.quote(data, 0, 5);

        // compute data set standard deviation
        StandardDeviation stdFactory  = new StandardDeviation();
        double std         = stdFactory.quote(data, 0, 5);

        // close values for verifying results
        double d00 = Math.round( ((data.get(0).getClose() - mean) / std) * 1000.0 ) / 1000.0;
        double d01 = Math.round( ((data.get(1).getClose() - mean) / std) * 1000.0 ) / 1000.0;
        double d02 = Math.round( ((data.get(2).getClose() - mean) / std) * 1000.0 ) / 1000.0;
        double d03 = Math.round( ((data.get(3).getClose() - mean) / std) * 1000.0 ) / 1000.0;
        double d04 = Math.round( ((data.get(4).getClose() - mean) / std) * 1000.0 ) / 1000.0;

        ZScoreFactory factory = new ZScoreFactory();
        List<ZScore> result = factory.quote(data, 0, 5);

        // Enough data
        Assert.assertEquals(d00 , result.get(0).getScore(), 0.001 );
        Assert.assertEquals(data.get(0).getSymbol() , result.get(0).getSymbol() );
        Assert.assertEquals(data.get(0).getDate() , result.get(0).getDate() );

        Assert.assertEquals(d01 , result.get(1).getScore(), 0.001 );
        Assert.assertEquals(data.get(1).getSymbol() , result.get(1).getSymbol());
        Assert.assertEquals(data.get(1).getDate() , result.get(1).getDate());

        Assert.assertEquals(d02 , result.get(2).getScore(), 0.001 );
        Assert.assertEquals(data.get(2).getSymbol() , result.get(2).getSymbol());
        Assert.assertEquals(data.get(2).getDate() , result.get(2).getDate());

        Assert.assertEquals(d03 , result.get(3).getScore(), 0.001 );
        Assert.assertEquals(data.get(3).getSymbol() , result.get(3).getSymbol());
        Assert.assertEquals(data.get(3).getDate() , result.get(3).getDate());

        Assert.assertEquals(d04 , result.get(4).getScore(), 0.001 );
        Assert.assertEquals(data.get(4).getSymbol() , result.get(4).getSymbol());
        Assert.assertEquals(data.get(4).getDate() , result.get(4).getDate());
    }

    @Test
    public void test2() throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        // Create list of quotes
        List<Quote> data1 = new ArrayList<>(Arrays.asList(
                new Quote("SPY", sdf.parse("20160211"), 182.34, 184.1, 181.09, 258.05, 218437700),
                new Quote("SPY", sdf.parse("20160210"), 186.41, 188.34, 185.12, 263.67, 148214300),
                new Quote("SPY", sdf.parse("20160209"), 183.36, 186.94, 183.2, 270.43, 184512600),
                new Quote("SPY", sdf.parse("20160208"), 185.77, 186.12, 182.8, 270.95, 191526500),
                new Quote("SPY", sdf.parse("20160205"), 190.99, 191.67, 187.2, 270.49, 180788300)
        ));

        // second data set values divided by 2
        List<Quote> data2 = new ArrayList<>(Arrays.asList(
                new Quote("SPY", sdf.parse("20160211"), 182.34, 184.1, 181.09, 258.05, 218437700),
                new Quote("SPY", sdf.parse("20160210"), 186.41, 188.34, 185.12, 263.67, 148214300),
                new Quote("SPY", sdf.parse("20160209"), 183.36, 186.94, 183.2, 270.43, 184512600),
                new Quote("SPY", sdf.parse("20160208"), 185.77, 186.12, 182.8, 270.95, 191526500),
                new Quote("SPY", sdf.parse("20160205"), 190.99, 191.67, 187.2, 270.49, 180788300)
        ));

        ZScoreFactory factory = new ZScoreFactory();
        List<ZScore> result1 = factory.quote(data1, 0, 5);
        List<ZScore> result2 = factory.quote(data2, 0, 5);

        for (int i=0; i<5; i++) {
          //  Assert.assertEquals(result1.get(i).getScore(), result2.get(i).getScore(), 0.001 );
        }

    }

}

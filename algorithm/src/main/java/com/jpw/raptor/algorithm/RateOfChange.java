package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Jdk;
import com.jpw.raptor.model.Roc;
import com.jpw.raptor.model.Quote;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Ported to new design 5/18/2019
 *
 * Rate of change is used to mathematically describe the percentage change in value
 * over a defined period of time, and it represents the momentum of a variable.
 *
 * Momentum is the price difference in an equity ove a period of time
 * To construct a 10-day momentum line, simply subtract the closing price
 * 10 days ago from the last closing price.
 *
 * A security with high momentum, or one that has a positive ROC, normally outperforms
 * the market in the short term.
 *
 * If the ROC of an index or other broad-market security is over 50%,
 * investors should be wary of a bubble.
 *
 */

public class RateOfChange {


    public List<Roc> quote(List<Quote> data, int period) {

        int newElements = data.size() - period;

        // allocate space for object list
        List<Roc> list = new ArrayList<>(newElements);

        // Create the roc list
        for ( int i=0; i<newElements; i++) {
            double prior  = data.get(i+period).getClose();
            double result = ((data.get(i).getClose() - prior) / prior) * 100.0;
            Roc    roc    = new Roc(data.get(i));
            roc.setValue( Math.round(result * 100.0 ) / 100.0 );
            list.add(roc);
        }

        return list;
    }


    public List<Jdk> momentumJdk(List<Jdk> data, int period) {

        int newElements = data.size() - period;

        // allocate space for object list
        List<Jdk> list = new ArrayList<>(newElements);

        // Create the roc list
        for ( int i=0; i<newElements; i++) {
            double prior  = data.get(i+period).getRsRatio();
            double result = ((data.get(i).getRsRatio() - prior) / prior) * 100.0;
            Jdk    jdk    = new Jdk(data.get(i).getSymbol(), data.get(i).getDate(), data.get(i).getRsRatio(),
                    Math.round(result * 100.0) / 100.0);
            list.add(jdk);
        }

        return list;
    }
}

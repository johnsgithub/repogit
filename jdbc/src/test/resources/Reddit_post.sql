
CREATE TABLE reddit_post_tbl
(
  row_id  	BIGSERIAL,
  date_tx 	date not NULL,
  stock 	character varying(64),
  community 	character varying(64),
  post_id 	character varying(128),
  author 	character varying(64),
  title 	text,
  ctype	character varying(8),
  contents 	text,
  
  CONSTRAINT reddit_post_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX idx_reddit_post_stock
  ON reddit_post_tbl
  USING btree
  (stock);

CREATE INDEX idx_reddit_post_community
  ON reddit_post_tbl
  USING btree
  (community);
  
CREATE INDEX idx_reddit_post_id
  ON reddit_post_tbl
  USING btree
  (post_id);


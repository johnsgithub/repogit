package com.jpw.raptor.jdbc.track;
import com.jpw.raptor.model.TrackRec;
import org.junit.*;
import org.junit.runner.RunWith;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class Test01 {

    static class PostgresResource  {

        PostgreSQLContainer postgreSQLContainer;
        PGSimpleDataSource  dataSource;

        public PostgresResource() {
            postgreSQLContainer = new PostgreSQLContainer("postgres:10.11")
                    .withDatabaseName("test-jdbc")
                    .withUsername("sa")
                    .withPassword("sa");

            postgreSQLContainer.start();

            dataSource = new PGSimpleDataSource();
            dataSource.setDatabaseName(postgreSQLContainer.getDatabaseName());
            dataSource.setServerName(postgreSQLContainer.getContainerIpAddress());
            dataSource.setPortNumber(postgreSQLContainer.getMappedPort(5432));
            dataSource.setUser(postgreSQLContainer.getUsername());
            dataSource.setPassword(postgreSQLContainer.getPassword());

        }

        public void stop() { postgreSQLContainer.stop(); }

        public PGSimpleDataSource getDataSource () {return dataSource;}
    }

    private static PostgresResource myPostgresResource;
    private static TrackDAO         tbl;

    @BeforeClass
    public static void setUp() throws Exception {
        myPostgresResource = new PostgresResource();

        // Create connection to docker database
        tbl = new TrackDAO();
        tbl.setDataSource(myPostgresResource.getDataSource());
        tbl.postConstruct();

        // Create table for testing
        File   resource = new ClassPathResource("Track.sql").getFile();
        String cmds     = new String(Files.readAllBytes(resource.toPath()));
        tbl.sqlScript(cmds);
    }

    @AfterClass
    public static void tearDown(){
        myPostgresResource.stop();
    }

    // rec01 "sym01", "analyst01"
    // rec02 "sym02", "analyst02"
    // rec03 "sym03", "analyst03"
    // rec04 "sym01", "analyst02",
    // rec05 "sym05", "analyst02",


    @Test
    public void test00() throws Exception {

        System.out.println("Start");

        TrackData d = new TrackData();
        List<TrackRec> l;

        // add records
        tbl.add(d.getRec01());
        tbl.add(d.getRec02());
        tbl.add(d.getRec03());
        tbl.add(d.getRec04());
        tbl.add(d.getRec05());

        // read a record
        d.validateRec01(tbl.get(d.getRec01().getSymbol(), d.getRec01().getAnalyst()));

        // read by symbol
        l = tbl.getSymbol(d.getRec01().getSymbol());
        assertEquals(2, l.size());

        // read by analyst
        l = tbl.getAnalyst(d.getRec02().getAnalyst());
        assertEquals(3, l.size());

        // delete
        tbl.delete(d.getRec01().getSymbol(), d.getRec01().getAnalyst());

        // update
        TrackRec upd = d.getRecUpdate(d.getRec02());
        tbl.update(upd);
        d.validateUpdate(tbl.get(upd.getSymbol(), upd.getAnalyst()));

        // read all
        l = tbl.getAll();
        assertEquals(4, l.size());

        System.out.println("End");
    }
}

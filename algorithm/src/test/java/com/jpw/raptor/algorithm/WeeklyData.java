package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Jdk;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.ZScore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WeeklyData  {

    public List<Quote> ivvQuotes() throws java.text.ParseException {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

        List<Quote> ivv = new ArrayList<>(Arrays.asList(
                new Quote("IVV", fmt.parse("2018-12-18"), 0.0, 0.0, 0.0, 255.62, 0),
                new Quote("IVV", fmt.parse("2018-12-11"), 0.0, 0.0, 0.0, 266.05, 0),
                new Quote("IVV", fmt.parse("2018-12-04"), 0.0, 0.0, 0.0, 272.53, 0),
                new Quote("IVV", fmt.parse("2018-11-27"), 0.0, 0.0, 0.0, 270.32, 0),
                new Quote("IVV", fmt.parse("2018-11-20"), 0.0, 0.0, 0.0, 266.10, 0),
                new Quote("IVV", fmt.parse("2018-11-13"), 0.0, 0.0, 0.0, 274.11, 0),
                new Quote("IVV", fmt.parse("2018-11-06"), 0.0, 0.0, 0.0, 277.16, 0),
                new Quote("IVV", fmt.parse("2018-10-30"), 0.0, 0.0, 0.0, 269.86, 0),
                new Quote("IVV", fmt.parse("2018-10-23"), 0.0, 0.0, 0.0, 275.57, 0),
                new Quote("IVV", fmt.parse("2018-10-16"), 0.0, 0.0, 0.0, 282.16, 0),
                new Quote("IVV", fmt.parse("2018-10-09"), 0.0, 0.0, 0.0, 289.59, 0),
                new Quote("IVV", fmt.parse("2018-10-02"), 0.0, 0.0, 0.0, 293.75, 0),
                new Quote("IVV", fmt.parse("2018-09-25"), 0.0, 0.0, 0.0, 294.16, 0),
                new Quote("IVV", fmt.parse("2018-09-18"), 0.0, 0.0, 0.0, 293.07, 0),
                new Quote("IVV", fmt.parse("2018-09-11"), 0.0, 0.0, 0.0, 291.22, 0),
                new Quote("IVV", fmt.parse("2018-09-04"), 0.0, 0.0, 0.0, 291.95, 0),
                new Quote("IVV", fmt.parse("2018-08-28"), 0.0, 0.0, 0.0, 291.89, 0),
                new Quote("IVV", fmt.parse("2018-08-21"), 0.0, 0.0, 0.0, 288.23, 0),
                new Quote("IVV", fmt.parse("2018-08-14"), 0.0, 0.0, 0.0, 285.93, 0),
                new Quote("IVV", fmt.parse("2018-08-07"), 0.0, 0.0, 0.0, 287.47, 0),
                new Quote("IVV", fmt.parse("2018-07-31"), 0.0, 0.0, 0.0, 283.28, 0),
                new Quote("IVV", fmt.parse("2018-07-24"), 0.0, 0.0, 0.0, 283.55, 0),
                new Quote("IVV", fmt.parse("2018-07-17"), 0.0, 0.0, 0.0, 282.47, 0),
                new Quote("IVV", fmt.parse("2018-07-10"), 0.0, 0.0, 0.0, 280.83, 0),
                new Quote("IVV", fmt.parse("2018-07-03"), 0.0, 0.0, 0.0, 272.73, 0)
        ));
        return ivv;
    }

    public List<Quote> vigQuotes() throws java.text.ParseException {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        List<Quote> vig = new ArrayList<>(Arrays.asList(
                new Quote("VIG", fmt.parse("2018-12-18"), 0.0, 0.0, 0.0, 99.15, 0),
                new Quote("VIG", fmt.parse("2018-12-11"), 0.0, 0.0, 0.0, 103.64, 0),
                new Quote("VIG", fmt.parse("2018-12-04"), 0.0, 0.0, 0.0, 105.60, 0),
                new Quote("VIG", fmt.parse("2018-11-27"), 0.0, 0.0, 0.0, 105.01, 0),
                new Quote("VIG", fmt.parse("2018-11-20"), 0.0, 0.0, 0.0, 103.92, 0),
                new Quote("VIG", fmt.parse("2018-11-13"), 0.0, 0.0, 0.0, 106.21, 0),
                new Quote("VIG", fmt.parse("2018-11-06"), 0.0, 0.0, 0.0, 106.48, 0),
                new Quote("VIG", fmt.parse("2018-10-30"), 0.0, 0.0, 0.0, 103.29, 0),
                new Quote("VIG", fmt.parse("2018-10-23"), 0.0, 0.0, 0.0, 104.09, 0),
                new Quote("VIG", fmt.parse("2018-10-16"), 0.0, 0.0, 0.0, 106.62, 0),
                new Quote("VIG", fmt.parse("2018-10-09"), 0.0, 0.0, 0.0, 109.11, 0),
                new Quote("VIG", fmt.parse("2018-10-02"), 0.0, 0.0, 0.0, 110.94, 0),
                new Quote("VIG", fmt.parse("2018-09-25"), 0.0, 0.0, 0.0, 111.37, 0),
                new Quote("VIG", fmt.parse("2018-09-18"), 0.0, 0.0, 0.0, 111.46, 0),
                new Quote("VIG", fmt.parse("2018-09-11"), 0.0, 0.0, 0.0, 110.21, 0),
                new Quote("VIG", fmt.parse("2018-09-04"), 0.0, 0.0, 0.0, 109.16, 0),
                new Quote("VIG", fmt.parse("2018-08-28"), 0.0, 0.0, 0.0, 109.38, 0),
                new Quote("VIG", fmt.parse("2018-08-21"), 0.0, 0.0, 0.0, 108.69, 0),
                new Quote("VIG", fmt.parse("2018-08-14"), 0.0, 0.0, 0.0, 106.63, 0),
                new Quote("VIG", fmt.parse("2018-08-07"), 0.0, 0.0, 0.0, 107.06, 0),
                new Quote("VIG", fmt.parse("2018-07-31"), 0.0, 0.0, 0.0, 106.38, 0),
                new Quote("VIG", fmt.parse("2018-07-24"), 0.0, 0.0, 0.0, 105.14, 0),
                new Quote("VIG", fmt.parse("2018-07-17"), 0.0, 0.0, 0.0, 104.96, 0),
                new Quote("VIG", fmt.parse("2018-07-10"), 0.0, 0.0, 0.0, 104.26, 0),
                new Quote("VIG", fmt.parse("2018-07-03"), 0.0, 0.0, 0.0, 101.53, 0)
        ));
        return vig;
    }

    public List<Double> rpValues() {
        List<Double> rpValues = new ArrayList<>(Arrays.asList(
                0.388,
                0.39,
                0.387,
                0.388,
                0.391,
                0.387,
                0.384,
                0.383,
                0.378,
                0.378,
                0.377,
                0.378,
                0.379,
                0.38,
                0.378,
                0.374,
                0.375,
                0.377,
                0.373,
                0.372,
                0.376,
                0.371,
                0.372,
                0.371,
                0.372));
        return rpValues;
    }

    public List<ZScore> zScores() throws java.text.ParseException {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        List<ZScore> zScores = new ArrayList<>(Arrays.asList(
                new ZScore("IVV:VIG", fmt.parse("2018-12-18"), 0.8),
                new ZScore("IVV:VIG", fmt.parse("2018-12-11"), 1.0),
                new ZScore("IVV:VIG", fmt.parse("2018-12-04"), 0.7),
                new ZScore("IVV:VIG", fmt.parse("2018-11-27"), 0.8),
                new ZScore("IVV:VIG", fmt.parse("2018-11-20"), 1.1),
                new ZScore("IVV:VIG", fmt.parse("2018-11-13"), 0.7),
                new ZScore("IVV:VIG", fmt.parse("2018-11-06"), 0.4),
                new ZScore("IVV:VIG", fmt.parse("2018-10-30"), 0.3),
                new ZScore("IVV:VIG", fmt.parse("2018-10-23"), -0.2),
                new ZScore("IVV:VIG", fmt.parse("2018-10-16"), -0.2),
                new ZScore("IVV:VIG", fmt.parse("2018-10-09"), -0.3),
                new ZScore("IVV:VIG", fmt.parse("2018-10-02"), -0.2),
                new ZScore("IVV:VIG", fmt.parse("2018-09-25"), -0.1),
                new ZScore("IVV:VIG", fmt.parse("2018-09-18"), 0.0),
                new ZScore("IVV:VIG", fmt.parse("2018-09-11"), -0.2),
                new ZScore("IVV:VIG", fmt.parse("2018-09-04"), -0.6),
                new ZScore("IVV:VIG", fmt.parse("2018-08-28"), -0.5),
                new ZScore("IVV:VIG", fmt.parse("2018-08-21"), -0.3),
                new ZScore("IVV:VIG", fmt.parse("2018-08-14"), -0.7),
                new ZScore("IVV:VIG", fmt.parse("2018-08-07"), -0.8),
                new ZScore("IVV:VIG", fmt.parse("2018-07-31"), -0.4),
                new ZScore("IVV:VIG", fmt.parse("2018-07-24"), -0.9),
                new ZScore("IVV:VIG", fmt.parse("2018-07-17"), -0.8),
                new ZScore("IVV:VIG", fmt.parse("2018-07-10"), -0.9),
                new ZScore("IVV:VIG", fmt.parse("2018-07-03"), -0.8)));
        return zScores;
    }

    public List<Jdk> jdkRatios() throws java.text.ParseException {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        List<Jdk> jdkRatios = new ArrayList<>(Arrays.asList(
                new Jdk("IVV:VIG", fmt.parse("2018-12-18"), 100.0 + 0.8, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-12-11"), 100.0 + 1.0, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-12-04"), 100.0 + 0.7, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-11-27"), 100.0 + 0.8, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-11-20"), 100.0 + 1.1, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-11-13"), 100.0 + 0.7, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-11-06"), 100.0 + 0.4, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-10-30"), 100.0 + 0.3, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-10-23"), 100.0 + -0.2, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-10-16"), 100.0 + -0.2, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-10-09"), 100.0 + -0.3, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-10-02"), 100.0 + -0.2, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-09-25"), 100.0 + -0.1, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-09-18"), 100.0 + 0.0, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-09-11"), 100.0 + -0.2, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-09-04"), 100.0 + -0.6, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-08-28"), 100.0 + -0.5, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-08-21"), 100.0 + -0.3, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-08-14"), 100.0 + -0.7, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-08-07"), 100.0 + -0.8, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-07-31"), 100.0 + -0.4, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-07-24"), 100.0 + -0.9, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-07-17"), 100.0 + -0.8, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-07-10"), 100.0 + -0.9, 0.0),
                new Jdk("IVV:VIG", fmt.parse("2018-07-03"), 100.0 + -0.8, 0.0)));
        return jdkRatios;
    }

    public List<Jdk> jdkValues() throws java.text.ParseException {

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        List<Jdk> jdkValues = new ArrayList<>(Arrays.asList(
                new Jdk("IVV:VIG", fmt.parse("2018-12-18"), 100.0 + 0.8, Math.round((100.0 + 0.8 - 1.0) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-12-11"), 100.0 + 1.0, Math.round((100.0 + 1.0 - 0.7) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-12-04"), 100.0 + 0.7, Math.round((100.0 + 0.7 - 0.8) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-11-27"), 100.0 + 0.8, Math.round((100.0 + 0.8 - 1.1) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-11-20"), 100.0 + 1.1, Math.round((100.0 + 1.1 - 0.7) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-11-13"), 100.0 + 0.7, Math.round((100.0 + 0.7 - 0.4) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-11-06"), 100.0 + 0.4, Math.round((100.0 + 0.4 - 0.3) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-10-30"), 100.0 + 0.3, Math.round((100.0 + 0.3 - -0.2) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-10-23"), 100.0 + -0.2, Math.round((100.0 + -0.2 - -0.2) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-10-16"), 100.0 + -0.2, Math.round((100.0 + -0.2 - -0.3) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-10-09"), 100.0 + -0.3, Math.round((100.0 + -0.3 - -0.2) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-10-02"), 100.0 + -0.2, Math.round((100.0 + -0.2 - -0.1) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-09-25"), 100.0 + -0.1, Math.round((100.0 + -0.1 - 0.0) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-09-18"), 100.0 + 0.0, Math.round((100.0 + 0.0 - -0.2) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-09-11"), 100.0 + -0.2, Math.round((100.0 + -0.2 - -0.6) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-09-04"), 100.0 + -0.6, Math.round((100.0 + -0.6 - -0.5) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-08-28"), 100.0 + -0.5, Math.round((100.0 + -0.5 - -0.3) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-08-21"), 100.0 + -0.3, Math.round((100.0 + -0.3 - -0.7) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-08-14"), 100.0 + -0.7, Math.round((100.0 + -0.7 - -0.8) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-08-07"), 100.0 + -0.8, Math.round((100.0 + -0.8 - -0.4) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-07-31"), 100.0 + -0.4, Math.round((100.0 + -0.4 - -0.9) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-07-24"), 100.0 + -0.9, Math.round((100.0 + -0.9 - -0.8) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-07-17"), 100.0 + -0.8, Math.round((100.0 + -0.8 - -0.9) * 100.0) / 100.0),
                new Jdk("IVV:VIG", fmt.parse("2018-07-10"), 100.0 + -0.9, Math.round((100.0 + -0.9 - -0.8) * 100.0) / 100.0)
        ));
        return jdkValues;
    }
}

package com.jpw.raptor.search.finance;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.jpw.raptor.model.FinanceKnowledge;
import com.jpw.raptor.model.FinanceKnowledgeModel;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.javatuples.Pair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class KnowledgeSearchTest {

    protected FinanceKnowledge    k1;
    protected FinanceKnowledge    k2;
    protected FinanceKnowledge    k3;
    protected FinanceKnowledge    k4;
    protected FinanceKnowledge    k5;
    protected FinanceKnowledge    k6;


    protected String              eol;
    protected String              index;

    protected KnowledgeSearch   search;

    @Before
    public void setup() throws IOException {

        search  = new KnowledgeSearch();
        index   = "index-learn";
        eol     ="\n";
        search.openIndex(index);

        setupK1();
        setupK2();
        setupK3();
        setupK4();
        setupK5();
        setupK6();
    }

    @After
    public void tearDown() throws IOException {
        search.closeIndex();
    }

    public void setupK1() {

        //
        // K1
        // query wilson audio
        // contained in title, tag body
        //
        SimpleDateFormat formatter  = new SimpleDateFormat("yyyy");
        String           year       = formatter.format(new Date());

        StringBuilder sb = new StringBuilder();
        sb.append("the quick brown fox ran").append(eol);
        sb.append("humpy dumpty fell down").append(eol);
        sb.append("wilson audio watt puppy").append(eol);
        sb.append("washington").append(eol);
        sb.append("now is the time for all good men");

        k1 = new FinanceKnowledge();

        k1.setBody(sb.toString());

        k1.setTitle("knowledge 1 wilson audio watt puppy");
        k1.setTag("wilson audio watt puppy");
        k1.setId(DigestUtils.md5Hex(k1.getBody()).toUpperCase());
        k1.setLoc( "/doc/" + year + "/" + k1.getId() + ".txt" );
        k1.setUrl("/html/" + year + "/" + k1.getId() + ".html" );
    }


    public void setupK2() {

        //
        // K2
        // query wilson audio
        // contained in tag body
        //

        SimpleDateFormat formatter  = new SimpleDateFormat("yyyy");
        String           year       = formatter.format(new Date());

        StringBuilder sb = new StringBuilder();
        sb.append("Earnings per share (EPS) is generally considered to be the single most important variable ");
        sb.append("in determining a share's price.").append(eol);
        sb.append("wilson audio watt puppy").append(eol);
        sb.append("washington nationals").append(eol);
        sb.append("The diluted EPS is the worst-case scenario for the earnings per share").append(eol);
        sb.append("EPS = (Net Income - Dividends on Preferred Stock) / Average Outstanding Shares. ");

        k2 = new FinanceKnowledge();
        k2.setBody(sb.toString());

        k2.setTitle("knowledge 2");
        k2.setTag("Earnings Per Share wilson audio");
        k2.setId(DigestUtils.md5Hex(k2.getBody()).toUpperCase());
        k2.setLoc( "/doc/" + year + "/" + k2.getId() + ".txt" );
        k2.setUrl("/html/" + year + "/" + k2.getId() + ".html" );
    }


    public void setupK3() {

        //
        // K3
        // query wilson audio
        // contained in  body
        //
        SimpleDateFormat formatter  = new SimpleDateFormat("yyyy");
        String           year       = formatter.format(new Date());

        StringBuilder sb = new StringBuilder();
        sb.append("it shows how much investors are willing to pay per dollar of earnings.").append(eol);
        sb.append("wilson audio watt puppy").append(eol);
        sb.append("washington nationals baseball").append(eol);
        sb.append("The price-earnings ratio (P/E ratio) is the ratio for valuing a company that measures ");
        sb.append("its current share price relative to its per share earnings. ");

        k3 = new FinanceKnowledge();
        k3.setBody(sb.toString());

        k3.setTitle("knowledge 3");
        k3.setTag("Price-Earnings Ratio");
        k3.setId(DigestUtils.md5Hex(k3.getBody()).toUpperCase());
        k3.setLoc( "/doc/" + year + "/" + k3.getId() + ".txt" );
        k3.setUrl("/html/" + year + "/" + k3.getId() + ".html" );
        k3.setBody(sb.toString());
    }

    public void setupK4() {

        SimpleDateFormat formatter  = new SimpleDateFormat("yyyy");
        String           year       = formatter.format(new Date());

        StringBuilder sb = new StringBuilder();
        sb.append("In 2015, Bricasti Design already had a first-rate digital-to-analog converter.").append(eol);
        sb.append("I have auditioned half a dozen more DACs ranging from sub-$1,000 to over $20,000.").append(eol);
        sb.append("There are separate left and right boards to every section. ");

        k4 = new FinanceKnowledge();
        k4.setBody(sb.toString());

        k4.setTitle("knowledge 4 dagogo equipment review");
        k4.setTag("bricasti M21 DAC DSD");
        k4.setId(DigestUtils.md5Hex(k4.getBody()).toUpperCase());
        k4.setLoc( "/doc/" + year + "/" + k4.getId() + ".txt" );
        k4.setUrl("/html/" + year + "/" + k4.getId() + ".html" );
        k4.setBody(sb.toString());
    }


    public void setupK5() {

        SimpleDateFormat formatter  = new SimpleDateFormat("yyyy");
        String           year       = formatter.format(new Date());

        StringBuilder sb = new StringBuilder();
        sb.append("The technology of the GAT Preamplifier in the more cost effective ET5 enhance triode line-stage preamplifier.").append(eol);
        sb.append("The amplifier stage is connected to the preamplifier outputs through a high-current MOSFET buffer.").append(eol);
        sb.append("All control functions of the ET5 operate through microprocessor-controlled relays. ");

        k5 = new FinanceKnowledge();
        k5.setBody(sb.toString());

        k5.setTitle("knowledge 5 audiogon add");
        k5.setTag("Price-Earnings Ratio text in   tags");
        k5.setId(DigestUtils.md5Hex(k5.getBody()).toUpperCase());
        k5.setLoc( "/doc/" + year + "/" + k5.getId() + ".txt" );
        k5.setUrl("/html/" + year + "/" + k5.getId() + ".html" );
        k5.setBody(sb.toString());
    }


    public void setupK6() {

        SimpleDateFormat formatter  = new SimpleDateFormat("yyyy");
        String           year       = formatter.format(new Date());

        StringBuilder sb = new StringBuilder();
        sb.append("Pickleball is a racket or paddle sport in which two or four.").append(eol);
        sb.append("players hit a perforated, hollow plastic ball with paddles.").append(eol);
        sb.append("over a 34-inch-high net until one side is unable to return the. ").append(eol);
        sb.append("ball or commits an infraction.  ").append(eol);
        sb.append("text   in   context.  ");

        k6 = new FinanceKnowledge();
        k6.setBody(sb.toString());

        k6.setTitle("knowledge 6 senior sport");
        k6.setTag("old man sports");
        k6.setId(DigestUtils.md5Hex(k6.getBody()).toUpperCase());
        k6.setLoc( "/doc/" + year + "/" + k6.getId() + ".txt" );
        k6.setUrl("/html/" + year + "/" + k6.getId() + ".html" );
        k6.setBody(sb.toString());
    }

    public void printHit(Double score, FinanceKnowledge doc) {

        System.out.println();
        System.out.println("score: " + score);
        System.out.println("id   : " + doc.getId());
        System.out.println("title: " + doc.getTitle());
        System.out.println("tag  : " + doc.getTag());
        System.out.println("url  : " + doc.getUrl());
        System.out.println("loc  : " + doc.getLoc());
        System.out.println("body : " + doc.getBody());
        System.out.println();
    }


    @Test
    public void test00() throws IOException {

        //
        // Delete all documents before testing
        //
        List<FinanceKnowledge> recs = search.getAll();

        for ( FinanceKnowledge rec : recs) {
            search.deleteDocument( rec.getId() );
        }

    }

    @Test
    public void test01() throws IOException {

        //
        // Test indexing documents
        search.indexDocument(k1);
        search.indexDocument(k2);
        search.indexDocument(k3);
        search.indexDocument(k4);
        search.indexDocument(k5);
        search.indexDocument(k6);
    }

    @Test
    public void test02() throws IOException {

        //
        // Test getting a specific document
        FinanceKnowledge rec = search.getDocument(k1.getId());
        assertTrue(rec.getTitle().contains("knowledge 1"));

    }


    @Test
    public void test03() throws IOException {

        //
        // Test getting all documents from an index
        //
        List<FinanceKnowledge> recs = search.getAll();
        assertEquals(6, recs.size());

    }


    @Test
    public void test04() throws IOException {

        //
        // Test all words query
        //
        List<FinanceKnowledge> recs = search.allWordsQuery("audio wilson");

        assertTrue(recs.get(0).getTitle().contains("knowledge 1"));
        assertTrue(recs.get(1).getTitle().contains("knowledge 2"));
        assertTrue(recs.get(2).getTitle().contains("knowledge 3"));

    }


    @Test
    public void test05() throws IOException {

        //
        // Test some words query
        //
        List<FinanceKnowledge> recs = search.wordsQuery("WAShington natIONALS BASEBALL");

        assertTrue(recs.get(0).getTitle().contains("knowledge 3"));
        assertTrue(recs.get(1).getTitle().contains("knowledge 2"));
        assertTrue(recs.get(2).getTitle().contains("knowledge 1"));

    }


    @Test
    public void test06() throws IOException {

        //
        // Test all words query in tittle
        //
        List<FinanceKnowledge> recs = search.allWordsQuery("dagogo equipment review");

        assertEquals(1, recs.size());
        assertTrue(recs.get(0).getTitle().contains("knowledge 4"));

    }


    @Test
    public void test07() throws IOException {

        //
        // Test all words query in tags
        //
        List<FinanceKnowledge> recs = search.allWordsQuery("text in tags");

        assertEquals(1, recs.size());
        assertTrue(recs.get(0).getTitle().contains("knowledge 5"));

    }


    @Test
    public void test08() throws IOException {

        //
        // Test all words query in body
        //
        List<FinanceKnowledge> recs = search.allWordsQuery("text in context");

        assertEquals(1, recs.size());
        assertTrue(recs.get(0).getTitle().contains("knowledge 6"));

    }


    @Test
    public void test09() throws IOException {

        //
        // Test all phrase query slop 1
        //
        List<FinanceKnowledge> recs = search.phraseQuery("connected to the preamplifier", 0);

        assertEquals(1, recs.size());
        assertTrue(recs.get(0).getTitle().contains("knowledge 5"));

    }



    @Test
    public void test10() throws IOException {

        //
        // Test all phrase query slop 1
        //
        List<FinanceKnowledge> recs = search.phraseQuery("connected preamplifier", 2);

        assertEquals(1, recs.size());
        assertTrue(recs.get(0).getTitle().contains("knowledge 5"));

    }

}

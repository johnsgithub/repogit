package com.jpw.raptor.cmdline.reports;


import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.jpw.raptor.cmdline.reports.track.TrackReport;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.jdbc.track.TrackDAO;

import com.jpw.raptor.model.Stock;
import com.jpw.raptor.model.TrackRec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class ReportsApplication implements CommandLineRunner {

    @Autowired
    public QuoteDAO quoteTbl;

    @Autowired
    public TrackDAO trackTbl;

    public void trackIngest() throws java.io.IOException {
        
       
    }


    // Main loop
    @Override
    public void run(String... args) throws Exception {

        System.out.println("***************  Hello  *******************");
        System.out.println();

        TrackReport tr = new TrackReport();
        SimpleDateFormat format  = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse("2020-12-24");
        tr.runReport(quoteTbl, trackTbl, "", date);

        /*
        // define the run time parameters
        AppParameters params = new AppParameters();

        // create parameter parser
        JCommander cmd = new JCommander(params);

        try {
            cmd.parse(args);

            //trackIngest();

            TrackReport tr = new TrackReport();
            tr.runReport();
                
        } catch (ParameterException ex) {
            System.out.println(ex.getMessage());
            cmd.usage();
        }
*/
        System.out.println("***************  Good bye  *******************");
        System.out.println();

    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(ReportsApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit( app.run(args) ) );
    }

}

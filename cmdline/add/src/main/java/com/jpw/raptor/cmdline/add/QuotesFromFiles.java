package com.jpw.raptor.cmdline.add;

import com.jpw.raptor.model.Quote;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class QuotesFromFiles {

    private static final String[] NASDAQ_DIRS = {
            "/home/finance/etf/data/nasdaq/2021",
            "/home/finance/etf/data/nasdaq/2022",
            "/home/finance/etf/data/nasdaq/2023",
            "/home/finance/etf/data/nasdaq/2024"
    };

    private static final String[] AMEX_DIRS = {
            "/home/finance/etf/data/amex/2021",
            "/home/finance/etf/data/amex/2022",
            "/home/finance/etf/data/amex/2023",
            "/home/finance/etf/data/amex/2024"
    };

    private static final String[] NYSE_DIRS = {
            "/home/finance/nyse/2021",
            "/home/finance/nyse/2022",
            "/home/finance/nyse/2023",
            "/home/finance/nyse/2024"
    };

    private static final String[] INDEX_DIRS = {
            "/home/finance/indexs/data/2021",
            "/home/finance/indexs/data/2022",
            "/home/finance/indexs/data/2023",
            "/home/finance/indexs/data/2024"
    };

    public List<Quote> getQuotes(String exchange, String symbol) throws IOException {

        if ( exchange.equalsIgnoreCase("nasdaq") )
            return getQuotesFromDirs(NASDAQ_DIRS, symbol);
        else if ( exchange.equalsIgnoreCase("amex") )
            return getQuotesFromDirs(AMEX_DIRS, symbol);
        else if ( exchange.equalsIgnoreCase("nyse") )
            return getQuotesFromDirs(NYSE_DIRS, symbol);
        else if ( exchange.equalsIgnoreCase("index") )
            return getQuotesFromDirs(INDEX_DIRS, symbol);
        else
            return Collections.emptyList();

    }

    public List<Quote> getQuotesFromDirs(String[] dirs, String symbol) throws IOException {

        // List to contain results
        List<Quote> quotes = new ArrayList<>();

        // for each directory
        for ( String dir : dirs ) {
            // get files in directory
            List<String> files = getFiles(dir);
            // get quotes from files
            List<Quote> quotesInDir = getRecords (files, symbol);
            // save results
            quotes.addAll(quotesInDir);
        }

        return quotes;
    }


    public List<String> getFiles(String dir) {

        // create a file that is really a directory
        File aDirectory = new File(dir);

        // get a listing of all files in the directory
        String[] filesInDir = aDirectory.list();

        // Create a map where the key is date and value is the full path
        Map<String, String> unsortedMap = new HashMap<>();
        for ( String f : filesInDir ) {
            int dash        = f.indexOf('_');
            int period      = f.indexOf('.');
            // date is f.substring(dash+1, period)
            // index is f.substring(0, dash)
            String key      = f.substring(dash+1, period);
            String fileName = dir + "/" + f;
            File   dataFile = new File(fileName);
            if ( dataFile.isFile() ) {
                unsortedMap.put(key, fileName);
            }
        }

        // Create a sorted map
        Map<String, String> treeMap = new TreeMap<>(unsortedMap);

        // Create list and add files
        List<String> list = new ArrayList<>(treeMap.size());
        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            list.add(entry.getValue());
        }

        return list;
    }


    public List<Quote> getRecords(List<String> files, String symbol) throws IOException {

        // Create list for quotes
        List<Quote> quotes = new ArrayList<>(files.size());

        // Used to read Eod data file
        BufferedReader br      = null;

        // Used to parse the EOD data file
        EodParser parser = new EodParser();

        // Find quote in each file
        for ( String fileName : files ) {
            br         = new BufferedReader(new FileReader(fileName));
            String val = null;
            while ((val = br.readLine()) != null) {
                Quote quote = parser.parseCsvLine(val);

                if ( quote != null && quote.getSymbol().equalsIgnoreCase(symbol) ) {
                    quotes.add(quote);
                    break;
                }
            }
        }

        return quotes;
    }
}

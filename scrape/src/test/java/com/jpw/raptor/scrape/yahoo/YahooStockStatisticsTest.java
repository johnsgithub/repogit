package com.jpw.raptor.scrape.yahoo;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class YahooStockStatisticsTest {
    @Test
    //
    // Risk Stats
    public void testit() throws IOException {

        System.out.println("test it started");


        //
        // Java script parse test





        System.out.println("  parse java script ");
        InputStream jsis = getClass().getClassLoader().getResourceAsStream("stock-js.txt");
        String jsPage     = new String(jsis.readAllBytes(), StandardCharsets.UTF_8);
        YahooStockStatParseJS   js =  new YahooStockStatParseJS();
        YahooStockStatistics jsss = new YahooStockStatistics();

        // Valuation Measures
        js.valuationMeasuresTable(jsPage, jsss);

        // Stock Price History
        js.stockPriceHistoryTable(jsPage, jsss);

        // Share Statistics
        js.shareStatisticsTable(jsPage, jsss);

        // Dividend and Splits
        js.dividendAndSplitsTable(jsPage, jsss);

        // Financial Highlights

        // Profitability
        js.profitabilityTable(jsPage, jsss);

        // Management Effectiveness
        js.managementEffectivenessTable(jsPage, jsss);

        // Income Statement
        js.incomeStatementTable(jsPage, jsss);

        // Balance Sheet
        js.balanceSheetTable(jsPage, jsss);

        // Cash Flow
        js.cashFlowTable(jsPage, jsss);


        jsss.print();

        System.out.println();
        System.out.println();

        InputStream nojsis = getClass().getClassLoader().getResourceAsStream("stock-nojs.txt");
        String nojsPage     = new String(nojsis.readAllBytes(), StandardCharsets.UTF_8);
        YahooStockStatParseNoJS nojs = new YahooStockStatParseNoJS();

        YahooStockStatistics nojsss = new YahooStockStatistics();

        // Valuation Measures
        nojs.valuationMeasuresTable(nojsPage, nojsss);

        // Stock Price History
        nojs.stockPriceHistoryTable(nojsPage, nojsss);

        // Share Statistics
        nojs.shareStatisticsTable(nojsPage, nojsss);

        // Dividend and Splits
        nojs.dividendAndSplitsTable(nojsPage, nojsss);

        // Financial Highlights

        // Profitability
        nojs.profitabilityTable(nojsPage, nojsss);

        // Management Effectiveness
        nojs.managementEffectivenessTable(nojsPage, nojsss);

        // Income Statement
        nojs.incomeStatementTable(nojsPage, nojsss);

        // Balance Sheet
        nojs.balanceSheetTable(nojsPage, nojsss);

        // Cash Flow
        nojs.cashFlowTable(nojsPage, nojsss);


        nojsss.print();
    }

    @Test
    public void testit02() throws IOException {

        System.out.println("test 02 started");

        YahooStockStatParse  parser = new YahooStockStatParse();

        // Read the yahoo stock page
        YahooStockStatistics yss = parser.readPage("AA");

        yss.print();
        System.out.println("test 02 ended");

        //
    }

}

package com.jpw.raptor.cmdline.scrapeweb;


import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.google.gson.Gson;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.fund.FundDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.treasury.TreasuryDAO;

import com.jpw.raptor.model.EquityToScrape;
import com.jpw.raptor.model.Index;
import com.jpw.raptor.jdbc.index.IndexDAO;
import com.jpw.raptor.model.Quote;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import jakarta.annotation.Resource;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class ScrapeWebApplication implements CommandLineRunner {

    @Autowired
    private ConfigurableApplicationContext context;

    static final String pattern = "HH:mm:ss:SSS";

    @Resource
    ScrapeWorker worker;

    @Autowired
    public EtfDAO etfTbl;

    @Autowired
    public FundDAO fundTbl;

    @Autowired
    public TreasuryDAO treasuryTbl;

    //Logger log = LoggerFactory.getLogger(this.getClass().getName());

    protected BlockingQueue<EquityToScrape>   queue;
    protected BlockingQueue<EquityToScrape>   errors;

    public void readInputFile() {

        BufferedReader reader;

        Gson gson = new Gson();

        try {
            reader = new BufferedReader(new FileReader("/home/finance/runtime/files/scrape/scrape-symbols.txt"));
            String line;

            while ((line = reader.readLine()) != null) {
                try {
                    queue.put(gson.fromJson(line, EquityToScrape.class));
                } catch ( InterruptedException ex ) {
                    System.out.println("InterruptedException in queueing message: " + ex.getMessage());
                }
                // read next line
                System.out.println("Read : " + line + " :");
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeOutputFile() {

        Gson gson = new Gson();

        try (FileWriter writer = new FileWriter("/home/finance/runtime/files/scrape/scrape-errors.txt");
             BufferedWriter bw = new BufferedWriter(writer)) {

            boolean dataFound = true;

            while ( dataFound ) {
                EquityToScrape equity = errors.poll(1l, TimeUnit.SECONDS);

                if (equity != null) {
                    bw.write( gson.toJson(equity) );
                    bw.newLine();
                } else {
                    dataFound = false;
                }
            }

            bw.flush();

        } catch (IOException | InterruptedException e) {
            System.err.format("Exception: %s%n", e);
        }

    }


    // Main loop
    @Override
    public void run(String... args) throws Exception {

        System.out.println();
        System.out.println("***************  Hello  *******************");

        Gson gson = new Gson();
        queue     = new LinkedBlockingQueue<>();
        errors    = new LinkedBlockingQueue<>();

        // Sleep to let things shake out
        TimeUnit.SECONDS.sleep(1);

        // Read the symbols
        readInputFile();

        int equitiesToProcess = queue.size();
        System.out.println("Equities queued for processing " + equitiesToProcess);

        long startTime = System.currentTimeMillis();

        // Start the threads
        Future<String> process1 = worker.process(queue, errors);
        Future<String> process2 = worker.process(queue, errors);
        Future<String> process3 = worker.process(queue, errors);

        // Wait until all threads are finish
        // Wait until They are all Done
        // If all are not Done. Pause 2s for next re-check
        while(!(process1.isDone() && process2.isDone() && process3.isDone())){
            Thread.sleep(2000);
        }

        //log.info("All Processes are DONE!");
        // Log results
        //log.info("Process 1: " + process1.get());
        //log.info("Process 2: " + process2.get());
        //log.info("Process 3: " + process3.get());

        writeOutputFile();

        long endTime = System.currentTimeMillis();
        System.out.println("Processed " + equitiesToProcess + " equities in " + (endTime - startTime) + " milliseconds" );

       
        System.out.println("***************  Good bye  *******************");
        System.out.println();

        System.exit(SpringApplication.exit(context));
    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(ScrapeWebApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);

    }

}

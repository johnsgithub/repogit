package com.jpw.raptor.scrape.yahoo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class YahooEtfHoldingsParse {


    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public YahooEtfHoldings readPage(String symbol) throws IOException {

        String                    page;
        YahooEtfHoldings          results    = new YahooEtfHoldings();
        YahooEtfHoldingsParseJS   parserJS   = new YahooEtfHoldingsParseJS();
        YahooEtfHoldingsParseNoJS parserNoJS = new YahooEtfHoldingsParseNoJS();

        // Yahoo replaces . with -
        String ticker = symbol.toUpperCase().replace('.', '-');
        String baseUrl = "https://finance.yahoo.com/quote/" + ticker + "/holdings?p=" + ticker;
        try {
            Document doc = Jsoup.connect(baseUrl).get();
            page = doc.toString();
        } catch ( org.jsoup.HttpStatusException ex ) {
            System.out.println("### " + symbol + " ETF holdings HttpStatusException **** ");
            System.out.println(ex.toString());
            results.setReadFailed(true);
            return results;
        }

        // Validate page was found
        if (page.contains("<span>No results for")  || page.contains("<span>Symbols similar to")) {
            System.out.println("### " + symbol + " ETF holdings Symbol not found ");
            results.setReadFailed(true);
            return results;
        }

        if ( page.contains("NoJs ") ) {
            try {
                //System.out.println(" No Java Script " );
                parserNoJS.parse(page, results);
            } catch ( Exception ex ) {
                results.setReadFailed(true);
                System.out.println("### " + symbol + " **** Etf Holdings No Java Script parse failed ***** " );
                System.out.println(ex.toString());
                BufferedWriter writer = new BufferedWriter(new FileWriter(
                        "/home/finance/runtime/files/scrape/page-error-etf-holdings-no-js-" + symbol + ".txt"));
                writer.write(page);
            }
        } else {
            try {
                //System.out.println(" Java Script " );
                parserJS.parse(page, results);
            } catch ( Exception ex ) {
                results.setReadFailed(true);
                System.out.println("### " + symbol + "  **** Etf Holdings Java Script parse failed ***** " );
                System.out.println(ex.toString());
                BufferedWriter writer = new BufferedWriter(new FileWriter(
                        "/home/finance/runtime/files/scrape/page-error-etf-holdings-js=" + symbol + ".txt"));
                writer.write(page);
            }
        }

        return results;
    }

}

package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.cmdline.web.service.SummaryDataService;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.model.Etf;
import com.jpw.raptor.model.SummaryModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;


/**
 * Created by john on 4/8/18.
 */
@Controller
public class EtfSummaryController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    EtfDAO etfTbl;

    @Autowired
    SummaryDataService summaryDataService;

    protected static final  String RECORDS = "summ_model";
    protected static final  String PAGE_TITLE = "pagetitle";
    protected static final  String ENCODING = "UTF-8";
    protected static final  String SUMMARY_PAGE = "etf-summary";
    protected static final  String SUMMARY_FACTOR_PAGE = "etf-summary-factor";

    @RequestMapping("/etf-summary-all")
    public String etfSummaryAllReq (
            Model model ) {

        //logger.debug("etfSummaryAllReq {}", "");

        // Get data to summarize
        List<Etf> etfRecs = etfTbl.getAll();

        // Generate summary data
        List<SummaryModel> recs = summaryDataService.etfType(etfRecs);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, " ALL ETFS SUMMARY");

        return SUMMARY_PAGE;
    }


    @RequestMapping("/etf-summary-type")
    public String etfSummaryByTypeReq (
            @RequestParam(value="class", required=true) String assetClass,
            Model model ) {

        String assetClassClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("etfSummaryClassReq {}", assetClassClean);

        // Get data to summarize
        List<Etf> etfRecs = etfTbl.getByAssetClass(assetClassClean);

        // Invoke service to format data
        List<SummaryModel> recs = summaryDataService.etfType(etfRecs);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, assetClassClean.toUpperCase() + " - SUMMARY");

        return SUMMARY_PAGE;
    }


    @RequestMapping("/etf-summary-subtype")
    public String etfSummaryBySubtypeReq (
            @RequestParam(value="class", required=true) String assetClass,
            Model model ) {

        String assetClassClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("etfSummaryClassReq {}", assetClassClean);

        // Get data to summarize
        List<Etf> etfRecs = etfTbl.getByAssetClass(assetClassClean);

        // Invoke service to format data
        List<SummaryModel> recs = summaryDataService.etfSubType(etfRecs);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, assetClassClean.toUpperCase() + " - SUMMARY");

        return SUMMARY_PAGE;
    }

    @RequestMapping("/etf-summary-factor")
    public String etfSummaryFactorReq (
            @RequestParam(value="class", required=true) String assetClass,
            Model model ) {

        String assetClassClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("SummaryFactorReq {}", assetClassClean);

        // Get data to summarize
        List<Etf> etfRecs = etfTbl.getFactorsByAssetClass(assetClassClean);

        // Invoke service to format data
        List<SummaryModel> recs = summaryDataService.etfFactor(etfRecs);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, assetClassClean.toUpperCase() + " - FACTOR SUMMARY");

        return SUMMARY_FACTOR_PAGE;
    }

}

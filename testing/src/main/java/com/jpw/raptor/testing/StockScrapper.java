package com.jpw.raptor.testing;

import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.fund.FundDAO;
import com.jpw.raptor.jdbc.index.IndexDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.model.Etf;
import com.jpw.raptor.model.Stock;
import com.jpw.raptor.scrape.yahoo.YahooEtfScrape;
import com.jpw.raptor.scrape.yahoo.YahooStockScrape;


import java.io.IOException;
import java.util.HashMap;

/**
 * Created by john on 11/6/18.
 */
public class StockScrapper {

    public void doit(StockDAO stockTbl, EtfDAO etfTbl) throws IOException, InterruptedException{

        stockDoit(stockTbl, "IBM");

        etfDoit(etfTbl, "IVV");
        
    }

    public void stockDoit(StockDAO stockTbl, String symbol) throws IOException {

        /*

        https://finance.yahoo.com/quote/IBM/key-statistics?p=IBM
        https://finance.yahoo.com/quote/IBM/profile?p=IBM

         */

        // Read record from database
        Stock stockRecord = stockTbl.get(symbol);

        // Scrape data
        YahooStockScrape scrapper = new YahooStockScrape();
        Stock updatedStockRecord = scrapper.scrape(stockRecord);

        // Validate the scrape
        if ( updatedStockRecord == null ) {
            // Error occurred requeue the equity
            //log.info("### page read fail " + equity.getSymbol() + " " + threadName );
            System.out.println("### page read fail " + symbol );

        } else {
            // Update the database

        }

    }
    public void etfDoit(EtfDAO etfTbl, String symbol) throws IOException, InterruptedException {

        /*

        https://finance.yahoo.com/quote/IVV/
        https://finance.yahoo.com/quote/IVV/risk?p=IVV
        https://finance.yahoo.com/quote/IVV/holdings?p=IVV

         */

        // Read record from database
        Etf etfRecord = etfTbl.get(symbol);

        // Scrape data
        YahooEtfScrape scrapper = new YahooEtfScrape();
        Etf updatedEtfRecord = scrapper.scrape(etfRecord);

        // Validate the scrape
        if ( updatedEtfRecord == null ) {
            // Error occurred requeue the equity
            //log.info("### page read fail " + equity.getSymbol() + " " + threadName );
            System.out.println("### page read fail " + symbol );

        } else {
            // Update the database

        }

    }

}

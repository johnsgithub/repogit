package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.algorithm.TechnicalSummaryFactory;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.fund.FundDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.lib.properties.FinanceProperties;
import com.jpw.raptor.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

@Controller
public class EquitySignalController {


    @Autowired
    public QuoteDAO quoteTbl;

    @Autowired
    EtfDAO   etfTbl;

    @Autowired
    FundDAO  fundTbl;

    @Autowired
    StockDAO stockTbl;

    protected static final String REF_SYMBOL = "SPY";
    protected static final String RECORDS = "tech_model";
    protected static final String PAGE_TITLE = "pagetitle";
    protected static final String ENCODING = "UTF-8";
    protected static final String SIGNAL_PAGE = "signals";
    protected static final String NOT_FOUND = "notfound";
    protected static final String DEFAULT_DAYS = "220";

    @RequestMapping("/equity-signal-by-symbol")
    public String equitySignalBySymbol(
            @RequestParam(value="file", required=true) String fileName,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String fileNameClean  = null;

        try {
            fileNameClean  = URLDecoder.decode(fileName,  ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        String lineSeparator = System.lineSeparator();

        //
        // Read symbols from file
        FinanceProperties fp       = new FinanceProperties();
        Properties        prop     = fp.get();
        String            filePath = prop.getProperty("file_dir") + "/" +fileNameClean;

        List<String> lines = Collections.emptyList();
        try {
            // Get symbols from file
            lines = Files.readAllLines(Paths.get(filePath));
        } catch (IOException ignored) {}

        // Convert lines read to usable symbols
        ArrayList<String> symbols = new ArrayList<>(lines.size());
        for ( String line : lines ) {
            symbols.add( line.replace(lineSeparator, "").trim().toUpperCase() );
        }

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(symbols.size());

        for ( String s : symbols ) {
            String name   = NOT_FOUND;

            // Check for etf
            Etf etf = etfTbl.get(s);
            if (etf != null) {
                name   = etf.getName();
            } else {
                // check for a stock
                Stock stock = stockTbl.get(s);
                if (stock != null) {
                    name   = stock.getName();
                } else {
                    // check for a mutual fund
                    Fund fund = fundTbl.get(s);
                    if (fund != null) {
                        name   = fund.getName();
                    }
                }
            }

            if ( !name.equalsIgnoreCase(NOT_FOUND) ) {
                List<Quote> equityQuotes = quoteTbl.getAllDesc(s, days);
                signals.add(summaryService.generate(refQuotes, equityQuotes, s.toUpperCase(), name));
            }
        }

        model.addAttribute(RECORDS, signals);
        if ( days < 200 ) {
            model.addAttribute(PAGE_TITLE, fileNameClean + " Short");
        } else {
            model.addAttribute(PAGE_TITLE, fileNameClean + " Long");
        }

        return SIGNAL_PAGE;
    }

}

package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.cmdline.web.service.SummaryDataService;
import com.jpw.raptor.jdbc.stock.StockDAO;

import com.jpw.raptor.model.Stock;
import com.jpw.raptor.model.StockSummaryModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * Created by john on 11/14/18.
 */
@Controller
public class StockSummaryController {

    @Autowired
    public StockDAO stockTbl;

    @Autowired
    SummaryDataService summaryDataService;

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    protected static final  String RECORDS = "summ_model";
    protected static final  String PAGE_TITLE = "pagetitle";
    protected static final  String SUMMARY_PAGE = "stock-summary";


    @RequestMapping("/stock-summary-sector")
    public String stockSectorSummary ( Model model ) {

        //logger.debug("stockSectorSummary ");

        // Get all stocks
        List<Stock> allStocks = stockTbl.getAll();

        // Invoke service to format data
        List<StockSummaryModel> recs = summaryDataService.stockSectorSummary(allStocks);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, " SECTOR - SUMMARY");

        return SUMMARY_PAGE;
    }


    @RequestMapping("/stock-summary-industry")
    public String stockIndustrySummary ( Model model ) {

        //logger.debug("stockIndustrySummary ");

        // Get all stocks
        List<Stock> allStocks = stockTbl.getAll();

        // Invoke service to format data
        List<StockSummaryModel> recs = summaryDataService.stockIndustrySummary(allStocks);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, " INDUSTRY - SUMMARY");

        return SUMMARY_PAGE;
    }

}

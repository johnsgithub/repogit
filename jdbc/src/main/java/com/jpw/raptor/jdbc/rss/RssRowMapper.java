package com.jpw.raptor.jdbc.rss;

import com.jpw.raptor.model.Event;

import com.jpw.raptor.model.Rss;
import com.jpw.raptor.model.RssEntry;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by john on 5/13/18.
 */
public class RssRowMapper implements RowMapper<RssEntry>  {

    //private static final Logger logger = LoggerFactory.getLogger(RssRowMapper.class);

    @Override
    public RssEntry mapRow(ResultSet rs, int rowNum) throws SQLException {

        RssEntry rec = new RssEntry();
        rec.setHash(rs.getString("hash"));
        rec.setDatePosted (rs.getDate("date_posted"));
        rec.setTitle(rs.getString("title"));
        rec.setUrl(rs.getString("url"));
        rec.setUri(rs.getString("uri"));
        return rec;
    }


}

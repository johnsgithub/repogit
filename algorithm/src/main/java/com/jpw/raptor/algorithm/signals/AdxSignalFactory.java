package com.jpw.raptor.algorithm.signals;

import com.jpw.raptor.model.Adx;
import com.jpw.raptor.model.AdxSignal;
import com.jpw.raptor.model.Stochastic;
import com.jpw.raptor.model.StochasticSignal;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by john on 6/20/18.
 */
public class AdxSignalFactory {

    public AdxSignalFactory() {}

    // summarize the trend based upon Average Directional index values
    public String adxTrend(Adx obj) {
        String trend = "";
        if (obj.getAverageDx() > 25.0) {
            trend = "Trending";
        } else if (obj.getAverageDx() < 20.0) {
            trend = "Not Trending";
        } else {
            trend = "unknown";
        }

        return trend;
    }

    // summarize the trend based upon Average true range value
    public String atrTrend (Adx obj) {

        String trend = "";
        if ( obj.getAverageTrueRange() > obj.getAverageTrueRange20() ) {
            trend = "trending";
        } else {
            trend = "range bound";
        }

        return trend;
    }

    public String adxSignal(Adx curr, Adx prior ) {

        String    signal = "";

        // Positive crossover
        if ( curr.getAverageDx() >= 25 ) {

            // Strong trend
            if ( curr.getDiPlus() >= curr.getDiMinus() ) {
                // diPlus is above diMinus
                if ( prior.getDiPlus() >= prior.getDiMinus() ) {
                    signal = "Strong trend"; //"Strong trend up "
                } else {
                    // Positive Cross over
                    signal = "Strong trend"; //"Strong trend crossing up "
                }
            } else {
                // diPlus is below diMinus
                if ( prior.getDiMinus() >= prior.getDiPlus() ) {
                    signal = "Strong trend"; //"Strong trend down "
                } else {
                    // Negative Cross over
                    signal = "Strong trend"; //"Strong trend crossing down "
                }
            }
        } else if ( curr.getAverageDx() >= 20 ) {

            // unknown trend
            if ( curr.getDiPlus() >= curr.getDiMinus() ) {
                // diPlus is above diMinus
                if ( prior.getDiPlus() >= prior.getDiMinus() ) {
                    signal = "Unknown trend"; //"Unknown trend up " + curr.getAverageDx()
                } else {
                    // Positive Cross over
                    signal = "Unknown trend"; //"Unknown trend crossing up " + curr.getAverageDx()
                }
            } else {
                // diPlus is below diMinus
                if ( prior.getDiMinus() >= prior.getDiPlus() ) {
                    signal = "Unknown trend"; //"Unknown trend down " + curr.getAverageDx()
                } else {
                    // Negative Cross over
                    signal = "Unknown trend"; //"Unknown trend crossing down " + curr.getAverageDx()
                }
            }
        } else {

            // no trend
            if ( curr.getDiPlus() >= curr.getDiMinus() ) {
                // diPlus is above diMinus
                if ( prior.getDiPlus() >= prior.getDiMinus() ) {
                    signal = "No trend"; //"No trend up " + curr.getAverageDx()
                } else {
                    // Positive Cross over
                    signal = "No trend"; //"No trend crossing up " + curr.getAverageDx()
                }
            } else {
                // diPlus is below diMinus
                if ( prior.getDiMinus() >= prior.getDiPlus() ) {
                    signal = "No trend"; //"No trend down " + curr.getAverageDx()
                } else {
                    // Negative Cross over
                    signal = "No trend";
                }
            }
        }

        return signal;
    }
}

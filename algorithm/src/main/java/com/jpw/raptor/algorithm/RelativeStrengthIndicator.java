package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Quote;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Ported to new design 5/18/2019
 *
 * The relative strength index (RSI) is a momentum indicator
 * displaying how strongly price is moving up or down
 *
 * RSI values above 70 to define strong bullish moves and RSI values
 * below 30 to define strong bearish moves.
 *
 * Values above 50 indicate average gains are greater than average losses while
 * values below 50 indicate average losses are greater than average gains
 *
 * values between 60 and 40 indicate range bound
 */

public class RelativeStrengthIndicator {

    //
    // Data is in descending order by date
    public double quote (List<Quote> data, int offset, int period) {

        // validate data
        if ( (offset + period + 1) > data.size() )
            return 0.0;

        int    end      = offset + period;

        //
        // compute gains and losses
        double gain     = 0.0;
        double loss     = 0.0;
        for ( int i=offset; i<end; i++ ) {
            double change = data.get(i).getClose() - data.get(i+1).getClose();

            if ( change > 0.0 ) {
                gain += change;
            } else {
                loss += Math.abs(change);
            }
        }

        //
        // compute average gain and loss
        double avgGain  = Math.round( (gain / (double) period ) * 100.0 ) / 100.0;
        double avgLoss  = Math.round( (loss / (double) period ) * 100.0 ) / 100.0;

        //
        // Compute relative strength
        double rs = 0.0;
        if ( loss > 0.0 ) {
            rs = Math.round( (avgGain / avgLoss) * 100.0 ) / 100.0 ;
        }

        //
        // Compute relative strength index
        double rsi = Math.round( (100.0 - (100.0 / (1.0 + rs)) ) * 100.0 ) / 100.0;

        return rsi;
    }

}

package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 3/2/18.
 */
public class SimpleMovingAverage {

    //
    // Compute number of elements to use
    // If enough data is provided then return period
    // If not enough data is provide then return 0
    //
    public int elementCount(int offset, int period, int arraySize) {
        int number = offset + period + 1;
        if ( number > arraySize )
            return arraySize - offset;
        else
            return period;
    }

    // The moving average includes the current day
    // data is in descending order by date
    public long adl(List<Adl> data, int offset, int period) {

        int   count    = elementCount( offset,  period, data.size());
        int   end      = offset + count;
        long  result   = 0;

        // The moving average includes the current day
        // Note data is in descending order by date

        // loop through array and sum the closes
        for ( int i=offset; i<end; i++ ) {
            result += data.get(i).getValue();
        }

        // Compute the moving average
        result = result / count;

        // return it
        return result;
    }


    // Data is in descending order by date
    public double asbury(List<AsburyListModel> data, int offset, int period) {

        int     count    = elementCount( offset,  period, data.size());
        int     end      = offset + count;
        double  result   = 0;

        // The moving average includes the current day
        // Note data is in descending order by date

        // loop through array and sum the closes
        for ( int i=offset; i<end; i++ ) {
            result += data.get(i).getValue();
        }

        // Compute the moving average
        result = result / (double) count;

        // Round to 2 decimal places
        return Math.round(result * 100.0) / 100.0;
    }


    // Data is in descending order by date
    public double smaKeltnerAvgTrueRange(List<Keltner> data, int offset, int period) {

        int     count    = elementCount( offset,  period, data.size());
        int     end      = offset + count;
        double  result   = 0;

        // The moving average includes the current day
        // Note data is in descending order by date

        // loop through array and sum the closes
        for ( int i=offset; i<end; i++ ) {
            result += data.get(i).getTrueRange();
        }

        // Compute the moving average
        result = result / (double) count;

        // Round to 2 decimal places
        return Math.round(result * 100.0) / 100.0;
    }


    // Data is in descending order by date
    public double dSlow(List<Stochastic> data, int offset, int period) {

        int     count    = elementCount( offset,  period, data.size());
        int     end      = offset + count;
        double  result   = 0;

        // The moving average includes the current day
        // Note data is in descending order by date

        // loop through array and sum the closes
        for ( int i=offset; i<end; i++ ) {
            result += data.get(i).getKSlow();
        }

        // Compute the moving average
        result = result / (double) count;

        // Round to 2 decimal places
        return Math.round(result * 100.0) / 100.0;
    }


    // Data is in descending order by date
    public double dFast(List<Stochastic> data, int offset, int period) {

        int     count    = elementCount( offset,  period, data.size());
        int     end      = offset + count;
        double  result   = 0;

        // The moving average includes the current day
        // Note data is in descending order by date

        // loop through array and sum the closes
        for ( int i=offset; i<end; i++ ) {
            result += data.get(i).getKFast();
        }

        // Compute the moving average
        result = result / (double) count;

        // Round to 2 decimal places
        return Math.round(result * 100.0) / 100.0;
    }


    // Quotes are in descending order by date
    public long obv(List<Obv> data, int offset, int period) {

        int   count    = elementCount( offset,  period, data.size());
        int   end      = offset + count;
        long  result   = 0;

        // The moving average includes the current day
        // Note data is in descending order by date

        // loop through array and sum the closes
        for ( int i=offset; i<end; i++ ) {
            result += data.get(i).getValue();
        }

        // Compute the moving average
        result = result / count;

        // return it
        return result;
    }


    // Quotes are in descending order by date
    public double quote(List<Quote> data, int offset, int period) {

        int     count    = elementCount( offset,  period, data.size());
        int     end      = offset + count;
        double  result   = 0;

        // The moving average includes the current day
        // Note data is in descending order by date

        // loop through array and sum the closes
        for ( int i=offset; i<end; i++ ) {
            result += data.get(i).getClose();
        }

        // Compute the moving average
        result = result / (double) count;

        // Round to 2 decimal places
        return Math.round(result * 100.0) / 100.0;
    }


    // Quotes are in descending order by date
    public double rp(List<Rp> data, int offset, int period) {

        int     count    = elementCount( offset,  period, data.size());
        int     end      = offset + count;
        double  result   = 0;

        // The moving average includes the current day
        // Note data is in descending order by date

        // loop through array and sum the closes
        for ( int i=offset; i<end; i++ ) {
            result += data.get(i).getVal();
        }

        // Compute the moving average
        result = result / (double) count;

        // Round to 2 decimal places
        return Math.round(result * 1000.0) / 1000.0;
    }

    // Quotes are in descending order by date
    public double jdk(List<Jdk> data, int offset, int period) {

        int     count    = elementCount( offset,  period, data.size());
        int     end      = offset + count;
        double  result   = 0;

        // The moving average includes the current day
        // Note data is in descending order by date

        // loop through array and sum the closes
        for ( int i=offset; i<end; i++ ) {
            result += data.get(i).getRsRatio();
        }

        // Compute the moving average
        result = result / (double) count;

        // Round to 2 decimal places
        return Math.round(result * 100.0) / 100.0;
    }

    //
    // THis needs to be ported
    public List<AveragesModel> generateAverages(List<Quote> data) {

        // allocate space for averages object list
        List<AveragesModel> avgList = new ArrayList<>(data.size());

        // create averages for each quote
        for ( int i=0; i<data.size(); i++ ) {

            // Init Averages record
            AveragesModel avg = new AveragesModel();
            avg.setSymbol(data.get(i).getSymbol());
            avg.setDate(data.get(i).getDate());
            avg.setOpen(data.get(i).getOpen());
            avg.setHigh(data.get(i).getHigh());
            avg.setLow(data.get(i).getLow());
            avg.setClose(data.get(i).getClose());
            avg.setVolume(data.get(i).getVolume());

            // Generate Simple moving averages
            avg.setSimple5(quote(data, i, 5));
            avg.setSimple10(quote(data, i, 10));
            avg.setSimple20(quote(data, i, 20));
            avg.setSimple50(quote(data, i, 50));
            avg.setSimple100(quote(data, i, 100));
            avg.setSimple200(quote(data, i, 200));

            avgList.add(avg);
        }

        return avgList;
    }

    /*************** cut line *****************************/


    public double simpleMovingAverageDesc(List<Quote> data, int offset, int count) {

        int     start       = offset;
        int     end         = offset + count;
        int     arraySize   = data.size();
        double  divisor     = (double) count;
        double  result      = 0.0;

        // The moving average includes the current day
        // Note data is in descending order by date

        // count must be at least 1
        if ( count < 1 ) {
            System.out.println("Invalid moving average request " + count);
            return result;
        }

        // validate there is enough data to compute the moving average
        if ( end > arraySize ) {
            // adjust to compute moving average for provided data
            end     = arraySize;
            divisor = (double) (arraySize - offset);
            //System.out.println("Not enough data for moving average request " + data.get(0).getSymbol() + " " + count + " " + data.size());
            //return result;
        }


        // loop through array and sum the closes
        for ( int i=start; i<end; i++ ) {
            result += data.get(i).getClose();
        }

        // Compute the moving average
        result = result / divisor;

        // Round to 2 decimal places
        return Math.round(result * 100.0) / 100.0;
    }

}

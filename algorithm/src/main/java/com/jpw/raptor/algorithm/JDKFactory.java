package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
 * For JDK ratio the data point is equity / reference
 * ratio = 100 + (zscore +1)
 */
public class JDKFactory {

    // Quotes are in descending order by date
    // generate jdk ratio (zscore) based upon two equities relative performance ratio

    public List<Jdk> generate(List<Quote> refQuotes, List<Quote> equityQuotes) {

        double magic = 2.0;

        // generate relative performance for equity and reference
        RelativePerformance rpFactory = new RelativePerformance();
        List<Rp>            rpList    = rpFactory.computeRelativePerformance(refQuotes, equityQuotes);

        if ( rpList.size() == 0 ) return Collections.emptyList();

        // generate z scores ie normalize the relative performance values
        ZScoreFactory zScrFactory = new ZScoreFactory();
        List<ZScore> zScrList     = zScrFactory.rp(rpList, 0, rpList.size());

        // allocate space for object list and populate with jdk ratio
        List<Jdk> jdkList = new ArrayList<>(rpList.size());
        for ( int i=0; i<rpList.size(); i++ ) {
            double ratio = 100.0 + (zScrList.get(i).getScore() * magic);
            jdkList.add( new Jdk(rpList.get(i).getSymbol(), rpList.get(i).getDate(), ratio, 0.0) );
        }

        // generate simple moving average for rsRatio
        ExponentialMovingAverage ema = new ExponentialMovingAverage();
        //for ( int i=0; i<(jdkList.size() - 1); i++ ) {
        for ( int i=jdkList.size() - 2; i>=0; i-- ) {
            jdkList.get(i).setRsRatioMa( ema.getExpAvg(jdkList.get(i).getRsRatio(), jdkList.get(i+1).getRsRatioMa(), 12));
        }

        // Update the array with macd and generate momentum values
        MovingAvgConvergenceDivergence macdFactory = new MovingAvgConvergenceDivergence();
        List<Macd> macds = macdFactory.jdk(jdkList, 12, 26);

        if ( macds.size() == 0 ) return Collections.emptyList();

        for ( int i=0; i<jdkList.size(); i++ ) {
            jdkList.get(i).setRsMomentum( 100.0 + (macds.get(i).getMacd() * magic) );
            jdkList.get(i).setMacd( macds.get(i).getMacd() );
            jdkList.get(i).setSignal( macds.get(i).getSignal() );
            jdkList.get(i).setHistogram( macds.get(i).getHistogram() );
        }

        return jdkList;
    }
}

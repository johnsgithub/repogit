package com.jpw.raptor.cmdline.scrapeone;


import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.model.*;

import com.jpw.raptor.scrape.Scrapper;
import com.jpw.raptor.scrape.yahoo.YahooEtfScrape;
import com.jpw.raptor.scrape.yahoo.YahooStockProfile;
import com.jpw.raptor.scrape.yahoo.YahooStockProfileParse;
import com.jpw.raptor.scrape.yahoo.YahooStockScrape;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class ScrapeOneApplication implements CommandLineRunner {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    public EtfDAO etfTbl;

    @Autowired
    public StockDAO stockTbl;

    // Main loop
    @Override
    public void run(String... args) throws Exception {

        System.out.println();
        System.out.println("***************  Hello  *******************");

        // define the run time parameters
        AppParameters params = new AppParameters();

        // create parameter parser
        JCommander cmd = new JCommander(params);

        try {

            // parse the arguments
            cmd.parse(args);

            String symbol = params.getSymbol().toUpperCase();

            Stock stock = stockTbl.get(symbol);
            Etf   etf   = etfTbl.get(symbol);

            if ( stock == null && etf == null) {
                System.out.println( "Symbol not found    : " + params.getSymbol() );
            }

            Scrapper scrapper = new Scrapper();

            if ( stock != null ) {
                Stock updatedStockRecord = scrapper.scrapeStock(stock);

                // Validate the scrape
                if ( updatedStockRecord == null ) {
                    // Error occurred requeue the equity
                    //log.info("### page read fail " + equity.getSymbol() + " " + threadName );
                    System.out.println("### page read fail " + stock.getSymbol() );

                } else {
                    // Update the database
                    stockTbl.update(updatedStockRecord);
                    System.out.println( "Stock updated    : " + updatedStockRecord.getSymbol() );
                }

            }

            if ( etf != null ) {
                Etf updatedetfRecord = scrapper.scrapeEtf(etf);

                // Validate the scrape
                if ( updatedetfRecord == null ) {
                    // Error occurred requeue the equity
                    //log.info("### page read fail " + equity.getSymbol() + " " + threadName );
                    System.out.println("### page read fail " + etf.getSymbol() );

                } else {
                    // Update the database
                    etfTbl.update(updatedetfRecord);
                    System.out.println( "Stock updated    : " + updatedetfRecord.getSymbol() );
                }
            }

        } catch (ParameterException ex) {
            System.out.println(ex.getMessage());
            cmd.usage();
        }

        System.out.println("***************  Good bye  *******************");
        System.out.println();

    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(ScrapeOneApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit( app.run(args) ) );
    }

}

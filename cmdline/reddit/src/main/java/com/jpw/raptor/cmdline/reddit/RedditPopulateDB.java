package com.jpw.raptor.cmdline.reddit;

import com.jpw.raptor.jdbc.reddit.RedditPostDAO;
import com.jpw.raptor.jdbc.reddit.RedditSummDAO;
import com.jpw.raptor.model.RedditPost;
import com.jpw.raptor.model.RedditSumm;
import com.rometools.rome.io.FeedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class RedditPopulateDB {


    public void populateDb(RedditPostDAO postTbl, RedditSummDAO summTbl) throws IOException, FeedException, ParseException {

        List<RedditPost> posts;
        List<RedditPost> postsWithStocks;

        RedditRss        rssFeed        = new RedditRss();
        RedditNER        nerFeed        = new RedditNER();
        RedditSummary    summary        = new RedditSummary();
        RedditReport     report         = new RedditReport();
        RedditFile       file           = new RedditFile();


        String           stocksCommunity         = "https://www.reddit.com/r/stocks";
        String           optionsCommunity        = "https://www.reddit.com/r/options";
        String           stockMarketCommunity    = "https://www.reddit.com/r/StockMarket";
        String           robinHoodCommunity      = "https://www.reddit.com/r/RobinHood";
        String           financeCommunity        = "https://www.reddit.com/r/finance";
        String           investingCommunity      = "https://www.reddit.com/r/investing";
        String           wallStreetBetsCommunity = "https://www.reddit.com/r/wallstreetbets";

        // Format as of date
        SimpleDateFormat sdf        = new SimpleDateFormat("yyyy-MM-dd");
        String           dateString = sdf.format(new Date());
        Date             asOfDate   = sdf.parse(dateString);

        // Delete old data
        postTbl.deleteAll();
        summTbl.deleteAll();

        // Get the posts
        posts  = rssFeed.readRss(wallStreetBetsCommunity, asOfDate);

        // Get posts which reference stocks
        postsWithStocks = nerFeed.getPostsWithStocks(posts);

        // Write posts referencing stocks to data base
        for (RedditPost p : postsWithStocks ) {
            postTbl.add(p);
        //    System.out.println(p.getCommunity() + " " + p.getPostId() + " " + p.getTitle() + " " + p.getStock())
        }

        // If processing more than one community pull all posts from the data base
        // postsWithStocks = postTbl.getAll()

        // Generate summary database records
        List<RedditSumm> summRecs = summary.countStocks(postsWithStocks, asOfDate);

        // Write summary records to the database
        for ( RedditSumm s : summRecs) {
            summTbl.add(s);
        }

        // count the number of posts per stock
        List<String> reportLines = report.generateReport(summRecs);

        // Open the report file
        file.openResultsFile();

        // write the counts to a file
        for ( String line : reportLines ) {
            file.writeResultsFile(line);
        }

        // Close the results file
        file.closeResultsFile();
    }

}

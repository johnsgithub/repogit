package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.algorithm.RelativePerformance;
import com.jpw.raptor.jdbc.quote.QuoteDAO;

import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Rp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.List;

/**
 * Created by john on 7/28/18.
 */
@Controller
public class PerformanceController {

    @Autowired
    QuoteDAO quoteTbl;


    @RequestMapping("/relative-performance")
    public String relativePerformanceReq(
            @RequestParam(value="ref", required=true)       String ref,
            @RequestParam(value="symbol", required=true)    String symbol,
            Model model)
    {

        List<Quote>   refQuotes       = quoteTbl.getAllDesc(ref.toUpperCase());
        List<Quote>   equityQuotes    = quoteTbl.getAllDesc(symbol.toUpperCase());

        RelativePerformance rpFactory = new RelativePerformance();
        List<Rp>            rpList    = rpFactory.computeRelativePerformance(refQuotes, equityQuotes);

        // reverse the list from descending dates to ascending dates
        Collections.reverse(rpList);

        // 260 week days - 9 holidays equals 251 trading days in a year
        // 3 years equals 753 trading days
        if ( rpList.size() > 251 ) {
            // skip the first years worth
            model.addAttribute("data", rpList.subList(251, rpList.size()) );
        } else {
            model.addAttribute("data", rpList);
        }

        model.addAttribute("ref", ref);
        model.addAttribute("symbol", symbol);

        // Generate description
        String description = symbol.toUpperCase() + " out performs " + ref.toUpperCase() +
                " when the ratio is increasing and under performs when the ratio is decreasing";
        model.addAttribute("description", description);

        return "relative-performance";
    }
}

package com.jpw.raptor.testing;

import com.jpw.raptor.algorithm.*;
import com.jpw.raptor.algorithm.signals.*;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.fund.FundDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.model.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.jpw.raptor.model.DateConstants.*;
import static com.jpw.raptor.model.DateConstants.DAYS_200;

public class ZScoreDebug {


    @Autowired
    public QuoteDAO quoteTbl;

    public void doit(QuoteDAO quoteTbl) {



        List<Quote> equityQuotes    = quoteTbl.getAllDesc("LRGF", 60 );
        List<Quote> refQuotes = quoteTbl.getAllDesc("SPY", 60 );

        for ( Quote quote : equityQuotes ) {
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            String date = simpleDateFormat.format(quote.getDate());
            System.out.println(date);
        }

        TechnicalSummaryModel tsm = new TechnicalSummaryModel();
        tsm = generate(refQuotes, equityQuotes, "LRGF", "LRGF-name");
        System.out.println(tsm.getSymbol() + " " + tsm.getAdxSignal() );
    }

    public List<ZScore> quote(List<Quote> data, int offset, int sampleSize) {

        // compute data set mean
        Mean meanFactory = new Mean();
        double mean        = meanFactory.quote(data, offset, sampleSize);

        // compute data set standard deviation
        StandardDeviation stdFactory = new StandardDeviation();
        double            std        = stdFactory.quote(data, offset, sampleSize);

        // allocate space for zscore object array
        List<ZScore> list = new ArrayList<>(sampleSize);

        // generate z scores
        for ( int i=offset; i<(offset+sampleSize); i++) {
            double s = Math.round( ((data.get(i).getClose() - mean) / std) * 1000.0 ) / 1000.0;
            list.add( new ZScore(data.get(i).getSymbol(), data.get(i).getDate(), s) );
        }

        return list;
    }

    public TechnicalSummaryModel generate(List<Quote> refQuotes, List<Quote> equityQuotes, String symbol, String name ) {

        // Create summary record
        TechnicalSummaryModel summary         = new TechnicalSummaryModel();
        summary.setSymbol(symbol);
        summary.setName(name);
        summary.setDate(refQuotes.get(0).getDate());

        // JDK signal
        JDKFactory jdkFactory = new JDKFactory();
        JdkSignalFactory jdkSignal  = new JdkSignalFactory();
        List<Jdk>        jdks       = jdkFactory.generate(refQuotes, equityQuotes);
        if ( jdks.size() == 0 )
            return summary;
        else
            summary.setJdkSignal(jdks.get(0).getRsRatioMa());

        // ADX Signal
        AvgDirMovementIndicator adxFactory = new AvgDirMovementIndicator();
        AdxSignalFactory adxSignal  = new AdxSignalFactory();
        List<Adx>               adxs       = adxFactory.generateAdx(equityQuotes, 14);
        if ( adxs.size() == 0 )
            return summary;
        else
            summary.setAdxSignal(adxSignal.adxSignal(adxs.get(0), adxs.get(1)));

        // MACD signal
        MovingAvgConvergenceDivergence macdFactory = new MovingAvgConvergenceDivergence();
        MacdSignalFactory macdSignal  = new MacdSignalFactory();
        List<Macd>                     macds       = macdFactory.quote(equityQuotes, 12, 26);
        if ( macds.size() == 0 )
            return summary;
        else
            summary.setMacdSignal(macdSignal.macdMomentum(macds.get(0).getMacd(), macds.get(1).getMacd()));

        // Moving Average Signal
        // generate averages model for first quote
        SimpleMovingAverage        avgFactory       = new SimpleMovingAverage();
        MovingAverageSignalFactory avgSignalFactory = new MovingAverageSignalFactory();
        AveragesModel              avgRecord        = new AveragesModel(equityQuotes.get(0));
        avgRecord.setSimple5(avgFactory.quote(equityQuotes, 0, DAYS_5));
        avgRecord.setSimple10(avgFactory.quote(equityQuotes, 0, DAYS_10));
        avgRecord.setSimple20(avgFactory.quote(equityQuotes, 0, DAYS_20));
        avgRecord.setSimple50(avgFactory.quote(equityQuotes, 0, DAYS_50));
        avgRecord.setSimple100(avgFactory.quote(equityQuotes, 0, DAYS_100));
        avgRecord.setSimple200(avgFactory.quote(equityQuotes, 0, DAYS_200));
        if ( equityQuotes.size() >= 200 )
            summary.setMAvgSignal(avgSignalFactory.getSimpleMaSignal(avgRecord));
        else
            summary.setMAvgSignal(avgSignalFactory.getSimpleMaSignalShort(avgRecord));

        // relative strength
        RelativeStrengthIndicator rsi       = new RelativeStrengthIndicator();
        RsiSignalFactory rsiSignal = new RsiSignalFactory();
        String signal = rsiSignal.signal(
                rsi.quote(equityQuotes, 0, 14),
                rsi.quote(equityQuotes, 1, 14));
        summary.setRsiSignal(signal);

        // average true range
        AverageTrueRange atrFactory = new AverageTrueRange();
        List<Atr>        atrList = atrFactory.generateAtr(equityQuotes, 14);
        if ( atrList.size() == 0 )
            return summary;
        else
            summary.setAtrSignal(atrList.get(0).getAverageTruePercentage());

        return summary;
    }

    public List<Jdk> generateJdk(List<Quote> refQuotes, List<Quote> equityQuotes) {

        double magic = 2.0;

        // generate relative performance for equity and reference
        RelativePerformance rpFactory = new RelativePerformance();
        List<Rp>            rpList    = rpFactory.computeRelativePerformance(refQuotes, equityQuotes);

        if ( rpList.size() == 0 ) return Collections.emptyList();

        // generate z scores ie normalize the relative performance values
        ZScoreFactory zScrFactory = new ZScoreFactory();
        List<ZScore> zScrList     = zScrFactory.rp(rpList, 0, rpList.size());

        // allocate space for object list and populate with jdk ratio
        List<Jdk> jdkList = new ArrayList<>(rpList.size());
        for ( int i=0; i<rpList.size(); i++ ) {
            double ratio = 100.0 + (zScrList.get(i).getScore() * magic);
            jdkList.add( new Jdk(rpList.get(i).getSymbol(), rpList.get(i).getDate(), ratio, 0.0) );
        }

        // generate simple moving average for rsRatio
        ExponentialMovingAverage ema = new ExponentialMovingAverage();
        //for ( int i=0; i<(jdkList.size() - 1); i++ ) {
        for ( int i=jdkList.size() - 2; i>=0; i-- ) {
            jdkList.get(i).setRsRatioMa( ema.getExpAvg(jdkList.get(i).getRsRatio(), jdkList.get(i+1).getRsRatioMa(), 12));
        }

        // Update the array with macd and generate momentum values
        MovingAvgConvergenceDivergence macdFactory = new MovingAvgConvergenceDivergence();
        List<Macd> macds = macdFactory.jdk(jdkList, 12, 26);

        if ( macds.size() == 0 ) return Collections.emptyList();

        for ( int i=0; i<jdkList.size(); i++ ) {
            jdkList.get(i).setRsMomentum( 100.0 + (macds.get(i).getMacd() * magic) );
            jdkList.get(i).setMacd( macds.get(i).getMacd() );
            jdkList.get(i).setSignal( macds.get(i).getSignal() );
            jdkList.get(i).setHistogram( macds.get(i).getHistogram() );
        }

        return jdkList;
    }
}

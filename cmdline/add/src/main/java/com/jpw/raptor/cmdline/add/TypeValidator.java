package com.jpw.raptor.cmdline.add;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.util.regex.Pattern;

public class TypeValidator implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {
        if ( value.equalsIgnoreCase("etf") ||
             value.equalsIgnoreCase("stock") ||
             value.equalsIgnoreCase("index") ) {

        } else {
            throw new ParameterException(
                    "Type must be Etf, Stock or Index:  " + value );
        }
    }
}

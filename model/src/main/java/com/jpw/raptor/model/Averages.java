package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by john on 4/8/18.
 */
@Getter
@Setter
public class Averages {

    private double 	lastClose;
    private double 	simple20;
    private double 	simple50;
    private double 	simple200;


    protected void init() {
        // Provide default values
        lastClose  = 0.0;
        simple20   = 0.0;
        simple50   = 0.0;
        simple200  = 0.0;

    }

    // Constructor
    public Averages () {
        init();
    }

}

package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RedditSumm {

    protected long    rowId;
    protected Date    date;
    protected String  stock;
    protected int     num;
    protected String  name;

    public RedditSumm() {}

    public RedditSumm (Date date, String stock, int num, String name) {
        this.date      = date;
        this.stock     = stock;
        this.num       = num;
        this.name      = name;
    }

}

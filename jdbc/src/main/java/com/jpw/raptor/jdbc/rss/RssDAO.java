package com.jpw.raptor.jdbc.rss;

import com.jpw.raptor.model.Event;

import com.jpw.raptor.model.Rss;
import com.jpw.raptor.model.RssEntry;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import jakarta.annotation.PostConstruct;
import javax.sql.DataSource;

import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by john on 5/13/18.
 */

public class RssDAO implements RssInterface {

    //private static final Logger logger = LoggerFactory.getLogger(RssDAO.class);

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private DataSource      dataSource;
    private JdbcTemplate    jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @PostConstruct
    public void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int sqlScript(String script) {
        return jdbcTemplate.update(script);
    }

    @Override
    public void add(RssEntry rec) {

        // Normalize date
        SimpleDateFormat    simpleDateFormat    = new SimpleDateFormat(DATE_FORMAT);
        String              dateString          = simpleDateFormat.format(rec.getDatePosted());
        Date                date                = new Date(-1);
        try {
            date = simpleDateFormat.parse(dateString);
        } catch ( java.text.ParseException ex) {
            System.out.println ("Quote add date format exception");
        }

        String sql = "INSERT INTO rss_tbl (hash, date_posted, title, url, uri) values (?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, rec.getHash(), date, rec.getTitle(), rec.getUrl(), rec.getUri());
    }

    @Override
    public void delete(String hash) {
        String sql = "DELETE FROM rss_tbl where hash=?";
        jdbcTemplate.update(sql, hash);
    }

    @Override
    public void deleteAll() {
        String sql = "DELETE FROM rss_tbl";
        jdbcTemplate.update(sql);
    }

    @Override
    public void deletePrior(Date date) {

        // Normalize date
        SimpleDateFormat    simpleDateFormat    = new SimpleDateFormat(DATE_FORMAT);
        String              dateString          = simpleDateFormat.format(date);
        Date				dateNormal;
        try {
            dateNormal = simpleDateFormat.parse(dateString);
        } catch ( java.text.ParseException ex) {
            dateNormal   		= new Date(-1);
            System.out.println ("RSS get date format exception");
        }

        String sql = "DELETE FROM rss_tbl WHERE date_posted<?";
        jdbcTemplate.update(sql);
    }


    @Override
    public RssEntry get(String hash) {
        String sql = "SELECT * FROM rss_tbl WHERE hash=?";
        RowMapper<RssEntry> rowMapper = new RssRowMapper();
        //return this.jdbcTemplate.queryForObject(sql, rowMapper, rowNo)
        List<RssEntry> results = this.jdbcTemplate.query(sql, rowMapper, hash);
        if ( !results.isEmpty() )
            return results.get(0);
        else
            return null;
    }

    @Override
    public List<RssEntry> getAll() {
        String sql = "SELECT * FROM rss_tbl";
        RowMapper<RssEntry> rowMapper = new RssRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    public List<RssEntry> getPrior(Date date) {

        // Normalize date
        SimpleDateFormat    simpleDateFormat    = new SimpleDateFormat(DATE_FORMAT);
        String              dateString          = simpleDateFormat.format(date);
        Date				dateNormal;
        try {
            dateNormal = simpleDateFormat.parse(dateString);
        } catch ( java.text.ParseException ex) {
            dateNormal   		= new Date(-1);
            System.out.println ("RSS get date format exception");
        }

        String sql = "SELECT * FROM rss_tbl WHERE date_posted<? ORDER BY date_posted";
        RowMapper<RssEntry> rowMapper = new RssRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper, dateNormal);
    }

}


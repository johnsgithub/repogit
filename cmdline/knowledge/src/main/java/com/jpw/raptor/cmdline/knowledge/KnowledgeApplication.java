package com.jpw.raptor.cmdline.knowledge;


import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.jpw.raptor.lib.properties.FinanceProperties;

import com.jpw.raptor.model.FinanceKnowledge;

import com.jpw.raptor.search.finance.KnowledgeSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class KnowledgeApplication implements CommandLineRunner {

    @Autowired
    public KnowledgeFile    knowFile;

    @Autowired
    KnowledgeSearch elasticSearch;

    public static final String KNOWLEDGE_INDEX = "index-know";

    public void knowledgeIngest() throws IOException {

        System.out.println();
        System.out.println("***************  Knowledge data ingest  *******************");


        // import directory
        FinanceProperties fp   = new FinanceProperties();
        Properties prop = fp.get();

        String dir  = prop.getProperty("knowledge_dir");
        if ( dir == null )
        {
            System.out.println("Knowledge storage directory not specified");
            return;
        }

        //
        // Open elastic search index
        elasticSearch.openIndex(KNOWLEDGE_INDEX);

        KnowledgeFileUtil fileUtil = new KnowledgeFileUtil();

        // Get a list of files to process
        List<File> filesToProcess = fileUtil.listFilesInDirAndSubDir();

        // Process each file
        for ( File file : filesToProcess ) {

            // do not process html files
            if ( !file.getCanonicalPath().endsWith(".html") ) {

                System.out.println("Processs " + file.getCanonicalPath());

                // Extract quotes and write them to the database
                FinanceKnowledge knowObj = knowFile.indexFile(elasticSearch, file);

                if (knowObj != null) {
                    String sourceDoc    = file.getCanonicalPath();
                    String sourceHtml   =
                            file.getCanonicalPath().substring(0,file.getCanonicalPath().lastIndexOf('.')) + ".html";

                    String destDoc      = dir + "/" + knowObj.getLoc();
                    String destHtml     = dir + "/" + knowObj.getUrl();

                    // need to move doc file
                    fileUtil.moveFile(sourceDoc, destDoc);

                    // need to move html file
                    fileUtil.moveFile(sourceHtml, destHtml);
                } else {
                    // Delete the file
                    System.out.println("Processs failed for " + file.getCanonicalPath());
                    fileUtil.delFile(file);
                }
            }
        }

        //
        // Close elastic search index
        elasticSearch.closeIndex();
    }


    // Main loop
    @Override
    public void run(String... args) throws Exception {

        // define the run time parameters
        AppParameters params = new AppParameters();

        // create parameter parser
        JCommander cmd = new JCommander(params);

        try {
            cmd.parse(args);
            knowledgeIngest();
        } catch (ParameterException ex) {
            System.out.println(ex.getMessage());
            cmd.usage();
        }

        System.out.println("***************  Good bye  *******************");
        System.out.println();

    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(KnowledgeApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit( app.run(args) ) );
    }

}

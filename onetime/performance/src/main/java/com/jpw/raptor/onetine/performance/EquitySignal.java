package com.jpw.raptor.onetine.performance;

import com.jpw.raptor.model.Etf;
import com.jpw.raptor.model.Stock;
import com.jpw.raptor.model.TechnicalSummaryModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EquitySignal {
    private double jdk;
    private TechnicalSummaryModel raw;
    private Stock stock;
    private Etf   etf;
    private String line;

    public EquitySignal(TechnicalSummaryModel s) {
        jdk   =  s.getJdkSignal();
        raw   =  s;
        stock = null;
        etf   = null;
        line  =  s.getSymbol() + ","
                + s.getName() + ","
                + s.getJdkSignal() + ","
                + s.getMAvgSignal() + ","
                + s.getMacdSignal() + ","
                + s.getRsiSignal() + ","
                + s.getAdxSignal() + ","
                + s.getAtrSignal();
    }

    public EquitySignal(TechnicalSummaryModel tsm, Stock s) {
        jdk   =  tsm.getJdkSignal();
        raw   =  tsm;
        stock = s;
        etf   = null;
        line  =  tsm.getSymbol() + ","
                + tsm.getName() + ","
                + tsm.getJdkSignal() + ","
                + tsm.getMAvgSignal() + ","
                + tsm.getMacdSignal() + ","
                + tsm.getRsiSignal() + ","
                + tsm.getAdxSignal() + ","
                + tsm.getAtrSignal();
    }

    public EquitySignal(TechnicalSummaryModel tsm, Etf e) {
        jdk   =  tsm.getJdkSignal();
        raw   =  tsm;
        stock = null;
        etf   = e;
        line  =  tsm.getSymbol() + ","
                + tsm.getName() + ","
                + tsm.getJdkSignal() + ","
                + tsm.getMAvgSignal() + ","
                + tsm.getMacdSignal() + ","
                + tsm.getRsiSignal() + ","
                + tsm.getAdxSignal() + ","
                + tsm.getAtrSignal();
    }
}

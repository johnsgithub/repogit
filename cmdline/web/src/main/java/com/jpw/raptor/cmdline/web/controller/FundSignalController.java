package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.algorithm.TechnicalSummaryFactory;
import com.jpw.raptor.jdbc.fund.FundDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 4/9/18.
 */
@Controller
public class FundSignalController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    public QuoteDAO quoteTbl;

    @Autowired
    public FundDAO fundTbl;

    protected static final String REF_SYMBOL = "SPY";
    protected static final String RECORDS = "tech_model";
    protected static final String PAGE_TITLE = "pagetitle";
    protected static final String ENCODING = "UTF-8";
    protected static final String SIGNAL_PAGE = "signals";
    protected static final String DEFAULT_DAYS = "220";

    @RequestMapping("/mf-signal-all")
    public String mfSignalAllReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("mfSignalAllReq {}", "");

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getAll();

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Fund equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "ALL FUND Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/mf-signal-relevant")
    public String mfSignalRelevantReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("mfSignalRelevantReq {}", "");

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getRelevant();

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Fund equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "RELEVANT FUND Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/mf-signal-tracked")
    public String mfSignalTrackedReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("mfSignalTrackedReq {}", "");

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getTracked();

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Fund equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "TRACKED FUND Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/mf-signal-owned")
    public String mfSignalOwnedReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("mfSignalTrackedReq {}", "");

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getOwned();

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Fund equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "OWNED FUND Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/mf-signal-class")
    public String etfSignalClassReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String assetClassClean = assetClass.replace('+', ' ');

        try {
            assetClassClean  = URLDecoder.decode(assetClass, "UTF-8");
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfSignalClassReq  asset class {} ", assetClassClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getByAssetClass(assetClassClean);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Fund equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, assetClassClean.toUpperCase() + " FUND Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/mf-signal-class-type")
    public String etfSignalClassTypeReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String assetClassClean = "";
        String fundTypeClean   = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfSignalClassTypeReq  asset class {} fund type {}", assetClassClean, fundTypeClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getByAssetClassFundType(assetClassClean, fundTypeClean);

        /// Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Fund equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE,
                assetClassClean.toUpperCase() + " - " + fundTypeClean.toUpperCase() + " FUND Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/mf-signal-class-type-sub")
    public String etfSignalClassTypeSubTypeReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            @RequestParam(value="sub", required=true)  String fundSubType,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String assetClassClean  = "";
        String fundTypeClean    = "";
        String fundSubTypeClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
            fundSubTypeClean = URLDecoder.decode(fundSubType, ENCODING);
        } catch (UnsupportedEncodingException ignore) {}

        //logger.debug("mfSignalClassTypeSubTypeReq  asset class {} fund type {} sub type {}",
        //        assetClassClean, fundTypeClean, fundSubTypeClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getByAssetClassFundTypeSubType(
                assetClassClean, fundTypeClean, fundSubTypeClean);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Fund equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE,
                assetClassClean.toUpperCase() + " - " + fundTypeClean.toUpperCase()
                        + " - " + fundSubTypeClean.toUpperCase() + " FUND Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/mf-signal-class-type-sub-factor")
    public String etfSignalClassTypeSubTypeFactorReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            @RequestParam(value="sub", required=true)  String fundSubType,
            @RequestParam(value="factor", required=true)  String factor,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String assetClassClean  = "";
        String fundTypeClean    = "";
        String fundSubTypeClean = "";
        String factorClean      = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
            fundSubTypeClean = URLDecoder.decode(fundSubType, ENCODING);
            factorClean      = URLDecoder.decode(factor, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfSignalClassTypeSubTypeFactorReq  asset class {} fund type {} sub type {} factor {} ",
        //        assetClassClean, fundTypeClean, fundSubTypeClean, factorClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getByAssetClassFundTypeSubTypeFactor(
                assetClassClean, fundTypeClean, fundSubTypeClean, factorClean);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Fund equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, assetClassClean.toUpperCase() + " - " + fundTypeClean.toUpperCase()
                + " - " + fundSubTypeClean.toUpperCase()
                + " - " + factorClean.toUpperCase() + " FUND Signals");

        return SIGNAL_PAGE;
    }

}

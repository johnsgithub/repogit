package com.jpw.raptor.cmdline.reddit;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunking;
import com.aliasi.dict.ExactDictionaryChunker;
import com.aliasi.dict.MapDictionary;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.jpw.raptor.model.RedditPost;

import java.util.*;

public class RedditNER {

    public List<RedditPost> getPostsWithStocks(List<RedditPost> posts) throws java.io.IOException {

        // Generated list of posts containing stock symbols
        // one entry for each stock symbol
        // So if a post references three stocks then there will be three entries
        List<RedditPost> list = new ArrayList<>();

        // Dictionary of stocks to search for
        RedditDictionary obj = new RedditDictionary();
        MapDictionary<String> dictionary = obj.getDictionary();

        // Lingpipe provide named entity recognition with this chunker
        // it returns all matches and is not case sensitive
        ExactDictionaryChunker dictionaryChunker =
                new ExactDictionaryChunker(dictionary,
                        IndoEuropeanTokenizerFactory.INSTANCE,
                        true,false);

        // Process each post
        for (RedditPost post : posts) {
            addPost(list, dictionaryChunker, post);
        }

        return list;
    }


    public void addPost(List<RedditPost> list, ExactDictionaryChunker chunker, RedditPost post) throws java.io.IOException {

        // Contains single entry for each identified stock
        HashSet<String> stocks = new HashSet<>();

        // Search text for stocks
        Chunking contentChunking = chunker.chunk(post.getContents());

        // process each stock found
        for (Chunk chunk : contentChunking.chunkSet()) {
            String stock = post.getContents().substring(chunk.start(), chunk.end());
            //System.out.println("stock from content " + stock);
            stocks.add(stock.toUpperCase(Locale.ROOT));
        }

        // if this is a post (not comment) also search the post title
        if ( post.getCType().equalsIgnoreCase("post") ) {
            // Search text for stocks
            Chunking titleChunking = chunker.chunk(post.getTitle());

            // process each stock found
            for (Chunk chunk : titleChunking.chunkSet()) {
                String stock = post.getTitle().substring(chunk.start(), chunk.end());
                stocks.add(stock.toUpperCase(Locale.ROOT));
            }
        }

        // Add one post for each stock found
        for ( String stock : stocks ) {
            list.add(
                    new RedditPost( post.getDate(), stock, post.getCommunity(), post.getPostId(),
                                    post.getAuthor(), post.getTitle(), post.getCType(), post.getContents()
                                  )
                    );
        }
    }

}

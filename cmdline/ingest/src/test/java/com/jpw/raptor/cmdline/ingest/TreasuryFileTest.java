package com.jpw.raptor.cmdline.ingest;

import com.jpw.raptor.model.FileQualified;
import com.jpw.raptor.model.Treasury;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.nio.file.Files;
import java.util.Deque;
import java.util.Iterator;

public class TreasuryFileTest {

    @Test
    public void test0() throws Exception {

        File resource = new ClassPathResource("daily-treasury-rates.csv").getFile();;

        TreasuryFile tf = new TreasuryFile();

        Deque<Treasury> result = tf.readFile (resource);

        for (Treasury t : result) {
            System.out.println(t.getDate() + " " + t.getOneMonth() + " " + t.getThirtyYears());
        }

    }



}

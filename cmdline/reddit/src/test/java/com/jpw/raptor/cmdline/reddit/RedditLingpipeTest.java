package com.jpw.raptor.cmdline.reddit;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunking;
import com.aliasi.dict.ExactDictionaryChunker;
import com.aliasi.dict.MapDictionary;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import org.junit.Test;

public class RedditLingpipeTest {

    @Test
    public void test01() throws Exception {

        System.out.println("test 01");

        RedditDictionary obj = new RedditDictionary();
        MapDictionary<String> dictionary = obj.getDictionary();

        // return all matches not case sensitive
        ExactDictionaryChunker dictionaryChunker =
                new ExactDictionaryChunker(dictionary,
                IndoEuropeanTokenizerFactory.INSTANCE,
                true,false);

        String text = "how now brown cow with $ibm and $RKT.";

        Chunking chunking = dictionaryChunker.chunk(text);

        System.out.println("Stocks found "  + chunking.chunkSet().size() );

        for (Chunk chunk : chunking.chunkSet()) {
            int start = chunk.start();
            int end = chunk.end();
            String type = chunk.type();
            double score = chunk.score();
            String phrase = text.substring(start,end);
            System.out.println("     phrase=|" + phrase + "|"
                    + " start=" + start
                    + " end=" + end
                    + " type=" + type
                    + " score=" + score);
        }
    }

}

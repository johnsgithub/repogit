package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
public class Rss {

    protected String  hash;
    protected Date    datePosted;
    protected String  title;
    protected String  url;
    protected String  uri;

    // Sat Aug 06 12:15:01 EDT 2022

    public Rss() {}

    public Rss(String hash, Date datePosted, String title, String url, String uri) {
        this.hash       = hash;
        this.datePosted = datePosted;
        this.title      = title;
        this.url        = url;
        this.uri        = uri;
    }

}

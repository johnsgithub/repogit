package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Jdk;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.RelativePerformanceModel;
import com.jpw.raptor.model.Rp;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.*;

public class WeeksFactory {

    // create a new list with one entry per week
    public List<Quote> quote(List<Quote> data) {

        // Create a sorted map to hold the model data
        // data will be stored in ascending order by date string
        TreeMap<String, Quote> map = new TreeMap<>();

        //
        // Populate a map with one entry per week
        for (Quote rec : data) {

            // Generate Local Date object for quote date
            SimpleDateFormat dateFormat     = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate  = dateFormat.format(rec.getDate());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(formattedDate, formatter);

            // Get week of year
            WeekFields woy        = WeekFields.of(Locale.getDefault());
            TemporalField temporal        = woy.weekOfWeekBasedYear();
            int weekNumber = localDate.get(temporal);

            // get Month
            int monthNumber = localDate.getMonthValue();

            // get year
            int year = localDate.getYear();

            // generate key (year plus week in year)
            String weekKey = String.format("%4d", year) + String.format("%02d", weekNumber);

            // fix bug where december date is in first week of next year
            if (weekNumber == 1 && monthNumber == 12)
                weekKey = String.format("%4d", year+1) + String.format("%02d", weekNumber);

            // Check to see if week has been added to tree
            // data is in descending order oby date so the first record found
            // will be the last day in the week
            if ( map.containsKey(weekKey) ) {
                // entry for week
            } else {
                // add entry for week
                map.put(weekKey, rec);
            }
        }

        //
        // Populate a list with entries that have both ref and equity values
        // traverse the map in sorted order which is ascending order
        List<Quote>  result = new ArrayList<>(map.size());
        for (Map.Entry<String, Quote> entrySetVal : map.entrySet()) {
            Quote entry = entrySetVal.getValue();
            result.add(entry);
        }

        Collections.reverse(result);

        return result;
    }

    // create a new list with one entry per week
    public List<Rp> rp(List<Rp> data) {

        // Create a sorted map to hold the model data
        // data will be stored in ascending order by date string
        TreeMap<String, Rp> map = new TreeMap<>();

        //
        // Populate a map with one entry per week
        for (Rp rec : data) {

            // Generate Local Date object for quote date
            SimpleDateFormat dateFormat     = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate  = dateFormat.format(rec.getDate());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(formattedDate, formatter);

            // Get week of year
            WeekFields woy        = WeekFields.of(Locale.getDefault());
            TemporalField temporal        = woy.weekOfWeekBasedYear();
            int weekNumber = localDate.get(temporal);

            // get Month
            int monthNumber = localDate.getMonthValue();

            // get year
            int year = localDate.getYear();

            // generate key (year plus week in year)
            String weekKey = String.format("%4d", year) + String.format("%02d", weekNumber);

            // fix bug where december date is in first week of next year
            if (weekNumber == 1 && monthNumber == 12)
                weekKey = String.format("%4d", year+1) + String.format("%02d", weekNumber);

            // Check to see if week has been added to tree
            // data is in descending order oby date so the first record found
            // will be the last day in the week
            if ( map.containsKey(weekKey) ) {
                // entry for week
            } else {
                // add entry for week
                map.put(weekKey, rec);
            }
        }

        //
        // Populate a list with entries that have both ref and equity values
        // traverse the map in sorted order which is ascending order
        List<Rp>  result = new ArrayList<>(map.size());
        for (Map.Entry<String, Rp> entrySetVal : map.entrySet()) {
            Rp entry = entrySetVal.getValue();
            result.add(entry);
        }

        Collections.reverse(result);

        return result;
    }



    // create a new list with one entry per week
    public List<Jdk> jdk(List<Jdk> data) {

        // Create a sorted map to hold the model data
        // data will be stored in ascending order by date string
        TreeMap<String, Jdk> map = new TreeMap<>();

        //
        // Populate a map with one entry per week
        for (Jdk rec : data) {

            // Generate Local Date object for quote date
            SimpleDateFormat dateFormat     = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate  = dateFormat.format(rec.getDate());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(formattedDate, formatter);

            // Get week of year
            WeekFields woy        = WeekFields.of(Locale.getDefault());
            TemporalField temporal        = woy.weekOfWeekBasedYear();
            int weekNumber = localDate.get(temporal);

            // get Month
            int monthNumber = localDate.getMonthValue();

            // get year
            int year = localDate.getYear();

            // generate key (year plus week in year)
            String weekKey = String.format("%4d", year) + String.format("%02d", weekNumber);

            // fix bug where december date is in first week of next year
            if (weekNumber == 1 && monthNumber == 12)
                weekKey = String.format("%4d", year+1) + String.format("%02d", weekNumber);

            // Check to see if week has been added to tree
            // data is in descending order oby date so the first record found
            // will be the last day in the week
            if ( map.containsKey(weekKey) ) {
                // entry for week
            } else {
                // add entry for week
                map.put(weekKey, rec);
            }
        }

        //
        // Populate a list with entries that have both ref and equity values
        // traverse the map in sorted order which is ascending order
        List<Jdk>  result = new ArrayList<>(map.size());
        for (Map.Entry<String, Jdk> entrySetVal : map.entrySet()) {
            Jdk entry = entrySetVal.getValue();
            result.add(entry);
        }

        Collections.reverse(result);

        return result;
    }

}

package com.jpw.raptor.cmdline.reports.track;

import com.jpw.raptor.algorithm.DateConstants;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.track.TrackDAO;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.TrackRec;
import org.apache.commons.lang3.StringUtils;
import org.javatuples.Pair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

public class TrackReport {

    public List<Pair<String,List<Double>>> generateData(QuoteDAO quoteTbl, TrackDAO trackTbl, String analyst, Date startDate)
            throws java.io.IOException, java.text.ParseException {

        // Worker routines for working with dates
        DateConstants dc = new DateConstants();

        // Format start date
        SimpleDateFormat format       = new SimpleDateFormat("yyyy-MM-dd");
        String           startDateStr = format.format(startDate);

        // populate track map this reduces multiple recommendations to one record
        TrackMap trackMap = new TrackMap();
        if ( StringUtils.isNotEmpty(analyst) )
            trackMap.addRecs(trackTbl.getAnalyst(analyst));
        else
            trackMap.addRecs(trackTbl.getAll());

        // Get a list of recommendations to include in the report
        List<TrackRec> list = trackMap.getRecs();

        // Allocate space for the report data
        List<Pair<String,List<Double>>> reportData = new ArrayList<>(list.size());

        // compute performance delta for each recommendation
        for ( TrackRec rec : list ) {

            // Report line text
            String recHeader = rec.getSymbol() + "," +
                    rec.getCount()  + "," +
                    rec.getAnalyst() + "," +
                    rec.getName() + "," +
                    rec.getSector() + "," +
                    rec.getDate() + "," +
                    startDateStr;

            // Get all quotes for an equity from the specified date
            // The first entry in the list will be the most current data
            List<Quote>  allQuotes     = quoteTbl.getForPerformanceDesc(rec.getSymbol().toUpperCase(), startDateStr);

            // Generate a list with one quote per week ie friday for a full week
            // The first entry in the list will be the most current data
            List<Quote>  weekEndQuotes = dc.getLastDayInWeek(allQuotes);

            // Allocate space for weekly returns from the start date
            List<Double> returns = new ArrayList<>(weekEndQuotes.size());

            // Populate the weekly returns list
            Quote first = weekEndQuotes.get(weekEndQuotes.size() - 1);
            for ( Quote last : weekEndQuotes ) {
                double r    = ((last.getClose() - first.getClose()) / first.getClose()) * 100.0;
                double rndR = Math.round(r * 100.0) / 100.0;
                returns.add(rndR);
            }

            // Create a report line tuple and add it to the list
            reportData.add(Pair.with(recHeader, returns));
        }

        return reportData;
    }

    /***/

    public Date getDate() throws java.text.ParseException {
        SimpleDateFormat format  = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse("2021-01-11");
    }

    Predicate<List<Double>> neg = (n) -> {
        boolean result = false;
        for ( Double entry : n) {
            if (entry < 0.0) result = true;
        }
        return result;
    };

    Predicate<List<Double>> decl = (n) -> {
        boolean result = false;
        for ( int i=0; i<n.size()-1; i++) {
            if (n.get(i) < n.get(i+1)) result = true;
        }
        return result;
    };

    public void runReport(QuoteDAO quoteTbl, TrackDAO trackTbl, String analyst, Date startDate)
            throws java.io.IOException, java.text.ParseException {

        Predicate<List<Double>> condition = neg;

        // Get the date for the report
        Quote forDate  = quoteTbl.getCurrent("SPY");

        // generate report data
        List<Pair<String,List<Double>>> data = generateData(quoteTbl, trackTbl, analyst, getDate());

        // open the report file
        TrackReportUtil fileUtil = new TrackReportUtil();
        fileUtil.openResultsFile(analyst, forDate.getDate());

        // compute performance delta for each recommendation
        for ( Pair<String,List<Double>> rec : data ) {

            StringBuilder sb = new StringBuilder();
            sb.append( rec.getValue0() );
            List<Double> returns = rec.getValue1();

            for ( int i = 0; i < returns.size() - 1; i++) {
                sb.append( "," );
                sb.append( returns.get(i) );
            }

            // Check to see if data should be written to report
            if ( !condition.test(returns) ) {
                fileUtil.writeResultsFile(sb.toString());
            }

        }

        fileUtil.closeResultsFile();
    }
}

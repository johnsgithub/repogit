package com.jpw.raptor.scrape.yahoo;

import com.google.gson.*;
import com.jpw.raptor.model.HoldingPercent;
import com.jpw.raptor.model.Holdings;
import org.junit.Test;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class TestJson {

    String s01 =
             "\\\"holdings\\\":["
                    +   "{\\\"symbol\\\":\\\"MSFT\\\",\\\"holdingName\\\":\\\"Microsoft Corp\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0726311,\\\"fmt\\\":\\\"7.26%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"AAPL\\\",\\\"holdingName\\\":\\\"Apple Inc\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0662632,\\\"fmt\\\":\\\"6.63%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"NVDA\\\",\\\"holdingName\\\":\\\"NVIDIA Corp\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0373541,\\\"fmt\\\":\\\"3.74%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"AMZN\\\",\\\"holdingName\\\":\\\"Amazon.com Inc\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0346911,\\\"fmt\\\":\\\"3.47%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"META\\\",\\\"holdingName\\\":\\\"Meta Platforms Inc Class A\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0212852,\\\"fmt\\\":\\\"2.13%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"GOOGL\\\",\\\"holdingName\\\":\\\"Alphabet Inc Class A\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0203794,\\\"fmt\\\":\\\"2.04%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"GOOG\\\",\\\"holdingName\\\":\\\"Alphabet Inc Class C\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.01736,\\\"fmt\\\":\\\"1.74%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"BRK-B\\\",\\\"holdingName\\\":\\\"Berkshire Hathaway Inc Class B\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0171613,\\\"fmt\\\":\\\"1.72%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"AVGO\\\",\\\"holdingName\\\":\\\"Broadcom Inc\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0127305,\\\"fmt\\\":\\\"1.27%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"TSLA\\\",\\\"holdingName\\\":\\\"Tesla Inc\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.012725,\\\"fmt\\\":\\\"1.27%\\\"}}],";

    String s2 =
            "\\\"holdings\\\":["
                    +   "{\\\"symbol\\\":\\\"MSFT\\\",\\\"holdingName\\\":\\\"Microsoft Corp\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0726311,\\\"fmt\\\":\\\"7.26%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"AAPL\\\",\\\"holdingName\\\":\\\"Apple Inc\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0662632,\\\"fmt\\\":\\\"6.63%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"NVDA\\\",\\\"holdingName\\\":\\\"NVIDIA Corp\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0373541,\\\"fmt\\\":\\\"3.74%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"AMZN\\\",\\\"holdingName\\\":\\\"Amazon.com Inc\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0346911,\\\"fmt\\\":\\\"3.47%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"META\\\",\\\"holdingName\\\":\\\"Meta Platforms Inc Class A\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0212852,\\\"fmt\\\":\\\"2.13%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"GOOGL\\\",\\\"holdingName\\\":\\\"Alphabet Inc Class A\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0203794,\\\"fmt\\\":\\\"2.04%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"GOOG\\\",\\\"holdingName\\\":\\\"Alphabet Inc Class C\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.01736,\\\"fmt\\\":\\\"1.74%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"BRK-B\\\",\\\"holdingName\\\":\\\"Berkshire Hathaway Inc Class B\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0171613,\\\"fmt\\\":\\\"1.72%\\\"}},"
                    +   "{\\\"symbol\\\":\\\"TSLA\\\",\\\"holdingName\\\":\\\"Tesla Inc\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.012725,\\\"fmt\\\":\\\"1.27%\\\"}}],";

    @Test
    //
    // Risk Stats
    public void test01() throws Exception {

        System.out.println("test it started");

        System.out.println(s01);



        System.out.println("test it finished");
    }

    @Test
    //
    // Risk Stats
    public void test02() throws Exception {

        System.out.println("test it started");

        //System.out.println(s01);



        String s02 = "holdingPercent\\\":{\\\"raw\\\":0.0726311,\\\"fmt\\\":\\\"7.26%\\\"}";

        String s03 = "{\\\"symbol\\\":\\\"MSFT\\\",\\\"holdingName\\\":\\\"Microsoft Corp\\\",\\\"holdingPercent\\\":{\\\"raw\\\":0.0726311,\\\"fmt\\\":\\\"7.26%\\\"}}";

        // holdings does parse
        String s04 = "{\"symbol\":\"MSFT\",\"holdingName\":\"Microsoft Corp\"}";

        // Holdings does not parse
        String s05 = "\"symbol\":\"MSFT\",\"holdingName\":\"Microsoft Corp\"";



        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Holdings obj1 = (Holdings) gson.fromJson(s04, Holdings.class);
        System.out.println("Gson");
        System.out.println("symbol " + obj1.getSymbol() + " " + "holdingName  " + obj1.getHoldingName() );

        // jackson
        ObjectMapper objectMapper = new ObjectMapper();
        Holdings h = (Holdings) objectMapper.readValue(s04, Holdings.class);
        System.out.println("Jackson");
        System.out.println("symbol " + h.getSymbol() + " " + "holdingName  " + h.getHoldingName() );

        /*
        JsonElement jsonElement = JsonParser.parseString(s04);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        JsonParser parser = new JsonParser();
        JsonObject object = (JsonObject) parser.parse(s04);// response will be the json String
        System.out.println("symbol " + jsonObject.get("symbol") + " " + "holdingName  " + jsonObject.get("holdingName") );
*/

        // ?
        String s06 = "{\"symbol\":\"MSFT\",\"holdingName\":\"Microsoft Corp\",\"holdingPercent\":{\"raw\":0.0726311,\"fmt\":\"7.26%\"}}";
        Holdings obj2 = (Holdings) gson.fromJson(s06, Holdings.class);
        System.out.println("Gson");
        System.out.println("symbol " + obj2.getSymbol() + " " + "holdingName  " + obj2.getHoldingName() + " raw " + obj2.getHoldingPercent().getRaw() + " " + "fmt  " + obj2.getHoldingPercent().getFmt() );

        String s07 =
                "["
                        +   "{\"symbol\":\"MSFT\",\"holdingName\":\"Microsoft Corp\",\"holdingPercent\":{\"raw\":0.0726311,\"fmt\":\"7.26%\"}},"
                        +   "{\"symbol\":\"TSLA\",\"holdingName\":\"Tesla Inc\",\"holdingPercent\":{\"raw\":0.012725,\"fmt\":\"1.27%\"}}]";

        Holdings [] a = gson.fromJson(s07,Holdings[].class);
        System.out.println("array size " + a.length);

        System.out.println("a0 " + a[0].getSymbol() + " " + a[0].getHoldingName() + " " + a[0].getHoldingPercent().getRaw() + " " + a[0].getHoldingPercent().getFmt() );
        System.out.println("a1 " + a[1].getSymbol() + " " + a[1].getHoldingName() + " " + a[1].getHoldingPercent().getRaw() + " " + a[1].getHoldingPercent().getFmt() );
        System.out.println("test it finished");
    }

}
package com.jpw.raptor.onetine.performance;

import com.jpw.raptor.algorithm.TechnicalSummaryFactory;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.model.Etf;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Stock;
import com.jpw.raptor.model.TechnicalSummaryModel;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EtfSignal {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    protected static final  String REF_SYMBOL = "SPY";
    protected static final  String RECORDS = "tech_model";
    protected static final  String PAGE_TITLE = "pagetitle";
    protected static final  String ENCODING = "UTF-8";
    protected static final  String SIGNAL_PAGE = "signals";
    protected static final String DEFAULT_DAYS = "220";

    public void getSignals(EtfDAO etfTbl, QuoteDAO quoteTbl) {

        int days = 60;

        //logger.debug("stockSignalAllReq {}", "");

        // Get the Stocks to display
        List<Etf> equities = etfTbl.getAll();

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        /*** Technical summary model
         String symbol;
         String name;
         double jdkSignal greater than 100;
         String macdSignal begins with 4 -
         String mAvgSignal begins with 90:
         ***/

        List<EquitySignal> signals = new ArrayList<>();
        for ( Etf equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            TechnicalSummaryModel s = summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName());

            if (s.getJdkSignal() > 100.0 && s.getJdkSignal() < 110.0 &&
                    s.getMAvgSignal().contains("90") && s.getMacdSignal().contains("4 -")) {
                EquitySignal sortedSignal = new EquitySignal(s);
                signals.add(sortedSignal);
            }

        }

        //Collections.sort(signals);

        Collections.sort(signals, new Comparator<EquitySignal>() {
            @Override
            public int compare(EquitySignal sortedSignal, EquitySignal t1) {
                if      ( sortedSignal.getJdk() < t1.getJdk() )
                    return 1;
                else if ( sortedSignal.getJdk() > t1.getJdk() )
                    return -1;
                else
                    return 0;
            }
        });


        for ( EquitySignal l : signals ) {
            System.out.println(l.getJdk() + " :  " + l.getLine());
        }

        System.out.println();
        System.out.println("Records " + signals.size());

    }

}

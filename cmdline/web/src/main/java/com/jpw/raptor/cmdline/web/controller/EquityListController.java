package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.cmdline.web.service.ListModelService;

import com.jpw.raptor.lib.properties.FinanceProperties;
import com.jpw.raptor.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Controller
public class EquityListController {


    @Autowired
    ListModelService listService;

    @RequestMapping("/equity-list-by-symbol")
    public String equityListBySymbol(
            @RequestParam(value="file", required=true) String fileName,
            @RequestParam(value="title", required=false)  String pageTitle,
            Model model)
    {
        String fileNameClean  = null;
        String pageTitleClean = null;

        try {
            fileNameClean  = URLDecoder.decode(fileName,  "UTF-8");
        } catch (UnsupportedEncodingException ignored) {}

        if ( pageTitle != null ) {
            try {
                pageTitleClean = URLDecoder.decode(pageTitle, "UTF-8");
            } catch (UnsupportedEncodingException ignored) {
            }
        } else {
            pageTitleClean = fileNameClean;
        }

        String lineSeparator = System.lineSeparator();


        //
        // Read symbols from file
        FinanceProperties   fp       = new FinanceProperties();
        Properties          prop     = fp.get();
        String              filePath = prop.getProperty("file_dir") + "/" +fileNameClean;

        List<String> lines = null;
        try {
            // Get symbols from file
            lines = Files.readAllLines(Paths.get(filePath));
        } catch (IOException ignored) {}

        // Convert lines read to usable symbols
        ArrayList<String> symbols = new ArrayList<>(lines.size());
        for ( String line : lines ) {
            symbols.add( line.replace(lineSeparator, "").trim().toUpperCase() );
        }

        // Generate equity list
        List<EquityListModel> recs = listService.equityListModel(symbols);

        model.addAttribute("list_model", recs);
        model.addAttribute("pagetitle",  pageTitleClean);

        return "equity-list";
    }

}

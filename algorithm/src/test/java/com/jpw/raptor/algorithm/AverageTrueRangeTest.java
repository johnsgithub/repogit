package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Adx;
import com.jpw.raptor.model.Atr;
import com.jpw.raptor.model.Quote;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AverageTrueRangeTest {

    @Test
    public void test() throws ParseException {
        System.out.println(" test started");

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

        Atr[] ra = {
          new Atr( new Quote("IVV", fmt.parse("2018-08-07"), 285.39, 286.01, 285.24, 285.58, 0)),
          new Atr( new Quote("IVV", fmt.parse("2018-08-06"), 283.64, 284.99, 283.2, 284.64, 0)),
          new Atr( new Quote("IVV", fmt.parse("2018-08-03"), 282.53, 283.66, 282.33, 283.6, 0)),
          new Atr( new Quote("IVV", fmt.parse("2018-08-02"), 279.39, 282.58, 279.16, 282.39, 0)),
          new Atr( new Quote("IVV", fmt.parse("2018-08-01"), 281.56, 282.13, 280.13, 280.86, 0)),
          new Atr( new Quote("IVV", fmt.parse("2018-07-31"), 280.81, 282.02, 280.38, 281.33, 0)),
          new Atr( new Quote("IVV", fmt.parse("2018-07-30"), 281.51, 281.69, 279.36, 279.95, 0)),
          new Atr( new Quote("IVV", fmt.parse("2018-07-27"), 283.71, 283.82, 280.38, 281.42, 0))
        };

        Quote[] dataArray = {
         new Quote("IVV", fmt.parse("2018-08-07"), 285.39, 286.01, 285.24, 285.58, 0),
         new Quote("IVV", fmt.parse("2018-08-06"), 283.64, 284.99, 283.2, 284.64, 0),
         new Quote("IVV", fmt.parse("2018-08-03"), 282.53, 283.66, 282.33, 283.6, 0),
         new Quote("IVV", fmt.parse("2018-08-02"), 279.39, 282.58, 279.16, 282.39, 0),
         new Quote("IVV", fmt.parse("2018-08-01"), 281.56, 282.13, 280.13, 280.86, 0),
         new Quote("IVV", fmt.parse("2018-07-31"), 280.81, 282.02, 280.38, 281.33, 0),
         new Quote("IVV", fmt.parse("2018-07-30"), 281.51, 281.69, 279.36, 279.95, 0),
         new Quote("IVV", fmt.parse("2018-07-27"), 283.71, 283.82, 280.38, 281.42, 0)
        };

        ra[0].setTrueRange(1.37);
        ra[1].setTrueRange(1.79);
        ra[2].setTrueRange(1.33);
        ra[3].setTrueRange(3.42);
        ra[4].setTrueRange(2.0);
        ra[5].setTrueRange(2.07);
        ra[6].setTrueRange(2.33);
        ra[7].setTrueRange(3.44);

        ra[0].setAverageTrueRange(1.77);
        ra[1].setAverageTrueRange(2.04);
        ra[2].setAverageTrueRange(2.2);
        ra[3].setAverageTrueRange(2.8);
        ra[4].setAverageTrueRange(2.38);
        ra[5].setAverageTrueRange(2.63);
        ra[6].setAverageTrueRange(3.0);
        ra[7].setAverageTrueRange(3.44);

        AverageTrueRange atr = new AverageTrueRange();
        List<Atr> results =  atr.generateAtr(Arrays.asList(dataArray), 4);

        for ( int i=results.size() - 1; i>=0; i-- ) {
            assertEquals(ra[i].getTrueRange(), results.get(i).getTrueRange(), 0.01);
            assertEquals(ra[i].getAverageTrueRange(), results.get(i).getAverageTrueRange(), 0.01);
        }
        System.out.println(" test finished");
    }
}

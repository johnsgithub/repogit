package com.jpw.raptor.scrape.yahoo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class YahooStockProfileParse {

static final String GREATER_THAN = ">";
static final String LESS_THAN = "<";
static final String SPAN_END = "</span>";
static final String P_START = "<p ";
static final String P_END = "</p>";



    public YahooStockProfile readPage(String symbol) throws IOException {

        String  page;
        YahooStockProfile p = new YahooStockProfile();

        String ticker = symbol.toUpperCase().replace('.', '-');
        String baseUrl = "https://finance.yahoo.com/quote/" + ticker + "/profile?p=" + ticker;

        try {
            Document doc = Jsoup.connect(baseUrl).get();
            page = doc.toString();
        } catch ( org.jsoup.HttpStatusException ex ) {
            System.out.println("Stock Profile HttpStatusException " + ex.toString());
            p.setReadFailed(true);
            return p;
        }

        // Validate page was found
        if (page.contains("<span>No results for")  || page.contains("<span>Symbols similar to")) {
            System.out.println("Stock Profile Symbol not found " + symbol);
            p.setReadFailed(true);
            return p;
        }

        if ( page.contains("NoJs ") ) {
            try {
                parseNoJavaScript(page, p);
            } catch ( Exception ex ) {
                System.out.println(" **** Stock Profile No Java Script parse failed ***** " );
                BufferedWriter writer = new BufferedWriter(new FileWriter("/home/software/page-error-stock-profile-no-js.txt"));
                writer.write(page);
                p.setReadFailed(true);
                return p;
            }
        } else {
            try {
                parseJavaScript(page, p);
            } catch ( Exception ex ) {
                System.out.println(" **** Stock Profile Java Script parse failed ***** " );
                BufferedWriter writer = new BufferedWriter(new FileWriter("/home/software/page-error-stock-profile-js.txt"));
                writer.write(page);
                p.setReadFailed(true);
                return p;
            }
        }

        p.setReadFailed(false);
        return p;
    }


    public void parseJavaScript(String page, YahooStockProfile p) {

        //  Sector
        int sectorStartTag  = page.indexOf("Sector:");

        if ( sectorStartTag > 0 ) {
            int sectorStartLink = page.indexOf("<a ", sectorStartTag);
            int sectorStartData = page.indexOf(GREATER_THAN, sectorStartLink) + 1;
            int sectorEndData = page.indexOf(LESS_THAN, sectorStartData);
            p.setSector( page.substring(sectorStartData, sectorEndData) );
        } else {
            System.out.println("Stock profile sector not found " );

        }

        //  Industry
        int industryStartTag  = page.indexOf("Industry:");

        if ( industryStartTag > 0 ) {
            int industryStartLink = page.indexOf("<a ", industryStartTag);
            int industryStartData = page.indexOf(GREATER_THAN, industryStartLink) + 1;
            int industryEndData = page.indexOf(LESS_THAN, industryStartData);
            p.setIndustry( page.substring(industryStartData, industryEndData) );
        } else {
            System.out.println("Stock profile industry not found " );

        }

        //  description
        int descriptionStartTag  = page.indexOf(">Description");

        if ( descriptionStartTag > 0 ) {
            int paragraphStart = page.indexOf("<p>", descriptionStartTag);
            int descriptionStartData = page.indexOf("<p>", paragraphStart) + 3;
            int descriptionEndData = page.indexOf(P_END, descriptionStartData);
            p.setDescription( page.substring(descriptionStartData, descriptionEndData));
        } else {
            System.out.println("Stock profile description not found " );
        }

    }


    public void parseNoJavaScript(String page, YahooStockProfile p) {

        //  Sector
        int sectorStartTag  = page.indexOf("<span>Sector(s)</span>");

        if ( sectorStartTag > 0 ) {
            int sectorStartData = page.indexOf(GREATER_THAN, sectorStartTag+23) + 1;
            int sectorEndData = page.indexOf(SPAN_END, sectorStartData);
            p.setSector( Jsoup.parse(page.substring(sectorStartData, sectorEndData)).text() );
        } else {
            System.out.println("Stock profile sector not found " );

        }

        //  Industry
        int industryStartTag  = page.indexOf("<span>Industry</span>");

        if ( industryStartTag > 0 ) {
            int industryStartData = page.indexOf(GREATER_THAN, industryStartTag+22) + 1;
            int industryEndData = page.indexOf(SPAN_END, industryStartData);
            p.setIndustry( Jsoup.parse(page.substring(industryStartData, industryEndData)).text() );
        } else {
            System.out.println("Stock profile industry not found " );

        }

        //  description
        int descriptionStartTag  = page.indexOf("<span>Description</span>");

        if ( descriptionStartTag > 0 ) {
            int paragraphStart = page.indexOf(GREATER_THAN, descriptionStartTag);
            int descriptionStartData = page.indexOf(GREATER_THAN, paragraphStart) + 1;
            int descriptionEndData = page.indexOf(P_END, descriptionStartData);
            p.setDescription( Jsoup.parse(page.substring(descriptionStartData, descriptionEndData)).text() );
        } else {
            System.out.println("Stock profile description not found " );
        }

    }

}


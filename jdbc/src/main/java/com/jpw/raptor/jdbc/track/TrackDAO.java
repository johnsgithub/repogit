package com.jpw.raptor.jdbc.track;

import com.jpw.raptor.model.TrackRec;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import jakarta.annotation.PostConstruct;
import javax.sql.DataSource;

import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;

@Repository
public class TrackDAO implements TrackInterface{

    //private static final Logger logger = LoggerFactory.getLogger(TrackDAO.class);

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private DataSource      dataSource;
    private JdbcTemplate    jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @PostConstruct
    public void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int sqlScript(String script) {
        return jdbcTemplate.update(script);
    }

    @Override
    public void add(TrackRec rec) {

        // Normalize date
        SimpleDateFormat    simpleDateFormat    = new SimpleDateFormat(DATE_FORMAT);
        String              dateString          = simpleDateFormat.format(rec.getDate());
        Date                date                = new Date(-1);
        try {
            date = simpleDateFormat.parse(dateString);
        } catch ( java.text.ParseException ex) {
            System.out.println ("Track add date format exception");
        }

        // Normalize double values
        double delta  = Precision.round(rec.getDelta(),2);

        String sql = "INSERT INTO track_tbl (symbol, analyst, name, sector, date_tx, delta) values (?, ?, ?, ?, ?, ?)";
        try {
            jdbcTemplate.update(sql, rec.getSymbol(), rec.getAnalyst(), rec.getName(), rec.getSector(), date, rec.getDelta());
        } catch ( org.springframework.dao.DuplicateKeyException ex ) {
            System.out.println ("Duplicate track " + rec.getSymbol() + " " + rec.getAnalyst());
        }
    }

    @Override
    public void update(TrackRec rec) {

        // Normalize date
        SimpleDateFormat    simpleDateFormat    = new SimpleDateFormat(DATE_FORMAT);
        String              dateString          = simpleDateFormat.format(rec.getDate());
        Date                date                = new Date(-1);
        try {
            date = simpleDateFormat.parse(dateString);
        } catch ( java.text.ParseException ex) {
            System.out.println ("Quote update date format exception");
        }

        // Normalize double values
        double delta  = Precision.round(rec.getDelta(),2);

        String sql = "UPDATE track_tbl SET name=?, sector=?, date_tx=?, delta=? where symbol=? and analyst=?";

        jdbcTemplate.update(sql, rec.getName(), rec.getSector(), date, rec.getDelta(), rec.getSymbol(), rec.getAnalyst());
    }

    @Override
    public void delete(String symbol, String analyst) {

        String sql = "DELETE FROM track_tbl where symbol=? and analyst=?";
        jdbcTemplate.update(sql, symbol, analyst);
    }

    @Override
    public void deleteSymbol(String symbol) {

        String sql = "DELETE FROM track_tbl where symbol=?";
        jdbcTemplate.update(sql, symbol);
    }

    @Override
    public void deleteAnalyst(String analyst) {

        String sql = "DELETE FROM track_tbl where analyst=?";
        jdbcTemplate.update(sql, analyst);
    }

    @Override
    public TrackRec get(String symbol, String analyst) {

        String sql = "SELECT * FROM track_tbl WHERE symbol=? and analyst=?";
        RowMapper<TrackRec> rowMapper = new TrackRowMapper();
        //return this.jdbcTemplate.queryForObject(sql, rowMapper, symbol, date_normal);
        List<TrackRec> results = this.jdbcTemplate.query(sql, rowMapper, symbol, analyst);
        if ( results.size() > 0 )
            return results.get(0);
        else
            return null;
    }

    @Override
    public List<TrackRec> getSymbol(String symbol) {
        String sql = "SELECT * FROM track_tbl WHERE symbol=? ORDER BY analyst ASC";
        RowMapper<TrackRec> rowMapper = new TrackRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper, symbol);
    }

    @Override
    public List<TrackRec> getAnalyst(String analyst) {
        String sql = "SELECT * FROM track_tbl WHERE analyst=? ORDER BY analyst ASC";
        RowMapper<TrackRec> rowMapper = new TrackRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper, analyst);
    }

    @Override
    public List<TrackRec> getAll() {
        String sql = "SELECT * FROM track_tbl  ORDER BY symbol ASC, analyst ASC";
        RowMapper<TrackRec> rowMapper = new TrackRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }
}

package com.jpw.raptor.testing;

import com.jpw.raptor.algorithm.DateConstants;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.track.TrackDAO;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.TrackRec;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TrackReport {


    public void runReport(QuoteDAO quoteTbl, TrackDAO trackTbl) throws java.io.IOException, java.text.ParseException {

        // Hard code runtime parameters
        SimpleDateFormat format  = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate =  format.parse("2020-12-24");
        String analyst = "all";

        // Worker routines for working with dates
        DateConstants dc = new DateConstants();

        // populate track map
        TrackMap trackMap = new TrackMap();
        trackMap.addRecs(trackTbl.getAll());

        // Get a list of recommendations to include in the report
        List<TrackRec> list = trackMap.getRecs();

        // Get the date for the report
        Quote forDate  = quoteTbl.getCurrent("SPY");

        // open the report file
        TrackReportUtil fileUtil = new TrackReportUtil();
        fileUtil.openResultsFile(analyst, forDate.getDate());

        // Format start date
        //SimpleDateFormat format  = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = format.format(startDate);

        // compute performance delta for each recommendation
        for ( TrackRec rec : list ) {
            Quote first = quoteTbl.get(rec.getSymbol().toUpperCase(), startDate);
            Quote last  = quoteTbl.getCurrent(rec.getSymbol().toUpperCase());
            double r = ((last.getClose() - first.getClose()) / first.getClose()) * 100.0;
            rec.setDelta(Math.round(r * 100.0) / 100.0);

            fileUtil.writeResultsFile(
                    rec.getSymbol() + "," +
                            rec.getCount()  + "," +
                            rec.getAnalyst() + "," +
                            rec.getName() + "," +
                            rec.getSector() + "," +
                            rec.getDelta()  + "," +
                            rec.getDate() + "," +
                            dateStr);
        }

        fileUtil.closeResultsFile();
    }
}

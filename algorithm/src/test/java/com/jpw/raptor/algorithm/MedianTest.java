package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Quote;
import org.junit.Assert;
import org.junit.Test;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MedianTest {

    /*
        258.05
        263.67
        270.43
        270.49
        270.95
        271.00
    */

    @Test
    public void test() throws ParseException {
        System.out.println("median test started");

        SimpleDateFormat sdf    = new SimpleDateFormat("yyyyMMdd");
        DecimalFormat    df     = new DecimalFormat("#.##");
        ArrayList<Quote> data   = new ArrayList(253);

        data.add(new Quote("SPY", sdf.parse("20180323"), 0.0, 0.0, 0.0, 258.05, 0));
        data.add(new Quote("SPY", sdf.parse("20180322"), 0.0, 0.0, 0.0, 263.67, 0));
        data.add(new Quote("SPY", sdf.parse("20180319"), 0.0, 0.0, 0.0, 271.00, 0));
        data.add(new Quote("SPY", sdf.parse("20180320"), 0.0, 0.0, 0.0, 270.95, 0));
        data.add(new Quote("SPY", sdf.parse("20180321"), 0.0, 0.0, 0.0, 270.43, 0));
        data.add(new Quote("SPY", sdf.parse("20180319"), 0.0, 0.0, 0.0, 270.49, 0));

        Median doit = new Median();

        double result;

        // odd
        double test1 = doit.medianQuote(data, 0, 5);
        Assert.assertEquals(270.43, test1, 0.001);

        // even
        double test2 = doit.medianQuote(data, 0, 6);
        result = (270.43 + 270.49) / 2.0;
        Assert.assertEquals(Double.parseDouble(df.format(result)), test2, 0.001);

        System.out.println("median  test finished");
    }

}

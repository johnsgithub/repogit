package com.jpw.raptor.jdbc.etf;

import com.jpw.raptor.model.Etf;

import java.util.List;

/**
 * Created by John on 10/1/2017.
 */
public interface EtfInterface {

    int sqlScript(String script);

    void addEmpty(String symbol, String name);

    int update(Etf rec);
    
    int delete(String symbol);
    
    Etf get(String symbol);

    List<Etf> getAll();

    List<Etf> getOwned();

    List<Etf> getTracked();
    
    List<Etf> getRelevant();

	List<Etf> getByAssetClass(String assetClass);
	 
	List<Etf> getByAssetClassFundType(String assetClass, String fundType);
	 
	List<Etf> getByAssetClassFundTypeSubType(String assetClass, String fundType, String subType);

	List<Etf> getByAssetClassFundTypeSubTypeFactor(String assetClass, String fundType, String subType, String factor);
	 
	List<Etf> getFactors();

    List<Etf> getFactorsAll();

	List<Etf> getFactorsByAssetClass(String assetClass);

    List<Etf> getFactorAndAssetClass(String assetClass, String assetFactor);

}

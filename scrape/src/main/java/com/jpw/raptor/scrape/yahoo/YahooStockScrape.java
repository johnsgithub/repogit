package com.jpw.raptor.scrape.yahoo;

import com.jpw.raptor.model.Stock;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class YahooStockScrape {

    public Stock scrape(Stock stock) throws IOException {

        YahooStockStatParse  parser = new YahooStockStatParse();

        // Read the yahoo stock page
        YahooStockStatistics yss = parser.readPage(stock.getSymbol());
        if (yss.isReadFailed() ) {
            // read failed
            logError(stock.getSymbol());
            return null;
        }

        LocalDate localDate = LocalDate.now();
        stock.setLastUpdate(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));

        // no source for the following data
        stock.setRecommendations(-1);
        stock.setStrongBuy(-1);
        stock.setBuy(-1);
        stock.setHold(-1);
        stock.setSell(-1);
        stock.setStrongSell(-1);

        // Update the stock record from the page read
        stock.setMarketCap(yss.getMarketCap());
        stock.setEnterpriseValue(yss.getEnterpriseValue());

        stock.setOperatingCashflow(-1);

        stock.setEbitda(yss.getEbitda());
        stock.setFreeCashflow(yss.getFreeCashFlow());
        stock.setTotalCash(yss.getTotalCash());
        stock.setTotalDebt(yss.getTotalDebt());
        stock.setTotalRevenue(yss.getRevenue());
        stock.setAvgDailyVol(yss.getAvgVol10Day());
        stock.setDividendRate(yss.getDivRateForward());
        stock.setDividendYield(yss.getDivYieldForward() * 100.0);
        stock.setBeta(yss.getBeta());
        stock.setPe(yss.getTrailingPE());
        stock.setPeForward(yss.getForwardPE());
        stock.setPs(yss.getPriceSales());
        stock.setPb(yss.getPriceBook());
        stock.setTrailingEps(yss.getDilutedEps());
        stock.setPegRatio(yss.getPegRatio());
        stock.setEnterpriseToRevenue(yss.getEvRevenue());
        stock.setEnterpriseToEbitda(yss.getEvEbitda());

        stock.setEbitdaMargins(-1.0);
        stock.setGrossMargins(-1.0);

        stock.setRevenueGrowth(yss.getRevenueGrowth() * 100.0);
        stock.setOperatingMargins(yss.getOperatingMargin() * 100.0);
        stock.setProfitMargins(yss.getProfitMargin() * 100.0);
        stock.setEarningsGrowth(yss.getEarningsGrowth() * 100.0);
        stock.setCurrentRatio(yss.getCurrentRatio());
        stock.setReturnOnAssets(yss.getReturnOnAssets() * 100.0);

        stock.setDebtToEquity(-1.0);

        stock.setReturnOnEquity(yss.getReturnOnEquity() * 100.0);
        stock.setTotalCashPerShare(yss.getTotalCashPerShare());

        return stock;
    }

    private void logError(String symbol) {
        
    }
}

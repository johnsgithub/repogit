package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Average;
import com.jpw.raptor.model.Quote;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Ported to new design 5/16/2019
 *
 * An exponential moving average (EMA) is a type of moving average (MA) that places a
 * greater weight and significance on the most recent data points.
 *
 * An exponentially weighted moving average reacts more significantly to recent price changes
 * than a simple moving average (SMA), which applies an equal weight to all observations in the period.
 *
 */
public class ExponentialMovingAverage {

    public double getExpAvg(double value, double priorExpAvg, double days) {

        double multiplier = 2.0 / (days + 1.0);
        double result1 = (value * multiplier) + (priorExpAvg * (1.0 - multiplier));

        /*
         *  note the two equations are the same
         *  (value * multiplier) + ( priorExpAvg * (1.0 - multiplier) )
         *  ( (value-priorExpAvg) * multiplier ) + (priorExpAvg)
         */

        // Round to 2 decimal places and return it
        return Math.round(result1 * 100.0) / 100.0;

    }


    // Quotes are in descending order by date
    public List<Average> generateEmaList(List<Quote> data, int days) {

        // allocate space for averages object list
        List<Average> avgList = new ArrayList<>(data.size());

        // populate the list with default data
        for ( Quote q : data ) {
            Average a = new Average();
            a.setSymbol(q.getSymbol());
            a.setDate(q.getDate());
            a.setAvgType(Average.AverageType.EMA);
            a.setDays(days);
            a.setVal(q.getClose());
            avgList.add(a);
        }

        // Since dates are descending start at the end of the array
        // and process to the front
        int start = data.size() - 1;

        // Note the first EMA is just the close which has already been set

        // create averages for each quote
        for ( int i=start -1; i>=0; i-- ) {

            // Init Average record
            Average curr  = avgList.get(i);
            Average prior = avgList.get(i + 1);

            // Generate Exponental moving averages
            curr.setVal(getExpAvg(curr.getVal(), prior.getVal(), (double) days));
        }

        return avgList;
    }
    
}

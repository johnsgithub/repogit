package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

@Getter
@Setter
public class YahooEtfMain {
    static final String SPAN_START = "<span";
    static final String SPAN_END = "</span>";
    static final String TD_START = "<td ";
    static final String TD_END = "</td>";
    static final String HREF_START = "<a href";
    static final String HREF_END = "</a>";
    static final String GREATER_THAN = ">";

    private long  averageVolume;
    private double  yield;
    private double  expenseRatio;

    private String  page;
    private boolean readFailed;

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public boolean readPage(String symbol) throws IOException {

        // Yahoo replaces . with -

        String ticker = symbol.toUpperCase().replace('.', '-');
        String baseUrl = "https://finance.yahoo.com/quote/" + ticker + "/";
        try {
            Document doc = Jsoup.connect(baseUrl).get();
            page = doc.toString();
        } catch ( org.jsoup.HttpStatusException ex ) {
            readFailed = true;
        }

        // Validate page was found
        if (readFailed || page.contains("<span>No results for")  || page.contains("<span>Symbols similar to")) {
            return false;
        }

        ValueObject vo = new ValueObject();

        //  Average Volume
        int averageVolumeLabelStart = page.indexOf("<span>Avg. Volume") + 1;
        int averageVolumeStartDataSpan = page.indexOf(TD_START, averageVolumeLabelStart) + 1;
        int averageVolumeStartData = page.indexOf(GREATER_THAN, averageVolumeStartDataSpan) + 1;
        int averageVolumeEndData = page.indexOf(TD_END, averageVolumeStartData);
        averageVolume = vo.getLong(page.substring(averageVolumeStartData, averageVolumeEndData));

        //  Yield
        int yieldLabelStart = page.indexOf("<span>Yield") + 1;
        int yieldStartDataSpan = page.indexOf(TD_START, yieldLabelStart) + 1;
        int yieldStartData = page.indexOf(GREATER_THAN, yieldStartDataSpan) + 1;
        int yieldEndData = page.indexOf(TD_END, yieldStartData);
        yield = vo.getPercent(page.substring(yieldStartData, yieldEndData));

        //  Expense Ratio
        int expenseRatioLabelStart = page.indexOf("<span>Expense Ratio") + 1;
        int expenseRatioStartTd = page.indexOf(TD_START, expenseRatioLabelStart) + 1;
        int expenseRatioStartData = page.indexOf(GREATER_THAN, expenseRatioStartTd) + 1;
        int expenseRatioEndData = page.indexOf(TD_END, expenseRatioStartData);
        expenseRatio = vo.getPercent(page.substring(expenseRatioStartData, expenseRatioEndData));

        return true;
    }

    public void print() {
        System.out.println("averageVolume      " + averageVolume);
        System.out.println("yield              " + yield);
        System.out.println("expenseRatio       " + expenseRatio);
    }

}

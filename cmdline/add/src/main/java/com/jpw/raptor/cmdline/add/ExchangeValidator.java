package com.jpw.raptor.cmdline.add;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class ExchangeValidator implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {
        if ( value.equalsIgnoreCase("nasdaq") ||
                value.equalsIgnoreCase("amex") ||
                value.equalsIgnoreCase("nyse") ||
                value.equalsIgnoreCase("index") ) {

        } else {
            throw new ParameterException(
                    "Exchange must be Nasdaq, Amex, Nyse or Index:  " + value );
        }
    }
}
package com.jpw.raptor.cmdline.track;

import com.jpw.raptor.model.TrackRec;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class TrackMapTest {

    @Test
    public void test01() throws java.text.ParseException {
        System.out.println("test 01 started");

        ArrayList<TrackRec> dbList = new ArrayList<>(
                Arrays.asList(
                        new TrackRec("sym1","ana1"),  // duplicate
                        new TrackRec("sym1","ana2"),  // duplicate
                        new TrackRec("sym1","ana3"),  // delete
                        new TrackRec("sym2","ana3"),  // duplicate
                        new TrackRec("sym2","ana2"),  // duplicate
                        new TrackRec("sym3","ana3")   // delete
                )
        );

        ArrayList<TrackRec> newList = new ArrayList<>(
                Arrays.asList(
                        new TrackRec("sym1","ana1"),  // duplicate
                        new TrackRec("sym1","ana2"),  // duplicate
                        new TrackRec("sym4","ana3"),  // New
                        new TrackRec("sym2","ana3"),  // duplicate
                        new TrackRec("sym2","ana2"),  // duplicate
                        new TrackRec("sym5","ana1")   // New
                )
        );

        TrackMap map = new TrackMap();
        map.trackDbRecords(dbList);
        map.newRecomendations(newList);

        List<TrackRec> toAdd = map.getTrackDbAdds();
        assertEquals(2, toAdd.size());
        assertTrue(toAdd.get(0).getSymbol().equalsIgnoreCase("sym4"));
        assertTrue(toAdd.get(0).getAnalyst().equalsIgnoreCase("ana3"));
        assertTrue(toAdd.get(1).getSymbol().equalsIgnoreCase("sym5"));
        assertTrue(toAdd.get(1).getAnalyst().equalsIgnoreCase("ana1"));


        List<TrackRec> toDel = map.getTrackDbDeletes();
        assertEquals(2, toDel.size());
        assertTrue(toDel.get(0).getSymbol().equalsIgnoreCase("sym1"));
        assertTrue(toDel.get(0).getAnalyst().equalsIgnoreCase("ana3"));
        assertTrue(toDel.get(1).getSymbol().equalsIgnoreCase("sym3"));
        assertTrue(toDel.get(1).getAnalyst().equalsIgnoreCase("ana3"));
    }

}

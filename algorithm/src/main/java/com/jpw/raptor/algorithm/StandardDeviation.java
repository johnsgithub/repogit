package com.jpw.raptor.algorithm;


import com.jpw.raptor.model.Macd;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Rp;

import java.text.DecimalFormat;
import java.util.List;


public class StandardDeviation {

    //
    // Compute number of elements to use for the mean
    // If enough data is provided then return sampleSize
    // If not enough data is provide then return arraySize minus offset
    //
    public int elementCount(int offset, int sampleSize, int arraySize) {
        int number = offset + sampleSize;
        if ( number > arraySize )
            return arraySize - offset;
        else
            return sampleSize;

    }

    // Data is in descending order by date
    public double quote(List<Quote> data, int offset, int sampleSize) {

        int    count  = elementCount(offset, sampleSize, data.size());
        int    end    = offset + count;

        double  result      = 0.0;

        // Compute the mean
        Mean   meanFactory = new Mean();
        double mean        = meanFactory.quote(data, offset, end);

        // Compute standard deviation
        double diff   = 0.0;
        double stdDev = 0.0;
        double period = 0.0;
        for ( int i=offset; i<end; i++){
            diff    = data.get(i).getClose() - mean;
            stdDev  = stdDev + (diff * diff);
            period += 1.0;
        }

        if ( period > 0.0 ) {
            result = Math.sqrt(stdDev / period);
        }

        // Round to 3 decimal places
        result = Math.round(result * 1000.0) / 1000.0;

        // return it
        return result;
    }


    // Data is in descending order by date
    public double rp(List<Rp> data, int offset, int sampleSize) {

        int    count  = elementCount(offset, sampleSize, data.size());
        int    end    = offset + count;

        double  result      = 0.0;

        // Compute the mean
        Mean   meanFactory = new Mean();
        double mean        = meanFactory.rp(data, offset, end);

        // Compute standard deviation
        double diff   = 0.0;
        double stdDev = 0.0;
        double period = 0.0;
        for ( int i=offset; i<end; i++){
            diff    = data.get(i).getVal() - mean;
            stdDev  = stdDev + (diff * diff);
            period += 1.0;
        }

        if ( period > 0.0 ) {
            result = Math.sqrt(stdDev / period);
        }

        // Round to 3 decimal places
        result = Math.round(result * 1000.0) / 1000.0;

        // return it
        return result;
    }


    // Data is in descending order by date
    public double macd(List<Macd> data, int offset, int sampleSize) {

        int    count  = elementCount(offset, sampleSize, data.size());
        int    end    = offset + count;

        double  result      = 0.0;

        // Compute the mean
        Mean   meanFactory = new Mean();
        double mean        = meanFactory.macd(data, offset, end);

        // Compute standard deviation
        double diff   = 0.0;
        double stdDev = 0.0;
        double period = 0.0;
        for ( int i=offset; i<end; i++){
            diff    = data.get(i).getMacd() - mean;
            stdDev  = stdDev + (diff * diff);
            period += 1.0;
        }

        if ( period > 0.0 ) {
            result = Math.sqrt(stdDev / period);
        }

        // Round to 3 decimal places
        result = Math.round(result * 1000.0) / 1000.0;

        // return it
        return result;
    }

}

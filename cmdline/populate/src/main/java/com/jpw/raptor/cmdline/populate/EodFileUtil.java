package com.jpw.raptor.cmdline.populate;

import com.jpw.raptor.lib.properties.FinanceProperties;
import com.jpw.raptor.model.FileQualified;

import java.io.File;
import java.util.*;

/**
 * Created by john on 5/15/18.
 */
public class EodFileUtil {

    public List<FileQualified> getFileList( String[] directories ) {

        // note each entry is a fully qualified director path
        // example: /home/finance/runtime/ingest

        // Create a map where the key is date and equity type
        // value is the full path
        Map<String, String> unsortedMap = new HashMap<>();

        // process each directory
        for ( String directory : directories ) {

            // add entries to the unsorted map
            // create a file that is really a directory
            File aDirectory = new File(directory);

            // get a listing of all files in the directory
            String[] filesInDir = aDirectory.list();

            // Populate map
            for ( String f : filesInDir ) {
                if ( f.endsWith(".txt") ) {
                    int dash = f.indexOf('_');
                    int period = f.indexOf('.');
                    String key = f.substring(dash + 1, period) + "_" + f.substring(0, dash);
                    String fileName = directory + "/" + f;
                    File dataFile = new File(fileName);
                    if (dataFile.isFile()) {
                        unsortedMap.put(key, fileName);
                    }
                }
            }
        }

        // Create a sorted map
        Map<String, String> treeMap = new TreeMap(unsortedMap);

        // create an output list
        List<FileQualified> list = new ArrayList<>();
        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            String type = entry.getKey().substring(entry.getKey().indexOf('_')+1);
            list.add(new FileQualified(type, entry.getValue()));
        }

        return list;
    }

}

package com.jpw.raptor.cmdline.reddit;

import com.jpw.raptor.model.RedditPost;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class RedditRssTest {

    @Test
    public void test01() throws Exception {
        System.out.println("test 01");

        RedditRss rss = new RedditRss();
        /*****************************
        List<RedditPost> list = rss.readRss("https://www.reddit.com/r/StockMarket");
        for ( RedditPost p : list ) {
            System.out.println("----------------------------" );
            System.out.println( "Type: " + p.getCType() + " " + p.getPostId() + " " + p.getTitle() );
            System.out.println("Author: " + p.getAuthor() + " " + p.getDate() );
            System.out.println( "Content: " + p.getContents() );
            System.out.println("++++++++++++++++++++++++++++" );
        }
         **************************************************************************/
    }

    @Test
    public void test02() throws Exception {
        System.out.println("test 02");

        RedditRss rss = new RedditRss();
        assertTrue(rss.stripLastSlash("https://www.reddit.com/r/StockMarket").equals("https://www.reddit.com/r/StockMarket"));
        assertTrue(rss.stripLastSlash("https://www.reddit.com/r/StockMarket/").equals("https://www.reddit.com/r/StockMarket"));
    }

    @Test
    public void test03() throws Exception {
        System.out.println("test 03");

        RedditRss rss = new RedditRss();
        assertTrue(rss.getCommunity("https://www.reddit.com/r/StockMarket").equals("StockMarket"));
        assertTrue(rss.getCommunity("https://www.reddit.com/r/StockMarket/").equals("StockMarket"));
    }

    @Test
    public void test04() throws Exception {
        System.out.println("test 04");
        String url = "https://www.reddit.com/r/StockMarket/comments/mxq8dj/a_message_for_hedge_funds/";

        RedditRss rss = new RedditRss();
        assertTrue(rss.getPostId(url).equals("https://www.reddit.com/r/StockMarket/comments/mxq8dj"));

    }

    @Test
    public void test05() throws Exception {
        System.out.println("test 05");

        String dateString = "Mon Apr 26 12:11:37 EDT 2021";

        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
        Date d1 = sdf.parse(dateString);

        System.out.println(d1);
    }
}

package com.jpw.raptor.algorithm.signals;

import com.jpw.raptor.model.Macd;

public class MacdSignalFactory {

    // summarize momentum based upon macd values
    public String macdMomentum (double macd, double signal) {

        String momentum = "";

        if (macd >= 0) {
            if (macd >= signal)
                momentum = "4 - Positive momentum";
            else
                momentum = "3 - Positive momentum reversing";
        } else {
            if (macd < signal)
                momentum = "1 - Negative momentum";
            else
                momentum = "2 - Negative momentum reversing";
        }

        return momentum;
    }

    // summarize momentum based upon macd values
    public String  macdSignal (Macd curr, Macd prior) {

    /*
      90 momentum above signal
      80 momentum cross above signal
      70 momentum cross below signal
      60 momentum below signal

      55 cross to up momentum
      45 cross to down momentum

      40 down momentum above signal
      30 down momentum cross above signal
      20 down momentum cross below signal
      10 down momentum below signal
     */

        String signal = "";

        if ( curr.getMacd() >= 0.0 && prior.getMacd() >= 0.0 ) {

            // up momentum
            if ( curr.getMacd() >= curr.getSignal() &&  prior.getMacd() >= prior.getSignal()) {
                // above signal
                signal = "90 momentum above signal " + curr.getHistogram();
            } else if ( curr.getMacd() >= curr.getSignal() &&  prior.getMacd() < prior.getSignal()) {
                // cross above signal
                signal = "80 momentum cross above signal " + curr.getHistogram();
            } else if ( curr.getMacd() < curr.getSignal() &&  prior.getMacd() >= prior.getSignal()) {
                // cross below signal
                signal = "70 momentum cross below signal " + curr.getHistogram();
            } else if ( curr.getMacd() < curr.getSignal() &&  prior.getMacd() < prior.getSignal()) {
                // below signal
                signal = "60 momentum below signal " + curr.getHistogram();
            }

        } else if ( curr.getMacd() < 0.0 && prior.getMacd() < 0.0 ) {

            // down momentum
            if ( curr.getMacd() >= curr.getSignal() &&  prior.getMacd() >= prior.getSignal()) {
                // above signal
                signal = "40 down momentum above signal " + curr.getHistogram();
            } else if ( curr.getMacd() >= curr.getSignal() &&  prior.getMacd() < prior.getSignal()) {
                // cross above signal
                signal = "30 down momentum cross above signal " + curr.getHistogram();
            } else if ( curr.getMacd() < curr.getSignal() &&  prior.getMacd() >= prior.getSignal()) {
                // cross below signal
                signal = "20 down momentum cross below signal " + curr.getHistogram();
            } else if ( curr.getMacd() < curr.getSignal() &&  prior.getMacd() < prior.getSignal()) {
                // below signal
                signal = "10 down momentum below signal " + curr.getHistogram();
            }

        } else if ( curr.getMacd() >= 0.0 && prior.getMacd() < 0.0 ) {

            // cross to up momentum
            if ( curr.getMacd() >= curr.getSignal() ) {
                signal = "55 cross to up momentum (above signal) " + curr.getHistogram();
            } else {
                signal = "55 cross to up momentum (below signal) " + curr.getHistogram();
            }
        } else if ( curr.getMacd() < 0.0 && prior.getMacd() >= 0.0 ) {

            // cross to down momentum
            if ( curr.getMacd() >= curr.getSignal() ) {
                signal = "45 cross to down momentum (above signal) " + curr.getHistogram();
            } else {
                signal = "45 cross to down momentum (above signal) " + curr.getHistogram();
            }
        }

        return signal;
    }

}

package com.jpw.raptor.jdbc.reddit;

import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.quote.QuoteInterface;
import com.jpw.raptor.jdbc.quote.QuoteRowMapper;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.RedditPost;
import org.apache.commons.math3.util.Precision;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import jakarta.annotation.PostConstruct;
import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class RedditPostDAO implements RedditPostInterface {

    //private static final Logger logger = LoggerFactory.getLogger(RedditPostDAO.class);

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private DataSource   dataSource;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @PostConstruct
    public void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int sqlScript(String script) {
        return jdbcTemplate.update(script);
    }

    @Override
    public void add(RedditPost rec) {

        // Normalize date
        SimpleDateFormat simpleDateFormat    = new SimpleDateFormat(DATE_FORMAT);
        String           dateString          = simpleDateFormat.format(rec.getDate());
        Date             date                = new Date(-1);
        try {
            date = simpleDateFormat.parse(dateString);
        } catch ( java.text.ParseException ex) {
            System.out.println ("RedditPost add date format exception");
        }

        String sql = "INSERT INTO reddit_post_tbl (date_tx, stock, community, post_id, author, title, ctype, contents) values (?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            jdbcTemplate.update(sql, date, rec.getStock(), rec.getCommunity(), rec.getPostId(), rec.getAuthor(), rec.getTitle(), rec.getCType(), rec.getContents());
        } catch ( org.springframework.dao.DuplicateKeyException ex ) {
            System.out.println ("Post add failed " );
        }
    }

    @Override
    public void delete(long rowId) {

        String sql = "DELETE FROM reddit_post_tbl where row_id=?";
        jdbcTemplate.update(sql, rowId);
    }

    @Override
    public void deleteAll() {
        String sql = "DELETE FROM reddit_post_tbl";
        jdbcTemplate.update(sql);
    }

    @Override
    public RedditPost get(long rowId) {

        String sql = "SELECT * FROM reddit_post_tbl WHERE row_id=?";
        RowMapper<RedditPost> rowMapper = new RedditPostRowMapper();
        //return this.jdbcTemplate.queryForObject(sql, rowMapper, symbol, date_normal);
        List<RedditPost> results = this.jdbcTemplate.query(sql, rowMapper, rowId);
        if ( results.size() > 0 )
            return results.get(0);
        else
            return null;
    }

    @Override
    public List<RedditPost> getAll() {
        String sql = "SELECT * FROM reddit_post_tbl ORDER BY stock ASC, date_tx DESC";
        RowMapper<RedditPost> rowMapper = new RedditPostRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    public List<RedditPost> getStock(String symbol) {
        String sql = "SELECT * FROM reddit_post_tbl WHERE stock=? ORDER BY stock ASC, date_tx DESC";

        RowMapper<RedditPost> rowMapper = new RedditPostRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper, symbol);
    }

    @Override
    public List<RedditPost> getCommunity(String symbol) {
        String sql = "SELECT * FROM reddit_post_tbl WHERE community=? ORDER BY stock ASC, date_tx DESC";

        RowMapper<RedditPost> rowMapper = new RedditPostRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper, symbol);
    }

    @Override
    public List<RedditPost> getPost(String symbol) {
        String sql = "SELECT * FROM reddit_post_tbl WHERE post_id=? ORDER BY stock ASC, date_tx DESC";

        RowMapper<RedditPost> rowMapper = new RedditPostRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper, symbol);
    }
}

package com.jpw.raptor.cmdline.web.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jpw.raptor.model.HoldingModel;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class HoldingsModelService {

    public List<HoldingModel> format(String holdings) {

        // Get the holdings
        Gson gson = new GsonBuilder().setLenient().create();
        HoldingModel[] topHoldings = gson.fromJson(holdings, HoldingModel[].class);

        // Convert to list
        List<HoldingModel> list = Arrays.asList(topHoldings);

        // Format percent
        for ( HoldingModel holding : topHoldings ) {
            double temp = holding.getHoldingPercent() * 100.0;
            holding.setHoldingPercent( Math.round(temp * 100.0) / 100.0 );
        }

        return list;
    }
}

package com.jpw.raptor.cmdline.add;

import com.beust.jcommander.Parameter;

/**
 * Created by john on 5/3/18.
 */
public class AppParameters {

    @Parameter(names = "-type",
            validateWith = TypeValidator.class,
            description = "ETF, Stock or Index",
            required = true)
    private String type;

    public String getType() {
        return type;
    }

    @Parameter(names = "-exchange",
            validateWith = ExchangeValidator.class,
            description = "Nasdaq, NYSE, Amex or Index",
            required = true)
    private String exchange;

    public String getExchange() {
        return exchange;
    }

    @Parameter(names = "-symbol",
            description = "Equity symbol",
            required = true)
    private String symbol;

    public String getSymbol() {
        return symbol;
    }

    @Parameter(names = "-name",
            description = "Equity name",
            required = false)
    private String name;

    public String getName() {
        return name;
    }
}

package com.jpw.raptor.cmdline.reddit;

import com.jpw.raptor.jdbc.reddit.RedditPostDAO;
import com.jpw.raptor.jdbc.reddit.RedditSummDAO;
import com.jpw.raptor.lib.properties.FinanceProperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class RedditApplication implements CommandLineRunner {


    @Autowired
    public RedditPostDAO tbl;

    @Autowired
    public RedditSummDAO summTbl;

    // Main loop
    @Override
    public void run(String... args) throws Exception {

        System.out.println("***************  Hello     *******************");
        RedditPopulateDB app = new RedditPopulateDB();
        app.populateDb(tbl, summTbl);
        System.out.println("***************  Good bye  *******************");
        System.out.println();

    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(RedditApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit( app.run(args) ) );
    }

}

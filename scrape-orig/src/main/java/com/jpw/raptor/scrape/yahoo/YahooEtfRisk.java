package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

@Getter
@Setter
public class YahooEtfRisk {
    static final String SPAN_START = "<span";
    static final String SPAN_END = "</span>";
    static final String TD_START = "<td ";
    static final String TD_END = "</td>";
    static final String HREF_START = "<a href";
    static final String HREF_END = "</a>";
    static final String GREATER_THAN = ">";

    private double  alpha;
    private double  beta;
    private double  annualReturn;
    private double  rSquared;
    private double  standardDeviation;
    private double  sharpeRatio;
    private double  treynorRatio;

    private String  page;
    private boolean readFailed;
    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public boolean readPage(String symbol) throws IOException {

        // Yahoo replaces . with -

        String ticker = symbol.toUpperCase().replace('.', '-');
        String baseUrl = "https://finance.yahoo.com/quote/" + ticker + "/risk?p=" + ticker;
        try {
            Document doc = Jsoup.connect(baseUrl).get();
            page = doc.toString();
        } catch ( org.jsoup.HttpStatusException ex ) {
            readFailed = true;
        }

        // Validate page was found
        if (readFailed || page.contains("<span>No results for")  || page.contains("<span>Symbols similar to")) {
            return false;
        }

        ValueObject vo = new ValueObject();

        //  Alpha
        int alphaLabelStart = page.indexOf("<span>Alpha") + 1;
        int alphaStartDataSpan = page.indexOf(SPAN_START, alphaLabelStart) + 1;
        int alphaStartData = page.indexOf(GREATER_THAN, alphaStartDataSpan) + 1;
        int alphaEndData = page.indexOf(SPAN_END, alphaStartData);
        alpha = vo.getDouble(page.substring(alphaStartData, alphaEndData));

        //  BETA
        int betaLabelStart = page.indexOf("<span>BETA") + 1;
        int betaStartDataSpan = page.indexOf(SPAN_START, betaLabelStart) + 1;
        int betaStartData = page.indexOf(GREATER_THAN, betaStartDataSpan) + 1;
        int betaEndData = page.indexOf(SPAN_END, betaStartData);
        beta = vo.getDouble(page.substring(betaStartData, betaEndData));

        //  Mean Annual Return
        int annualReturnLabelStart = page.indexOf("<span>Mean Annual Return") + 1;
        int annualReturnStartDataSpan = page.indexOf(SPAN_START, annualReturnLabelStart) + 1;
        int annualReturnStartData = page.indexOf(GREATER_THAN, annualReturnStartDataSpan) + 1;
        int annualReturnEndData = page.indexOf(SPAN_END, annualReturnStartData);
        annualReturn = vo.getDouble(page.substring(annualReturnStartData, annualReturnEndData));

        //  R-squared
        int rSquaredLabelStart = page.indexOf("<span>R-squared") + 1;
        int rSquaredStartDataSpan = page.indexOf(SPAN_START, rSquaredLabelStart) + 1;
        int rSquaredStartData = page.indexOf(GREATER_THAN, rSquaredStartDataSpan) + 1;
        int rSquaredEndData = page.indexOf(SPAN_END, rSquaredStartData);
        rSquared = vo.getDouble(page.substring(rSquaredStartData, rSquaredEndData));

        //  Standard Deviation
        int standardDeviationLabelStart = page.indexOf("<span>Standard Deviation") + 1;
        int standardDeviationStartDataSpan = page.indexOf(SPAN_START, standardDeviationLabelStart) + 1;
        int standardDeviationStartData = page.indexOf(GREATER_THAN, standardDeviationStartDataSpan) + 1;
        int standardDeviationEndData = page.indexOf(SPAN_END, standardDeviationStartData);
        standardDeviation = vo.getDouble(page.substring(standardDeviationStartData, standardDeviationEndData));

        //  Sharpe Ratio
        int sharpeRatioLabelStart = page.indexOf("<span>Sharpe Ratio") + 1;
        int sharpeRatioStartDataSpan = page.indexOf(SPAN_START, sharpeRatioLabelStart) + 1;
        int sharpeRatioStartData = page.indexOf(GREATER_THAN, sharpeRatioStartDataSpan) + 1;
        int sharpeRatioEndData = page.indexOf(SPAN_END, sharpeRatioStartData);
        sharpeRatio = vo.getDouble(page.substring(sharpeRatioStartData, sharpeRatioEndData));

        //  Treynor Ratio
        int treynorRatioLabelStart = page.indexOf("<span>Treynor Ratio") + 1;
        int treynorRatioStartDataSpan = page.indexOf(SPAN_START, treynorRatioLabelStart) + 1;
        int treynorRatioStartData = page.indexOf(GREATER_THAN, treynorRatioStartDataSpan) + 1;
        int treynorRatioEndData = page.indexOf(SPAN_END, treynorRatioStartData);
        treynorRatio = vo.getDouble(page.substring(treynorRatioStartData, treynorRatioEndData));

        return true;
    }


    public void print() {
        System.out.println("alpha              " + alpha);
        System.out.println("beta               " + beta);
        System.out.println("annualReturn       " + annualReturn);
        System.out.println("rSquared           " + rSquared);
        System.out.println("standardDeviation  " + standardDeviation);
        System.out.println("sharpeRatio        " + sharpeRatio);
        System.out.println("treynorRatio       " + treynorRatio);
    }

}

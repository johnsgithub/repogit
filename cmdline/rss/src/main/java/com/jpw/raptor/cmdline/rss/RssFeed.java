package com.jpw.raptor.cmdline.rss;

import com.jpw.raptor.jdbc.rss.RssDAO;
import com.jpw.raptor.model.RssEntry;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import org.apache.commons.codec.digest.DigestUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RssFeed {

    public List<RssEntry> getEntries(String urlString, Date today) throws Exception {

        URL           feedUrl = new URL(urlString);
        SyndFeedInput input   = new SyndFeedInput();
        SyndFeed      feed    = input.build(new XmlReader(feedUrl));

        // Read the feed entries
        java.util.List<SyndEntry> entries = feed.getEntries();

        // allocate space for return results
        ArrayList<RssEntry> results = new ArrayList(entries.size());

        // process each entry
        for ( SyndEntry entry : entries ) {
            // Generate unique key for all feeds
            String hash = DigestUtils.md5Hex(entry.getUri()).toUpperCase();

            // generate publish date if one is not provided
            if (entry.getPublishedDate() == null) {
                entry.setPublishedDate(new Date()) ;
            }

            // add entries to list
            if ( entry.getPublishedDate().before(today) ) {
                // ignore entries prior to today's date
            } else {
                results.add(new RssEntry(hash, entry.getPublishedDate(), entry.getTitle(), entry.getLink(), entry.getUri()));
            }
            //System.out.println("*****************************************")
            //System.out.println("Title   :" + entry.getTitle())
            //System.out.println("Link    :" + entry.getLink())
            //System.out.println("Pub dte :" + entry.getPublishedDate())
            //System.out.println("Uri     :" + entry.getUri())
            //System.out.println("MD5     :" + DigestUtils.md5Hex(entry.getUri()).toUpperCase())
            //System.out.println("");
        }

        return results;
    }


    public List<RssEntry> removeDuplicates(RssDAO db, List<RssEntry> listWithDups )  {

        // allocate space for return results
        ArrayList<RssEntry> results = new ArrayList(listWithDups.size());

        // process each entry
        for ( RssEntry entry : listWithDups ) {

            // Read database to see if entry exists
            RssEntry rec = db.get(entry.getHash());

            // if record is not found add to list
            if (rec == null) {
                results.add(entry);
            }
        }

        return results;
    }


    public void addToDb(RssDAO db, List<RssEntry> entries )  {
        // process each entry and write to the database
        for ( RssEntry entry : entries ) {
            db.add(entry);
        }

    }
}

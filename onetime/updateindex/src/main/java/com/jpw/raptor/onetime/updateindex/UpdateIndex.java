package com.jpw.raptor.onetime.updateindex;

import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.model.Stock;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UpdateIndex {


    private StockDAO stockTbl;

    public UpdateIndex( StockDAO stock ) {
        stockTbl = stock;
    }

    public void updateSp ( String fileName, String index ) throws IOException {

        List<Entry> entries = getStocks(fileName);

        for ( Entry entry : entries ) {
            // Check to see if stock is in the table
            Stock stock = stockTbl.get(entry.getSymbol());
            if (stock != null) {
                stock.setSpIndex( index );
                stockTbl.update( stock );
            }
        }
    }

    public void updateRussell ( String fileName, String index ) throws IOException {

        List<Entry> entries = getStocks(fileName);

        for ( Entry entry : entries ) {
            // Check to see if stock is in the table
            Stock stock = stockTbl.get(entry.getSymbol());
            if (stock != null) {
                stock.setRussellIndex( index );
                stockTbl.update( stock );
            }
        }
    }

    public List<Entry> getStocks(String fileName) throws IOException {

        ArrayList<Entry> list = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader(fileName));

        String line = null;

        // Read each record in the file
        while ( (line = br.readLine()) != null ) {
            String[] str = line.split(",");
            if ( str.length >=3 ) {
                // Valid number of columns
                Entry entry = new Entry();
                entry.setSymbol(str[0]);
                entry.setName(str[1]);
                entry.setSector(str[2]);
                list.add(entry);
            }
        }

        br.close();
        return list;
    }

}

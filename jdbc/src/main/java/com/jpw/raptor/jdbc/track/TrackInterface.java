package com.jpw.raptor.jdbc.track;

import com.jpw.raptor.model.TrackRec;

import java.util.Date;
import java.util.List;

public interface TrackInterface {

    public int sqlScript(String script);

    public void add(TrackRec rec);

    public void update(TrackRec rec);

    public void delete(String symbol, String analyst);

    public void deleteSymbol(String symbol);

    public void deleteAnalyst(String analyst);

    public TrackRec get(String symbol, String analyst);

    public List<TrackRec> getSymbol(String symbol);

    public List<TrackRec> getAnalyst(String symbol);

    public List<TrackRec> getAll();
}

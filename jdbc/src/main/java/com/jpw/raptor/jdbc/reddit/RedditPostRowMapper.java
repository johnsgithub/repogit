package com.jpw.raptor.jdbc.reddit;

import com.jpw.raptor.model.RedditPost;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RedditPostRowMapper implements RowMapper<RedditPost> {

    //private static final Logger logger = LoggerFactory.getLogger(RedditPostRowMapper.class);

    @Override
    public RedditPost mapRow(ResultSet rs, int rowNum) throws SQLException {

        RedditPost rec = new RedditPost();
        rec.setRowId(rs.getLong("row_id"));
        rec.setDate(rs.getDate("date_tx"));
        rec.setStock(rs.getString("stock"));
        rec.setCommunity(rs.getString("community"));
        rec.setPostId(rs.getString("post_id"));
        rec.setAuthor(rs.getString("author"));
        rec.setTitle(rs.getString("title"));
        rec.setCType(rs.getString("ctype"));
        rec.setContents(rs.getString("contents"));

        return rec;
    }
}

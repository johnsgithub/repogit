package com.jpw.raptor.cmdline.reddit;

import com.jpw.raptor.lib.properties.FinanceProperties;

import java.io.FileWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class RedditFile {

    Writer fileWriter;

    public void openResultsFile() throws java.io.IOException {

        // import directory
        FinanceProperties fp   = new FinanceProperties();
        Properties prop = fp.get();

        String dir  = prop.getProperty("reddit_dir");
        if ( dir == null )
        {
            System.out.println("Reddit directory not specified");
            return;
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String strDate = dateFormat.format(new Date());
        fileWriter = new FileWriter(dir + "/" + strDate + ".csv");

    }

    public void closeResultsFile() throws java.io.IOException {
        fileWriter.flush();
        fileWriter.close();
    }

    public void writeResultsFile(String line) throws java.io.IOException {
        fileWriter.write(line + "\n");
    }
}

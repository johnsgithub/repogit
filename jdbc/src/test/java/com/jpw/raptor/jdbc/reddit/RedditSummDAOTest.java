package com.jpw.raptor.jdbc.reddit;

import com.jpw.raptor.model.RedditPost;
import com.jpw.raptor.model.RedditSumm;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.core.io.ClassPathResource;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RedditSummDAOTest {

    RedditSummDAO tbl;

    RedditSumm  r1;

    Date        date;
    Date        d1;

    String      dateString;

    static class PostgresResource  {

        // postgres:10:11

        PostgreSQLContainer postgreSQLContainer;
        PGSimpleDataSource dataSource;

        public PostgresResource() {
            postgreSQLContainer = new PostgreSQLContainer("postgres:10.11")
                    .withDatabaseName("test-jdbc")
                    .withUsername("sa")
                    .withPassword("sa");

            postgreSQLContainer.start();

            dataSource = new PGSimpleDataSource();
            dataSource.setDatabaseName(postgreSQLContainer.getDatabaseName());
            dataSource.setServerName(postgreSQLContainer.getContainerIpAddress());
            dataSource.setPortNumber(postgreSQLContainer.getMappedPort(5432));
            dataSource.setUser(postgreSQLContainer.getUsername());
            dataSource.setPassword(postgreSQLContainer.getPassword());

        }

        public void stop() { postgreSQLContainer.stop(); }

        public PGSimpleDataSource getDataSource () {return dataSource;}

    }

    private static RedditSummDAOTest.PostgresResource myPostgresResource;

    @BeforeClass
    public static void setUp() throws java.text.ParseException {
        myPostgresResource = new RedditSummDAOTest.PostgresResource();
    }

    @Before
    public void setUpTest() throws java.text.ParseException {

        // Create test fields
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        d1 = sdf.parse("20171101");


        // Create test record
        r1 = new RedditSumm(d1,"stock",5,"name");

    }

    @AfterClass
    public static void tearDown(){
        myPostgresResource.stop();
    }


    @Test
    public void test01() throws Exception {

        System.out.println("test01");

        // Create connection to docker database
        tbl = new RedditSummDAO();
        tbl.setDataSource(myPostgresResource.getDataSource());
        tbl.postConstruct();

        // Create table for testing
        File resource;
        String cmds;

        resource = new ClassPathResource("Reddit_summ.sql").getFile();
        cmds = new String(Files.readAllBytes(resource.toPath()));
        tbl.sqlScript(cmds);

        // Write 1 record
        tbl.add(r1);

        // read all records only 1 should be found
        List<RedditSumm> recs = tbl.getAll();

        assertEquals(1,recs.size());

        RedditSumm read1 = recs.get(0);
        assertEquals(d1.compareTo(read1.getDate()),0);
        assertTrue(r1.getStock().equalsIgnoreCase(read1.getStock()));
        assertEquals(5, r1.getNum());
        assertTrue(r1.getName().equalsIgnoreCase(read1.getName()));

        // Delete all records
        tbl.deleteAll();

        // Verify no records remain
        List<RedditSumm> empty = tbl.getAll();

        assertEquals(0,empty.size());
    }
}

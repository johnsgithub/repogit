package com.jpw.raptor.cmdline.track;

import com.jpw.raptor.model.TrackRec;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TrackMap {

    protected TreeMap<String, TrackRec> map;
    protected List<TrackRec>            toAdd;

    public TrackMap() {
        map      = new TreeMap<String, TrackRec>();
        toAdd    = new ArrayList<TrackRec>(250);
    }

    String getKey(TrackRec rec) {
        return rec.getSymbol() + ":" + rec.getAnalyst();
    }

    public void trackDbRecords(List<TrackRec> list) {

        // Create a map for all of the database records
        for (TrackRec rec : list) {
            map.put(getKey(rec), rec);
        }
    }

    public void newRecomendations(List<TrackRec> list) {

        for (TrackRec rec : list) {
            // If the record is in the map then
            // delete the record from the map
            // other wise add the record to the toAdd list
            if ( map.containsKey(getKey(rec)) ) {
                map.remove(getKey(rec));
            } else {
                toAdd.add(rec);
            }
        }
    }

    public List<TrackRec> getTrackDbAdds() {
        return toAdd;
    }

    public List<TrackRec> getTrackDbDeletes() {
        // any entries remaining in the map are to be deleted from
        // the database since they are no longer recommended
        List<TrackRec> toDelete = new ArrayList<TrackRec>(map.size());
        for (Map.Entry<String, TrackRec> entrySetVal : map.entrySet()) {
            TrackRec entry = entrySetVal.getValue();
            toDelete.add(entry);
        }

        return toDelete;
    }
  
}

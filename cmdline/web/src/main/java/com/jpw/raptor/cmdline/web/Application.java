package com.jpw.raptor.cmdline.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class Application 
{


    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        SpringApplication application = new SpringApplication(Application.class);


        application.run(args);

        System.out.println( "Goodbye World!" );
    }
}

package com.jpw.raptor.algorithm.signals;

import com.jpw.raptor.model.Adx;

public class RsiSignalFactory {

    public RsiSignalFactory() {}

    // summarize the trend based upon Average Directional index values

    public String signal(double current, double prior) {

        String signal = "Normal";

        if ( prior <  70.0 && current >= 70.0 ) signal = "Cross to overbought";
        if ( prior >= 70.0 && current <  70.0 ) signal = "Cross from overbought";
        if ( prior >= 70.0 && current >= 70.0 ) signal = "Overbought";

        if ( prior >  30.0 && current <= 30.0 ) signal = "Cross to oversold";
        if ( prior <= 30.0 && current >  30.0 ) signal = "Cross from oversold";
        if ( prior <= 30.0 && current <= 30.0 ) signal = "Oversold";

        return signal;
    }
}

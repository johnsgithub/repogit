package com.jpw.raptor.search.finance;

import co.elastic.clients.elasticsearch._types.query_dsl.*;
import org.javatuples.Pair;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.SourceConfig;
import co.elastic.clients.elasticsearch.indices.CloseIndexRequest;
import co.elastic.clients.elasticsearch.indices.CloseIndexResponse;
import co.elastic.clients.elasticsearch.indices.OpenRequest;
import co.elastic.clients.elasticsearch.indices.OpenResponse;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.jpw.raptor.model.FinanceKnowledge;
import com.jpw.raptor.model.FinanceKnowledgeModel;
import com.jpw.raptor.model.RaptorConstants;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.jpw.raptor.model.RaptorConstants.SEARCH_ANALYZER;

@Service
public class KnowledgeSearch {

    private ElasticsearchClient client;

    private String elasticIndex;

    //private static final Logger logger      = LoggerFactory.getLogger(ElasticSearch.class);

    static final int MAX_RESULTS = 100;

    // Constructor
    public KnowledgeSearch() {

        // Create the low-level client
        RestClient restClient = RestClient.builder(
                new HttpHost("localhost", 9200)).build();

        // Create the transport with a Jackson mapper
        ElasticsearchTransport transport = new RestClientTransport(
                restClient, new JacksonJsonpMapper());

        // And create the API client
        client = new ElasticsearchClient(transport);
    }


    public void openIndex(String index) throws IOException {

        OpenRequest req = new OpenRequest.Builder()
                .index(index)
                .build();

        OpenResponse rsp = client.indices().open(req);


        elasticIndex = index;
        //System.out.println(rsp.toString());

    }

    public void closeIndex() throws IOException {

        CloseIndexRequest req = new CloseIndexRequest.Builder()
                .index(elasticIndex)
                .build();

        CloseIndexResponse rsp = client.indices().close(req);
        //System.out.println(rsp.toString());

    }

    public void deleteDocument(String docId) throws IOException {

        DeleteResponse rsp = client.delete(deleteRequestBuilder -> deleteRequestBuilder
                .index(elasticIndex)
                .id(docId)
                .timeout(s -> s .time("1s"))
        );

        //System.out.println(rsp.toString());
    }

    public void indexDocument(FinanceKnowledge rec) throws IOException {

        rec.setScore(0.0);
        IndexResponse response = client.index(IndexRequest.of(indexRequestBuilder -> indexRequestBuilder
                .index(elasticIndex)
                .id(rec.getId())
                .document(rec))
        );

        //System.out.println("Index result " + response.result() + "  version " + response.version());

    }

    public FinanceKnowledge getDocument(String docId) throws IOException {

        GetResponse<FinanceKnowledge> rsp = client.get(getRequestBuilder -> getRequestBuilder
                        .index(elasticIndex)
                        .id(docId),
                //.sourceIncludes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                //.sourceExcludes(RaptorConstants.FIELD_BODY),
                FinanceKnowledge.class
        );

        //System.out.println(rsp.toString());
        if (rsp.found()) {
            FinanceKnowledge rec = rsp.source();
            return rec;
        } else {
            return null;
        }
    }


    public List<FinanceKnowledge> getAll() throws IOException {

        SearchResponse<FinanceKnowledge> responses = client.search(searchReqBuilder -> searchReqBuilder
                        // index to search
                        .index(elasticIndex)
                        // fields to return from matching documents
                        .source(sourceConfigBuilder -> sourceConfigBuilder
                                .filter(sourceFilterBuilder -> sourceFilterBuilder
                                        .includes(RaptorConstants.FIELD_ID, RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                                        .excludes(RaptorConstants.FIELD_BODY)
                                ))
                        // wait time for a shards response
                        .timeout("1s")
                        // this is not needed since the default score sort is desc and match all query every document has a 1 score
                        .sort(sortOptionsBuilder -> sortOptionsBuilder .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)))
                        .size(MAX_RESULTS)
                        .query(MatchAllQuery.of(matchAllQueryBuilder -> matchAllQueryBuilder )._toQuery()),
                FinanceKnowledge.class
        );

        return generateList(responses);

    }

    public List<FinanceKnowledge> phraseQuery(String searchText, int slop) throws IOException {


        // match documents with one or more words
        Query query = MultiMatchQuery.of(multiMatchQueryBuilder -> multiMatchQueryBuilder
                .fields(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_TAG, RaptorConstants.FIELD_BODY)
                .query(searchText)
                .analyzer(SEARCH_ANALYZER)
                .type(TextQueryType.Phrase)
                .slop(slop)
                .operator(Operator.And)
        )._toQuery();

        SearchResponse<FinanceKnowledge> responses = client.search(searchReqBuilder -> searchReqBuilder
                        // index to search
                        .index(elasticIndex)
                        // fields to return from matching documents
                        .source(sourceConfigBuilder -> sourceConfigBuilder
                                .filter(sourceFilterBuilder -> sourceFilterBuilder
                                        .includes(RaptorConstants.FIELD_ID, RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                                        .excludes(RaptorConstants.FIELD_BODY)
                                ))
                        // wait time for a shards response
                        .timeout("1s")
                        // this is not needed since the default score sort is desc and match all query every document has a 1 score
                        .sort(sortOptionsBuilder -> sortOptionsBuilder .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)))
                        .size(MAX_RESULTS)
                        .query(query),
                FinanceKnowledge.class
        );

        return generateList(responses);
    }


    public List<FinanceKnowledge> allWordsQuery(String searchText) throws IOException {

        // match documents with one or more words
        Query query = MultiMatchQuery.of(multiMatchQueryBuilder -> multiMatchQueryBuilder
                .fields(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_TAG, RaptorConstants.FIELD_BODY)
                .query(searchText)
                .analyzer(SEARCH_ANALYZER)
                .operator(Operator.And)
        )._toQuery();

        SearchResponse<FinanceKnowledge> responses = client.search(searchReqBuilder -> searchReqBuilder
                        // index to search
                        .index(elasticIndex)
                        // fields to return from matching documents
                        .source(sourceConfigBuilder -> sourceConfigBuilder
                                .filter(sourceFilterBuilder -> sourceFilterBuilder
                                        .includes(RaptorConstants.FIELD_ID, RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                                        .excludes(RaptorConstants.FIELD_BODY)
                                ))
                        // wait time for a shards response
                        .timeout("1s")
                        // this is not needed since the default score sort is desc and match all query every document has a 1 score
                        .sort(sortOptionsBuilder -> sortOptionsBuilder .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)))
                        .size(MAX_RESULTS)
                        .query(query),
                FinanceKnowledge.class
        );

        return generateList(responses);
    }


    public List<FinanceKnowledge> wordsQuery(String searchText) throws IOException {

        // match documents with one or more words
        Query query = MultiMatchQuery.of(multiMatchQueryBuilder -> multiMatchQueryBuilder
                .fields(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_TAG, RaptorConstants.FIELD_BODY)
                .query(searchText)
                .analyzer(SEARCH_ANALYZER)
                .operator(Operator.Or)
        )._toQuery();

        SearchResponse<FinanceKnowledge> responses = client.search(searchReqBuilder -> searchReqBuilder
                        // index to search
                        .index(elasticIndex)
                        // fields to return from matching documents
                        .source(sourceConfigBuilder -> sourceConfigBuilder
                                .filter(sourceFilterBuilder -> sourceFilterBuilder
                                        .includes(RaptorConstants.FIELD_ID, RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                                        .excludes(RaptorConstants.FIELD_BODY)
                                ))
                        // wait time for a shards response
                        .timeout("1s")
                        // this is not needed since the default score sort is desc and match all query every document has a 1 score
                        .sort(sortOptionsBuilder -> sortOptionsBuilder .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)))
                        .size(MAX_RESULTS)
                        .query(query),
                FinanceKnowledge.class
        );

        return generateList(responses);
    }

    public List<FinanceKnowledge> generateList(SearchResponse<FinanceKnowledge> responses) {

        // allocate space for results
        ArrayList<FinanceKnowledge> results = new ArrayList<>(responses.hits().hits().size());

        for (Hit<FinanceKnowledge> hit: responses.hits().hits()) {
            FinanceKnowledge doc = hit.source();
            doc.setScore( hit.score() );
            results.add(doc);

            //System.out.println( "************");
            //System.out.println( "id     " + doc.getId());
            //System.out.println( "score  " + doc.getScore());
            //System.out.println( "************");
        }

        return results;

    }
}

package com.jpw.raptor.jdbc.track;

import com.jpw.raptor.model.TrackRec;
import org.junit.*;
import org.junit.runner.RunWith;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TrackData {

    private static final SimpleDateFormat sdf     = new SimpleDateFormat("yyyyMMdd");
    private static final String           date01  = "20210101";
    private static final String           date02  = "20210102";
    private static final String           date03  = "20210103";
    private static final String           date04  = "20210104";
    private static final String           date05  = "20210105";
    private static final String           datenew = "20210106";

    TrackRec getRec01() throws java.text.ParseException {
        return  new TrackRec ("sym01", "analyst01", "name01", "sector01",
                sdf.parse(date01), 1.01);
    }

    void validateRec01 (TrackRec r) throws java.text.ParseException {
        assertTrue(r.getSymbol().equalsIgnoreCase("sym01"));
        assertTrue(r.getAnalyst().equalsIgnoreCase("analyst01"));
        assertTrue(r.getName().equalsIgnoreCase("name01"));
        assertTrue(r.getSector().equalsIgnoreCase("sector01"));
        assertEquals(r.getDate().compareTo(sdf.parse(date01)),0);
        assertEquals(r.getDelta(),1.01,0.001);
    }

    TrackRec getRec02() throws java.text.ParseException {
        return  new TrackRec ("sym02", "analyst02", "name02", "sector02",
                sdf.parse(date02), 2.02);
    }

    void validateRec02 (TrackRec r) throws java.text.ParseException {
        assertTrue(r.getSymbol().equalsIgnoreCase("sym02"));
        assertTrue(r.getAnalyst().equalsIgnoreCase("analyst02"));
        assertTrue(r.getName().equalsIgnoreCase("name02"));
        assertTrue(r.getSector().equalsIgnoreCase("sector02"));
        assertEquals(r.getDate().compareTo(sdf.parse(date02)),0);
        assertEquals(r.getDelta(),2.02,0.001);
    }


    TrackRec getRec03() throws java.text.ParseException {
        return  new TrackRec ("sym03", "analyst03", "name03", "sector03",
                sdf.parse(date03), 3.03);
    }

    void validateRec03 (TrackRec r) throws java.text.ParseException {
        assertTrue(r.getSymbol().equalsIgnoreCase("sym03"));
        assertTrue(r.getAnalyst().equalsIgnoreCase("analyst03"));
        assertTrue(r.getName().equalsIgnoreCase("name03"));
        assertTrue(r.getSector().equalsIgnoreCase("sector03"));
        assertEquals(r.getDate().compareTo(sdf.parse(date03)),0);
        assertEquals(r.getDelta(),3.03,0.001);
    }


    TrackRec getRec04() throws java.text.ParseException {
        return  new TrackRec ("sym01", "analyst02", "name01", "sector01",
                sdf.parse(date04), 4.04);
    }

    void validateRec04 (TrackRec r) throws java.text.ParseException {
        assertTrue(r.getSymbol().equalsIgnoreCase("sym01"));
        assertTrue(r.getAnalyst().equalsIgnoreCase("analyst02"));
        assertTrue(r.getName().equalsIgnoreCase("name01"));
        assertTrue(r.getSector().equalsIgnoreCase("sector01"));
        assertEquals(r.getDate().compareTo(sdf.parse(date04)),0);
        assertEquals(r.getDelta(),4.04,0.001);
    }


    TrackRec getRec05() throws java.text.ParseException {
        return  new TrackRec ("sym05", "analyst02", "name05", "sector05",
                sdf.parse(date05), 5.05);
    }

    void validateRec05 (TrackRec r) throws java.text.ParseException {
        assertTrue(r.getSymbol().equalsIgnoreCase("sym05"));
        assertTrue(r.getAnalyst().equalsIgnoreCase("analyst02"));
        assertTrue(r.getName().equalsIgnoreCase("name05"));
        assertTrue(r.getSector().equalsIgnoreCase("sector05"));
        assertEquals(r.getDate().compareTo(sdf.parse(date05)),0);
        assertEquals(r.getDelta(),5.05,0.001);
    }

    TrackRec getRecUpdate(TrackRec old) throws java.text.ParseException {
        return  new TrackRec (old.getSymbol(), old.getAnalyst(), "new-name", "new-sector",
                sdf.parse(datenew), 6.06);
    }

    void validateUpdate (TrackRec r) throws java.text.ParseException {
        assertTrue(r.getName().equalsIgnoreCase("new-name"));
        assertTrue(r.getSector().equalsIgnoreCase("new-sector"));
        assertEquals(r.getDate().compareTo(sdf.parse(datenew)),0);
        assertEquals(r.getDelta(),6.06,0.001);
    }
}

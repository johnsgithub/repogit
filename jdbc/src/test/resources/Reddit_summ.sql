
CREATE TABLE reddit_summ_tbl
(
  row_id  	BIGSERIAL,
  date_tx 	date not NULL,
  stock 	character varying(64),
  num  	integer DEFAULT '0'::numeric,
  name 	character varying(64),
  
  CONSTRAINT reddit_sum_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);


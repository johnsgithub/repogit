package com.jpw.raptor.cmdline.reddit;

import com.jpw.raptor.model.RedditPost;
import com.jpw.raptor.model.RedditSumm;

import java.util.*;

public class RedditSummary {

    public List<RedditSumm> countStocks(List<RedditPost> stockList, Date asOfDate) throws java.io.IOException {

        HashMap<String,Integer> stockCount = new HashMap<>();

        for (RedditPost post : stockList) {
            if ( stockCount.containsKey(post.getStock().toUpperCase(Locale.ROOT))  ) {
                Integer count = stockCount.get(post.getStock().toUpperCase(Locale.ROOT));
                count++;
                stockCount.put(post.getStock().toUpperCase(Locale.ROOT), count);
            } else {
                Integer count = 1;
                stockCount.put(post.getStock().toUpperCase(Locale.ROOT), count);
            }
        }

        // Sort the map by count ie value
        List<Map.Entry<String, Integer>> list = new ArrayList<>(stockCount.entrySet());
        list.sort(Map.Entry.comparingByValue());
        Collections.reverse(list);

        RedditDictionary dictionary = new RedditDictionary();
        TreeMap<String,String> map = dictionary.getStockMap();

        ArrayList<RedditSumm> results = new ArrayList<>(list.size());
        Iterator i = list.iterator();
        while ( i.hasNext() ) {
            Map.Entry me     = (Map.Entry)i.next();
            String    ticker = (String) me.getKey();
            results.add(new RedditSumm( asOfDate, ticker, (Integer) me.getValue(), map.get(ticker)));
        }

        return results;
    }


}

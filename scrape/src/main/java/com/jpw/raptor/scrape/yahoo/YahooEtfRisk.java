package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@Getter
@Setter
public class YahooEtfRisk {

    private double  alpha;
    private double  beta;
    private double  annualReturn;
    private double  rSquared;
    private double  standardDeviation;
    private double  sharpeRatio;
    private double  treynorRatio;

    private boolean readFailed;

    public YahooEtfRisk() {
        init();
    }

    public void init () {
        alpha               = 0.0;
        beta                = 0.0;
        annualReturn        = 0.0;
        rSquared            = 0.0;
        standardDeviation   = 0.0;
        sharpeRatio         = 0.0;
        treynorRatio        = 0.0;

        readFailed = false;
    }

    public void print() {
        System.out.println("alpha              " + alpha);
        System.out.println("beta               " + beta);
        System.out.println("annualReturn       " + annualReturn);
        System.out.println("rSquared           " + rSquared);
        System.out.println("standardDeviation  " + standardDeviation);
        System.out.println("sharpeRatio        " + sharpeRatio);
        System.out.println("treynorRatio       " + treynorRatio);
    }

}

package com.jpw.raptor.cmdline.populate;


import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;

import com.jpw.raptor.model.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;

@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class IngestApplication implements CommandLineRunner {


    @Autowired
    public QuoteDAO quoteTbl;
    @Autowired
    public EodDataFile      eodFile;

    @Autowired
    public StockDAO stockTbl;

    public void oneTime() throws Exception {
        // Create dummy record and add to database
        // symbol, name, sp, dow, russell
        stockTbl.addEmpty("META", "META PLATFORMS INC", "500", "", "1000");

        BufferedReader br = new BufferedReader(new FileReader("/home/temp_save/meta-quotes-final.txt"));

        EodParser parser = new EodParser();

        String val = null;
        while ((val = br.readLine()) != null) {
            Quote rec = parser.parseCsvLine(val);
            quoteTbl.add(rec);
        }
    }

    // Main loop
    @Override
    public void run(String... args) throws Exception {

        System.out.println();
        System.out.println("***************  Populate quote data base  *******************");

        /*
        // Directories to process
        String[] directories = new String[] {
                "/home/finance/indexs/data/2020",
                "/home/finance/etf/data/amex/2020",
                "/home/finance/etf/data/nasdaq/2020",
                "/home/finance/nyse/2020",
                "/home/finance/indexs/data/2021",
                "/home/finance/etf/data/amex/2021",
                "/home/finance/etf/data/nasdaq/2021",
                "/home/finance/nyse/2021"
        };

        EodFileUtil fileUtil = new EodFileUtil();

        // Get a list of files to process
        List<FileQualified> filesToProcess = fileUtil.getFileList(directories);

        // Process each file
        for ( FileQualified fileQualified : filesToProcess ) {
            System.out.println("Processs " + fileQualified.getName());

            // Extract quotes and write them to the database
            eodFile.processFile(fileQualified);
        }
*/
        //oneTime();

        // Update market returns
        System.out.println("Update performance");
        eodFile.updatePerformance();

        System.out.println("***************  Good bye  *******************");
        System.out.println();

    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(IngestApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit( app.run(args) ) );
    }

}

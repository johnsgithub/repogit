package com.jpw.raptor.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.*;

/**
 * Created by john on 4/7/18.
 */
public class DateConstants {

    public static final int DAYS_5      = 5;
    public static final int DAYS_10     = 10;
    public static final int DAYS_20     = 20;
    public static final int DAYS_50     = 50;
    public static final int DAYS_100    = 100;
    public static final int DAYS_200    = 200;

    static final String qtrEnd[] = {"", "-03-31", "-06-30", "-09-30", "-12-31"};
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    public Date getQuarterEndDate(int year, int qtr) {

        Date date = null;

        try {
            if (year >= 2000 && year <= 2035) {
                if (qtr >= 1 && qtr <= 4) {
                    String dateString = String.valueOf(year) + qtrEnd[qtr];
                    SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
                    date = formatter.parse(dateString);
                }
            }
        } catch (ParseException ignore) {
            //logger.error(ex.toString());
        }

        return date;
    }


    public Date getYearStart(int year) {

        Date date = null;

        try {
            if (year >= 2000 && year <= 2035) {
                String dateString = String.valueOf(year) + "-01-01";
                SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
                date = formatter.parse(dateString);
            }
        } catch (ParseException ignore) {
            //logger.error(ex.toString());
        }

        return date;
    }


    public Date getYearEnd(int year) {

        Date date = null;

        try {
            if (year >= 2000 && year <= 2035) {
                String dateString = String.valueOf(year) + "-12-31";
                SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
                date = formatter.parse(dateString);
            }
        } catch (ParseException ignore) {
            //logger.error(ex.toString());
        }

        return date;
    }

    // Quotes are in descending order by date
    public List<Quote> getLastDayInWeek(List<Quote> equityQuotes) {

        ZoneId defaultZoneId = ZoneId.systemDefault();

        // Allocate space for the relative performance list
        List<Quote>  result = new ArrayList<>();

        // Create a sorted map to hold the quote data
        // data will be stored in ascending order by date string
        TreeMap<String, Quote> map = new TreeMap<>();

        //
        // Populate a map with one quote per week
        for ( Quote quote : equityQuotes ) {

            // Generate Local Date object for quote date
            SimpleDateFormat    dateFormat     = new SimpleDateFormat(DATE_FORMAT);
            String              formattedDate  = dateFormat.format(quote.getDate());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
            LocalDate localDate = LocalDate.parse(formattedDate, formatter);

            // Get week of year
            TemporalField woy = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();
            int weekNumber = localDate.get(woy);

            // get month
            int monthNumber = localDate.getMonthValue();

            // Get year
            int year = localDate.getYear();

            // generate key (year plus week in year)
            // bug where december date is in first week of next year
            String weekKey;
            if ( weekNumber == 1 && monthNumber == 12 ) {
                weekKey = String.format("%4d", year+1) + String.format("%02d", weekNumber);
            } else {
                weekKey = String.format("%4d", year) + String.format("%02d", weekNumber);
            }

            // Check to see if week has been added to tree
            Quote mapQuote = map.get(weekKey);

            if ( mapQuote == null ) {
                // no entry in the map add this quote to the map
                // Since data is in descending order the first entry found
                // for a week will always be the last day of the week
                map.put(weekKey, quote);
            }
        }

        //
        // Populate a list with one quote per week
        // traverse the map in sorted order which is ascending order
        for (Map.Entry<String, Quote> entrySetVal : map.entrySet()) {
            Quote entry = entrySetVal.getValue();
            result.add(entry);
        }

        // Reverse the list from ascending to descending order
        Collections.reverse(result);

        return result;
    }



    // Quotes are in descending order by date
    public List<Quote> getLastDayInMonth(List<Quote> equityQuotes) {

        // Allocate space for the relative performance list
        List<Quote>  result = new ArrayList<>();

        // Create a sorted map to hold the quote data
        // data will be stored in ascending order by date string
        TreeMap<String, Quote> map = new TreeMap<>();

        //
        // Populate a map with one quote per week
        for ( Quote quote : equityQuotes ) {

            // Generate Local Date object for quote date
            SimpleDateFormat    dateFormat     = new SimpleDateFormat(DATE_FORMAT);
            String              formattedDate  = dateFormat.format(quote.getDate());

            DateTimeFormatter   formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
            LocalDate           localDate = LocalDate.parse(formattedDate, formatter);

            // get month
            int monthNumber = localDate.getMonthValue();

            // Get year
            int year = localDate.getYear();

            // generate key (year plus week in year)
            String monthKey = String.format("%4d", year) + String.format("%02d", monthNumber);


            // Check to see if week has been added to tree
            Quote mapQuote = map.get(monthKey);

            if ( mapQuote == null ) {
                // no entry in the map add this quote to the map
                // Since data is in descending order the first entry found
                // for a week will always be the last day of the week
                map.put(monthKey, quote);
            }
        }

        //
        // Populate a list with one quote per week
        // traverse the map in sorted order which is ascending order
        for (Map.Entry<String, Quote> entrySetVal : map.entrySet()) {
            Quote entry = entrySetVal.getValue();
            result.add(entry);
        }

        // Reverse the list from ascending to descending order
        Collections.reverse(result);

        return result;
    }

}

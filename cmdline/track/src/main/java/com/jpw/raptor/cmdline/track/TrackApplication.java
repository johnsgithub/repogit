package com.jpw.raptor.cmdline.track;


import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.jdbc.track.TrackDAO;

import com.jpw.raptor.model.Stock;
import com.jpw.raptor.model.TrackRec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.List;


@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class TrackApplication implements CommandLineRunner {

    @Autowired
    public TrackDAO trackTbl;

    @Autowired
    public StockDAO stockTbl;

    public void trackIngest() throws java.io.IOException {
        
        TrackMap      map      = new TrackMap();
        TrackParser   parser   = new TrackParser();
        TrackFileUtil fileUtil = new TrackFileUtil();

        // populate the map with track records
        map.trackDbRecords( trackTbl.getAll() );

        // get the files to process
        List<TrackFileEntry> fileList = fileUtil.getFileList();

        // Process each file and update the map
        for ( TrackFileEntry file : fileList ) {
            // get file contents
            List<String> fileContents = fileUtil.getFileContents(file.getFileName());

            // convert to track records
            List<TrackRec> recs = parser.linesToRecords(file.getAnalyst(), file.getDate(), fileContents);

            // update map with records
            map.newRecomendations(recs);

            // Delete the file
            fileUtil.deleteFile(file.getFileName());
        }

        fileUtil.openResultsFile();

        // Delete dropped recommendations from the database
        List<TrackRec> toDelete = map.getTrackDbDeletes();
        for ( TrackRec rec : toDelete ) {
            trackTbl.delete(rec.getSymbol(), rec.getAnalyst());
            fileUtil.writeResultsFile("del," +
                    rec.getSymbol() + "," +
                    rec.getAnalyst() + "," +
                    rec.getName() + "," +
                    rec.getSector() );
        }

        // Add new recommendations to the database
        List<TrackRec> toAdd = map.getTrackDbAdds();
        for ( TrackRec rec : toAdd ) {
            Stock stock = stockTbl.get(rec.getSymbol().toUpperCase());
            if ( stock == null ) {
                fileUtil.writeResultsFile("ign," +
                        rec.getSymbol() + "," +
                        rec.getAnalyst() + "," +
                        rec.getName() + "," +
                        rec.getSector());
            } else {
                trackTbl.add(rec);
                fileUtil.writeResultsFile("add," +
                        rec.getSymbol() + "," +
                        rec.getAnalyst() + "," +
                        rec.getName() + "," +
                        rec.getSector());
            }
        }

        fileUtil.closeResultsFile();
    }


    // Main loop
    @Override
    public void run(String... args) throws Exception {

        // define the run time parameters
        AppParameters params = new AppParameters();

        // create parameter parser
        JCommander cmd = new JCommander(params);

        try {
            cmd.parse(args);

            trackIngest();
                
        } catch (ParameterException ex) {
            System.out.println(ex.getMessage());
            cmd.usage();
        }

        System.out.println("***************  Good bye  *******************");
        System.out.println();

    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(TrackApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit( app.run(args) ) );
    }

}

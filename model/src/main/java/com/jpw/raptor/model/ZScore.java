package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ZScore {

    String symbol;
    Date   date;
    double score;

    public ZScore () {
        symbol = null;
        date   = null;
        score  = 0.0;
    }

    public ZScore(String symbol, Date   date, double score) {
        this.symbol = symbol;
        this.date   = date;
        this.score  = score;
    }
}

package com.jpw.raptor.testing;

import com.jpw.raptor.algorithm.DateConstants;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.model.Quote;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DateTest {
    public void doit(QuoteDAO quoteTbl) throws  java.text.ParseException {

        // Hard code runtime parameters
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = format.parse("2020-12-24");


        // Worker routines for working with dates
        DateConstants dc = new DateConstants();

        List<Quote> allQuotes = quoteTbl.getForPerformanceDesc("SPY", "2020-12-24");
        List<Quote> weekEndQuotes = dc.getLastDayInWeek(allQuotes);

        System.out.println("Size " + weekEndQuotes.size());
        Quote firstQuote = weekEndQuotes.get(weekEndQuotes.size() - 1);
        for ( Quote q : weekEndQuotes ) {
            System.out.println( "SPY " + q.getDate() + " " + firstQuote.getDate());
        }
    }
}

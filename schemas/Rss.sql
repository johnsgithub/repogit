
CREATE TABLE rss_tbl
(
  hash  	character(32),
  date_posted   date NOT NULL,
  title 	text,
  url 		text,
  uri 		text,
  CONSTRAINT 	rss_pkey PRIMARY KEY (hash)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX rss_posted_date
  ON rss_tbl
  USING btree
  (date_posted);



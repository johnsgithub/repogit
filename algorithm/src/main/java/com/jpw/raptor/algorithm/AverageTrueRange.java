package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Adx;
import com.jpw.raptor.model.Atr;
import com.jpw.raptor.model.Quote;

import java.util.ArrayList;
import java.util.List;

public class AverageTrueRange {

    private double roundIt(double v) {
        return ( Math.round(v * 100.0) / 100.0 );
    }

    // Quotes are in descending order by date
    public List<Atr> generateAtr(List<Quote> data, int periods) {

        ExponentialMovingAverage ema = new ExponentialMovingAverage();

        // allocate space for object list
        List<Atr> list = new ArrayList<>(data.size());

        // create Average true range object for each quote
        for ( Quote q : data) {
            list.add( new Atr( q) );
        }

        // The first true range is ABS(first high - first low)
        int    firstIdx = data.size() - 1;
        double firstVal = Math.abs( data.get(firstIdx).getHigh() - data.get(firstIdx).getLow() );
        double firstPercent = roundIt( firstVal / data.get(firstIdx).getClose() );
        list.get(firstIdx).setTrueRange(firstVal);
        list.get(firstIdx).setAverageTrueRange(firstVal);
        list.get(firstIdx).setAverageTruePercentage(firstPercent);


        // compute average true range, and true percentage
        for ( int i =  firstIdx - 1; i >= 0; i-- ) {
            Atr    obj       = list.get(i);
            Atr    priorObj  = list.get(i+1);

            obj.setTrueRange( getTrueRange(list.get(i), list.get(i+1)) );
            double averageTrueRange = ema.getExpAvg(obj.getTrueRange(), priorObj.getAverageTrueRange(), periods);
            obj.setAverageTrueRange( roundIt(averageTrueRange) );
            obj.setAverageTruePercentage( roundIt( (obj.getAverageTrueRange() / obj.getClose()) * 100.0) );

        }

        return list;
    }


    // get true range
    private double getTrueRange(Quote today, Quote yesterday ) {

        double result = 0.0;

        if ( Math.abs(today.getHigh() - today.getLow()) > result ) {
            result = Math.abs(today.getHigh() - today.getLow());
            result = Math.round(result * 100.0) / 100.0;
        }

        if ( Math.abs(today.getHigh() - yesterday.getClose()) > result) {
            result = Math.abs(today.getHigh() - yesterday.getClose());
            result = Math.round(result * 100.0) / 100.0;
        }

        if ( Math.abs(today.getLow() - yesterday.getClose()) > result) {
            result = Math.abs(today.getLow() - yesterday.getClose());
            result = Math.round(result * 100.0) / 100.0;
        }

        return result;
    }

}

package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.algorithm.TechnicalSummaryFactory;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.model.Etf;

import com.jpw.raptor.model.Quote;

import com.jpw.raptor.model.TechnicalSummaryModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 4/9/18.
 */
@Controller
public class EtfSignalController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    public QuoteDAO quoteTbl;

    @Autowired
    EtfDAO etfTbl;

    protected static final  String REF_SYMBOL = "SPY";
    protected static final  String RECORDS = "tech_model";
    protected static final  String PAGE_TITLE = "pagetitle";
    protected static final  String ENCODING = "UTF-8";
    protected static final  String SIGNAL_PAGE = "signals";
    protected static final String DEFAULT_DAYS = "220";

    @RequestMapping("/etf-signal-all")
    public String etfSignalAllReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("etfSignalAllReq {}", "");

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Etf> equities = etfTbl.getAll();

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Etf equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "ALL ETF Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/etf-signal-relevant")
    public String etfSignalRelevantReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("etfSignalRelevantReq {}", "");

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Etf> equities = etfTbl.getRelevant();

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Etf equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "RELEVANT ETF Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/etf-signal-tracked")
    public String etfSignalTrackedReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("etfSignalTrackedReq {}", "");

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Etf> equities = etfTbl.getTracked();

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Etf equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "TRACKED ETF Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/etf-signal-owned")
    public String etfSignalOwnedReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("etfReturnOwnedReq {}", "");

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Etf> equities = etfTbl.getOwned();

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Etf equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "OWNED ETF Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/etf-signal-class")
    public String etfSignalClassReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            @RequestParam(value="class", required=true) String assetClass,
            Model model)
    {
        String assetClassClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
        } catch (UnsupportedEncodingException ex) {};

        //logger.debug("etfSignalClassReq  asset class {} ", assetClassClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Etf> equities = etfTbl.getByAssetClass(assetClassClean);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Etf equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, assetClassClean.toUpperCase() + " ETF Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/etf-signal-class-type")
    public String etfSignalClassTypeReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String assetClassClean = "";
        String fundTypeClean   = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
        } catch (UnsupportedEncodingException ex) {};

        //logger.debug("etfSignalClassTypeReq  asset class {} fund type {}", assetClassClean, fundTypeClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Etf> equities = etfTbl.getByAssetClassFundType(assetClassClean, fundTypeClean);

        /// Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Etf equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE,
                assetClassClean.toUpperCase() + " - " + fundTypeClean.toUpperCase() + " ETF Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/etf-signal-class-type-sub")
    public String etfSignalClassTypeSubTypeReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            @RequestParam(value="sub", required=true)  String fundSubType,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String assetClassClean  = "";
        String fundTypeClean    = "";
        String fundSubTypeClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
            fundSubTypeClean = URLDecoder.decode(fundSubType, ENCODING);
        } catch (UnsupportedEncodingException ex) {};

        //logger.debug("etfSignalClassTypeSubTypeReq  asset class {} fund type {} sub type {}",
        //        assetClassClean, fundTypeClean, fundSubTypeClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Etf> equities = etfTbl.getByAssetClassFundTypeSubType(
                assetClassClean, fundTypeClean, fundSubTypeClean);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Etf equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE,
                assetClassClean.toUpperCase() + " - " + fundTypeClean.toUpperCase()
                        + " - " + fundSubTypeClean.toUpperCase() + " ETF Signals");

        return SIGNAL_PAGE;
    }

    @RequestMapping("/etf-signal-class-type-sub-factor")
    public String etfSignalClassTypeSubTypeFactorReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            @RequestParam(value="sub", required=true)  String fundSubType,
            @RequestParam(value="factor", required=true)  String factor,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String assetClassClean  = "";
        String fundTypeClean    = "";
        String fundSubTypeClean = "";
        String factorClean      = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
            fundSubTypeClean = URLDecoder.decode(fundSubType, ENCODING);
            factorClean      = URLDecoder.decode(factor, ENCODING);
        } catch (UnsupportedEncodingException ex) {};

        //logger.debug("etfSignalClassTypeSubTypeFactorReq  asset class {} fund type {} sub type {} factor {} ",
        //        assetClassClean, fundTypeClean, fundSubTypeClean, factorClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get the equities we are interested in
        List<Etf> equities = etfTbl.getByAssetClassFundTypeSubTypeFactor(
                assetClassClean, fundTypeClean, fundSubTypeClean, factorClean);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Etf equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, assetClassClean.toUpperCase() + " - " + fundTypeClean.toUpperCase()
                + " - " + fundSubTypeClean.toUpperCase()
                + " - " + factorClean.toUpperCase() + " ETF Signals");

        return SIGNAL_PAGE;
    }

}

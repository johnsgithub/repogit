package com.jpw.raptor.cmdline.knowledge;

import com.jpw.raptor.model.FinanceKnowledge;

import com.jpw.raptor.search.finance.KnowledgeSearch;
import org.apache.commons.codec.digest.DigestUtils;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.stereotype.Component;

import org.apache.tika.Tika;

import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by john on 5/3/18.
 */
@Component
public class KnowledgeFile {
    

    public String generateTitleFromFile(File file) {
        String title = null;

        title = file.getName().substring(0, file.getName().lastIndexOf('.') );
        title = title.replaceAll("-", " ");
        title = title.replaceAll("_", " ");

        return title;
    }


    public String generateTitleFromLine(String line) {

        if ( line == null ) {
            return null;
        }

        int index = line.indexOf("***title");
        if (index == -1) {
            return null;
        }

        return line.substring(index+8).trim();

    }


    public String generateTagFromLine(String line) {

        if ( line == null ) {
            return "";
        }

        int index = line.indexOf("***tag");
        if (index == -1) {
            return "";
        }

        // Split the string
        return line.substring(index+6).replace(";", " ").trim();
    }


    public String generateMd5(String line) {

        if ( line == null ) {
            return null;
        }

        return DigestUtils.md5Hex(line).toUpperCase();

    }


    public FinanceKnowledge generateFinancialKnowledge(File file) {

        boolean         error       = false;
        String          eol         = System.getProperty("line.separator");
        String          title       = null;
        String          tag         = null;
        StringBuilder   sb          = new StringBuilder();

        String          fileSuffix  = file.getName().substring(file.getName().indexOf('.'));

        // compute the current year
        SimpleDateFormat formatter    = new SimpleDateFormat("yyyy");
        String           year         = formatter.format(new Date());

        try
        {
            Tika                tika        = new Tika();
            AutoDetectParser    parser      = new AutoDetectParser();
            BodyContentHandler  handler     = new BodyContentHandler();
            Metadata            metadata    = new Metadata();
            String              content;

            InputStream stream = new FileInputStream(file);

            parser.parse(stream, handler, metadata);
            content   = handler.toString();

            // Split the string
            String[] result = content.split(eol);

            // Read each record in the file
            for ( String val : result )
            {
                // Process each line based upon its contents
                if        ( val.contains("***title") ) {
                    title = generateTitleFromLine(val);
                } else if ( val.contains("***tag") ) {
                    tag = generateTagFromLine(val);
                } else if ( val.contains("***date") ) {
                    // ignore
                } else {
                    // default is body
                    sb.append(val).append(eol);
                }
            }
        }
        catch (IOException | TikaException | SAXException e)
        {
            error = true;
            e.printStackTrace();
        }

        // if no title was found use file name
        if ( title == null ) {
            title = generateTitleFromFile(file);
        }

        //
        // Create the knowledge object
        FinanceKnowledge knowledge = null;
        if ( !error ) {
            knowledge = new FinanceKnowledge();
            knowledge.setTitle(title);
            knowledge.setTag(tag);
            knowledge.setBody(sb.toString());
            knowledge.setId(generateMd5(knowledge.getBody()));
            // TBD date

            // generate file and url locations
            knowledge.setLoc( "/doc/" + year + "/" + knowledge.getId() + fileSuffix );
            knowledge.setUrl("/html/" + year + "/" + knowledge.getId() + ".html" );
        }

        return knowledge;
    }

    public FinanceKnowledge indexFile(KnowledgeSearch elasticSearch, File file) throws IOException {

        //
        // Create the knowledge object
        FinanceKnowledge knowledge = generateFinancialKnowledge(file);
        if ( knowledge != null ) {
            elasticSearch.indexDocument(knowledge);
        }

        return knowledge;
    }

}


package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RedditPost {

    protected long    rowId;
    protected Date    date;
    protected String  stock;
    protected String  community;
    protected String  postId;
    protected String  author;
    protected String  title;
    protected String  cType;
    protected String  contents;

    public RedditPost() {}

    public RedditPost (Date date, String stock, String community, String postId, String author, String title, String cType, String contents) {
        this.date      = date;
        this.stock     = stock;
        this.community = community;
        this.postId    = postId;
        this.author    = author;
        this.title     = title;
        this.cType     = cType;
        this.contents  = contents;
    }

}

package com.jpw.raptor.onetime.addstocks;

import com.jpw.raptor.algorithm.*;
import com.jpw.raptor.algorithm.signals.*;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.fund.FundDAO;
import com.jpw.raptor.jdbc.hiyieldspread.HiYieldSpreadDAO;
import com.jpw.raptor.jdbc.index.IndexDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.jdbc.treasury.TreasuryDAO;
import com.jpw.raptor.lib.properties.FinanceProperties;

import com.jpw.raptor.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;


/**
 * Created by john on 8/30/18.
 */
@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class Application implements CommandLineRunner {

    @Autowired
    public QuoteDAO quoteTbl;

    @Autowired
    public StockDAO stockTbl;

    // Main loop
    @Override
    public void run(String... args) throws Exception {

        System.out.println("***************  End Test  *******************");
        System.out.println();

        MissingStocks worker = new MissingStocks(stockTbl, quoteTbl);

        System.out.println("***************  russell_2000  *******************");
        System.out.println();
        worker.processFile( "/home/finance/next_development/work_russell_2000.csv" );

        System.out.println("***************  russell_1000  *******************");
        System.out.println();
        worker.processFile( "/home/finance/next_development/work_russell_1000.csv" );

        System.out.println("***************  SP_400  *******************");
        System.out.println();
        worker.processFile( "/home/finance/next_development/work_SP_400.csv" );

        System.out.println("***************  SP_500  *******************");
        System.out.println();
        worker.processFile( "/home/finance/next_development/work_SP_500.csv" );

        System.out.println("***************  SP_600  *******************");
        System.out.println();
        worker.processFile( "/home/finance/next_development/work_SP_600.csv" );

        System.out.println("***************  End Test  *******************");
        System.out.println();
    }

    public static void main(String[] args) throws Exception {

        SpringApplication app = new SpringApplication(Application.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit(app.run(args)) );
    }

}


package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;

@Getter
public class YahooEtfTableRow {

    static final String SPAN_START = "<span";
    static final String SPAN_END = "</span>";
    static final String TBODY_START = "<tbody>";
    static final String TBODY_END = "</tbody>";
    static final String TR_START = "<tr ";
    static final String TR_END = "</tr>";
    static final String TD_START = "<td ";
    static final String TD_END = "</td>";
    static final String HREF_START = "<a href";
    static final String HREF_END = "</a>";
    static final String GREATER_THAN = ">";
    static final String LESS_THAN = "<";

    private boolean found;
    private String rowText;
    private int rowStart;
    private int rowEnd;
    private String column1;
    private String column2;
    private String column3;

    public void parseRow (String page, int startOffset, int endOffset) {

        rowStart = page.indexOf(TR_START, startOffset);

        if ( rowStart == -1 || rowStart > endOffset ) {
            found   = false;
            rowText = null;
        }
        else {
            found    = true;
            rowEnd  = page.indexOf(TR_END, rowStart) + 5;
            rowText  = page.substring(rowStart, rowEnd);

            parseColumns(rowText);
        }
    }

    public void parseColumns (String row) {

        column1 = "";
        column2 = "";
        column3 = "";


        // First colum
        int start1 = row.indexOf(TD_START);
        if ( start1 == -1 ) {
            return;
        }

        int end1 = row.indexOf(TD_END, start1) + 5;
        column1  = parseValue(row.substring(start1, end1)).replace("'", "");

        // column 2
        int start2 = row.indexOf(TD_START, end1);
        if ( start2 == -1 ) {
            return;
        }

        int end2     = row.indexOf(TD_END, start2) + 5;
        column2 = parseValue(row.substring(start2, end2)).replace("'", "");

        // column 3
        int start3 = row.indexOf(TD_START, end2);
        if ( start3 == -1 ) {
            return;
        }

        int end3     = row.indexOf(TD_END, start3) + 5;
        column3 = parseValue(row.substring(start3, end3)).replace("'", "");

    }

    private String parseValue(String column) {

        if ( column.contains(HREF_START) ) {
            int hrefStart = column.indexOf(HREF_START);
            int s         = column.indexOf(GREATER_THAN, hrefStart) + 1;
            int e         = column.indexOf(LESS_THAN, s);
            return column.substring(s, e);
        } else if ( column.contains(SPAN_START) ) {
            int spanStart = column.indexOf(SPAN_START);
            int s         = column.indexOf(GREATER_THAN, spanStart) + 1;
            int e         = column.indexOf(LESS_THAN, s);
            return column.substring(s, e);
        } else {
            int s         = column.indexOf(GREATER_THAN) + 1;
            int e         = column.indexOf(LESS_THAN, s);
            return column.substring(s, e);
        }
    }
}

package com.jpw.raptor.cmdline.track;

import com.jpw.raptor.lib.properties.FinanceProperties;
import com.jpw.raptor.model.FileQualified;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import java.util.Date;

/**
 * Created by john on 5/15/18.
 */
public class TrackFileUtil {

    Writer fileWriter;

    String getAnalyst(String f) {
        int    dash    = f.indexOf('_');
        return f.substring(0, dash);
    }

    Date getDate(String f) {
        int    dash    = f.indexOf('_');
        int    period  = f.indexOf('.');

        // validate date
        try {
            // Convert string to date object
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            Date   date = format.parse(f.substring(dash + 1, period));
            return date;
        }
        catch(Exception e) {
            System.out.println("Quote parse date error " + f);
            return null;
        }
    }

    public List<TrackFileEntry> getFileList() {

        // import directory
        FinanceProperties fp   = new FinanceProperties();
        Properties prop = fp.get();

        String dir  = prop.getProperty("import_dir");
        if ( dir == null )
        {
            System.out.println("Import directory not specified");
            return Collections.emptyList();
        }

        // create a file that is really a directory
        File aDirectory = new File(dir);

        // get a listing of all files in the directory
        String[] filesInDir = aDirectory.list();

        // create an output list
        List<TrackFileEntry> list = new ArrayList<>();
        for (String f : filesInDir) {
            if ( f.endsWith(".csv") ) {
                String fileName = dir + "/" + f;
                File dataFile = new File(fileName);
                if (dataFile.isFile()) {
                    list.add(new TrackFileEntry(getDate(f), getAnalyst(f), fileName));
                }
            }

        }

        return list;
    }

    public List<String> getFileContents(String fileName) {
        List<String> list = null;

        try {
            list = Files.readAllLines(
                    new File(fileName).toPath(), Charset.defaultCharset());
        } catch (Exception ex) {}

        return list;
    }

    public void deleteFile (String srcPath )
    {
        try
        {
            File file = new File(srcPath);

            if ( file.delete() )
            {
                System.out.println(file.getName() + " is deleted!");
            }
            else
            {
                System.out.println("Delete operation is failed.");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void openResultsFile() throws java.io.IOException {

        // import directory
        FinanceProperties fp   = new FinanceProperties();
        Properties prop = fp.get();

        String dir  = prop.getProperty("track_dir");
        if ( dir == null )
        {
            System.out.println("Track directory not specified");
            return;
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String strDate = dateFormat.format(new Date());
        fileWriter = new FileWriter(dir + "/" + strDate + "-ingest.csv");

    }

    public void closeResultsFile() throws java.io.IOException {
        fileWriter.flush();
        fileWriter.close();
    }

    public void writeResultsFile(String line) throws java.io.IOException {
        fileWriter.write(line + "\n");
    }
}

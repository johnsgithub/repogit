package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;



/**
 * Created by john on 5/4/18.
 */
@Getter
@Setter
public class FinanceKnowledge {

    private String          id;
    private String          title;
    private String          body;
    private String          tag;
    private String          url;
    private String          loc;
    private double          score;

    // constructor
    public FinanceKnowledge() {
        id    = null;
        title = null;
        url   = null;
        loc   = null;
        tag   = null;
        body  = null;
        score = 0.0;
    }
}

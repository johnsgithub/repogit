package com.jpw.raptor.algorithm.signals;

import com.jpw.raptor.model.Adx;
import com.jpw.raptor.model.Jdk;

public class JdkSignalFactory {

    // summarize momentum based upon macd values
    public String jdkMomentum(double macd, double signal) {
        String momentum = "";

        if (macd >= 0) {
            if (macd >= signal)
                momentum = "positive";
            else
                momentum = "positive declining";
        } else {
            if (macd < signal)
                momentum = "negative";
            else
                momentum = "negative increasing";
        }

        return momentum;
    }

    // summarize momentum based upon macd values
    public String jdkMomentum(Jdk obj) {
        return jdkMomentum (obj.getMacd(), obj.getSignal());
    }

}

package com.jpw.raptor.onetime.addstocks;

import com.jpw.raptor.model.Quote;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class QuotesFromFiles {

    private static final String[] NASDAQ_DIRS = {
            "/home/finance/etf/data/nasdaq/2020",
            "/home/finance/etf/data/nasdaq/2021",
            "/home/finance/etf/data/nasdaq/2022"
    };

    private static final String[] NYSE_DIRS = {
            "/home/finance/nyse/2020",
            "/home/finance/nyse/2021",
            "/home/finance/nyse/2022"
    };


    public List<Quote> getQuotes(String symbol) throws IOException {

        List<Quote> nyseQuotes = getNyseQuotes(symbol);
        if ( !nyseQuotes.isEmpty())
            return nyseQuotes;

        List<Quote> nasdaqQuotes = getNasdaqQuotes(symbol);
        if ( !nasdaqQuotes.isEmpty())
            return nasdaqQuotes;

        return Collections.emptyList();
    }


    private List<Quote> getNyseQuotes(String symbol) throws IOException {
        return getQuotesByExchange(NYSE_DIRS, symbol);
    }

    private List<Quote> getNasdaqQuotes(String symbol) throws IOException {
        return getQuotesByExchange(NASDAQ_DIRS, symbol);
    }

    private List<Quote> getQuotesByExchange(String[] dirs, String symbol) throws IOException {

        // List to contain results
        List<Quote> quotes = new ArrayList<>();

        // for each directory
        for ( String dir : dirs ) {
            // get files in directory
            List<String> files = getFiles(dir);
            // get quotes from files
            List<Quote> quotesInDir = getRecords (files, symbol);
            // save results
            quotes.addAll(quotesInDir);
        }

        return quotes;
    }


    private List<String> getFiles(String dir) {

        // create a file that is really a directory
        File aDirectory = new File(dir);

        // get a listing of all files in the directory
        String[] filesInDir = aDirectory.list();

        // Create a map where the key is date and value is the full path
        Map<String, String> unsortedMap = new HashMap<>();
        for ( String f : filesInDir ) {
            int dash        = f.indexOf('_');
            int period      = f.indexOf('.');
            // date is f.substring(dash+1, period)
            // index is f.substring(0, dash)
            String key      = f.substring(dash+1, period);
            String fileName = dir + "/" + f;
            File   dataFile = new File(fileName);
            if ( dataFile.isFile() ) {
                unsortedMap.put(key, fileName);
            }
        }

        // Create a sorted map
        Map<String, String> treeMap = new TreeMap<>(unsortedMap);

        // Create list and add files
        List<String> list = new ArrayList<>(treeMap.size());
        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            list.add(entry.getValue());
        }

        return list;
    }


    private List<Quote> getRecords(List<String> files, String symbol) throws IOException {

        // Create list for quotes
        List<Quote> quotes = new ArrayList<>(files.size());

        // Used to read Eod data file
        BufferedReader br      = null;

        // Find quote in each file
        for ( String fileName : files ) {
            br         = new BufferedReader(new FileReader(fileName));
            String val = null;
            while ((val = br.readLine()) != null) {
                Quote quote = parseCsvLine(val);

                if ( quote != null && quote.getSymbol().equalsIgnoreCase(symbol) ) {
                    quotes.add(quote);
                    break;
                }
            }
        }

        return quotes;
    }

    private Quote parseCsvLine (String line) {

        // Parse the string into an array of fields
        // Validate number of fields parsed
        String[] str = line.split(",");
        if ( str.length >= 7 ) {
            // Valid number of columns
        } else {
            // invalid number of fields
            return null;
        }

        // Create the quote
        Quote quote = new Quote();

        // Symbol
        quote.setSymbol(str[0]);

        // validate date
        try {
            // Convert string to date object
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            quote.setDate(format.parse(str[1]));
        }
        catch(Exception e) {
            System.out.println("Quote parse date error " + line);
            return null;
        }

        // Validate open, high, low and close
        try {
            quote.setOpen(Double.parseDouble(str[2]));
            quote.setHigh(Double.parseDouble(str[3]));
            quote.setLow(Double.parseDouble(str[4]));
            quote.setClose(Double.parseDouble(str[5]));
        }
        catch(NumberFormatException e) {
            System.out.println("Quote parse open/high/low/close error " + line);
            return null;
        }

        // Validate volume
        try {
            quote.setVolume(Long.parseLong(str[6]));
        }
        catch(NumberFormatException e) {
            System.out.println("Quote parse volume error " + line);
            return null;
        }

        return quote;
    }
}


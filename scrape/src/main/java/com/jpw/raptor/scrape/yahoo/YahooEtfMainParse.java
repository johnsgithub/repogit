package com.jpw.raptor.scrape.yahoo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class YahooEtfMainParse {

    static final String SPAN_START = "<span";
    static final String SPAN_END = "</span>";
    static final String TD_START = "<td ";
    static final String TD_END = "</td>";
    static final String HREF_START = "<a href";
    static final String HREF_END = "</a>";
    static final String GREATER_THAN = ">";
    static final String LESS_THAN = "<";

    private String  page;

    public YahooEtfMain readPage(String symbol) throws IOException {

        YahooEtfMain m = new YahooEtfMain();

        // Yahoo replaces . with -

        String ticker = symbol.toUpperCase().replace('.', '-');
        String baseUrl = "https://finance.yahoo.com/quote/" + ticker + "/";
        try {
            Document doc = Jsoup.connect(baseUrl).get();
            page = doc.toString();
        } catch ( org.jsoup.HttpStatusException ex ) {
            System.out.println("### " + symbol + " ETF main HttpStatusException " + ex.toString());
            m.setReadFailed(true);
            return m;
        }

        // Validate page was found
        if (page.contains("<span>No results for")  || page.contains("<span>Symbols similar to")) {
            System.out.println("### " + symbol + " ETF main Symbol not found ");
            m.setReadFailed(true);
            return m;
        }



        if ( page.contains("NoJs ") ) {
            try {
                //System.out.println(" No Java Script " );
                parseNoJavaScript(page, m);
            } catch ( Exception ex ) {
                System.out.println("### " + symbol + "  **** Etf Main No Java Script parse failed ***** " );
                System.out.println(ex.toString());
                BufferedWriter writer = new BufferedWriter(new FileWriter("/home/software/page-error-etf-main-no-js.txt"));
                writer.write(page);
                m.setReadFailed(true);
                return m;
            }
        } else {
            try {
                //System.out.println(" Java Script " );
                parseJavaScript(page, m);
            } catch ( Exception ex ) {
                System.out.println("### " + symbol + "  **** Etf Main Java Script parse failed ***** " );
                System.out.println(ex.toString());
                BufferedWriter writer = new BufferedWriter(new FileWriter("/home/software/page-error-etf-main-js.txt"));
                writer.write(page);
                m.setReadFailed(true);
                return m;
            }
        }

        m.setReadFailed(false);
        return m;
    }

    public void parseJavaScript ( String page, YahooEtfMain m ) {
        ValueObject vo = new ValueObject();

        //  Average Volume
        int averageVolumeLabelStart = page.indexOf(">Avg. Volume<");
        if ( averageVolumeLabelStart > 0 ) {
            int averageVolumeStartDataSpan = page.indexOf(SPAN_START, averageVolumeLabelStart) + 1;
            int averageVolumeFin = page.indexOf(LESS_THAN, averageVolumeStartDataSpan) + 1;
            int averageVolumeStartData = page.indexOf(GREATER_THAN, averageVolumeFin) + 1;
            int averageVolumeEndData = page.indexOf(LESS_THAN, averageVolumeStartData);
            m.setAverageVolume(vo.getLong(page.substring(averageVolumeStartData, averageVolumeEndData)));
        }

        //  Yield
        int yieldLabelStart = page.indexOf(">Yield<");
        if ( yieldLabelStart > 0 ) {
            int yieldStartDataSpan = page.indexOf(SPAN_START, yieldLabelStart) + 1;
            int yieldStartData = page.indexOf(GREATER_THAN, yieldStartDataSpan) + 1;
            int yieldEndData = page.indexOf(LESS_THAN, yieldStartData);
            m.setYield(vo.getPercent(page.substring(yieldStartData, yieldEndData)));
        }

        //  Expense Ratio
        int expenseRatioLabelStart = page.indexOf(">Expense Ratio (net)<");
        if ( expenseRatioLabelStart > 0 ) {
            int expenseRatioStartDataSpan = page.indexOf(SPAN_START, expenseRatioLabelStart) + 1;
            int expenseRatioStartData = page.indexOf(GREATER_THAN, expenseRatioStartDataSpan) + 1;
            int expenseRatioEndData = page.indexOf(LESS_THAN, expenseRatioStartData);
            m.setExpenseRatio(vo.getPercent(page.substring(expenseRatioStartData, expenseRatioEndData)));
        }
    }

    public void parseNoJavaScript ( String page, YahooEtfMain m  ) {
        ValueObject vo = new ValueObject();

        //  Average Volume
        int averageVolumeLabelStart = page.indexOf(">Avg. Volume<");
        if ( averageVolumeLabelStart > 0 ) {
            int averageStartTD = page.indexOf(TD_START, averageVolumeLabelStart) + 1;
            int averageVolumeStartData = page.indexOf(GREATER_THAN, averageStartTD) + 1;
            int averageVolumeEndData = page.indexOf(TD_END, averageVolumeStartData);
            m.setAverageVolume(vo.getLong(page.substring(averageVolumeStartData, averageVolumeEndData)));
        }

        //  Yield
        int yieldLabelStart = page.indexOf(">Yield<");
        if ( yieldLabelStart > 0 ) {
            int yieldStartTD = page.indexOf(TD_START, yieldLabelStart) + 1;
            int yieldStartData = page.indexOf(GREATER_THAN, yieldStartTD) + 1;
            int yieldEndData = page.indexOf(TD_END, yieldStartData);
            m.setYield(vo.getPercent(page.substring(yieldStartData, yieldEndData)));
        }

        //  Expense Ratio
        int expenseRatioLabelStart = page.indexOf(">Expense Ratio (net)<");
        if ( expenseRatioLabelStart > 0 ) {
            int expenseRatioStartTD = page.indexOf(TD_START, expenseRatioLabelStart) + 1;
            int expenseRatioStartData = page.indexOf(GREATER_THAN, expenseRatioStartTD) + 1;
            int expenseRatioEndData = page.indexOf(TD_END, expenseRatioStartData);
            m.setExpenseRatio(vo.getPercent(page.substring(expenseRatioStartData, expenseRatioEndData)));
        }
    }

}

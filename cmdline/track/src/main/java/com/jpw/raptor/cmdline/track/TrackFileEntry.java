package com.jpw.raptor.cmdline.track;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
public class TrackFileEntry {
    protected Date   date;
    protected String analyst;
    protected String fileName;
}

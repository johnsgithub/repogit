package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Holdings {
    private String symbol;
    private String holdingName;
    private HoldingPercent holdingPercent;
}

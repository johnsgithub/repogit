package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.HttpStatusException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@Getter
@Setter
public class YahooStockStatistics {

    private long    marketCap;
    private long    enterpriseValue;
    private double  trailingPE;
    private double  forwardPE;
    private double  pegRatio;
    private double  priceSales;
    private double  priceBook;
    private double  evRevenue;
    private double  evEbitda;
    private double  beta;
    private long    avgVol3Month;
    private long    avgVol10Day;
    private double  shortRatio;
    private double  divRateForward;
    private double  divYieldForward;
    private double  profitMargin;
    private double  operatingMargin;
    private double  returnOnAssets;
    private double  returnOnEquity;
    private long    revenue;
    private double  revenueGrowth;
    private long    ebitda;
    private double  dilutedEps;
    private double  earningsGrowth;
    private long    totalCash;
    private double  totalCashPerShare;
    private long    totalDebt;
    private double  currentRatio;
    private long    operatingCashFlow;
    private long    freeCashFlow;

    private boolean readFailed;

    public YahooStockStatistics() {
        init();
    }

    public void init () {
        marketCap           = 0;
        enterpriseValue     = 0;
        trailingPE          = 0.0;
        forwardPE           = 0.0;
        pegRatio            = 0.0;
        priceSales          = 0.0;
        priceBook           = 0.0;
        evRevenue           = 0.0;
        evEbitda            = 0.0;
        beta                = 0.0;
        avgVol3Month        = 0;
        avgVol10Day         = 0;
        shortRatio          = 0.0;
        divRateForward      = 0.0;
        divYieldForward     = 0.0;
        profitMargin        = 0.0;
        operatingMargin     = 0.0;
        returnOnAssets      = 0.0;
        returnOnEquity      = 0.0;
        revenue             = 0;
        revenueGrowth       = 0.0;
        ebitda              = 0;
        dilutedEps          = 0.0;
        earningsGrowth      = 0.0;
        totalCash           = 0;
        totalCashPerShare   = 0.0;
        totalDebt           = 0;
        currentRatio        = 0.0;
        operatingCashFlow   = 0;
        freeCashFlow        = 0;
        readFailed          = false;
    }

    public void print() {
        System.out.println( "marketCap         " + marketCap );
        System.out.println( "enterpriseValue   " + enterpriseValue );
        System.out.println( "trailingPE        " + trailingPE );
        System.out.println( "forwardPE         " + forwardPE );
        System.out.println( "pegRatio          " + pegRatio );
        System.out.println( "priceSales        " + priceSales );
        System.out.println( "priceBook         " + priceBook );
        System.out.println( "evRevenue         " + evRevenue );
        System.out.println( "evEbitda          " + evEbitda );
        System.out.println( " " );
        System.out.println( "beta              " + beta );
        System.out.println( "avgVol3Month      " + avgVol3Month );
        System.out.println( "avgVol10Day       " + avgVol10Day );
        System.out.println( "shortRatio        " + shortRatio );
        System.out.println( "divRateForward    " + divRateForward );
        System.out.println( "divYieldForward   " + divYieldForward );

        System.out.println( " " );
        System.out.println( "profitMargin      " + profitMargin );
        System.out.println( "operatingMargin   " + operatingMargin );
        System.out.println( "returnOnAssets    " + returnOnAssets );
        System.out.println( "returnOnEquity    " + returnOnEquity );

        System.out.println( " " );
        System.out.println( "revenue           " + revenue );
        System.out.println( "revenueGrowth     " + revenueGrowth );
        System.out.println( "ebitda            " + ebitda );
        System.out.println( "dilutedEps        " + dilutedEps );
        System.out.println( "earningsGrowth    " + earningsGrowth );

        System.out.println( "totalCash         " + totalCash );
        System.out.println( "totalCashPerShare " + totalCashPerShare );
        System.out.println( "totalDebt         " + totalDebt );
        System.out.println( "currentRatio      " + currentRatio );
        System.out.println( "operatingCashFlow " + operatingCashFlow );
        System.out.println( "freeCashFlow      " + freeCashFlow );
    }
}

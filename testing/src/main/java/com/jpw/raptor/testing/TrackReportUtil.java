package com.jpw.raptor.testing;

import com.jpw.raptor.lib.properties.FinanceProperties;

import java.io.FileWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class TrackReportUtil {

    Writer fileWriter;

    public void openResultsFile(String analyst, Date date) throws java.io.IOException {

        // import directory
        FinanceProperties fp   = new FinanceProperties();
        Properties prop = fp.get();

        String dir  = prop.getProperty("reports_dir");
        if ( dir == null )
        {
            System.out.println("Track directory not specified");
            return;
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String strDate = dateFormat.format(date);
        fileWriter = new FileWriter(dir + "/" + "track-" + analyst + "-" + strDate + ".csv");

    }

    public void closeResultsFile() throws java.io.IOException {
        fileWriter.flush();
        fileWriter.close();
    }

    public void writeResultsFile(String line) throws java.io.IOException {
        fileWriter.write(line + "\n");
    }
}

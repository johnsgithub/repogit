
CREATE TABLE track_tbl
(
  symbol  character varying(10) NOT NULL,
  analyst character varying(16) NOT NULL,
  name    character varying(126),
  sector  character varying(126),
  date_tx date,
  delta   double precision DEFAULT '-9999.0'::numeric,
  primary key (symbol,analyst)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX idx_track_primary
  ON track_tbl
  USING btree
  (symbol,analyst);

CREATE INDEX idx_track_symbol
  ON track_tbl
  USING btree
  (symbol COLLATE pg_catalog."default");


CREATE INDEX idx_track_analyst
  ON track_tbl
  USING btree
  (analyst COLLATE pg_catalog."default");


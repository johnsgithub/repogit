package com.jpw.raptor.algorithm;

import com.jpw.raptor.algorithm.signals.*;
import com.jpw.raptor.model.*;

import java.util.List;

import static com.jpw.raptor.model.DateConstants.*;
import static com.jpw.raptor.model.DateConstants.DAYS_50;

public class TechnicalSummaryFactory {

    public TechnicalSummaryModel generate(List<Quote> refQuotes,  List<Quote> equityQuotes, String symbol, String name ) {

        // Create summary record
        TechnicalSummaryModel summary         = new TechnicalSummaryModel();
        summary.setSymbol(symbol);
        summary.setName(name);
        summary.setDate(refQuotes.get(0).getDate());

        // JDK signal
        JDKFactory       jdkFactory = new JDKFactory();
        JdkSignalFactory jdkSignal  = new JdkSignalFactory();
        List<Jdk>        jdks       = jdkFactory.generate(refQuotes, equityQuotes);
        if ( jdks.size() == 0 )
            return summary;
        else
            summary.setJdkSignal(jdks.get(0).getRsRatioMa());

        // ADX Signal
        AvgDirMovementIndicator adxFactory = new AvgDirMovementIndicator();
        AdxSignalFactory        adxSignal  = new AdxSignalFactory();
        List<Adx>               adxs       = adxFactory.generateAdx(equityQuotes, 14);
        if ( adxs.size() == 0 )
            return summary;
        else
            summary.setAdxSignal(adxSignal.adxSignal(adxs.get(0), adxs.get(1)));

        // MACD signal
        MovingAvgConvergenceDivergence macdFactory = new MovingAvgConvergenceDivergence();
        MacdSignalFactory              macdSignal  = new MacdSignalFactory();
        List<Macd>                     macds       = macdFactory.quote(equityQuotes, 12, 26);
        if ( macds.size() == 0 )
            return summary;
        else
            summary.setMacdSignal(macdSignal.macdMomentum(macds.get(0).getMacd(), macds.get(1).getMacd()));

        // Moving Average Signal
        // generate averages model for first quote
        SimpleMovingAverage        avgFactory       = new SimpleMovingAverage();
        MovingAverageSignalFactory avgSignalFactory = new MovingAverageSignalFactory();
        AveragesModel              avgRecord        = new AveragesModel(equityQuotes.get(0));
        avgRecord.setSimple5(avgFactory.quote(equityQuotes, 0, DAYS_5));
        avgRecord.setSimple10(avgFactory.quote(equityQuotes, 0, DAYS_10));
        avgRecord.setSimple20(avgFactory.quote(equityQuotes, 0, DAYS_20));
        avgRecord.setSimple50(avgFactory.quote(equityQuotes, 0, DAYS_50));
        avgRecord.setSimple100(avgFactory.quote(equityQuotes, 0, DAYS_100));
        avgRecord.setSimple200(avgFactory.quote(equityQuotes, 0, DAYS_200));
        if ( equityQuotes.size() >= 200 )
            summary.setMAvgSignal(avgSignalFactory.getSimpleMaSignal(avgRecord));
        else
            summary.setMAvgSignal(avgSignalFactory.getSimpleMaSignalShort(avgRecord));

        // relative strength
        RelativeStrengthIndicator rsi       = new RelativeStrengthIndicator();
        RsiSignalFactory          rsiSignal = new RsiSignalFactory();
        String signal = rsiSignal.signal(
                rsi.quote(equityQuotes, 0, 14),
                rsi.quote(equityQuotes, 1, 14));
        summary.setRsiSignal(signal);

        // average true range
        AverageTrueRange atrFactory = new AverageTrueRange();
        List<Atr>        atrList = atrFactory.generateAtr(equityQuotes, 14);
        if ( atrList.size() == 0 )
            return summary;
        else
            summary.setAtrSignal(atrList.get(0).getAverageTruePercentage());

        return summary;
    }


    public TechnicalAnalysisModel analysis(List<Quote> refQuotes,  List<Quote> equityQuotes, String symbol, String name ) {

        // Create summary record
        TechnicalAnalysisModel summary = new TechnicalAnalysisModel();
        summary.setSymbol(symbol);
        summary.setName(name);
        summary.setDate(refQuotes.get(0).getDate());
        summary.setClose(refQuotes.get(0).getClose());

        // JDK signal
        JDKFactory       jdkFactory = new JDKFactory();
        List<Jdk>        jdks       = jdkFactory.generate(refQuotes, equityQuotes);
        summary.setJdk(jdks.get(0));
        summary.setJdkSignal(jdks.get(0).getRsRatioMa());

        // ADX Signal
        AvgDirMovementIndicator adxFactory = new AvgDirMovementIndicator();
        AdxSignalFactory        adxSignal  = new AdxSignalFactory();
        List<Adx>               adxs       = adxFactory.generateAdx(equityQuotes, 14);
        summary.setAdx(adxs.get(0));
        summary.setAdxSignal(adxSignal.adxSignal(adxs.get(0), adxs.get(1)));

        // MACD signal
        MovingAvgConvergenceDivergence macdFactory = new MovingAvgConvergenceDivergence();
        MacdSignalFactory              macdSignal  = new MacdSignalFactory();
        List<Macd>                     macds       = macdFactory.quote(equityQuotes, 12, 26);
        summary.setMacd(macds.get(0));
        summary.setMacdSignal(macdSignal.macdMomentum(macds.get(0).getMacd(), macds.get(1).getMacd()));

        // Moving Average Signal
        // generate averages model for first quote
        SimpleMovingAverage        avgFactory       = new SimpleMovingAverage();
        MovingAverageSignalFactory avgSignalFactory = new MovingAverageSignalFactory();
        AveragesModel              avgRecord        = new AveragesModel(equityQuotes.get(0));
        avgRecord.setSimple5(avgFactory.quote(equityQuotes, 0, DAYS_5));
        avgRecord.setSimple10(avgFactory.quote(equityQuotes, 0, DAYS_10));
        avgRecord.setSimple20(avgFactory.quote(equityQuotes, 0, DAYS_20));
        avgRecord.setSimple50(avgFactory.quote(equityQuotes, 0, DAYS_50));
        avgRecord.setSimple100(avgFactory.quote(equityQuotes, 0, DAYS_100));
        avgRecord.setSimple200(avgFactory.quote(equityQuotes, 0, DAYS_200));
        summary.setAvgs(avgRecord);
        if ( equityQuotes.size() >= 200 )
            summary.setMAvgSignal(avgSignalFactory.getSimpleMaSignal(avgRecord));
        else
            summary.setMAvgSignal(avgSignalFactory.getSimpleMaSignalShort(avgRecord));

        // relative strength
        RelativeStrengthIndicator rsi       = new RelativeStrengthIndicator();
        RsiSignalFactory          rsiSignal = new RsiSignalFactory();
        summary.setRsi(rsi.quote(equityQuotes, 0, 14));
        String signal = rsiSignal.signal(
                rsi.quote(equityQuotes, 0, 14),
                rsi.quote(equityQuotes, 1, 14));
        summary.setRsiSignal(signal);

        // average true range
        AverageTrueRange atrFactory = new AverageTrueRange();
        List<Atr>        atrList = atrFactory.generateAtr(equityQuotes, 14);
        summary.setAtr(atrList.get(0));
        summary.setAtrSignal(atrList.get(0).getAverageTruePercentage());

        return summary;
    }

}

package com.jpw.raptor.cmdline.web.service;

import com.jpw.raptor.algorithm.RelativePerformance;
import com.jpw.raptor.algorithm.SimpleMovingAverage;
import com.jpw.raptor.model.*;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class AsburyRiskService {

    protected static final String DATE_FORMAT = "yyyy-MM-dd";

    public List<AsburyListModel> hiYieldSpreadList(List<HiYieldSpread> recs, int days) {

        SimpleMovingAverage         sma     = new SimpleMovingAverage();
        SimpleDateFormat            sdf     = new SimpleDateFormat(DATE_FORMAT);
        ArrayList<AsburyListModel>  result  = new ArrayList<>(recs.size());

        // create the list
        for ( HiYieldSpread rec : recs ) {
            AsburyListModel alm = new AsburyListModel();
            alm.setSymbol("Spread");
            alm.setDate(rec.getDate());
            alm.setDatestr( sdf.format(rec.getDate()) );
            alm.setValue(rec.getSpread());
            result.add(alm);
        }

        // populate the moving average
        for ( int i=0; i<result.size(); i++ ) {
            result.get(i).setSma( sma.asbury(result,i,days) );
        }

        return result;
    }


    public List<AsburyListModel> quoteList(String symbol, List<Quote> recs, int days) {

        SimpleMovingAverage         sma     = new SimpleMovingAverage();
        SimpleDateFormat            sdf     = new SimpleDateFormat(DATE_FORMAT);
        ArrayList<AsburyListModel>  result  = new ArrayList<>(recs.size());

        // create the list
        for ( Quote rec : recs ) {
            AsburyListModel alm = new AsburyListModel();
            alm.setSymbol(symbol);
            alm.setDate(rec.getDate());
            alm.setDatestr( sdf.format(rec.getDate()) );
            alm.setValue(rec.getClose());
            result.add(alm);
        }

        // populate the moving average
        for ( int i=0; i<result.size(); i++ ) {
            result.get(i).setSma( sma.asbury(result,i,days) );
        }

        return result;
    }

    public List<AsburyListModel> relativePerformanceList(List<Quote> spy, List<Quote> jnk, int days) {

        SimpleMovingAverage         sma     = new SimpleMovingAverage();
        SimpleDateFormat            sdf     = new SimpleDateFormat(DATE_FORMAT);

        // Get the relative performance list
        RelativePerformance         rp      = new RelativePerformance();
        List<Rp>                    rpList  = rp.computeRelativePerformance(jnk, spy);

        // convert relative performance to asbury list model
        ArrayList<AsburyListModel>  result  = new ArrayList<>(rpList.size());

        for ( Rp rec : rpList ) {
            AsburyListModel alm = new AsburyListModel();
            alm.setSymbol(rec.getSymbol());
            alm.setDate(rec.getDate());
            alm.setDatestr( sdf.format(rec.getDate()) );
            alm.setValue(rec.getVal());
            result.add(alm);
        }

        // populate the moving average
        for ( int i=0; i<result.size(); i++ ) {
            result.get(i).setSma( sma.asbury(result,i,days) );
        }

        return result;
    }


}

package com.jpw.raptor.cmdline.web.controller;


import com.jpw.raptor.cmdline.web.service.TreasuryModelService;
import com.jpw.raptor.jdbc.treasury.TreasuryDAO;
import com.jpw.raptor.model.Treasury;

import com.jpw.raptor.model.TreasuryModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.List;

/**
 * Created by john on 12/9/18.
 */
@Controller
public class TreasuryController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    // this controller displays the treasury yield difference between different length treasuries
    // Send all data to the web page; it decides what to display

    @Autowired
    TreasuryDAO treasuryTbl;

    @Autowired
    TreasuryModelService treasuryModelService;

    public void treasuryProcessReq(Model model)
    {
        //logger.debug("treasuryProcessReq");

        // Get the data in descending order by dates
        List<Treasury> toConvert = treasuryTbl.getAll();

        // convert data to Treasury model
        List<TreasuryModel> values = treasuryModelService.getTreasuryModel(toConvert);

        // reverse the list from descending dates to ascending dates
        Collections.reverse(values);

        // 260 week days - 9 holidays equals 251 trading days in a year
        // 3 years equals 753 trading days
        if ( values.size() > 251 ) {
            // skip the first years worth
            model.addAttribute("data", values.subList(251, values.size()) );
        } else {
            model.addAttribute("data", values);
        }

    }

    @RequestMapping("/treasury3-30")
    public String treasury3and30Req(Model model)
    {
        //logger.debug("treasury3_30Req ");

        treasuryProcessReq(model);

        return "treasury3-30";
    }


    @RequestMapping("/treasury-short")
    public String treasuryShortDetailReq(Model model)
    {
        //logger.debug("treasuryShortDetailReq ");

        treasuryProcessReq(model);

        return "treasury-short";
    }

    @RequestMapping("/treasury-long")
    public String treasuryLongDetailReq(Model model)
    {
        //logger.debug("treasuryLongDetailReq ");

        treasuryProcessReq(model);

        return "treasury-long";
    }
}

package com.jpw.raptor.cmdline.rss;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FeedDescription {
    String name;
    String address;

    public FeedDescription( String name, String address) {
        this.name    = name;
        this.address = address;
    }
}

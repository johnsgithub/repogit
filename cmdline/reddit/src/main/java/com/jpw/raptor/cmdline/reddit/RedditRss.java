package com.jpw.raptor.cmdline.reddit;

import com.jpw.raptor.model.RedditPost;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import java.io.InputStreamReader;

public class RedditRss {

    public List<RedditPost> readRss(String webSite, Date asOfDate) throws IOException, FeedException {

        // Generated list of posts
        List<RedditPost> list = new ArrayList<>(20000);

        //
        // Get the URL for the RSS feed
        //URL             url       = new URL(stripLastSlash(webSite) + ".rss");
        URL           url       = new URL(stripLastSlash(webSite) + ".rss?limit=100");
        //URL           url       = new URL(stripLastSlash(webSite) + "/new/.rss?sort=new&limit=100");

        //
        // Get the community name
        String community = getCommunity(webSite);

        //
        // Get a list of community posts
        SyndFeedInput   input     = new SyndFeedInput();
        SyndFeed        feed      = input.build(new XmlReader(url));
        List<SyndEntry> postList  = feed.getEntries();

        //
        // Process the posts
        for ( SyndEntry postEntry : postList) {
            //
            // extract title, author and post id
            String postTitle  = postEntry.getTitleEx().getValue();
            String postAuthor = postEntry.getAuthor();
            String postId     = getPostId(postEntry.getLink());

            // Extract post text if there is any
            String rawText = Jsoup.parse(postEntry.getContents().get(0).getValue()).text();
            String postText = "";
            int submitted = rawText.indexOf("submitted by");
            if ( submitted >= 0 )
                postText = rawText.substring(0, submitted);

            RedditPost post = new RedditPost( asOfDate, "", community, postId,
                                              postAuthor, postTitle, "post", postText);
            list.add(post);

            //
            // Get post comments
            SyndFeedInput   commentInput     = new SyndFeedInput();
            URL             commentUrl       = new URL(postEntry.getLink() + ".rss");
            SyndFeed        commentFeed      = commentInput.build(new XmlReader(commentUrl));
            List<SyndEntry> commentList      = commentFeed.getEntries();

            // Skip the first comment it is the post
            Iterator<SyndEntry> commentIt = commentList.iterator();
            commentIt.next();
            while ( commentIt.hasNext() ) {

                // Get the comment
                SyndEntry commentEntry = commentIt.next();
                String commentAuthor = commentEntry.getAuthor();
                String commentText = Jsoup.parse(commentEntry.getContents().get(0).getValue()).text();
                RedditPost comment = new RedditPost(asOfDate, "", community, postId,
                        commentAuthor, postTitle, "comment", commentText);
                list.add(comment);
            }
        }

        return list;
    }

    public String getPostId(String url) {
        // "../comments/post-id/.."
        int postStart = url.indexOf("/comments/") + 10;
        int postEnd   = url.indexOf("/", postStart);
        return url.substring(0, postEnd);
    }

    public String getCommunity(String url) {
        String stripped = stripLastSlash (url);

        int lastSlash = stripped.lastIndexOf("/");
        return stripped.substring(lastSlash+1);
    }

    public String stripLastSlash (String url) {
        int lastSlash = url.lastIndexOf("/");

        if ( (url.length() - 1) == lastSlash ) {
            // URL ends with a /
            return url.substring(0, lastSlash);
        } else {
            // url does not end with a slash
            return url;
        }
    }

}

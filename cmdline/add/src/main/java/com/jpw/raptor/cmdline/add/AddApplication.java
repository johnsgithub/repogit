package com.jpw.raptor.cmdline.add;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.jpw.raptor.lib.properties.FinanceProperties;
import com.jpw.raptor.model.FileQualified;
import com.jpw.raptor.model.FinanceKnowledge;
import com.jpw.raptor.model.Index;
import com.jpw.raptor.jdbc.index.IndexDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class
AddApplication implements CommandLineRunner {

    @Autowired
    public AddEquity addEquity;

    // Main loop
    @Override
    public void run(String... args) throws Exception {

        // define the run time parameters
        AppParameters params = new AppParameters();

        // create parameter parser
        JCommander cmd = new JCommander(params);

        try {
            cmd.parse(args);

            if         ( params.getType().equalsIgnoreCase("stock") ) {
                addEquity.addStock(params.getExchange(), params.getSymbol(), params.getName());
            } else if  ( params.getType().equalsIgnoreCase("etf") ) {
                addEquity.addEtf(params.getExchange(), params.getSymbol(), params.getName());
            } else if  ( params.getType().equalsIgnoreCase("index") ) {
                addEquity.addIndex(params.getExchange(), params.getSymbol(), params.getName());
            } else {
                System.out.println(" Unknown type     " + params.getType());
            }

        } catch (ParameterException ex) {
            System.out.println(ex.getMessage());
            cmd.usage();
        }

        System.out.println("***************  Good bye  *******************");
        System.out.println();

    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(AddApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit( app.run(args) ) );
    }

}

package com.jpw.raptor.cmdline.reports.track;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class LamdaTest {

    public boolean tryLamda(Predicate<List<Double>> v, List<Double> l) {
        return  v.test(l);
    }


    @Test
    public void testit() {
        List<Double> l1 = Arrays.asList(0.0, 0.0, 0.0);
        List<Double> l2 = Arrays.asList(0.0, -1.0, 0.0);
        List<Double> l3 = Arrays.asList(3.0, 1.0, 2.0);

        Predicate<List<Double>> neg = (n) -> {
            boolean result = false;
            for ( Double entry : n) {
                if (entry < 0.0) result = true;
            }
            return result;
        };

        Predicate<List<Double>> decl = (n) -> {
            boolean result = false;
            for ( int i=0; i<n.size()-1; i++) {
                if (n.get(i) < n.get(i+1)) result = true;
            }
            return result;
        };

        System.out.println("Test array 1 for negative values " + tryLamda(neg, l1));
        System.out.println("Test array 2 for negative values " + tryLamda(neg, l2));
        System.out.println("Test array 3 for negative values " + tryLamda(neg, l3));
        
        System.out.println(" " );

        System.out.println("Test array 1 for declining values " + tryLamda(decl, l1));
        System.out.println("Test array 2 for declining values " + tryLamda(decl, l2));
        System.out.println("Test array 3 for declining values " + tryLamda(decl, l3));
    }

}

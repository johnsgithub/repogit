package com.jpw.raptor.scrape.yahoo;

import com.jpw.raptor.model.Etf;
import com.jpw.raptor.model.Stock;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static java.lang.Thread.sleep;


public class YahooEtfScrape {

    static final  long SLEEP_TIME = 250L;

    public Etf scrape(Etf etf) throws IOException, InterruptedException {

        YahooEtfMain     main     = new YahooEtfMain();
        YahooEtfRisk     risk     = new YahooEtfRisk();
        YahooEtfHoldings holdings = new YahooEtfHoldings();

        // Scrape the web pages
        if ( !main.readPage(etf.getSymbol()) ) {
            logError(etf.getSymbol(), "yahoo etf main");
            return null;
        }
        else
            sleep(SLEEP_TIME);

        if ( !risk.readPage(etf.getSymbol()) ) {
            logError(etf.getSymbol(), "yahoo etf risk");
            return null;
        }
        else
            sleep(SLEEP_TIME);

        if ( !holdings.readPage(etf.getSymbol()) ) {
            logError(etf.getSymbol(), "yahoo etf holdings");
            return null;
        }

        etf.setAvgDailyVol(main.getAverageVolume());

        etf.setMarketCap(holdings.getMarketCap());
        etf.setExpenseRatio(main.getExpenseRatio() * 100.0);
        etf.setDividendYield(main.getYield() * 100.0);

        LocalDate localDate = LocalDate.now();
        etf.setLastUpdate(Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));

        etf.setMorningRating("");
        etf.setMorningStars(-9999);
        etf.setLipperTotal(-9999);
        etf.setLipperConsistent(-9999);
        etf.setLipperPreservation(-9999);
        etf.setLipperTax(-9999);
        etf.setLipperExpense(-9999);

        etf.setBasicMaterials(holdings.getBasicMaterials() * 100.0);
        etf.setConsumerCyclical(holdings.getConsumerCyclical() * 100.0);
        etf.setFinancialServices(holdings.getFinancialServices() * 100.0);
        etf.setRealestate(holdings.getRealEstate() * 100.0);
        etf.setConsumerDefensive(holdings.getConsumerDefensive() * 100.0);
        etf.setHealthcare(holdings.getHealthcare() * 100.0);
        etf.setUtilities(holdings.getUtilities() * 100.0);
        etf.setCommunicationServices(holdings.getCommunicationServices() * 100.0);
        etf.setEnergy(holdings.getEnergy() * 100.0);
        etf.setIndustrials(holdings.getIndustrials() * 100.0);
        etf.setTechnology(holdings.getTechnology() * 100.0);

        etf.setAlpha(risk.getAlpha());
        etf.setBeta(risk.getBeta());
        etf.setMeanAnnualReturn(risk.getAnnualReturn());
        etf.setRSquared(risk.getRSquared());
        etf.setDeviation(risk.getStandardDeviation());
        etf.setSharpeRatio(risk.getSharpeRatio());
        etf.setTreynorRatio(risk.getTreynorRatio());

        etf.setPe(holdings.getPriceEarnings());
        etf.setPb(holdings.getPriceBook());
        etf.setPs(holdings.getPriceSales());
        etf.setPc(holdings.getPriceCashFlow());

        etf.setEarningsGrowth(0.0);
        etf.setMedianMarketCap(0.0);

        etf.setBondMaturity(0.0);
        etf.setBondDuration(0.0);
        etf.setBondCreditQuality(0.0);

        etf.setBondAaaPercent(holdings.getBondAaa() * 100.0);
        etf.setBondAaPercent(holdings.getBondAa() * 100.0);
        etf.setBondAPercent(holdings.getBondA() * 100.0);
        etf.setBondBbbPercent(holdings.getBondBbb() * 100.0);
        etf.setBondBbPercent(holdings.getBondBb() * 100.0);
        etf.setBondBPercent(holdings.getBondB() * 100.0);
        etf.setBondBelowbPercent(holdings.getBondBelowB() * 100.0);
        etf.setBondUsPercent(holdings.getBondUs() * 100.0);
        etf.setBondOtherPercent(holdings.getBondOthers() * 100.0);

        etf.setBondPosition(holdings.getBonds() * 100.0);
        etf.setStockPosition(holdings.getStocks() * 100.0);

        etf.setCashPosition(0.0);
        etf.setConvertiblePosition(0.0);
        etf.setOtherPosition(0.0);
        etf.setPreferredPosition(0.0);

        etf.setTopHoldings(holdings.getHoldings());

        return etf;
    }

    private void logError(String symbol, String page) {
        System.out.println( symbol + " " + page + " read failed " );
    }
}

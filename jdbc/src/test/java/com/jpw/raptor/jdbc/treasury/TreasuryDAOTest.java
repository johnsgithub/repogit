package com.jpw.raptor.jdbc.treasury;

import com.jpw.raptor.model.Treasury;
import org.apache.commons.lang3.StringUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.File;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by John on 11/25/2017.
 */

public class TreasuryDAOTest {

    TreasuryDAO        tbl;

    Treasury           testRec;
    Treasury           r1;
    Treasury           r2;
    Treasury           r3;


    static class PostgresResource  {

        // postgres:9.6.8

        PostgreSQLContainer postgreSQLContainer;
        PGSimpleDataSource dataSource;

        public PostgresResource() {
            postgreSQLContainer = new PostgreSQLContainer("postgres:10.11")
                     .withDatabaseName("test-jdbc")
                    .withUsername("sa")
                    .withPassword("sa");

            postgreSQLContainer.start();

            dataSource = new PGSimpleDataSource();
            dataSource.setDatabaseName(postgreSQLContainer.getDatabaseName());
            dataSource.setServerName(postgreSQLContainer.getContainerIpAddress());
            dataSource.setPortNumber(postgreSQLContainer.getMappedPort(5432));
            dataSource.setUser(postgreSQLContainer.getUsername());
            dataSource.setPassword(postgreSQLContainer.getPassword());

        }

        public void stop() { postgreSQLContainer.stop(); }

        public PGSimpleDataSource getDataSource () {return dataSource;}

    }


    private static PostgresResource myPostgresResource;

    @BeforeClass
    public static void setUp() throws java.text.ParseException {

        myPostgresResource = new PostgresResource();
    }


    Date formatDate (String raw ) {
        if ( !StringUtils.isEmpty(raw) )
            return new Date(-1);

        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        try {
            return sdf.parse(raw.replace('/','-'));
        } catch (ParseException ex) {
            System.out.println(" Treasury DATE format exception ");
            return new Date(-1);
        }
    }

    double formatNumber ( String raw ) {
        if ( !StringUtils.isEmpty(raw) ) {
            try {
                return Double.parseDouble(raw.replace("N/A","0.0"));
            } catch (NumberFormatException ex) {
                System.out.println(" Treasury number format exception ");
                return 0.0;
            }
        } else {
            return 0.0;
        }
    }

    @Before
    public void setUpTest() {

        // Create test fields
        String [] rec1 = {"12-02-2018","2.37","2.42","2.45","2.58","2.71","2.80","2.81","2.79","2.84","2.91","3.05","3.16"};
        String [] rec2 = {"12-03-2018","2.36","2.43","2.46","5.55","2.59","2.72","2.81","2.82","2.80","2.85","2.92","3.06","3.17"};
        String [] rec3 = {"12-04-2018","2.39","2.44","2.47","6.66","2.60","2.73","2.82","2.83","2.81","2.86","2.93","3.07","3.18"};

        r1 = new Treasury();
        r1.setDate(formatDate(rec1[0]));
        r1.setOneMonth(formatNumber(rec1[1]));
        r1.setTwoMonths(formatNumber(rec1[2]));
        r1.setThreeMonths(formatNumber(rec1[3]));
        r1.setSixMonths(formatNumber(rec1[4]));
        r1.setOneYear(formatNumber(rec1[5]));
        r1.setTwoYears(formatNumber(rec1[6]));
        r1.setThreeYears(formatNumber(rec1[7]));
        r1.setFiveYears(formatNumber(rec1[8]));
        r1.setSevenYears(formatNumber(rec1[9]));
        r1.setTenYears(formatNumber(rec1[10]));
        r1.setTwentyYears(formatNumber(rec1[11]));
        r1.setThirtyYears(formatNumber(rec1[12]));

        r2 = new Treasury();
        r2.setDate(formatDate(rec2[0]));
        r2.setOneMonth(formatNumber(rec2[1]));
        r2.setTwoMonths(formatNumber(rec2[2]));
        r2.setThreeMonths(formatNumber(rec2[3]));
        r2.setSixMonths(formatNumber(rec2[5]));
        r2.setOneYear(formatNumber(rec2[6]));
        r2.setTwoYears(formatNumber(rec2[7]));
        r2.setThreeYears(formatNumber(rec2[8]));
        r2.setFiveYears(formatNumber(rec2[9]));
        r2.setSevenYears(formatNumber(rec2[10]));
        r2.setTenYears(formatNumber(rec2[11]));
        r2.setTwentyYears(formatNumber(rec2[12]));
        r2.setThirtyYears(formatNumber(rec2[13]));

        r3 = new Treasury();
        r3.setDate(formatDate(rec3[0]));
        r3.setOneMonth(formatNumber(rec3[1]));
        r3.setTwoMonths(formatNumber(rec3[2]));
        r3.setThreeMonths(formatNumber(rec3[3]));
        r3.setSixMonths(formatNumber(rec3[5]));
        r3.setOneYear(formatNumber(rec3[6]));
        r3.setTwoYears(formatNumber(rec3[7]));
        r3.setThreeYears(formatNumber(rec3[8]));
        r3.setFiveYears(formatNumber(rec3[9]));
        r3.setSevenYears(formatNumber(rec3[10]));
        r3.setTenYears(formatNumber(rec3[11]));
        r3.setTwentyYears(formatNumber(rec3[12]));
        r3.setThirtyYears(formatNumber(rec3[13]));
    }

    @AfterClass
    public static void tearDown(){
        myPostgresResource.stop();
    }


    @Test
    public void test00() throws Exception {

        System.out.println("Start");

        // Create connection to docker database
        tbl = new TreasuryDAO();
        tbl.setDataSource(myPostgresResource.getDataSource());
        tbl.postConstruct();

        // Create table for testing
        File resource;
        String cmds;

        resource = new ClassPathResource("schema.sql").getFile();
        cmds = new String(Files.readAllBytes(resource.toPath()));
        tbl.sqlScript(cmds);

        resource = new ClassPathResource("data.sql").getFile();
        cmds = new String(Files.readAllBytes(resource.toPath()));
        tbl.sqlScript(cmds);

        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        Date d1 = sdf.parse("12-02-2018");
        Date d2 = sdf.parse("12-03-2018");
        Date d3 = sdf.parse("12-04-2018");

        // Write 4 records
        tbl.upsert(r1);
        tbl.upsert(r2);
        tbl.upsert(r3);


        Treasury rr;

        // Read and validate the 3 records
        rr = tbl.get(d1);
        assertEquals(rr.getOneMonth(),      2.37,   0.001);
        assertEquals(rr.getTwoMonths(),     2.42,   0.001);
        assertEquals(rr.getThreeMonths(),   2.45,   0.001);
        assertEquals(rr.getSixMonths(),     2.58,   0.001);
        assertEquals(rr.getOneYear(),       2.71,   0.001);
        assertEquals(rr.getTwoYears(),      2.8,    0.001);
        assertEquals(rr.getThreeYears(),    2.81,   0.001);
        assertEquals(rr.getFiveYears(),     2.79,   0.001);
        assertEquals(rr.getSevenYears(),    2.84,   0.001);
        assertEquals(rr.getTenYears(),      2.91,   0.001);
        assertEquals(rr.getTwentyYears(),   3.05,   0.001);
        assertEquals(rr.getThirtyYears(),   3.16,   0.001);

        rr = tbl.get(d2);
        assertEquals(rr.getOneMonth(),      2.36,   0.001);
        assertEquals(rr.getTwoMonths(),     2.43,   0.001);
        assertEquals(rr.getThreeMonths(),   2.46,   0.001);
        assertEquals(rr.getSixMonths(),     2.59,   0.001);
        assertEquals(rr.getOneYear(),       2.72,   0.001);
        assertEquals(rr.getTwoYears(),      2.81,   0.001);
        assertEquals(rr.getThreeYears(),    2.82,   0.001);
        assertEquals(rr.getFiveYears(),     2.80,   0.001);
        assertEquals(rr.getSevenYears(),    2.85,   0.001);
        assertEquals(rr.getTenYears(),      2.92,   0.001);
        assertEquals(rr.getTwentyYears(),   3.06,   0.001);
        assertEquals(rr.getThirtyYears(),   3.17,   0.001);

        rr = tbl.get(d3);
        assertEquals(rr.getOneMonth(),      2.39,   0.001);
        assertEquals(rr.getTwoMonths(),     2.44,   0.001);
        assertEquals(rr.getThreeMonths(),   2.47,   0.001);
        assertEquals(rr.getSixMonths(),     2.60,   0.001);
        assertEquals(rr.getOneYear(),       2.73,   0.001);
        assertEquals(rr.getTwoYears(),      2.82,   0.001);
        assertEquals(rr.getThreeYears(),    2.83,   0.001);
        assertEquals(rr.getFiveYears(),     2.81,   0.001);
        assertEquals(rr.getSevenYears(),    2.86,   0.001);
        assertEquals(rr.getTenYears(),      2.93,   0.001);
        assertEquals(rr.getTwentyYears(),   3.07,   0.001);
        assertEquals(rr.getThirtyYears(),   3.18,   0.001);


        // Validate delete one
        tbl.delete(d3);

        // Validate read all
        List<Treasury> l;
        l = tbl.getAll();
        assertEquals(2,l.size());


        // validate read last
        rr = tbl.getLast();
        assertEquals(rr.getOneMonth(),      2.36,   0.001);
        assertEquals(rr.getTwoMonths(),     2.43,   0.001);
        assertEquals(rr.getThreeMonths(),   2.46,   0.001);
        assertEquals(rr.getSixMonths(),     2.59,   0.001);
        assertEquals(rr.getOneYear(),       2.72,   0.001);
        assertEquals(rr.getTwoYears(),      2.81,   0.001);
        assertEquals(rr.getThreeYears(),    2.82,   0.001);
        assertEquals(rr.getFiveYears(),     2.80,   0.001);
        assertEquals(rr.getSevenYears(),    2.85,   0.001);
        assertEquals(rr.getTenYears(),      2.92,   0.001);
        assertEquals(rr.getTwentyYears(),   3.06,   0.001);
        assertEquals(rr.getThirtyYears(),   3.17,   0.001);

        // Validate Update
        r1.setOneYear(22.72);
        r1.setThirtyYears(33.17);
        tbl.upsert(r1);

        // validate update
        rr = tbl.get(d1);
        assertEquals(rr.getOneMonth(),      2.37,   0.001);
        assertEquals(rr.getTwoMonths(),     2.42,   0.001);
        assertEquals(rr.getThreeMonths(),   2.45,   0.001);
        assertEquals(rr.getSixMonths(),     2.58,   0.001);
        assertEquals(rr.getOneYear(),      22.72,   0.001);
        assertEquals(rr.getTwoYears(),      2.8,    0.001);
        assertEquals(rr.getThreeYears(),    2.81,   0.001);
        assertEquals(rr.getFiveYears(),     2.79,   0.001);
        assertEquals(rr.getSevenYears(),    2.84,   0.001);
        assertEquals(rr.getTenYears(),      2.91,   0.001);
        assertEquals(rr.getTwentyYears(),   3.05,   0.001);
        assertEquals(rr.getThirtyYears(),  33.17,   0.001);

        System.out.println("End");
    }
}

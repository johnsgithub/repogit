package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.HttpStatusException;

import java.io.IOException;

@Getter
@Setter
public class YahooStockStatistics {

    static final String GREATER_THAN = ">";
    static final String TABLE_START = "<table";
    static final String TABLE_END = "</table>";
    static final String TBODY_START = "<tbody";
    static final String TBODY_END = "</tbody>";
    static final String TR_START = "<tr";
    static final String TR_END = "</tr>";
    static final String TD_START = "<td ";
    static final String TD_END = "</td>";

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private long    marketCap;
    private long    enterpriseValue;
    private double  trailingPE;
    private double  forwardPE;
    private double  pegRatio;
    private double  priceSales;
    private double  priceBook;
    private double  evRevenue;
    private double  evEbitda;
    private double  beta;
    private long    avgVol3Month;
    private long    avgVol10Day;
    private double  shortRatio;
    private double  divRateForward;
    private double  divYieldForward;
    private double  profitMargin;
    private double  operatingMargin;
    private double  returnOnAssets;
    private double  returnOnEquity;
    private long    revenue;
    private double  revenueGrowth;
    private long    ebitda;
    private double  dilutedEps;
    private double  earningsGrowth;
    private long    totalCash;
    private double  totalCashPerShare;
    private long    totalDebt;
    private double  currentRatio;
    private long    operatingCashFlow;
    private long    freeCashFlow;

    private String  page;
    private boolean readFailed;

    public boolean readPage(String symbol) throws IOException {

        readFailed = false;

        // Yahoo replaces . with -
        String ticker = symbol.toUpperCase().replace('.','-');
        String baseUrl = "https://finance.yahoo.com/quote/" + ticker + "/key-statistics?p=" + ticker;
        try {
            Document doc = Jsoup.connect(baseUrl).get();
            page = doc.toString();
        } catch ( org.jsoup.HttpStatusException ex ) {
            readFailed = true;
        }

        // Validate page was found
        if (readFailed || page.contains("<span>No results for")  || page.contains("<span>Symbols similar to")) {
            return false;
        }

        // Valuation Measures
        valuationMeasuresTable();

        // Stock Price History
        stockPriceHistoryTable();

        // Share Statistics
        shareStatisticsTable();

        // Dividend and Splits
        dividendAndSplitsTable();

        // Financial Highlights

        // Profitability
        profitabilityTable();

        // Management Effectiveness
        managementEffectivenessTable();

        // Income Statement
        incomeStatementTable();

        // Balance Sheet
        balanceSheetTable();

        // Cash Flow
        cashFlowTable();

        return true;
    }

    public void valuationMeasuresTable() {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf("<span>Valuation Measures");

        if ( tableLabelStart == -1 ) {
            marketCap = vo.getLong("N/A");
            enterpriseValue = vo.getLong("N/A");
            trailingPE = vo.getDouble( "N/A" );
            forwardPE = vo.getDouble("N/A");
            pegRatio = vo.getDouble("N/A");
            priceSales = vo.getDouble("N/A");
            priceBook = vo.getDouble("N/A");
            evRevenue = vo.getDouble("N/A");
            evEbitda = vo.getDouble("N/A");
            return;
        }

        // find table body start and end
        int tableBodyStart = page.indexOf(TBODY_START, tableLabelStart);
        int tableBodyEnd   = page.indexOf(TBODY_END,   tableBodyStart);

        //  Market Cap
        // Start and end of row
        int marketCapStartTag  = page.indexOf("<span>Market Cap", tableBodyStart);
        int marketCapRowEnd    = page.indexOf(TR_END, marketCapStartTag);

        if ( marketCapStartTag > 0 && marketCapRowEnd < tableBodyEnd ) {
            // First data column value
            int marketCapStartTd = page.indexOf(TD_START, marketCapStartTag);
            int marketCapStartData = page.indexOf(GREATER_THAN, marketCapStartTd) + 1;
            int marketCapEndData = page.indexOf(TD_END, marketCapStartData);
            marketCap = vo.getLong(page.substring(marketCapStartData, marketCapEndData));

            if (page.substring(marketCapStartData, marketCapEndData).contains("N/A")) {
                // First column has no data check the second column
                marketCapStartTd = page.indexOf(TD_START, marketCapEndData) + 5;
                // make sure there is a second column in the row
                if (marketCapStartTd > 0 && marketCapStartTd < marketCapRowEnd) {
                    marketCapStartData = page.indexOf(GREATER_THAN, marketCapStartTd) + 1;
                    marketCapEndData = page.indexOf(TD_END, marketCapStartData);
                    marketCap = vo.getLong(page.substring(marketCapStartData, marketCapEndData));
                }
            }
        } else {
            marketCap = vo.getLong("N/A");
        }

        // Enterprise Value
        // start and end row
        int enterpriseValueStartTag  = page.indexOf("<span>Enterprise Value", tableBodyStart);
        int enterpriseValueRowEnd    = page.indexOf(TR_END, enterpriseValueStartTag);

        if ( enterpriseValueStartTag > 0 && enterpriseValueRowEnd < tableBodyEnd ) {
            // First data column value
            int enterpriseValueStartTd = page.indexOf(TD_START, enterpriseValueStartTag);
            int enterpriseValueStartData = page.indexOf(GREATER_THAN, enterpriseValueStartTd) + 1;
            int enterpriseValueEndData = page.indexOf(TD_END, enterpriseValueStartData);
            enterpriseValue = vo.getLong(page.substring(enterpriseValueStartData, enterpriseValueEndData));

            if (page.substring(enterpriseValueStartData, enterpriseValueEndData).contains("N/A")) {
                // First column has no data check the second column
                enterpriseValueStartTd = page.indexOf(TD_START, enterpriseValueEndData) + 5;
                // make sure there is a second column in the row
                if (enterpriseValueStartTd > 0 && enterpriseValueStartTd < enterpriseValueRowEnd) {
                    enterpriseValueStartData = page.indexOf(GREATER_THAN, enterpriseValueStartTd) + 1;
                    enterpriseValueEndData = page.indexOf(TD_END, enterpriseValueStartData);
                    enterpriseValue = vo.getLong(page.substring(enterpriseValueStartData, enterpriseValueEndData));
                }
            }
        } else {
            enterpriseValue = vo.getLong("N/A");
        }


        // Trailing P/E
        // start and end row
        int trailingPEStartTag  = page.indexOf("<span>Trailing P/E", tableBodyStart);
        int trailingPERowEnd    = page.indexOf(TR_END, trailingPEStartTag);

        if ( trailingPEStartTag > 0 && trailingPERowEnd < tableBodyEnd ) {
            // First data column value
            int trailingPEStartTd   = page.indexOf(TD_START, trailingPEStartTag);
            int trailingPEStartData = page.indexOf(GREATER_THAN, trailingPEStartTd) + 1;
            int trailingPEEndData   = page.indexOf(TD_END, trailingPEStartData);
            trailingPE = vo.getDouble( page.substring(trailingPEStartData, trailingPEEndData) );

            if ( page.substring(trailingPEStartData, trailingPEEndData).contains("N/A") ) {
                // First column has no data check the second column
                trailingPEStartTd   = page.indexOf(TD_START, trailingPEEndData) + 5;
                // make sure there is a second column in the row
                if ( trailingPEStartTd > 0 && trailingPEStartTd < trailingPERowEnd) {
                    trailingPEStartData = page.indexOf(GREATER_THAN, trailingPEStartTd) + 1;
                    trailingPEEndData   = page.indexOf(TD_END, trailingPEStartData);
                    trailingPE = vo.getDouble( page.substring(trailingPEStartData, trailingPEEndData) );
                }
            }
        } else {
            trailingPE = vo.getDouble( "N/A" );
        }

        // Forward P/E
        // start and end row
        int forwardPEStartTag  = page.indexOf("<span>Forward P/E", tableBodyStart);
        int forwardPERowEnd    = page.indexOf(TR_END, forwardPEStartTag);

        if ( forwardPEStartTag > 0 && forwardPERowEnd < tableBodyEnd ) {
            // First data column value
            int forwardPEStartTd = page.indexOf(TD_START, forwardPEStartTag);
            int forwardPEStartData = page.indexOf(GREATER_THAN, forwardPEStartTd) + 1;
            int forwardPEEndData = page.indexOf(TD_END, forwardPEStartData);
            forwardPE = vo.getDouble(page.substring(forwardPEStartData, forwardPEEndData));

            if (page.substring(forwardPEStartData, forwardPEEndData).contains("N/A")) {
                // First column has no data check the second column
                forwardPEStartTd = page.indexOf(TD_START, forwardPEEndData) + 5;
                // make sure there is a second column in the row
                if (forwardPEStartTd > 0 && forwardPEStartTd < forwardPERowEnd) {
                    forwardPEStartData = page.indexOf(GREATER_THAN, forwardPEStartTd) + 1;
                    forwardPEEndData = page.indexOf(TD_END, forwardPEStartData);
                    forwardPE = vo.getDouble(page.substring(forwardPEStartData, forwardPEEndData));
                }
            }
        } else {
            forwardPE = vo.getDouble("N/A");
        }


        // PEG Ratio
        // start and end row
        int pegRatioStartTag  = page.indexOf("<span>PEG Ratio", tableBodyStart);
        int pegRatioRowEnd    = page.indexOf(TR_END, pegRatioStartTag);

        if ( pegRatioStartTag > 0 && pegRatioRowEnd < tableBodyEnd ) {
            // First data column value
            int pegRatioStartTd = page.indexOf(TD_START, pegRatioStartTag);
            int pegRatioStartData = page.indexOf(GREATER_THAN, pegRatioStartTd) + 1;
            int pegRatioEndData = page.indexOf(TD_END, pegRatioStartData);
            pegRatio = vo.getDouble(page.substring(pegRatioStartData, pegRatioEndData));

            if (page.substring(pegRatioStartData, pegRatioEndData).contains("N/A")) {
                // First column has no data check the second column
                pegRatioStartTd = page.indexOf(TD_START, pegRatioEndData) + 5;
                // make sure there is a second column in the row
                if (pegRatioStartTd > 0 && pegRatioStartTd < pegRatioRowEnd) {
                    pegRatioStartData = page.indexOf(GREATER_THAN, pegRatioStartTd) + 1;
                    pegRatioEndData = page.indexOf(TD_END, pegRatioStartData);
                    pegRatio = vo.getDouble(page.substring(pegRatioStartData, pegRatioEndData));
                }
            }
        } else {
            pegRatio = vo.getDouble("N/A");
        }


        // Price/Sales
        // start and end row
        int priceSalesStartTag  = page.indexOf("<span>Price/Sales", tableBodyStart);
        int priceSalesRowEnd    = page.indexOf(TR_END, priceSalesStartTag);

        if ( priceSalesStartTag > 0 && priceSalesRowEnd < tableBodyEnd ) {
            // First data column value
            int priceSalesStartTd = page.indexOf(TD_START, priceSalesStartTag);
            int priceSalesStartData = page.indexOf(GREATER_THAN, priceSalesStartTd) + 1;
            int priceSalesEndData = page.indexOf(TD_END, priceSalesStartData);
            priceSales = vo.getDouble(page.substring(priceSalesStartData, priceSalesEndData));

            if (page.substring(priceSalesStartData, priceSalesEndData).contains("N/A")) {
                // First column has no data check the second column
                priceSalesStartTd = page.indexOf(TD_START, priceSalesEndData) + 5;
                if (priceSalesStartTd > 0 && priceSalesStartTd < priceSalesRowEnd) {
                    priceSalesStartData = page.indexOf(GREATER_THAN, priceSalesStartTd) + 1;
                    priceSalesEndData = page.indexOf(TD_END, priceSalesStartData);
                    priceSales = vo.getDouble(page.substring(priceSalesStartData, priceSalesEndData));
                }
            }
        } else {
            priceSales = vo.getDouble("N/A");
        }


        // Price/Book
        // start and end row
        int priceBookStartTag  = page.indexOf("<span>Price/Book", tableBodyStart);
        int priceBookRowEnd    = page.indexOf(TR_END, priceBookStartTag);

        if ( priceBookStartTag > 0 && priceBookRowEnd < tableBodyEnd ) {
            // First data column value
            int priceBookStartTd = page.indexOf(TD_START, priceBookStartTag);
            int priceBookStartData = page.indexOf(GREATER_THAN, priceBookStartTd) + 1;
            int priceBookEndData = page.indexOf(TD_END, priceBookStartData);
            priceBook = vo.getDouble(page.substring(priceBookStartData, priceBookEndData));

            if (page.substring(priceBookStartData, priceBookEndData).contains("N/A")) {
                // First column has no data check the second column
                priceBookStartTd = page.indexOf(TD_START, priceBookEndData) + 5;
                if (priceBookStartTd > 0 && priceBookStartTd < priceBookRowEnd) {
                    priceBookStartData = page.indexOf(GREATER_THAN, priceBookStartTd) + 1;
                    priceBookEndData = page.indexOf(TD_END, priceBookStartData);
                    priceBook = vo.getDouble(page.substring(priceBookStartData, priceBookEndData));
                }
            }
        } else {
            priceBook = vo.getDouble("N/A");
        }


        // Enterprise Value/Revenue
        // start and end row
        int evRevenueStartTag  = page.indexOf("<span>Enterprise Value/Revenue", tableBodyStart);
        int evRevenueRowEnd    = page.indexOf(TR_END, evRevenueStartTag);

        if ( evRevenueStartTag > 0 && evRevenueRowEnd < tableBodyEnd ) {
            // First data column value
            int evRevenueStartTd = page.indexOf(TD_START, evRevenueStartTag);
            int evRevenueStartData = page.indexOf(GREATER_THAN, evRevenueStartTd) + 1;
            int evRevenueEndData = page.indexOf(TD_END, evRevenueStartData);
            evRevenue = vo.getDouble(page.substring(evRevenueStartData, evRevenueEndData));

            if (page.substring(evRevenueStartData, evRevenueEndData).contains("N/A")) {
                // First column has no data check the second column
                evRevenueStartTd = page.indexOf(TD_START, evRevenueEndData) + 5;
                if (evRevenueStartTd > 0 && evRevenueStartTd < evRevenueRowEnd) {
                    evRevenueStartData = page.indexOf(GREATER_THAN, evRevenueStartTd) + 1;
                    evRevenueEndData = page.indexOf(TD_END, evRevenueStartData);
                    evRevenue = vo.getDouble(page.substring(evRevenueStartData, evRevenueEndData));
                }
            }
        } else {
            evRevenue = vo.getDouble("N/A");
        }


        // Enterprise Value/EBITDA
        // start and end row
        int evEbitdaStartTag  = page.indexOf("<span>Enterprise Value/EBITDA", tableBodyStart);
        int evEbitdaRowEnd    = page.indexOf(TR_END, evEbitdaStartTag);

        if ( evEbitdaStartTag > 0 && evRevenueRowEnd < tableBodyEnd ) {
            // First data column value
            int evEbitdaStartTd = page.indexOf(TD_START, evEbitdaStartTag);
            int evEbitdaStartData = page.indexOf(GREATER_THAN, evEbitdaStartTd) + 1;
            int evEbitdaEndData = page.indexOf(TD_END, evEbitdaStartData);
            evEbitda = vo.getDouble(page.substring(evEbitdaStartData, evEbitdaEndData));

            if (page.substring(evEbitdaStartData, evEbitdaEndData).contains("N/A")) {
                // First column has no data check the second column
                evEbitdaStartTd = page.indexOf(TD_START, evEbitdaEndData) + 5;
                if (evEbitdaStartTd > 0 && evEbitdaStartTd < evEbitdaRowEnd) {
                    evEbitdaStartData = page.indexOf(GREATER_THAN, evEbitdaStartTd) + 1;
                    evEbitdaEndData = page.indexOf(TD_END, evEbitdaStartData);
                    evEbitda = vo.getDouble(page.substring(evEbitdaStartData, evEbitdaEndData));
                }
            }
        } else {
            evEbitda = vo.getDouble("N/A");
        }
    }

    public void stockPriceHistoryTable() {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf("<span>Stock Price History");

        if ( tableLabelStart == -1 ) {
            beta = vo.getDouble("N/A");
            return;
        }

        // find table body start and end
        int tableBodyStart = page.indexOf(TBODY_START, tableLabelStart);
        int tableBodyEnd   = page.indexOf(TBODY_END,   tableBodyStart);

        // Beta (5Y Monthly)
        int betaStartTag  = page.indexOf("<span>Beta (5Y Monthly)", tableBodyStart);
        int betaStartTd   = page.indexOf(TD_START, betaStartTag);
        int betaStartData = page.indexOf(GREATER_THAN, betaStartTd) + 1;
        int betaEndData   = page.indexOf(TD_END, betaStartData);
        if ( betaEndData < tableBodyEnd ) {
            beta = vo.getDouble(page.substring(betaStartData, betaEndData));
        } else {
            beta = vo.getDouble("N/A");
        }
    }

    public void shareStatisticsTable() {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf("<span>Share Statistics");

        if ( tableLabelStart == -1 ) {
            avgVol3Month = vo.getLong("N/A");
            avgVol10Day = vo.getLong("N/A");
            shortRatio = vo.getDouble("N/A");
            return;
        }

        // find table body start and end
        int tableBodyStart = page.indexOf(TBODY_START, tableLabelStart);
        int tableBodyEnd   = page.indexOf(TBODY_END,   tableBodyStart);

        // AvgVol3Month
        int avgVol3MonthTag       = page.indexOf("<span>Avg Vol (3 month)", tableBodyStart);
        int avgVol3MonthTd        = page.indexOf(TD_START, avgVol3MonthTag);
        int avgVol3MonthStartData = page.indexOf(GREATER_THAN, avgVol3MonthTd) + 1;
        int avgVol3MonthEndData   = page.indexOf(TD_END,avgVol3MonthStartData);
        if ( avgVol3MonthEndData < tableBodyEnd ) {
            avgVol3Month = vo.getLong( page.substring(avgVol3MonthStartData, avgVol3MonthEndData) );
        } else {
            avgVol3Month = vo.getLong("N/A");
        }

        // AvgVol10Day
        int avgVol10DayTag       = page.indexOf("<span>Avg Vol (10 day)", tableBodyStart);
        int avgVol10DayTd        = page.indexOf(TD_START, avgVol10DayTag);
        int avgVol10DayStartData = page.indexOf(GREATER_THAN, avgVol10DayTd) + 1;
        int avgVol10DayEndData   = page.indexOf(TD_END,avgVol10DayStartData);
        if ( avgVol10DayEndData < tableBodyEnd ) {
            avgVol10Day = vo.getLong( page.substring(avgVol10DayStartData, avgVol10DayEndData) );
        } else {
            avgVol10Day = vo.getLong("N/A");
        }

        // ShortRatio
        int shortRatioTag       = page.indexOf("<span>Short Ratio ", tableBodyStart);
        int shortRatioTd        = page.indexOf(TD_START, shortRatioTag);
        int shortRatioStartData = page.indexOf(GREATER_THAN, shortRatioTd) + 1;
        int shortRatioEndData   = page.indexOf(TD_END,shortRatioStartData);
        if ( shortRatioEndData < tableBodyEnd ) {
            shortRatio = vo.getDouble( page.substring(shortRatioStartData, shortRatioEndData) );
        } else {
            shortRatio = vo.getDouble("N/A");
        }
    }

    public void dividendAndSplitsTable() {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf("<span>Dividends &amp; Splits");

        if ( tableLabelStart == -1 ) {
            divRateForward = vo.getPercent("N/A");
            divYieldForward = vo.getPercent("N/A");
            return;
        }

        // find table body start and end
        int tableBodyStart = page.indexOf(TBODY_START, tableLabelStart);
        int tableBodyEnd   = page.indexOf(TBODY_END,   tableBodyStart);


        // Forward Annual Dividend Rate
        int divRateForwardTag       = page.indexOf("<span>Forward Annual Dividend Rate", tableBodyStart);
        int divRateForwardTd        = page.indexOf(TD_START, divRateForwardTag);
        int divRateForwardStartData = page.indexOf(GREATER_THAN, divRateForwardTd) + 1;
        int divRateForwardEndData   = page.indexOf(TD_END,divRateForwardStartData);
        if ( divRateForwardEndData < tableBodyEnd ) {
            divRateForward = vo.getPercent( page.substring(divRateForwardStartData, divRateForwardEndData) );
        } else {
            divRateForward = vo.getPercent("N/A");
        }

        // Forward Annual Dividend Yield
        int divYieldForwardTag       = page.indexOf("<span>Forward Annual Dividend Yield", tableBodyStart);
        int divYieldForwardTd        = page.indexOf(TD_START, divYieldForwardTag);
        int divYieldForwardStartData = page.indexOf(GREATER_THAN, divYieldForwardTd) + 1;
        int divYieldForwardEndData   = page.indexOf(TD_END,divYieldForwardStartData);
        if ( divYieldForwardEndData < tableBodyEnd ) {
            divYieldForward = vo.getPercent( page.substring(divYieldForwardStartData, divYieldForwardEndData) );
        } else {
            divYieldForward = vo.getPercent("N/A");
        }
    }

    public void profitabilityTable() {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf("<span>Profitability");

        if ( tableLabelStart == -1 ) {
            profitMargin = vo.getPercent("N/A");
            operatingMargin = vo.getPercent("N/A");
            return;
        }

        // find table body start and end
        int tableBodyStart = page.indexOf(TBODY_START, tableLabelStart);
        int tableBodyEnd   = page.indexOf(TBODY_END,   tableBodyStart);

        // Profit Margin
        int profitMarginTag       = page.indexOf("<span>Profit Margin", tableBodyStart);
        int profitMarginTd        = page.indexOf(TD_START, profitMarginTag);
        int profitMarginStartData = page.indexOf(GREATER_THAN, profitMarginTd) + 1;
        int profitMarginEndData   = page.indexOf(TD_END,profitMarginStartData);
        if ( profitMarginEndData < tableBodyEnd ) {
            profitMargin = vo.getPercent( page.substring(profitMarginStartData, profitMarginEndData) );
        } else {
            profitMargin = vo.getPercent("N/A");
        }

        // Operating Margin
        int operatingMarginTag       = page.indexOf("<span>Operating Margin", tableBodyStart);
        int operatingMarginTd        = page.indexOf(TD_START, operatingMarginTag);
        int operatingMarginStartData = page.indexOf(GREATER_THAN, operatingMarginTd) + 1;
        int operatingMarginEndData   = page.indexOf(TD_END,operatingMarginStartData);
        if ( operatingMarginEndData < tableBodyEnd ) {
            operatingMargin = vo.getPercent( page.substring(operatingMarginStartData, operatingMarginEndData) );
        } else {
            operatingMargin = vo.getPercent("N/A");
        }
    }

    public void managementEffectivenessTable() {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf("<span>Management Effectiveness");

        if ( tableLabelStart == -1 ) {
            returnOnAssets = vo.getPercent("N/A");
            returnOnEquity = vo.getPercent("N/A");
            return;
        }

        // find table body start and end
        int tableBodyStart = page.indexOf(TBODY_START, tableLabelStart);
        int tableBodyEnd   = page.indexOf(TBODY_END,   tableBodyStart);

        // Return On Assets
        int returnOnAssetsTag       = page.indexOf("<span>Return on Assets", tableBodyStart);
        int returnOnAssetsTd        = page.indexOf(TD_START, returnOnAssetsTag);
        int returnOnAssetsStartData = page.indexOf(GREATER_THAN, returnOnAssetsTd) + 1;
        int returnOnAssetsEndData   = page.indexOf(TD_END,returnOnAssetsStartData);
        if ( returnOnAssetsEndData < tableBodyEnd ) {
            returnOnAssets = vo.getPercent( page.substring(returnOnAssetsStartData, returnOnAssetsEndData) );
        } else {
            returnOnAssets = vo.getPercent("N/A");
        }

        // Return On Equity
        int returnOnEquityTag       = page.indexOf("<span>Return on Equity", tableBodyStart);
        int returnOnEquityTd        = page.indexOf(TD_START, returnOnEquityTag);
        int returnOnEquityStartData = page.indexOf(GREATER_THAN, returnOnEquityTd) + 1;
        int returnOnEquityEndData   = page.indexOf(TD_END,returnOnEquityStartData);
        if ( returnOnEquityEndData < tableBodyEnd ) {
            returnOnEquity = vo.getPercent( page.substring(returnOnEquityStartData, returnOnEquityEndData) );
        } else {
            returnOnEquity = vo.getPercent("N/A");
        }
    }


    public void incomeStatementTable() {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf("<span>Income Statement");

        if ( tableLabelStart == -1 ) {
            revenue = vo.getLong("N/A");
            revenueGrowth = vo.getPercent("N/A");
            ebitda = vo.getLong("N/A");
            dilutedEps = vo.getDouble("N/A");
            earningsGrowth = vo.getDouble("N/A");
            return;
        }

        // find table body start and end
        int tableBodyStart = page.indexOf(TBODY_START, tableLabelStart);
        int tableBodyEnd   = page.indexOf(TBODY_END,   tableBodyStart);

        // Revenue
        int revenueTag       = page.indexOf("<span>Revenue", tableBodyStart);
        int revenueTd        = page.indexOf(TD_START, revenueTag);
        int revenueStartData = page.indexOf(GREATER_THAN, revenueTd) + 1;
        int revenueEndData   = page.indexOf(TD_END,revenueStartData);
        if ( revenueEndData < tableBodyEnd ) {
            revenue = vo.getLong( page.substring(revenueStartData, revenueEndData) );
        } else {
            revenue = vo.getLong("N/A");
        }

        // Quarterly Revenue Growth
        int revenueGrowthTag       = page.indexOf("<span>Quarterly Revenue Growth", tableBodyStart);
        int revenueGrowthTd        = page.indexOf(TD_START, revenueGrowthTag);
        int revenueGrowthStartData = page.indexOf(GREATER_THAN, revenueGrowthTd) + 1;
        int revenueGrowthEndData   = page.indexOf(TD_END,revenueGrowthStartData);
        if ( revenueGrowthEndData < tableBodyEnd ) {
            revenueGrowth = vo.getPercent( page.substring(revenueGrowthStartData, revenueGrowthEndData) );
        } else {
            revenueGrowth = vo.getPercent("N/A");
        }

        // EBITDA
        int ebitdaTag       = page.indexOf("<span>EBITDA", tableBodyStart);
        int ebitdaTd        = page.indexOf(TD_START, ebitdaTag);
        int ebitdaStartData = page.indexOf(GREATER_THAN, ebitdaTd) + 1;
        int ebitdaEndData   = page.indexOf(TD_END,ebitdaStartData);
        if ( ebitdaEndData < tableBodyEnd ) {
            ebitda = vo.getLong( page.substring(ebitdaStartData, ebitdaEndData) );
        } else {
            ebitda = vo.getLong("N/A");
        }

        // Diluted EPS
        int dilutedEpsTag       = page.indexOf("<span>Diluted EPS", tableBodyStart);
        int dilutedEpsTd        = page.indexOf(TD_START, dilutedEpsTag);
        int dilutedEpsStartData = page.indexOf(GREATER_THAN, dilutedEpsTd) + 1;
        int dilutedEpsEndData   = page.indexOf(TD_END,dilutedEpsStartData);
        if ( dilutedEpsEndData < tableBodyEnd ) {
            dilutedEps = vo.getDouble( page.substring(dilutedEpsStartData, dilutedEpsEndData) );
        } else {
            dilutedEps = vo.getDouble("N/A");
        }

        // Quarterly Earnings Growth
        int earningsGrowthTag       = page.indexOf("<span>Quarterly Earnings Growth", tableBodyStart);
        int earningsGrowthTd        = page.indexOf(TD_START, earningsGrowthTag);
        int earningsGrowthStartData = page.indexOf(GREATER_THAN, earningsGrowthTd) + 1;
        int earningsGrowthEndData   = page.indexOf(TD_END,earningsGrowthStartData);
        if ( earningsGrowthEndData < tableBodyEnd ) {
            earningsGrowth = vo.getPercent( page.substring(earningsGrowthStartData, earningsGrowthEndData) );
        } else {
            earningsGrowth = vo.getDouble("N/A");
        }
    }


    public void balanceSheetTable() {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf("<span>Balance Sheet");

        if ( tableLabelStart == -1 ) {
            totalCash = vo.getLong("N/A");
            totalCashPerShare = vo.getDouble("N/A");
            totalDebt = vo.getLong("N/A");
            currentRatio = vo.getDouble("N/A");
            return;
        }

        // find table body start and end
        int tableBodyStart = page.indexOf(TBODY_START, tableLabelStart);
        int tableBodyEnd   = page.indexOf(TBODY_END,   tableBodyStart);

        // Total Cash
        int totalCashTag       = page.indexOf("<span>Total Cash", tableBodyStart);
        int totalCashTd        = page.indexOf(TD_START, totalCashTag);
        int totalCashStartData = page.indexOf(GREATER_THAN, totalCashTd) + 1;
        int totalCashEndData   = page.indexOf(TD_END,totalCashStartData);
        if ( totalCashEndData < tableBodyEnd ) {
            totalCash = vo.getLong( page.substring(totalCashStartData, totalCashEndData) );
        } else {
            totalCash = vo.getLong("N/A");
        }

        // Total Cash Per Share
        int totalCashPerShareTag       = page.indexOf("<span>Total Cash Per Share", tableBodyStart);
        int totalCashPerShareTd        = page.indexOf(TD_START, totalCashPerShareTag);
        int totalCashPerShareStartData = page.indexOf(GREATER_THAN, totalCashPerShareTd) + 1;
        int totalCashPerShareEndData   = page.indexOf(TD_END,totalCashPerShareStartData);
        if ( totalCashPerShareEndData < tableBodyEnd ) {
            totalCashPerShare = vo.getDouble( page.substring(totalCashPerShareStartData, totalCashPerShareEndData) );
        } else {
            totalCashPerShare = vo.getDouble("N/A");
        }

        // Total Debt
        int totalDebtTag       = page.indexOf("<span>Total Debt", tableBodyStart);
        int totalDebtTd        = page.indexOf(TD_START, totalDebtTag);
        int totalDebtStartData = page.indexOf(GREATER_THAN, totalDebtTd) + 1;
        int totalDebtEndData   = page.indexOf(TD_END,totalDebtStartData);
        if ( totalDebtEndData < tableBodyEnd ) {
            totalDebt = vo.getLong( page.substring(totalDebtStartData, totalDebtEndData) );
        } else {
            totalDebt = vo.getLong("N/A");
        }

        // Current Ratio
        int currentRatioTag       = page.indexOf("<span>Current Ratio", tableBodyStart);
        int currentRatioTd        = page.indexOf(TD_START, currentRatioTag);
        int currentRatioStartData = page.indexOf(GREATER_THAN, currentRatioTd) + 1;
        int currentRatioEndData   = page.indexOf(TD_END,currentRatioStartData);
        if ( currentRatioEndData < tableBodyEnd ) {
            currentRatio = vo.getDouble( page.substring(currentRatioStartData, currentRatioEndData) );
        } else {
            currentRatio = vo.getDouble("N/A");
        }
    }

    public void cashFlowTable() {
        ValueObject vo = new ValueObject();

        // find table label
        int tableLabelStart = page.indexOf("<span>Cash Flow Statement");

        if ( tableLabelStart == -1 ) {
            operatingCashFlow = vo.getLong("N/A");
            freeCashFlow = vo.getLong("N/A");
            return;
        }

        // find table body start and end
        int tableBodyStart = page.indexOf(TBODY_START, tableLabelStart);
        int tableBodyEnd   = page.indexOf(TBODY_END,   tableBodyStart);

        // Operating Cash Flow
        int operatingCashFlowTag       = page.indexOf("<span>Operating Cash Flow", tableBodyStart);
        int operatingCashFlowTd        = page.indexOf(TD_START, operatingCashFlowTag);
        int operatingCashFlowStartData = page.indexOf(GREATER_THAN, operatingCashFlowTd) + 1;
        int operatingCashFlowEndData   = page.indexOf(TD_END,operatingCashFlowStartData);
        if ( operatingCashFlowEndData < tableBodyEnd ) {
            operatingCashFlow = vo.getLong( page.substring(operatingCashFlowStartData, operatingCashFlowEndData) );
        } else {
            operatingCashFlow = vo.getLong("N/A");
        }

        // Free Cash Flow
        int freeCashFlowTag       = page.indexOf("<span>Levered Free Cash Flow", tableBodyStart);
        int freeCashFlowTd        = page.indexOf(TD_START, freeCashFlowTag);
        int freeCashFlowStartData = page.indexOf(GREATER_THAN, freeCashFlowTd) + 1;
        int freeCashFlowEndData   = page.indexOf(TD_END,freeCashFlowStartData);
        if ( freeCashFlowEndData < tableBodyEnd ) {
            freeCashFlow = vo.getLong( page.substring(freeCashFlowStartData, freeCashFlowEndData) );
        } else {
            freeCashFlow = vo.getLong("N/A");
        }
    }


    public void print() {
        System.out.println( "marketCap         " + marketCap );
        System.out.println( "enterpriseValue   " + enterpriseValue );
        System.out.println( "trailingPE        " + trailingPE );
        System.out.println( "forwardPE         " + forwardPE );
        System.out.println( "pegRatio          " + pegRatio );
        System.out.println( "priceSales        " + priceSales );
        System.out.println( "priceBook         " + priceBook );
        System.out.println( "evRevenue         " + evRevenue );
        System.out.println( "evEbitda          " + evEbitda );
        System.out.println( " " );
        System.out.println( "beta              " + beta );
        System.out.println( "avgVol3Month      " + avgVol3Month );
        System.out.println( "avgVol10Day       " + avgVol10Day );
        System.out.println( "shortRatio        " + shortRatio );
        System.out.println( "divRateForward    " + divRateForward );
        System.out.println( "divYieldForward   " + divYieldForward );

        System.out.println( " " );
        System.out.println( "profitMargin      " + profitMargin );
        System.out.println( "operatingMargin   " + operatingMargin );
        System.out.println( "returnOnAssets    " + returnOnAssets );
        System.out.println( "returnOnEquity    " + returnOnEquity );

        System.out.println( " " );
        System.out.println( "revenue           " + revenue );
        System.out.println( "revenueGrowth     " + revenueGrowth );
        System.out.println( "ebitda            " + ebitda );
        System.out.println( "dilutedEps        " + dilutedEps );
        System.out.println( "earningsGrowth    " + earningsGrowth );

        System.out.println( "totalCash         " + totalCash );
        System.out.println( "totalCashPerShare " + totalCashPerShare );
        System.out.println( "totalDebt         " + totalDebt );
        System.out.println( "currentRatio      " + currentRatio );
        System.out.println( "operatingCashFlow " + operatingCashFlow );
        System.out.println( "freeCashFlow      " + freeCashFlow );
    }
}

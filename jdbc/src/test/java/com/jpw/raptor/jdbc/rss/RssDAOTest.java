package com.jpw.raptor.jdbc.rss;

import com.jpw.raptor.model.RssEntry;
import org.apache.commons.codec.digest.DigestUtils;

import com.jpw.raptor.model.Rss;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.core.io.ClassPathResource;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RssDAOTest {

    RssDAO tbl;

    RssEntry testRec;
    RssEntry  r1;
    RssEntry  r2;
    RssEntry  r3;
    RssEntry  r4;

    public static final String d1 = "2022-08-01";
    public static final String d2 = "2022-07-01";
    public static final String d3 = "2022-06-01";
    public static final String d4 = "2022-05-01";

    public static final String t1 = "The Dow closed above 34,000 Tuesday ";
    public static final String t2 = "Elon Musk said on Tuesday he was buying football club Manchester United Plc.";
    public static final String t3 = "Oil prices rose on Wednesday, recovering from six-month lows ";
    public static final String t4 = "Gold prices moved little in Asian trade on Wednesday";

    public static final String url1 = "https://mail.google.com/mail/u/0/#inbox";
    public static final String url2 = "https://www.elastic.co/guide/en/elasticsearch";
    public static final String url3 = "https://www.whatsbestforum.com/forums/";
    public static final String url4 = "https://www.foxnews.com/";

    public static final String uri1 = "abc";
    public static final String uri2 = "def";
    public static final String uri3 = "ghi";
    public static final String uri4 = "jkl";

    static class PostgresResource  {

        // postgres:9.6.8

        PostgreSQLContainer postgreSQLContainer;
        PGSimpleDataSource dataSource;

        public PostgresResource() {
            postgreSQLContainer = new PostgreSQLContainer("postgres:10.11")
                    .withDatabaseName("test-jdbc")
                    .withUsername("sa")
                    .withPassword("sa");

            postgreSQLContainer.start();

            dataSource = new PGSimpleDataSource();
            dataSource.setDatabaseName(postgreSQLContainer.getDatabaseName());
            dataSource.setServerName(postgreSQLContainer.getContainerIpAddress());
            dataSource.setPortNumber(postgreSQLContainer.getMappedPort(5432));
            dataSource.setUser(postgreSQLContainer.getUsername());
            dataSource.setPassword(postgreSQLContainer.getPassword());

        }

        public void stop() { postgreSQLContainer.stop(); }

        public PGSimpleDataSource getDataSource () {return dataSource;}

    }

    private static RssDAOTest.PostgresResource myPostgresResource;

    @BeforeClass
    public static void setUp() throws java.text.ParseException {
        myPostgresResource = new RssDAOTest.PostgresResource();
    }

    @Before
    public void setUpTest() throws java.text.ParseException {

        // Create 4 test records
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateAsString = sdf.format(new Date());

        r1 = new RssEntry(DigestUtils.md5Hex(uri1).toUpperCase(), sdf.parse(d1), t1, url1, uri1);
        r2 = new RssEntry(DigestUtils.md5Hex(uri2).toUpperCase(), sdf.parse(d2), t2, url2, uri2);
        r3 = new RssEntry(DigestUtils.md5Hex(uri3).toUpperCase(), sdf.parse(d3), t3, url3, uri3);
        r4 = new RssEntry(DigestUtils.md5Hex(uri4).toUpperCase(), sdf.parse(d4), t4, url4, uri4);
    }

    @AfterClass
    public static void tearDown(){
        myPostgresResource.stop();
    }


    @Test
    public void test01() throws Exception {

        System.out.println("test01");

        // Create connection to docker database
        tbl = new RssDAO();
        tbl.setDataSource(myPostgresResource.getDataSource());
        tbl.postConstruct();

        // Create table for testing
        File   resource;
        String cmds;

        resource = new ClassPathResource("Rss.sql").getFile();
        cmds = new String(Files.readAllBytes(resource.toPath()));
        tbl.sqlScript(cmds);

        tbl.add(r1);
        tbl.add(r2);
        tbl.add(r3);
        tbl.add(r4);

        // read all records only 1 should be found
        List<RssEntry> recs = tbl.getAll();

        assertEquals(4, recs.size());

        RssEntry read1 = recs.get(0);

        assertTrue(r1.getTitle().equalsIgnoreCase(t1));
        assertTrue(r1.getUrl().equalsIgnoreCase(url1));
        assertTrue(r1.getUri().equalsIgnoreCase(uri1));

        List<RssEntry> prior = tbl.getPrior(new Date());
        assertEquals(4, prior.size());

        tbl.deleteAll();

        List<RssEntry> noRecs = tbl.getAll();
        assertEquals(0, noRecs.size());
    }

}

package com.jpw.raptor.onetime.generic;

import javax.imageio.IIOException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

public class Worker {

    private static final String directory = "/home/finance/runtime/files/select_to_invest";
    private static final String inFile = "20240523_other.csv";
    private static final String outFile = "result.csv";

    public void doit() throws IOException {
        FileUtil fu = new FileUtil();

        List<Entry> inRecs = fu.getRecords(directory + "/" + inFile);

        double ytdMin       = 40.0;
        double oneYearMin   = 0.0;
        double mult         = 1.5;

        List<Entry> outRecs = fu.selectRecords(inRecs, ytdMin, oneYearMin, mult);

        fu.writeRecords(directory + "/" + outFile, outRecs);

    }


}

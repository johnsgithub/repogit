package com.jpw.raptor.analytic;

import com.beust.jcommander.Parameter;

/**
 * Created by john on 5/09/20.
 */
public class AppParameters {

    @Parameter(names = "-ref",
            description = "reference symbol",
            required = false)
    private String refSymbol;

    public String getRefSymbol() {
        return refSymbol;
    }


    @Parameter(names = "-equity",
            description = "Equity symbol",
            required = false)
    private String equitySymbol;

    public String getEquitySymbol() {
        return equitySymbol;
    }


    @Parameter(names = "-algo",
            description = "Algorithm",
            required = false)
    private String algorithm;

    public String getAlgorithm() {
        return algorithm;
    }

}

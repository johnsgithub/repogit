package com.jpw.raptor.jdbc.reddit;


import com.jpw.raptor.model.RedditPost;

import java.util.List;

public interface RedditPostInterface {

    public int sqlScript(String script);

    public void add(RedditPost rec);

    public void delete(long rowId);

    public void deleteAll();

    public RedditPost get(long rowId);

    public List<RedditPost> getAll();

    public List<RedditPost> getStock(String symbol);

    public List<RedditPost> getCommunity(String symbol);

    public List<RedditPost> getPost(String symbol);
}

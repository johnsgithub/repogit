package com.jpw.raptor.cmdline.ingest;

import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.model.FileQualified;
import com.jpw.raptor.model.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;


@Component
public class VixFile {

    @Autowired
    public QuoteDAO tbl;

    public void processFile(FileQualified fileQualified) {

        // Used to read data file
        BufferedReader br = null;
        String         line;

        // Get last date processed
        Quote lastProcessed = tbl.getCurrent("VIX");
        if ( lastProcessed == null ) {
            try {
                lastProcessed = new Quote();
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                lastProcessed.setDate(format.parse("20210101"));
            }
            catch(Exception e) {
            }
        }

        try {
            br  = new BufferedReader(new FileReader(fileQualified.getName()));

            // Ignore the firs line
            line = br.readLine();

            // Read each record in the file
            while ((line = br.readLine()) != null)
            {
                // Parse the string into an array of fields
                Quote rec = parseLine (line);

                // parse data entry
                if ( rec == null ) {
                    // do not process invalid data
                } else {
                    // Only new dates
                    if (rec.getDate().after(lastProcessed.getDate()) ){
                        tbl.add(rec);
                    }
                }
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if ( br != null ) {
                    br.close();
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public Quote parseLine (String line) {

        // Parse the string into an array of fields
        // Validate number of fields parsed
        String[] str = line.split(",");
        if ( str.length >= 2 ) {
            // Valid number of columns
        } else {
            // invalid number of fields
            return null;
        }

        // Create the quote
        Quote quote = new Quote();

        // Symbol
        quote.setSymbol("VIX");

        // validate date
        try {
            // Convert string to date object
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            quote.setDate(sdf.parse(str[0]));
        }
        catch(Exception e) {
            System.out.println("Quote parse date error " + line);
            return null;
        }

        // Validate open, high, low, close and volume
        try {
            double value = Double.parseDouble(str[1]);
            quote.setOpen(value);
            quote.setHigh(value);
            quote.setLow(value);
            quote.setClose(value);
            quote.setVolume(0L);
        }
        catch(NumberFormatException e) {
            System.out.println("VIX parse error " + line);
            return null;
        }

        return quote;
    }

}

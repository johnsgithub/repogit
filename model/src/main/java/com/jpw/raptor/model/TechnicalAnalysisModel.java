package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TechnicalAnalysisModel {
    String          symbol;
    String          name;
    Date            date;
    double          close;
    Jdk             jdk;
    double          jdkSignal;
    Adx             adx;
    String          adxSignal;
    Macd            macd;
    String          macdSignal;
    AveragesModel   avgs;
    String          mAvgSignal;
    double          rsi;
    String          rsiSignal;
    Atr             atr;
    double          atrSignal;

    public TechnicalAnalysisModel() {
        symbol        = null;
        name          = null;
        date          = null;
        close         = 0.0;
        jdk           = null;
        jdkSignal     = 0.0;
        adx           = null;
        adxSignal     = null;
        macd          = null;
        macdSignal    = null;
        avgs          = null;
        mAvgSignal    = null;
        rsi           = 0.0;
        rsiSignal     = null;
        atr           = null;
        atrSignal     = 0.0;
    }
}

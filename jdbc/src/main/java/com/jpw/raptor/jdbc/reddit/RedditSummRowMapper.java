package com.jpw.raptor.jdbc.reddit;

import com.jpw.raptor.model.RedditSumm;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RedditSummRowMapper implements RowMapper<RedditSumm> {

    //private static final Logger logger = LoggerFactory.getLogger(RedditSummRowMapper.class);

    @Override
    public RedditSumm mapRow(ResultSet rs, int rowNum) throws SQLException {

        RedditSumm rec = new RedditSumm();
        rec.setRowId(rs.getLong("row_id"));
        rec.setDate(rs.getDate("date_tx"));
        rec.setStock(rs.getString("stock"));
        rec.setNum(rs.getInt("num"));
        rec.setName(rs.getString("name"));

        return rec;
    }
}

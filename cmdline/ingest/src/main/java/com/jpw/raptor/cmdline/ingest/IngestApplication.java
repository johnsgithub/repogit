package com.jpw.raptor.cmdline.ingest;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import com.jpw.raptor.model.FileQualified;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;


@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class IngestApplication implements CommandLineRunner {

    @Autowired
    public EodDataFile      eodFile;

    @Autowired
    public HiYieldFile      hiYieldFile;

    @Autowired
    public TreasuryFile     treasuryFile;

    @Autowired
    public VixFile          vixFile;

    @Autowired
    public DollarIndexFile  dollarIndexFile;


    public void eodIngest() {

        System.out.println();
        System.out.println("***************  EOD data ingest  *******************");

        EodFileUtil fileUtil = new EodFileUtil();

        // Get a list of files to process
        List<FileQualified> filesToProcess = fileUtil.getFileList();

        // Process each file
        for ( FileQualified fileQualified : filesToProcess ) {
            System.out.println("Processs " + fileQualified.getName());

            // Extract quotes and write them to the database
            eodFile.processFile(fileQualified);

            // Delete the file
            fileUtil.deleteFile(fileQualified.getName());
        }

    }


    public void hiYieldIngest() {

        System.out.println();
        System.out.println("***************  Hi Yield  data ingest  *******************");

        EodFileUtil fileUtil = new EodFileUtil();

        // Get a list of files to process
        List<FileQualified> filesToProcess = fileUtil.getHiYieldFileList();

        // Process each file
        for ( FileQualified fileQualified : filesToProcess ) {
            System.out.println("Processs " + fileQualified.getName());

            // Extract quotes and write them to the database
            hiYieldFile.processFile(fileQualified);

            // Delete the file
            fileUtil.deleteFile(fileQualified.getName());
        }

    }

    public void treasuryIngest() {

        System.out.println();
        System.out.println("***************  Treasury  data ingest  *******************");

        EodFileUtil fileUtil = new EodFileUtil();

        // Get a list of files to process
        List<FileQualified> filesToProcess = fileUtil.getTreasuryFileList();

        // Process each file
        for ( FileQualified fileQualified : filesToProcess ) {
            System.out.println("Processs " + fileQualified.getName());

            // Extract quotes and write them to the database
            treasuryFile.processFile(fileQualified);

            // Delete the file
            fileUtil.deleteFile(fileQualified.getName());
        }

    }


    public void vixIngest() {

        System.out.println();
        System.out.println("***************  VIX  data ingest  *******************");

        EodFileUtil fileUtil = new EodFileUtil();

        // Get a list of files to process
        List<FileQualified> filesToProcess = fileUtil.getVixFileList();

        // Process each file
        for ( FileQualified fileQualified : filesToProcess ) {
            System.out.println("Processs " + fileQualified.getName());

            // Extract quotes and write them to the database
            vixFile.processFile(fileQualified);

            // Delete the file
            fileUtil.deleteFile(fileQualified.getName());
        }

    }

    public void dollarIndexIngest() {

        System.out.println();
        System.out.println("***************  Dollar Index  data ingest  *******************");

        EodFileUtil fileUtil = new EodFileUtil();

        // Get a list of files to process
        List<FileQualified> filesToProcess = fileUtil.getDollarIndexFileList();

        // Process each file
        for ( FileQualified fileQualified : filesToProcess ) {
            System.out.println("Processs " + fileQualified.getName());

            // Extract quotes and write them to the database
            dollarIndexFile.processFile(fileQualified);

            // Delete the file
            fileUtil.deleteFile(fileQualified.getName());
        }

    }

    // Main loop
    @Override
    public void run(String... args) throws Exception {

        // define the run time parameters
        AppParameters params = new AppParameters();

        // create parameter parser
        JCommander cmd = new JCommander(params);

        try {
            cmd.parse(args);
            eodIngest();
            hiYieldIngest();
            treasuryIngest();
            vixIngest();
            dollarIndexIngest();
            System.out.println("Update performance");
            eodFile.updatePerformance();
        } catch (ParameterException ex) {
            System.out.println(ex.getMessage());
            cmd.usage();
        }

        System.out.println("***************  Good bye  *******************");
        System.out.println();

    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(IngestApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit( app.run(args) ) );
    }

}

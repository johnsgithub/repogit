package com.jpw.raptor.jdbc.reddit;

import com.jpw.raptor.model.RedditSumm;

import java.util.List;

public interface RedditSummInterface {

    public int sqlScript(String script);

    public void add(RedditSumm rec);

    public void deleteAll();

    public List<RedditSumm> getAll();

    public List<RedditSumm> getAllAsc();

}

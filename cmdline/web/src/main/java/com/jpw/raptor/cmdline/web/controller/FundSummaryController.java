package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.cmdline.web.service.SummaryDataService;
import com.jpw.raptor.jdbc.fund.FundDAO;
import com.jpw.raptor.model.Fund;
import com.jpw.raptor.model.SummaryModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created by john on 4/9/18.
 */
@Controller
public class FundSummaryController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    FundDAO fundTbl;

    @Autowired
    SummaryDataService summaryDataService;

    protected static final  String RECORDS = "summ_model";
    protected static final  String PAGE_TITLE = "pagetitle";
    protected static final  String ENCODING = "UTF-8";
    protected static final  String SUMMARY_PAGE = "mf-summary";
    protected static final  String SUMMARY_FACTOR_PAGE = "mf-summary-factor";

    @RequestMapping("/mf-summary-all")
    public String mfSummaryAllReq (
            Model model ) {

        //logger.debug("mfSummaryAllReq {}", "");

        // Get data to summarize
        List<Fund> fundRecs = fundTbl.getAll();

        // Invoke service to format data
        List<SummaryModel> recs = summaryDataService.fundType(fundRecs);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE,
                " ALL FUNDS SUMMARY");

        return SUMMARY_PAGE;
    }


    @RequestMapping("/mf-summary-type")
    public String mfSummaryByTypeReq (
            @RequestParam(value="class", required=true) String assetClass,
            Model model ) {

        String assetClassClean = null;

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
        } catch (UnsupportedEncodingException ex) {};

        //logger.debug("mfSummaryByTypeReq {}", assetClassClean);

        // Get data to summarize
        List<Fund> fundRecs = fundTbl.getByAssetClass(assetClassClean);

        // Invoke service to format data
        List<SummaryModel> recs = summaryDataService.fundType(fundRecs);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE,
                assetClassClean.toUpperCase() + " - SUMMARY");

        return SUMMARY_PAGE;
    }


    @RequestMapping("/mf-summary-subtype")
    public String mfSummaryBySubtypeReq (
            @RequestParam(value="class", required=true) String assetClass,
            Model model ) {

        String assetClassClean = null;

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
        } catch (UnsupportedEncodingException ex) {};

        //logger.debug("mfSummaryBySubtypeReq {}", assetClassClean);

        // Get data to summarize
        List<Fund> fundRecs = fundTbl.getByAssetClass(assetClassClean);

        // Envoke service to format data
        List<SummaryModel> recs = summaryDataService.fundSubType(fundRecs);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE,
                assetClassClean.toUpperCase() + " - SUMMARY");

        return SUMMARY_PAGE;
    }

    @RequestMapping("/mf-summary-factor")
    public String mfSummaryFactorReq (
            @RequestParam(value="class", required=true) String assetClass,
            Model model ) {

        String assetClassClean    = null;

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
        } catch (UnsupportedEncodingException ex) {};

        //logger.debug("mfSummaryFactorReq {}", assetClassClean);

        // Get data to summarize
        List<Fund> fundRecs = fundTbl.getFactorsByAssetClass(assetClassClean);

        // Invoke service to format data
        List<SummaryModel> recs = summaryDataService.fundFactor(fundRecs);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE,
                assetClassClean.toUpperCase() + " - FACTOR SUMMARY");

        return SUMMARY_FACTOR_PAGE;
    }

}

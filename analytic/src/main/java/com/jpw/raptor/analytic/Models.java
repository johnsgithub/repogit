package com.jpw.raptor.analytic;

import com.jpw.raptor.algorithm.TechnicalSummaryFactory;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.TechnicalAnalysisModel;

import java.util.ArrayList;
import java.util.List;

public class Models {


    public List<TechnicalAnalysisModel> buildTechnicalAnalysisModels (List<Quote> refQuotes,  List<Quote> equityQuotes, String symbol, String name ) {

        int maxQuotes = Math.min(refQuotes.size(), equityQuotes.size());

        // lop off the last year
        if ( maxQuotes > 250 )
            maxQuotes -= 250;
        else
            maxQuotes = 0;

        return buildTechnicalAnalysisModels (maxQuotes, refQuotes,  equityQuotes, symbol, name );
    }

    public List<TechnicalAnalysisModel> buildTechnicalAnalysisModels (int maxQuotes, List<Quote> refQuotes,  List<Quote> equityQuotes, String symbol, String name ) {

        // factory to generate records
        var factory = new TechnicalSummaryFactory();

        // allocate space for the technical analysis records
        var techRecs = new ArrayList<TechnicalAnalysisModel>(maxQuotes);

        int sampleSize = 210;
        // generate records
        for ( int i=0; i<maxQuotes; i++) {
            techRecs.add(
              factory.analysis(
                refQuotes.subList(i, i+sampleSize),
                equityQuotes.subList(i, i+sampleSize),
                symbol, name
              )
            );
        }

        return techRecs;
    }
}

package com.jpw.raptor.onetime.prune;

import com.jpw.raptor.algorithm.*;
import com.jpw.raptor.algorithm.signals.*;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.fund.FundDAO;
import com.jpw.raptor.jdbc.hiyieldspread.HiYieldSpreadDAO;
import com.jpw.raptor.jdbc.index.IndexDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.jdbc.treasury.TreasuryDAO;
import com.jpw.raptor.lib.properties.FinanceProperties;

import com.jpw.raptor.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by john on 8/30/18.
 */
@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class Application implements CommandLineRunner {

    @Autowired
    public QuoteDAO quoteTbl;

    @Autowired
    public StockDAO stockTbl;

    @Autowired
    public EtfDAO etfTbl;

    // Main loop
    @Override
    public void run(String... args) throws Exception {
        System.out.println("***************  End Test  *******************");
        System.out.println();

        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
        Date cutoff = sdf.parse("2021-02-01");

        List<Stock> stocks = stockTbl.getAll();
        for ( Stock stock : stocks ) {
            if ( stock.getDate().before(cutoff) ) {
                System.out.println( stock.getDate() + " " + stock.getSymbol() + " " + stock.getName() );
                quoteTbl.deleteAll(stock.getSymbol());
                stockTbl.delete(stock.getSymbol());
            }
        }


        List<Etf> etfs = etfTbl.getAll();
        for ( Etf etf : etfs ) {
            if ( etf.getDate().before(cutoff) ) {
                System.out.println( etf.getDate() + " " + etf.getSymbol() + " " + etf.getName() );
                quoteTbl.deleteAll(etf.getSymbol());
                etfTbl.delete(etf.getSymbol());
            }
        }


        System.out.println("***************  End Test  *******************");
        System.out.println();
    }

    public static void main(String[] args) throws Exception {

        SpringApplication app = new SpringApplication(Application.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit(app.run(args)) );
    }

}


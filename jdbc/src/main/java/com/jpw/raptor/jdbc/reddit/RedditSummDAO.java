package com.jpw.raptor.jdbc.reddit;

import com.jpw.raptor.model.RedditPost;
import com.jpw.raptor.model.RedditSumm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import jakarta.annotation.PostConstruct;
import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class RedditSummDAO implements RedditSummInterface {

    //private static final Logger logger = LoggerFactory.getLogger(RedditSummDAO.class);

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @PostConstruct
    public void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int sqlScript(String script) {
        return jdbcTemplate.update(script);
    }

    @Override
    public void add(RedditSumm rec) {

        // Normalize date
        SimpleDateFormat simpleDateFormat    = new SimpleDateFormat(DATE_FORMAT);
        String           dateString          = simpleDateFormat.format(rec.getDate());
        Date             date                = new Date(-1);
        try {
            date = simpleDateFormat.parse(dateString);
        } catch ( java.text.ParseException ex) {
            System.out.println ("RedditSumm date format exception");
        }

        String sql = "INSERT INTO reddit_summ_tbl (date_tx, stock, num, name) values (?, ?, ?, ?)";
        try {
            jdbcTemplate.update(sql, date, rec.getStock(), rec.getNum(), rec.getName());
        } catch ( org.springframework.dao.DuplicateKeyException ex ) {
            System.out.println ("Summ add failed " );
        }
    }

    @Override
    public void deleteAll() {
        String sql = "DELETE FROM reddit_summ_tbl";
        jdbcTemplate.update(sql);
    }

    @Override
    public List<RedditSumm> getAll() {
        String sql = "SELECT * FROM reddit_summ_tbl ORDER BY num DESC";
        RowMapper<RedditSumm> rowMapper = new RedditSummRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    public List<RedditSumm> getAllAsc() {
        String sql = "SELECT * FROM reddit_summ_tbl ORDER BY num ASC";
        RowMapper<RedditSumm> rowMapper = new RedditSummRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }

}

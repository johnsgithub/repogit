package com.jpw.raptor.analytic;


import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import com.jpw.raptor.algorithm.AvgDirMovementIndicator;
import com.jpw.raptor.algorithm.BollingerBand;
import com.jpw.raptor.algorithm.SimpleMovingAverage;
import com.jpw.raptor.algorithm.StochasticOscillator;
import com.jpw.raptor.algorithm.signals.*;
import com.jpw.raptor.jdbc.etf.EtfDAO;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.lib.properties.FinanceProperties;

import com.jpw.raptor.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class AnalyticApplication implements CommandLineRunner {

    //////////////////////////////////////////////////////////////////////////////////////
    //
    // Must be rewritten for ascending quote order
    //
    /////////////////////////////////////////////////////////////////////////////////////

    @Autowired
    public QuoteDAO quoteTbl;


    public void generateSpreadSheet(String ref, String equity) throws IOException {

        System.out.println();
        System.out.println("***************  Generate Spread sheet  *******************");

        String symbol   = "SPY";

        // Application properties
        FinanceProperties fp   = new FinanceProperties();
        Properties prop = fp.get();

        String dir  = prop.getProperty("analysis_export_dir");
        if ( dir == null ) {
            dir = "/home/finance/analysis";
        }

        // get quotes
        var refList = quoteTbl.getAllDesc(ref.toUpperCase());
        var equityList = quoteTbl.getAllDesc(equity.toUpperCase());

        // generate model list
        var modelFactory = new Models();
        var modelList    = modelFactory.buildTechnicalAnalysisModels(refList, equityList, equity, "");

        // build and write the spreadsheet to a file
        var sheet = new SpreadSheet();
        for ( var model : modelList ) {
            sheet.addRow(model);
        }
        sheet.writeToFile(dir, equity);

    }


    // Main loop
    @Override
    public void run(String... args) throws Exception {

        // define the run time parameters
        AppParameters params = new AppParameters();

        // create parameter parser
        JCommander cmd = new JCommander(params);

        // Parameters
        String ref = null;
        String equity = null;

        try {
            cmd.parse(args);

            ref = params.getRefSymbol();
            if ( ref == null ) {
                ref = "SPY";
            }

            equity = params.getEquitySymbol();
            if ( equity == null ) {
                equity = "SPLV";
            }

        } catch (ParameterException ex) {
            System.out.println(ex.getMessage());
            cmd.usage();
        }

        generateSpreadSheet(ref, equity);

        System.out.println("***************  Good bye  *******************");
        System.out.println();

    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        //SpringApplication app = new SpringApplication(IngestApplication.class);
        //app.setBannerMode(Banner.Mode.OFF);
        //app.run(args);
        System.exit(SpringApplication
                .exit(SpringApplication.run(AnalyticApplication.class, args)));
    }

}

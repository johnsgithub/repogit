package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class JdkRatio {
    protected String    symbol;
    protected Date      date;
    protected double    ratio;
}

package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Rp;
import com.jpw.raptor.model.Macd;
import com.jpw.raptor.model.ZScore;

import java.util.ArrayList;
import java.util.List;

public class ZScoreFactory {
    /*
     * For JDK ratio the data point is equity / reference
     * ratio = 100 + (zscore +1)
     */

    /*
     * The Z score standardizes data points within a data set
     * using the mean and standard deviation
     *
     * data point z score = (data point - data set mean) / data set standard deviation
     *
     * This allows you to compare standardized points from different data sets
     */

    // Data is in descending order by date
    public List<ZScore> quote(List<Quote> data, int offset, int sampleSize) {

        // compute data set mean
        Mean   meanFactory = new Mean();
        double mean        = meanFactory.quote(data, offset, sampleSize);

        // compute data set standard deviation
        StandardDeviation stdFactory = new StandardDeviation();
        double            std        = stdFactory.quote(data, offset, sampleSize);

        // allocate space for zscore object array
        List<ZScore> list = new ArrayList<>(sampleSize);

        // generate z scores
        for ( int i=offset; i<(offset+sampleSize); i++) {
            double s = Math.round( ((data.get(i).getClose() - mean) / std) * 1000.0 ) / 1000.0;
            list.add( new ZScore(data.get(i).getSymbol(), data.get(i).getDate(), s) );
        }

        return list;
    }


    // Data is in descending order by date
    public List<ZScore> rp(List<Rp> data, int offset, int sampleSize) {

        // compute data set mean
        Mean   meanFactory = new Mean();
        double mean        = meanFactory.rp(data, offset, sampleSize);

        // compute data set standard deviation
        StandardDeviation stdFactory = new StandardDeviation();
        double            std        = stdFactory.rp(data, offset, sampleSize);

        // allocate space for zscore object array
        List<ZScore> list = new ArrayList<>(sampleSize);

        // generate z scores
        for ( int i=offset; i<(offset+sampleSize); i++) {
            double s = Math.round( ((data.get(i).getVal() - mean) / std) * 1000.0 ) / 1000.0;
            list.add( new ZScore(data.get(i).getSymbol(), data.get(i).getDate(), s) );
        }

        return list;
    }


    // Data is in descending order by date
    public List<ZScore> macd(List<Macd> data, int offset, int sampleSize) {

        // compute data set mean
        Mean   meanFactory = new Mean();
        double mean        = meanFactory.macd(data, offset, sampleSize);

        // compute data set standard deviation
        StandardDeviation stdFactory = new StandardDeviation();
        double            std        = stdFactory.macd(data, offset, sampleSize);

        // allocate space for zscore object array
        List<ZScore> list = new ArrayList<>(sampleSize);

        // generate z scores
        for ( int i=offset; i<(offset+sampleSize); i++) {
            double s = Math.round( ((data.get(i).getClose() - mean) / std) * 1000.0 ) / 1000.0;
            list.add( new ZScore(data.get(i).getSymbol(), data.get(i).getDate(), s) );
        }

        return list;
    }
}

package com.jpw.raptor.cmdline.scrapeone;

import com.beust.jcommander.Parameter;

/**
 * Created by john on 3/30/18.
 */
public class AppParameters {

    @Parameter(names = "-symbol",
            description = "Symbol",
            required = true)
    private String symbol;

    public String getSymbol() {
        return symbol;
    }


}

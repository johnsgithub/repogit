package com.jpw.raptor.scrape.yahoo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class YahooEtfRiskParse {

    static final String SPAN_START = "<span";
    static final String SPAN_END = "</span>";
    static final String TD_START = "<td ";
    static final String TD_END = "</td>";
    static final String HREF_START = "<a href";
    static final String HREF_END = "</a>";
    static final String GREATER_THAN = ">";
    static final String LESS_THAN = "<";

    public YahooEtfRisk readPage(String symbol) throws IOException, InterruptedException {

        // Yahoo replaces . with -

        String  page;
        YahooEtfRisk r = new YahooEtfRisk();

        String ticker = symbol.toUpperCase().replace('.', '-');
        String baseUrl = "https://finance.yahoo.com/quote/" + ticker + "/risk?p=" + ticker;

        try {
            Document doc = Jsoup.connect(baseUrl).get();
            page = doc.toString();
        } catch ( org.jsoup.HttpStatusException ex ) {
            System.out.println("### " + symbol + " ETF Risk HttpStatusException ****");
            System.out.println(ex.toString());
            r.setReadFailed(true);
            return r;
        }

        // Validate page was found
        if (page.contains("<span>No results for")  || page.contains("<span>Symbols similar to")) {
            System.out.println("### " + symbol + " ETF Risk Symbol not found " + symbol);
            r.setReadFailed(true);
            return r;
        }

        if ( page.contains("NoJs ") ) {
            try {
                parseNoJavaScript(page, r);
            } catch ( Exception ex ) {
                System.out.println("### " + symbol + " **** Etf Risk No Java Script parse failed ***** " );
                System.out.println(ex.toString());
                BufferedWriter writer = new BufferedWriter(new FileWriter("/home/software/page-error-etf-risk-no-js.txt"));
                writer.write(page);
                r.setReadFailed(true);
            }
        } else {
            try {
                parseJavaScript(page, r);
            } catch ( Exception ex ) {
                System.out.println("### " + symbol + " **** Etf Risk Java Script parse failed ***** " );
                System.out.println(ex.toString());
                BufferedWriter writer = new BufferedWriter(new FileWriter("/home/software/page-error-etf-risk-js.txt"));
                writer.write(page);
                r.setReadFailed(true);
            }
        }

        return r;
    }

    public void parseJavaScript(String page, YahooEtfRisk r) {
        ValueObject vo = new ValueObject();

        //  Alpha
        int alphaLabelStart = page.indexOf(">Alpha</td>") + 1;
        int alphaStartData = page.indexOf(GREATER_THAN, alphaLabelStart + 12) + 1;
        int alphaEndData = page.indexOf(LESS_THAN, alphaStartData);
        if ( alphaLabelStart > 0 ) {
            r.setAlpha(vo.getDouble(page.substring(alphaStartData, alphaEndData)));
        }

        //  BETA
        int betaLabelStart = page.indexOf(">BETA</td>", alphaEndData) + 1;
        int betaStartData = page.indexOf(GREATER_THAN, betaLabelStart + 11) + 1;
        int betaEndData = page.indexOf(LESS_THAN, betaStartData);
        if ( betaLabelStart > 0 ) {
            r.setBeta(vo.getDouble(page.substring(betaStartData, betaEndData)));
        }

        //  Mean Annual Return
        int annualReturnLabelStart = page.indexOf(">Mean Annual Return</td>", betaEndData) + 1;
        int annualReturnStartData = page.indexOf(GREATER_THAN, annualReturnLabelStart + 25) + 1;
        int annualReturnEndData = page.indexOf(LESS_THAN, annualReturnStartData);
        if ( annualReturnLabelStart > 0 ) {
            r.setAnnualReturn(vo.getDouble(page.substring(annualReturnStartData, annualReturnEndData)));
        }

        //  R-squared
        int rSquaredLabelStart = page.indexOf(">R-squared</td>", annualReturnEndData) + 1;
        int rSquaredStartData = page.indexOf(GREATER_THAN, rSquaredLabelStart + 16) + 1;
        int rSquaredEndData = page.indexOf(LESS_THAN, rSquaredStartData);
        if ( rSquaredLabelStart > 0 ) {
            r.setRSquared(vo.getDouble(page.substring(rSquaredStartData, rSquaredEndData)));
        }

        //  Standard Deviation
        int standardDeviationLabelStart = page.indexOf(">Standard Deviation</td>", rSquaredEndData) + 1;
        int standardDeviationStartData = page.indexOf(GREATER_THAN, standardDeviationLabelStart + 25) + 1;
        int standardDeviationEndData = page.indexOf(LESS_THAN, standardDeviationStartData);
        if ( standardDeviationLabelStart > 0 ) {
            r.setStandardDeviation(vo.getDouble(page.substring(standardDeviationStartData, standardDeviationEndData)));
        }

        //  Sharpe Ratio
        int sharpeRatioLabelStart = page.indexOf(">Sharpe Ratio</td>", standardDeviationEndData) + 1;
        int sharpeRatioStartData = page.indexOf(GREATER_THAN, sharpeRatioLabelStart + 19) + 1;
        int sharpeRatioEndData = page.indexOf(LESS_THAN, sharpeRatioStartData);
        if ( sharpeRatioLabelStart > 0 ) {
            r.setSharpeRatio(vo.getDouble(page.substring(sharpeRatioStartData, sharpeRatioEndData)));
        }

        //  Treynor Ratio
        int treynorRatioLabelStart = page.indexOf(">Treynor Ratio</td>", sharpeRatioEndData) + 1;
        int treynorRatioStartData = page.indexOf(GREATER_THAN, treynorRatioLabelStart + 20) + 1;
        int treynorRatioEndData = page.indexOf(LESS_THAN, treynorRatioStartData);
        if ( treynorRatioLabelStart > 0 ) {
            r.setTreynorRatio(vo.getDouble(page.substring(treynorRatioStartData, treynorRatioEndData)));
        }
    }


    public void parseNoJavaScript(String page, YahooEtfRisk r) {
        ValueObject vo = new ValueObject();

        //  Alpha
        int alphaLabelStart = page.indexOf("<span>Alpha</span>") + 1;
        if ( alphaLabelStart > 0 ) {
            int alphaDataSpan = page.indexOf(SPAN_START, alphaLabelStart) + 1;
            int alphaStartData = page.indexOf(GREATER_THAN, alphaDataSpan) + 1;
            int alphaEndData = page.indexOf(LESS_THAN, alphaStartData);
            r.setAlpha(vo.getDouble(page.substring(alphaStartData, alphaEndData)));
        }

        //  BETA
        int betaLabelStart = page.indexOf("<span>BETA</span") + 1;
        if ( betaLabelStart > 0 ) {
            int betaDataSpan = page.indexOf(SPAN_START, betaLabelStart) + 1;
            int betaStartData = page.indexOf(GREATER_THAN, betaDataSpan) + 1;
            int betaEndData = page.indexOf(LESS_THAN, betaStartData);
            r.setBeta(vo.getDouble(page.substring(betaStartData, betaEndData)));
        }

        //  Mean Annual Return
        int annualReturnLabelStart = page.indexOf("<span>Mean Annual Return</span>") + 1;
        if ( annualReturnLabelStart > 0 ) {
            int annualReturnDataSpan = page.indexOf(SPAN_START, annualReturnLabelStart) + 1;
            int annualReturnStartData = page.indexOf(GREATER_THAN, annualReturnDataSpan) + 1;
            int annualReturnEndData = page.indexOf(LESS_THAN, annualReturnStartData);
            r.setAnnualReturn(vo.getDouble(page.substring(annualReturnStartData, annualReturnEndData)));
        }

        //  R-squared
        int rSquaredLabelStart = page.indexOf("<span>R-squared</span>") + 1;
        if ( rSquaredLabelStart > 0 ) {
            int rSquaredDataSpan = page.indexOf(SPAN_START, rSquaredLabelStart) + 1;
            int rSquaredStartData = page.indexOf(GREATER_THAN, rSquaredDataSpan) + 1;
            int rSquaredEndData = page.indexOf(LESS_THAN, rSquaredStartData);
            r.setRSquared(vo.getDouble(page.substring(rSquaredStartData, rSquaredEndData)));
        }

        //  Standard Deviation
        int standardDeviationLabelStart = page.indexOf("<span>Standard Deviation</span>") + 1;
        if ( standardDeviationLabelStart > 0 ) {
            int standardDeviationDataSpan = page.indexOf(SPAN_START, standardDeviationLabelStart) + 1;
            int standardDeviationStartData = page.indexOf(GREATER_THAN, standardDeviationDataSpan) + 1;
            int standardDeviationEndData = page.indexOf(LESS_THAN, standardDeviationStartData);
            r.setStandardDeviation(vo.getDouble(page.substring(standardDeviationStartData, standardDeviationEndData)));
        }

        //  Sharpe Ratio
        int sharpeRatioLabelStart = page.indexOf("<span>Sharpe Ratio</span>") + 1;
        if ( sharpeRatioLabelStart > 0 ) {
            int sharpeRatioDataSpan = page.indexOf(SPAN_START, sharpeRatioLabelStart) + 1;
            int sharpeRatioStartData = page.indexOf(GREATER_THAN, sharpeRatioDataSpan) + 1;
            int sharpeRatioEndData = page.indexOf(LESS_THAN, sharpeRatioStartData);
            r.setSharpeRatio(vo.getDouble(page.substring(sharpeRatioStartData, sharpeRatioEndData)));
        }

        //  Treynor Ratio
        int treynorRatioLabelStart = page.indexOf("<span>Treynor Ratio</span>") + 1;
        if ( treynorRatioLabelStart > 0 ) {
            int treynorRatioDataSpan = page.indexOf(SPAN_START, treynorRatioLabelStart) + 1;
            int treynorRatioStartData = page.indexOf(GREATER_THAN, treynorRatioDataSpan) + 1;
            int treynorRatioEndData = page.indexOf(LESS_THAN, treynorRatioStartData);
            r.setTreynorRatio(vo.getDouble(page.substring(treynorRatioStartData, treynorRatioEndData)));
        }
    }

}

package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@Getter
@Setter
public class YahooEtfHoldings {

    private double  stocks;
    private double  bonds;
    private double  basicMaterials;
    private double  consumerCyclical;
    private double  financialServices;
    private double  realEstate;
    private double  consumerDefensive;
    private double  healthcare;
    private double  utilities;
    private double  industrials;
    private double  communicationServices;
    private double  energy;
    private double  technology;
    private double  priceEarnings;
    private double  priceBook;
    private double  priceSales;
    private double  priceCashFlow;
    private double  marketCap;
    private double  bondUs;
    private double  bondAaa;
    private double  bondAa;
    private double  bondA;
    private double  bondBbb;
    private double  bondBb;
    private double  bondB;
    private double  bondBelowB;
    private double  bondOthers;
    private String  holdings;

    private boolean readFailed;

    public YahooEtfHoldings() {
        init();
    }

    public void init() {
        stocks = 0.0;
        bonds = 0.0;
        basicMaterials = 0.0;
        consumerCyclical = 0.0;
        financialServices = 0.0;
        realEstate = 0.0;
        consumerDefensive = 0.0;
        healthcare = 0.0;
        utilities = 0.0;
        industrials = 0.0;
        communicationServices = 0.0;
        energy = 0.0;
        technology = 0.0;
        priceEarnings = 0.0;
        priceBook = 0.0;
        priceSales = 0.0;
        priceCashFlow = 0.0;
        marketCap = 0.0;
        bondUs = 0.0;
        bondAaa = 0.0;
        bondAa = 0.0;
        bondA = 0.0;
        bondBbb = 0.0;
        bondBb = 0.0;
        bondB = 0.0;
        bondBelowB = 0.0;
        bondOthers = 0.0;
        holdings  = "[]";
        readFailed = false;
    }

    public void print() {
        System.out.println("stocks                  " + stocks);
        System.out.println("bonds                   " + bonds);

        System.out.println("basicMaterials          " + basicMaterials);
        System.out.println("consumerCyclical        " + consumerCyclical);
        System.out.println("financialServices       " + financialServices);
        System.out.println("realEstate              " + realEstate);
        System.out.println("consumerDefensive       " + consumerDefensive);
        System.out.println("healthcare              " + healthcare);
        System.out.println("utilities               " + utilities);
        System.out.println("communicationServices   " + communicationServices);
        System.out.println("energy                  " + energy);
        System.out.println("industrials             " + industrials);
        System.out.println("technology              " + technology);

        System.out.println("priceEarnings           " + priceEarnings);
        System.out.println("priceBook               " + priceBook);
        System.out.println("priceSales              " + priceSales);
        System.out.println("priceCashFlow           " + priceCashFlow);
        System.out.println("marketCap               " + marketCap);

        System.out.println("bondUs                  " + bondUs);
        System.out.println("bondAaa                 " + bondAaa);
        System.out.println("bondAa                  " + bondAa);
        System.out.println("bondA                   " + bondA);
        System.out.println("bondBbb                 " + bondBbb);
        System.out.println("bondBb                  " + bondBb);
        System.out.println("bondB                   " + bondB);
        System.out.println("bondBelowB              " + bondBelowB);
        System.out.println("bondOthers              " + bondOthers);

        System.out.println("read failed             " + bondOthers);
        System.out.println(holdings);

    }

}

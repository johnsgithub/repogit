package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Quote;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

public class Median {

    // Quotes are in descending order by date
    public double medianQuote(List<Quote> data, int offset, int count) {

        int     end         = offset + count;
        int     arraySize   = data.size();
        double  result      = 0.0;

        // count must be at least 1
        if ( count < 1 ) {
            return result;
        }

        // validate there is enough data to compute the moving average
        if ( end > arraySize ) {
            return result;
        }

        // Create an array of close values
        double[] array = new double[count];
        // loop through array and sum the closes
        int index = 0;
        for ( int i=offset; i<end; i++ ) {
            array[index++] = data.get(i).getClose();
        }

        // sort the array
        Arrays.sort(array);

        // Compute the median
        if ( (count % 2) != 0 ) {
            // odd number of entries
            result = array[count / 2];
        } else {
            // even number of entries
            result = ( array[(count-1) / 2] + array[count / 2] ) / 2.0;
        }

        // Round to 2 decimal places
        return Math.round(result * 100.0) / 100.0;
    }
}

package com.jpw.raptor.onetime.addstocks;

import com.jpw.raptor.algorithm.EquityPerformance;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.model.Performance;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Stock;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;




public class MissingStocks {

    private StockDAO stockTbl;
    private QuoteDAO quoteTbl;

    public MissingStocks( StockDAO stock, QuoteDAO quote ) {
        stockTbl = stock;
        quoteTbl = quote;
    }

    public void processFile(String fileName) throws IOException {
        List<Entry> stocks = getStocks(fileName);

        for ( Entry stock : stocks ) {
            addStock(stock);
        }
    }

    public List<Entry> getStocks(String fileName) throws IOException {

        ArrayList<Entry> list = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader(fileName));

        String line = null;

        // Read each record in the file
        while ( (line = br.readLine()) != null ) {
           String[] str = line.split(",");
           if ( str.length >=3 ) {
               // Valid number of columns
               Entry entry = new Entry();
               entry.setSymbol(str[0]);
               entry.setName(str[1]);
               entry.setSector(str[2]);
               list.add(entry);
            }
        }

        br.close();
        return list;
    }


    public void addStock(Entry entry) throws IOException {

        // Check to see if stock is already in the table
        Stock stock = stockTbl.get(entry.getSymbol());
        if ( stock != null )
            return;

        // make sure there are enough quotes
        QuotesFromFiles qff = new QuotesFromFiles();
        List<Quote> quotes = qff.getQuotes(entry.getSymbol());
        if ( quotes.size() < 150 )
            return;

        System.out.println(entry.getSymbol());

        // Write the stock to the stock table
        stockTbl.addForIndex(entry.getSymbol(), entry.getName(), entry.getSector());

        // write the corresponding quotes to the quote table
        for ( Quote quote : quotes ) {
            quoteTbl.add(quote);
        }

        // Worker to compute performance
        EquityPerformance ep = new EquityPerformance();

        // get equity record to update
        Stock equity = stockTbl.get(entry.getSymbol().toUpperCase());

        // Get quotes for computing performance
        quotes = quoteTbl.getYearsWorthDesc(entry.getSymbol().toUpperCase());

        // Compute performance
        Performance rec = ep.computePerformance(entry.getSymbol().toUpperCase(),  quotes);

        // Update equity
        equity.updatePerformance(rec);
        stockTbl.update(equity);
    }

}

package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Atr extends Quote {

    private double 	        trueRange;
    private double 	        averageTrueRange;
    private double 	        averageTruePercentage;

    protected void init() {
        // Provide default values
        trueRange             = 0.0;
        averageTrueRange      = 0.0;
        averageTruePercentage = 0.0;
    }

    // Constructor
    public Atr () {
        super();
        init();
    }

    public Atr(Quote v) {
        super();
        init();
        setSymbol(v.getSymbol());
        setDate(v.getDate());
        setOpen(v.getOpen());
        setHigh(v.getHigh());
        setLow(v.getLow());
        setClose(v.getClose());
        setVolume(v.getVolume());
    }

}

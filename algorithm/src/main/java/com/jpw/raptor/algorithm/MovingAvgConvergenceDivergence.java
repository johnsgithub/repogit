package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * Moving Average Convergence Divergence (MACD) is a trend-following momentum indicator
 * that shows the relationship between two moving averages of a security’s price.
 *
 * The MACD is calculated by subtracting the 26-period Exponential Moving Average (EMA)
 * from the 12-period EMA. The result of that calculation is the MACD line.
 *
 * A nine-day EMA of the MACD, called the "signal line," is then plotted on top of the MACD line,
 * which can function as a trigger for buy and sell signals.
 *
 * Investors use the MACD by watching for a move above or below the "0" line.
 * If the MACD is above this line, the short-term moving average is above the long-term moving average,
 * signaling upward momentum. The opposite is true if the MACD is below the "0" line.
 *
 * If the MACD is above the signal line the values are positive and the rate of change
 * for the faster moving average (12-day) is higher than the rate of change for the slower
 * moving average (26-day). This would indicate increasing positive momentum and
 * price acceleration- definitely a bullish development.
 *
 */

public class MovingAvgConvergenceDivergence {


    // Quotes are in descending order by date
    public List<Macd> quote(List<Quote> data, int fastPeriods, int slowPeriods) {

        // validate enough data is provided
        if (data.size() <= slowPeriods)
            return Collections.<Macd>emptyList();

        // allocate space for stochastic object list
        List<Macd> list = new ArrayList<>(data.size());

        // populate the list with default data
        for ( Quote q : data ) {
            Macd m = new Macd(q);
            m.setFastEma(q.getClose());
            m.setSlowEma(q.getClose());
            list.add(m);
        }

        // helper objects
        double                   fastEma;
        double                   slowEma;
        double                   macd;
        double                   signal;
        double                   histogram;
        ExponentialMovingAverage ema        = new ExponentialMovingAverage();

        // Since dates are descending start at the end of the array
        // and process to the front
        int start = data.size() - 1;

        // Note the first entry has already been set

        // create averages for each quote
        for ( int i=start-1; i>=0; i-- ) {

            Macd obj = list.get(i);

            fastEma     = ema.getExpAvg(data.get(i).getClose(), list.get(i+1).getFastEma(), fastPeriods);
            slowEma     = ema.getExpAvg(data.get(i).getClose(), list.get(i+1).getSlowEma(), slowPeriods);
            macd        = fastEma - slowEma;
            signal      = ema.getExpAvg(macd, list.get(i+1).getSignal(), 9);
            histogram   = macd - signal;

            obj.setFastEma( Math.round(fastEma * 100.0) / 100.0);
            obj.setSlowEma( Math.round(slowEma * 100.0) / 100.0);
            obj.setMacd( Math.round(macd * 100.0) / 100.0);
            obj.setSignal( Math.round(signal * 100.0) / 100.0);
            obj.setHistogram( Math.round(histogram * 100.0) / 100.0);
        }

        return list;
    }

    // Quotes are in descending order by date
    public List<Macd> jdk(List<Jdk> data, int fastPeriods, int slowPeriods) {

        // validate enough data is provided
        if (data.size() <= slowPeriods)
            return Collections.<Macd>emptyList();

        // allocate space for stochastic object list
        List<Macd> list = new ArrayList<>(data.size());

        // populate the list with default data
        for ( Jdk j : data ) {
            Macd m = new Macd();
            m.setDate(j.getDate());
            m.setClose(j.getRsRatio());
            m.setFastEma(j.getRsRatio());
            m.setSlowEma(j.getRsRatio());
            list.add(m);
        }

        // helper objects
        double                   fastEma;
        double                   slowEma;
        double                   macd;
        double                   signal;
        double                   histogram;
        ExponentialMovingAverage ema        = new ExponentialMovingAverage();

        // Since dates are descending start at the end of the array
        // and process to the front
        int start = data.size() - 1;

        // Note the first entry has already been set

        // create averages for each quote
        for ( int i=start-1; i>=0; i-- ) {

            Macd obj = list.get(i);

            fastEma     = ema.getExpAvg(data.get(i).getRsRatio(), list.get(i+1).getFastEma(), fastPeriods);
            slowEma     = ema.getExpAvg(data.get(i).getRsRatio(), list.get(i+1).getSlowEma(), slowPeriods);
            macd        = fastEma - slowEma;
            signal      = ema.getExpAvg(macd, list.get(i+1).getSignal(), 9);
            histogram   = macd - signal;

            obj.setFastEma( Math.round(fastEma * 100.0) / 100.0);
            obj.setSlowEma( Math.round(slowEma * 100.0) / 100.0);
            obj.setMacd( Math.round(macd * 100.0) / 100.0);
            obj.setSignal( Math.round(signal * 100.0) / 100.0);
            obj.setHistogram( Math.round(histogram * 100.0) / 100.0);
        }

        return list;
    }
}

package com.jpw.raptor.cmdline.add;

import com.jpw.raptor.model.Quote;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class QuotesFromFilesTest {

    @Test
    public void test01() {

        QuotesFromFiles    worker  = new QuotesFromFiles();
        String dir = "/home/finance/runtime/test";

        // Generate list values
        List<String> list = worker.getFiles(dir);

        for (String s : list) {
            //System.out.println(s);
        }

        System.out.println("Size " + list.size());
    }


    @Test
    public void test02() throws IOException {

        QuotesFromFiles    worker  = new QuotesFromFiles();
        String   dir = "/home/finance/runtime/test";

        // Generate list values
        List<String> files = worker.getFiles(dir);

        List<Quote> quotes = worker.getRecords(files, "ab");
        for (Quote q : quotes) {
            //System.out.println(q.getSymbol() + " " + q.getDate());
        }

    }


    @Test
    public void test03() throws IOException {

        List<Quote> quotes;
        QuotesFromFiles    worker  = new QuotesFromFiles();

        // no quotes
        //quotes = worker.getQuotes("nasdaq", "ab");
        //for (Quote q : quotes) {
        //    System.out.println(q.getSymbol() + " " + q.getDate());
        //}

        // nasdaq quotes
        //quotes = worker.getQuotes("nasdaq", "aame");
        //for (Quote q : quotes) {
        //    System.out.println(q.getSymbol() + " " + q.getDate());
        //}

        // amex quotes
        //quotes = worker.getQuotes("amex", "aamc");
        //for (Quote q : quotes) {
        //    System.out.println(q.getSymbol() + " " + q.getDate());
        //}

        // amex quotes
        //quotes = worker.getQuotes("nyse", "aa");
        //for (Quote q : quotes) {
        //    System.out.println(q.getSymbol() + " " + q.getDate());
        //}

        // index quotes
        //quotes = worker.getQuotes("index", "addv");
        //for (Quote q : quotes) {
        //    System.out.println(q.getSymbol() + " " + q.getDate());
        //}

        //System.out.println("quotes found " + quotes.size());
    }
}

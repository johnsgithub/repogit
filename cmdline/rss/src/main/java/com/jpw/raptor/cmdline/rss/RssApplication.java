package com.jpw.raptor.cmdline.rss;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.jpw.raptor.lib.properties.FinanceProperties;

import java.net.URL;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import static org.apache.poi.hslf.record.RecordTypes.List;


@SpringBootApplication(scanBasePackages = "com.jpw.raptor")
public class RssApplication implements CommandLineRunner {

  
    // Main loop
    @Override
    public void run(String... args) throws Exception {

        FeedDescription[] feeds = {
                new FeedDescription("", "https://www.cnbc.com/id/10001147/device/rss/rss.html"),
                new FeedDescription("", "https://fortune.com/feed"),
                new FeedDescription("", "https://www.ft.com/?format=rss"),
                new FeedDescription("", "https://www.investing.com/rss/news.rss"),
                new FeedDescription("", "https://seekingalpha.com/market_currents.xml"),
                new FeedDescription("", "https://finance.yahoo.com/news/rssindex")
        };

        Date truncatedDate = DateUtils.truncate(new Date(), Calendar.DATE);

        try {

            URL feedUrl = new URL(feeds[0].getAddress());


            SyndFeedInput input = new SyndFeedInput();
            SyndFeed feed = input.build(new XmlReader(feedUrl));

            java.util.List<SyndEntry> entries = feed.getEntries();
            for ( SyndEntry entry : entries ) {
                System.out.println("*****************************************");
                System.out.println("Title   :" + entry.getTitle());
                System.out.println("Link    :" + entry.getLink());
                System.out.println("Pub dte :" + entry.getPublishedDate());
                System.out.println("Uri     :" + entry.getUri());
                System.out.println("MD5     :" + DigestUtils.md5Hex(entry.getUri()).toUpperCase());

                System.out.println("");
            }

            System.out.println("Current date " + truncatedDate);

        } catch (Exception ex) {
            ex.printStackTrace();
                System.out.println("ERROR: "+ex.getMessage());
        }
        
        System.out.println("***************  Good bye  *******************");
        System.out.println();

    }

    public static void main(String[] args) throws Exception {

        //disabled banner, don't want to see the spring logo
        SpringApplication app = new SpringApplication(RssApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        System.exit( SpringApplication.exit( app.run(args) ) );
    }

}

package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

@Getter
@Setter
public class YahooStockProfile {

    private String  page;
    private String  sector;
    private String  industry;
    private String  description;

    private boolean readFailed;

    public YahooStockProfile() {
        init();
    }

    public void init() {
        page = "";
        sector = "";
        industry = "";
        description = "";
        readFailed = false;
    }

    public void print() {
        System.out.println("sector      " + sector);
        System.out.println("industry    " + industry);
        System.out.println("description " + description);
    }
}

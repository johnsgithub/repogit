package com.jpw.raptor.scrape.yahoo;

public class ValueObject {

    public double getDouble(String buf) {
        if ( buf.contains("N/A") )
            return 0.0;

        double tempVal;

        if ( buf.contains("T") ) {
            // number is in trillions
            String stripped = buf.replace(",","").replace("T","").trim();
            tempVal = Double.parseDouble( stripped ) * 1000000000000.0;
        } else if ( buf.contains("B") ) {
            // number is in billions
            String stripped = buf.replace(",","").replace("B","").trim();
            tempVal = Double.parseDouble( stripped ) * 1000000000.0;
        } else if ( buf.contains("M") ) {
            // number is in millions
            String stripped = buf.replace(",","").replace("M","").trim();
            tempVal = Double.parseDouble( stripped ) * 1000000.0;
        } else if ( buf.contains("k") ) {
            // number is in thousands
            String stripped = buf.replace(",","").replace("k","").trim();
            tempVal = Double.parseDouble( stripped ) * 1000.0;
        }else {
            String stripped = buf.replace(",","").trim();
            tempVal = Double.parseDouble( stripped );
        }

        // Round to 2 decimal places
        return Math.round(tempVal * 100.0) / 100.0;
    }

    public double getPercent(String buf) {
        if ( buf.contains("N/A") )
            return 0.0;

        String stripped = buf.replace(",","").replace('%',' ').trim();
        double tempVal =  (Double.parseDouble( stripped ) / 100.0);

        // Round to 4 decimal places
        return Math.round(tempVal * 10000.0) / 10000.0;

    }

    public long getLong(String buffer) {
        if ( buffer.contains("N/A") )
            return 0;

        String buf = buffer.replace(",","");

        if ( buf.contains("T") ) {
            // number is in trillions
            String stripped = buf.replace(",","").replace("T","").trim();
            return (long) (Double.parseDouble( stripped ) * 1000000000000.0);
        } else if ( buf.contains("B") ) {
            // number is in billions
            String stripped = buf.replace(",","").replace("B","").trim();
            return (long) (Double.parseDouble( stripped ) * 1000000000.0);
        } else if ( buf.contains("M") ) {
            // number is in millions
            String stripped = buf.replace(",","").replace("M","").trim();
            return (long) (Double.parseDouble( stripped ) * 1000000.0);
        } else if ( buf.contains("k") ) {
            // number is in thousands
            String stripped = buf.replace(",","").replace("k","").trim();
            return (long) (Double.parseDouble( stripped ) * 1000.0);
        }else {
            String stripped = buf.replace(",","").trim();
            return (long) Double.parseDouble( stripped );
        }

    }

}

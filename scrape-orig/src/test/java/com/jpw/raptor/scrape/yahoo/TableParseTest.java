package com.jpw.raptor.scrape.yahoo;

import org.junit.Test;

import java.io.IOException;

public class TableParseTest {

    static final String TBODY_START = "<tbody>";
    static final String TBODY_END = "</tbody>";

    static final String TABLE =
            "<table class=)>" +                         // 0
            "<tbody>" +                                 // 15
            "<tr class=>" +                             // 22
            "<td class=>BlackRock</td>"+               // 33
            "<td class=><a href=>BISXX</a></td>" +      // 59
            "<td class=>8.21%</td>" +                   // 93
            "</tr>" +                                   // 114
            "<tr class=>" +                             // 119
            "<td class=>Federal 2%</td>" +              // 130
            "<td class><span>N/A</span></td>" +         // 156
            "<td class=>1.33%</td>" +                   // 187
            "</tr>" +                                   // 208
            "</tbody>" +                                // 213
            "</table>";                                 // 221

    @Test
    //
    // Risk Stats
    public void testit() throws IOException {

        // bracket table body
        int bodyStart = TABLE.indexOf(TBODY_START);
        int bodyEnd   = TABLE.indexOf(TBODY_END, bodyStart);
        System.out.println("Body start " + bodyStart + " body end " + bodyEnd );

        YahooEtfTableRow tr    = new YahooEtfTableRow();

        // row 1
        tr.parseRow(TABLE,0,221);
        if (tr.isFound() ) {
            System.out.println("Row " + tr.getRowText() );

            //column 1
            System.out.println("column 1 " + tr.getColumn1() );

            // column 2
            System.out.println("column 2 " + tr.getColumn2() );

            // column 3
            System.out.println("column 3 " + tr.getColumn3() );
        }

        System.out.println(" " );

        // row 2
        tr.parseRow(TABLE,tr.getRowEnd(),221);
        if (tr.isFound() ) {
            System.out.println("Row " + tr.getRowText() );

            //column 1
            System.out.println("column 1 " + tr.getColumn1() );

            // column 2
            System.out.println("column 2 " + tr.getColumn2() );

            // column 3
            System.out.println("column 3 " + tr.getColumn3() );
        }
        System.out.println(" " );

        // row 3
        tr.parseRow(TABLE,tr.getRowEnd(),221);
        if (tr.isFound() ) {
            System.out.println("Row " + tr.getRowEnd() );
        } else {
            System.out.println("Row not found "  );
        }
    }
}

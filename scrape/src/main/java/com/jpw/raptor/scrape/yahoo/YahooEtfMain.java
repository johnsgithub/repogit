package com.jpw.raptor.scrape.yahoo;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@Getter
@Setter
public class YahooEtfMain {


    private long    averageVolume;
    private double  yield;
    private double  expenseRatio;

    private boolean readFailed;

    public YahooEtfMain() {
        init();
    }

    public void init() {
        averageVolume   = 0;
        yield           = 0.0;
        expenseRatio    = 0.0;
        readFailed      = false;
    }

    public void print() {
        System.out.println("averageVolume      " + averageVolume);
        System.out.println("yield              " + yield);
        System.out.println("expenseRatio       " + expenseRatio);
    }

}

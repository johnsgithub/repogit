package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HoldingPercent {
    private  double raw;
    private  String fmt;
}

package com.jpw.raptor.jdbc.track;

import com.jpw.raptor.model.TrackRec;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TrackRowMapper implements RowMapper<TrackRec> {

    //private static final Logger logger = LoggerFactory.getLogger(TrackRowMapper.class);

    @Override
    public TrackRec mapRow(ResultSet rs, int rowNum) throws SQLException {
        TrackRec rec = new TrackRec();
        rec.setSymbol(rs.getString("symbol"));
        rec.setAnalyst(rs.getString("analyst"));
        rec.setName(rs.getString("name"));
        rec.setSector(rs.getString("sector"));
        rec.setDate(rs.getDate("date_tx"));
        rec.setDelta(rs.getDouble("delta"));
        return rec;
    }
}
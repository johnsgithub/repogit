package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@IdClass(TrackId.class)
@Table(name = "track_tbl")
public class TrackRec {

    @Id
    @Column(name = "symbol", columnDefinition="")
    protected String symbol;

    @Id
    @Column(name = "analyst", columnDefinition="")
    protected String analyst;

    @Column(name = "name", columnDefinition="")
    protected String name;

    @Column(name = "sector", columnDefinition="")
    protected String sector;

    @Column(name = "date_tx", columnDefinition="")
    @Temporal(TemporalType.DATE)
    protected Date date;

    @Column(name = "delta", columnDefinition="")
    protected double delta;

    protected int count;

    public TrackRec() { this.count = 0;}

    public TrackRec (String symbol, String analyst) {
        this.symbol  = symbol;
        this.analyst = analyst;
        this.count   = 0;
    }

    public TrackRec (String symbol, String analyst, String name, String sector, Date date, double delta) {
        this.symbol  = symbol;
        this.analyst = analyst;
        this.name    = name;
        this.sector  = sector;
        this.date    = date;
        this.delta   = delta;
        this.count   = 0;
    }
}

package com.jpw.raptor.cmdline.web.controller;


import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;


/**
 * Created by john on 8/13/18.
 */
@Controller
public class SearchEquityController {

    protected static final  String ENCODING = "UTF-8";

    @RequestMapping("/search-equity")
    public String eventReq(
            @RequestParam(value="type", required=true) String equityType,
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="fundtype", required=true) String fundType,
            @RequestParam(value="symbol", required=false) String symbol,
            Model model) {

        String equityTypeClean  = "";
        String assetClassClean  = "";
        String fundTypeClean    = "";


        try {
            equityTypeClean  = URLDecoder.decode(equityType, ENCODING);
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);

        } catch (UnsupportedEncodingException ex) {};


        model.addAttribute("event_model", null);
        model.addAttribute("pagetitle", "Events from " );

        return "event-list";
    }

}

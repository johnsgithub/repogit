package com.jpw.raptor.cmdline.reports;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by john on 5/3/18.
 */
public class AppParameters {


    @Parameter(names = "-report",
            description = "Report to run",
            required = true)
    private String report;

    public String getReport() {
        return report;
    }

    @Parameter(names = "-analyst",
            description = "Analyst",
            required = false)
    private String analyst;

    public String getAnalyst() {
        return analyst;
    }

    @Parameter(names = "-date",
            description = "Date for report start",
            validateWith = DateValidator.class,
            required = false)
    private String date;

    public Date getDate() throws java.text.ParseException{
        SimpleDateFormat format  = new SimpleDateFormat("yyyy-MM-dd");
        return format.parse(date);
    }

}

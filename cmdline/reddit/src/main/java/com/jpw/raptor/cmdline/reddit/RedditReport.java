package com.jpw.raptor.cmdline.reddit;

import com.jpw.raptor.model.RedditPost;
import com.jpw.raptor.model.RedditSumm;

import java.util.*;

public class RedditReport {


    public List<String> generateReport(List<RedditSumm> list) throws java.io.IOException {

        // Create list for csv file
        ArrayList<String> results = new ArrayList<>(list.size());
        Iterator i = list.iterator();
        for (RedditSumm rec : list) {
            results.add(rec.getNum() + "," + rec.getStock() + "," + rec.getName());
        }

        return results;
    }


}

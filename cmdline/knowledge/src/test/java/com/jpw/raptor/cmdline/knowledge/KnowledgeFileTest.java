package com.jpw.raptor.cmdline.knowledge;

import java.io.File;

import com.jpw.raptor.model.FinanceKnowledge;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by john on 5/3/18.
 */
public class KnowledgeFileTest {

    @Test
    public void test01() {

        String line1 = "";
        String line2 = "***title some title";

        KnowledgeFile kf = new KnowledgeFile();
        System.out.println(kf.generateTitleFromLine(line2));


    }


    @Test
    public void test02() {

        String line1 = "";
        String line2 = "***tag  some tag ";
        String line3 = "***tag  tag one ; tag two;tag three ";

        KnowledgeFile kf = new KnowledgeFile();
        String result;

        result = kf.generateTagFromLine(line1);

        result = kf.generateTagFromLine(line2);
        assertTrue(result.equalsIgnoreCase("some tag"));

        result = kf.generateTagFromLine(line3);
        assertTrue(result.equalsIgnoreCase("tag one   tag two tag three"));


    }


    @Test
    public void test03() {

        String hash = "35454B055CC325EA1AF2126E27707052";
        String password = "ILoveJava";

        KnowledgeFile kf = new KnowledgeFile();

        String md5Hex = kf.generateMd5(password);

        assertTrue(md5Hex.equalsIgnoreCase(hash));
    }


    @Test
    public void test04() throws IOException, SAXException, TikaException {

        //Get file from resources folder
        ClassLoader         classLoader = getClass().getClassLoader();
        File                file        = new File(classLoader.getResource("smart_beta.txt").getFile());
        KnowledgeFile       kf          = new KnowledgeFile();
        FinanceKnowledge    k           = kf.generateFinancialKnowledge(file);

        System.out.println("Id    " + k.getId());
        System.out.println("Loc   " + k.getLoc());
        System.out.println("Url   " + k.getUrl());
        System.out.println("Title " + k.getTitle());
        System.out.println("Tag   " + k.getTag());
        System.out.println("Score " + k.getScore());
        System.out.println("Body  " + k.getBody());

    }
}


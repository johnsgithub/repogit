package com.jpw.raptor.scrape.yahoo;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class YahooStockProfileTest {

    @Test
    //
    // Risk Stats
    public void testit() throws IOException {

        System.out.println("test it started");

        YahooStockProfileParse parser = new YahooStockProfileParse();

        System.out.println("  parse java script ");
        InputStream         jsis   = getClass().getClassLoader().getResourceAsStream("stock-profile-js.txt");
        String              jsPage = new String(jsis.readAllBytes(), StandardCharsets.UTF_8);
        YahooStockProfile   jsObj  = new YahooStockProfile();
        parser.parseJavaScript(jsPage, jsObj);
        jsObj.print();

        System.out.println(" ");

        System.out.println("  parse no java script ");
        InputStream nojsis   = getClass().getClassLoader().getResourceAsStream("stock-profile-nojs.txt");
        String      noJsPage = new String(nojsis.readAllBytes(), StandardCharsets.UTF_8);
        YahooStockProfile   noJsObj  = new YahooStockProfile();
        parser.parseNoJavaScript(noJsPage, noJsObj);
        noJsObj.print();

        System.out.println("test it finished");
    }

}

package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.*;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Ported to new design 5/16/2019
 *
 * Compute the performance of a equity from a start time which is the last entry in the list
 *
 * Compute the relative  performance of two equities from a start time which is the last entry in the list
 * The resultant list will only have entries the given date has a value for both equities
 *
 */
public class EquityPerformance {

    public static final int     ONE_DAY         = 1;
    public static final int     ONE_WEEK        = 5;
    public static final int     TWO_WEEKS       = 10;
    public static final int     THREE_WEEKS     = 15;
    public static final int     FOUR_WEEKS      = 21;
    public static final int     TWO_MONTHS      = 42;
    public static final int     THREE_MONTHS    = 63;
    public static final int     FOUR_MONTHS     = 84;
    public static final int     FIVE_MONTHS     = 105;
    public static final int     SIX_MONTHS      = 126;
    public static final int     ONE_YEAR        = 253;
    public static final int     THREE_YEARS     = 759;


    // Quotes are in descending order by dates
    public Performance computePerformance(String symbol, List<Quote> quotes) {

        // results
        Performance rec = new Performance();

        // Working variables to compute performance
        double              r;

        // get most recent quote
        Quote               today       = quotes.get(0);

        // compute the prior year
        SimpleDateFormat    formatter    = new SimpleDateFormat("yyyy");
        String              formatedDate = formatter.format(today.getDate());
        int                 priorYearInt = Integer.parseInt(formatedDate) - 1;

        // number of quotes
        int quoteCount = quotes.size();

        // Used to hold last quote from prior year
        Quote priorYearQuote = null;

        // note the first close is today so start processing at 1
        for ( int i=1; i<quoteCount; i++ ) {

            // we are processing the dates in descending order
            // so the first quote in the prior year is the
            // last quote of that year
            if ( priorYearQuote == null ) {
                // get the year from the quote
                int parsedYearInt = Integer.parseInt(formatter.format(quotes.get(i).getDate()));
                if ( parsedYearInt == priorYearInt ) {
                    // last quote of prior year
                    priorYearQuote = quotes.get(i);
                }
            }

            // Process 1 day return
            if ( i == ONE_DAY ) {
                r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
                rec.setOneDay(Math.round(r * 100.0) / 100.0);
                rec.setDate(today.getDate());
            }

            weeks(rec, quotes, today, i);
            months(rec, quotes, today, i);
            years(rec, quotes, today, i);

        }

        // compute ytd return
        if (priorYearQuote != null) {
            r = ( (today.getClose() - priorYearQuote.getClose()) / priorYearQuote.getClose() ) * 100.0;
            rec.setYtd(Math.round(r * 100.0) / 100.0);
        } else {
            rec.setYtd(9999.0);
        }

        // Reset three year return
        rec.setThreeYears(9999.0);

        // Update date_tx
        rec.setDate(today.getDate());

        return rec;
    }



    private void weeks(Performance rec, List<Quote> quotes, Quote today, int i) {

        // Working variables to compute performance
        double              r;

        // Process One week return
        if ( i == ONE_WEEK ) {
            r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
            rec.setOneWeek(Math.round(r * 100.0) / 100.0);
        }

        // Process two week return
        if ( i == TWO_WEEKS ) {
            r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
            rec.setTwoWeeks(Math.round(r * 100.0) / 100.0);
        }

        // Process three week return
        if ( i == THREE_WEEKS ) {
            r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
            rec.setThreeWeeks(Math.round(r * 100.0) / 100.0);
        }

        // Process four week return
        if ( i == FOUR_WEEKS ) {
            r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
            rec.setFourWeeks(Math.round(r * 100.0) / 100.0);
        }
    }


    private void months(Performance rec, List<Quote> quotes, Quote today, int i) {

        // Working variables to compute performance
        double              r;

        // Process two month return
        if ( i == TWO_MONTHS ) {
            r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
            rec.setTwoMonths(Math.round(r * 100.0) / 100.0);
        }

        // Process three month return
        if ( i == THREE_MONTHS ) {
            r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
            rec.setThreeMonths(Math.round(r * 100.0) / 100.0);
        }

        // Process four month return
        if ( i == FOUR_MONTHS ) {
            r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
            rec.setFourMonths(Math.round(r * 100.0) / 100.0);
        }

        // Process five month return
        if ( i == FIVE_MONTHS ) {
            r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
            rec.setFiveMonths(Math.round(r * 100.0) / 100.0);
        }

        // Process six month return
        if ( i == SIX_MONTHS ) {
            r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
            rec.setSixMonths(Math.round(r * 100.0) / 100.0);
        }
    }


    private void years(Performance rec, List<Quote> quotes, Quote today, int i) {

        // Working variables to compute performance
        double              r;

        // Process One year return
        if ( i == ONE_YEAR ) {
            r = ((today.getClose() - quotes.get(i).getClose()) / quotes.get(i).getClose()) * 100.0;
            rec.setOneYear(Math.round(r * 100.0) / 100.0);
        }
    }

    public Averages computeAverages(List<Quote> quotes) {

        // results
        Averages rec = new Averages();

        // number of quotes
        int quoteCount = quotes.size();

        // record current value
        rec.setLastClose( quotes.get(0).getClose());

        for (int i = 0; i<quoteCount; i++) {
            if ( i < 20 ) {
                rec.setSimple20( rec.getSimple20() + quotes.get(i).getClose() );
            }

            if ( i < 50 ) {
                rec.setSimple50( rec.getSimple50() + quotes.get(i).getClose() );
            }

            if ( i < 200 ) {
                rec.setSimple200( rec.getSimple200() + quotes.get(i).getClose() );
            }
        }

        rec.setSimple20(Math.round( (rec.getSimple20() / 20.0) * 100.0) / 100.0);
        rec.setSimple50(Math.round( (rec.getSimple50() / 50.0) * 100.0) / 100.0);
        rec.setSimple200(Math.round( (rec.getSimple200() / 200.0) * 100.0) / 100.0);

        return rec;
    }

}

package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Macd;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Rp;

import java.text.DecimalFormat;
import java.util.List;

public class Mean {

    //
    // Compute number of elements to use for the mean
    // If enough data is provided then return sampleSize
    // If not enough data is provide then return arraySize minus offset
    //
    public int elementCount(int offset, int sampleSize, int arraySize) {
        int number = offset + sampleSize;
        if ( number > arraySize )
            return arraySize - offset;
        else
            return sampleSize;

    }

    // Data is in descending order by date
    public double quote(List<Quote> data, int offset, int sampleSize) {

        int    count  = elementCount(offset, sampleSize, data.size());
        int    end    = offset + count;
        double sum    = 0.0;

        // loop through array and sum the data
        for ( int i=offset; i<end; i++ ) {
            sum = sum + data.get(i).getClose();
        }

        // Compute the mean
        double result = sum / (double) count;

        // Round to 2 decimal places
        return Math.round(result * 100.0) / 100.0;
    }


    // Data is in descending order by date
    public double rp(List<Rp> data, int offset, int sampleSize) {

        int    count  = elementCount(offset, sampleSize, data.size());
        int    end    = offset + count;
        double sum    = 0.0;

        // loop through array and sum the data
        for ( int i=offset; i<end; i++ ) {
            sum = sum + data.get(i).getVal();
        }

        // Compute the mean
        double result = sum / (double) count;

        // Round to 2 decimal places
        result = Math.round(result * 1000.0) / 1000.0;

        return result;
    }


    // Data is in descending order by date
    public double macd(List<Macd> data, int offset, int sampleSize) {

        int    count  = elementCount(offset, sampleSize, data.size());
        int    end    = offset + count;
        double sum    = 0.0;

        // loop through array and sum the data
        for ( int i=offset; i<end; i++ ) {
            sum = sum + data.get(i).getMacd();
        }

        // Compute the mean
        double result = sum / (double) count;

        // Round to 2 decimal places
        return Math.round(result * 100.0) / 100.0;
    }

}

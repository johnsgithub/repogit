package com.jpw.raptor.cmdline.web.controller;


import com.jpw.raptor.cmdline.web.service.ReturnModelService;
import com.jpw.raptor.jdbc.fund.FundDAO;
import com.jpw.raptor.model.Fund;
import com.jpw.raptor.model.ReturnModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * Created by john on 4/8/18.
 */
@Controller
public class FundReturnController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    FundDAO fundTbl;

    @Autowired
    ReturnModelService returnModelService;

    protected final static String RECORDS = "retrn";
    protected final static String PAGE_TITLE = "pagetitle";
    protected final static String ENCODING = "UTF-8";
    protected final static String RETURN_PAGE = "mf-returns";

    @RequestMapping("/mf-return-all")
    public String mfReturnAllReq(Model model)
    {
        //logger.debug("mfReturnAllReq {}", "");

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getAll();

        // get the returns
        List<ReturnModel> recs = returnModelService.getFundReturns(equities);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, "ALL FUND Returns");

        return RETURN_PAGE;
    }

    @RequestMapping("/mf-return-relevant")
    public String mfReturnRelevantReq(Model model)
    {
        //logger.debug("mfReturnRelevantReq {}", "");

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getRelevant();

        // get the returns
        List<ReturnModel> recs = returnModelService.getFundReturns(equities);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, "RELEVANT FUND Returns");

        return RETURN_PAGE;
    }

    @RequestMapping("/mf-return-tracked")
    public String mfReturnTrackedReq(Model model)
    {
        //logger.debug("mfReturnTrackedReq {}", "");

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getTracked();

        // get the returns
        List<ReturnModel> recs = returnModelService.getFundReturns(equities);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, "TRACKED FUND Returns");

        return RETURN_PAGE;
    }


    @RequestMapping("/mf-return-owned")
    public String mfReturnOwnedReq(Model model)
    {
        //logger.debug("mfReturnOwnedReq {}", "");

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getOwned();

        // get the returns
        List<ReturnModel> recs = returnModelService.getFundReturns(equities);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, "OWNED FUND Returns");

        return RETURN_PAGE;
    }

    @RequestMapping("/mf-return-class")
    public String mfReturnClassReq(
            @RequestParam(value="class", required=true) String assetClass,
            Model model)
    {
        String assetClassClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfReturnClassReq  asset class {} ", assetClassClean);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getByAssetClass(assetClassClean);

        // get the returns
        List<ReturnModel> recs = returnModelService.getFundReturns(equities);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE, assetClassClean.toUpperCase() + " FUND Returns");

        return RETURN_PAGE;
    }

    @RequestMapping("/mf-return-class-type")
    public String mfReturnClassTypeReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            Model model)
    {
        String assetClassClean = "";
        String fundTypeClean   = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfReturnClassTypeReq  asset class {} fund type {}", assetClassClean, fundTypeClean);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getByAssetClassFundType(assetClassClean, fundTypeClean);

        // get the returns
        List<ReturnModel> recs = returnModelService.getFundReturns(equities);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE,
                assetClassClean.toUpperCase() + " - " + fundTypeClean.toUpperCase() + " FUND Returns");

        return RETURN_PAGE;
    }

    @RequestMapping("/mf-return-class-type-sub")
    public String mfReturnClassTypeSubTypeReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            @RequestParam(value="sub", required=true)  String fundSubType,
            Model model)
    {
        String assetClassClean  = "";
        String fundTypeClean    = "";
        String fundSubTypeClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
            fundSubTypeClean = URLDecoder.decode(fundSubType, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfReturnClassTypeSubTypeReq  asset class {} fund type {} sub type {}",
        //        assetClassClean, fundTypeClean, fundSubTypeClean);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getByAssetClassFundTypeSubType(
                assetClassClean, fundTypeClean, fundSubTypeClean);

        // get the returns
        List<ReturnModel> recs = returnModelService.getFundReturns(equities);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE,
                assetClassClean.toUpperCase() + " - " + fundTypeClean.toUpperCase()
                        + " - " + fundSubTypeClean.toUpperCase() + " FUND Returns");

        return RETURN_PAGE;
    }

    @RequestMapping("/mf-return-class-type-sub-factor")
    public String mfReturnClassTypeSubTypeFactorReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            @RequestParam(value="sub", required=true)  String fundSubType,
            @RequestParam(value="factor", required=true)  String factor,
            Model model)
    {
        String assetClassClean  = "";
        String fundTypeClean    = "";
        String fundSubTypeClean = "";
        String factorClean      = "" ;

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
            fundSubTypeClean = URLDecoder.decode(fundSubType, ENCODING);
            factorClean      = URLDecoder.decode(factor, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfReturnClassTypeSubTypeFactorReq  asset class {} fund type {} sub type {} factor {} ",
        //        assetClassClean, fundTypeClean, fundSubTypeClean, factorClean);

        // Get the equities we are interested in
        List<Fund> equities = fundTbl.getByAssetClassFundTypeSubTypeFactor(
                assetClassClean, fundTypeClean, fundSubTypeClean, factorClean);

        // get the returns
        List<ReturnModel> recs = returnModelService.getFundReturns(equities);

        model.addAttribute(RECORDS, recs);
        model.addAttribute(PAGE_TITLE,
                assetClassClean.toUpperCase() + " - " + fundTypeClean.toUpperCase()
                        + " - " + fundSubTypeClean.toUpperCase()
                        + " - " + factorClean.toUpperCase() + " FUND Returns");

        return RETURN_PAGE;
    }

}

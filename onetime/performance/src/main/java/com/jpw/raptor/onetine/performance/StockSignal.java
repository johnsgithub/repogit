package com.jpw.raptor.onetine.performance;

import com.jpw.raptor.algorithm.TechnicalSummaryFactory;
import com.jpw.raptor.jdbc.quote.QuoteDAO;

import com.jpw.raptor.model.Etf;
import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.Stock;
import com.jpw.raptor.model.TechnicalSummaryModel;


import java.util.*;

public class StockSignal {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    protected static final  String REF_SYMBOL = "SPY";

    protected static final  double PRICE = 10.00;
    protected static final  long VOLUME = 200000L;
    protected static final  long CAP = 2000000000L;

    private List<EquitySignal> signals;
    private SectorSummary signalSummary;
    private SectorSummary stockSummary;

    public List<EquitySignal> getSignals() {
        return signals;
    }

    public SectorSummary getSignalSectors() {
        return signalSummary;
    }

    public SectorSummary getStockSectors() {
        return stockSummary;
    }

    private boolean meetsTechnicalSummary( TechnicalSummaryModel s ) {
        /*** Technical summary model
         double jdkSignal greater than 100;
         String macdSignal begins with 4 -
         String mAvgSignal begins with 90:
         ***/
        if (s.getJdkSignal() > 100.0 && s.getJdkSignal() < 110.0 &&
                s.getMAvgSignal().contains("90") &&
                s.getMacdSignal().contains("4 -"))
            return true;
        else
            return false;
    }

    private boolean meetsPrice(List<Quote> equityQuotes ) {
        if ( equityQuotes.get(0).getClose() > 10.00 )
            return true;
        else
            return false;
    }

    private boolean meetsCapAndVol( Stock equity ) {
        if ( equity.getAvgDailyVol() > 200000 && equity.getMarketCap() > 2000000000L )
            return true;
        else
            return false;
    }

    private boolean meetsReturn( Etf ref, Stock equity ) {
        if ( equity.getTwoWeeks() >= ref.getTwoWeeks() && equity.getFourWeeks() >= ref.getFourWeeks() )
            return true;
        else
            return false;
    }

    private boolean generatesSignal(TechnicalSummaryModel s, List<Quote> equityQuotes, Stock equity, Etf ref ) {
        if ( meetsTechnicalSummary(s) && meetsPrice(equityQuotes) &&  meetsCapAndVol(equity)  && meetsReturn(ref,equity) )
        //if ( meetsTechnicalSummary(s) && meetsPrice(equityQuotes) &&  meetsCapAndVol(equity)  && meetsReturn(ref,equity) )
            return true;
        else
            return false;
    }

    public StockSignal (Etf etf, List<Stock> equities, QuoteDAO quoteTbl) {

        signals = new ArrayList<>();
        stockSummary = new SectorSummary();
        signalSummary = new SectorSummary();

        int days = 60;

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        for ( Stock equity : equities ) {
            // Summarize all sectors
            stockSummary.addSector(equity.getSector());

            // Generate technical summary
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            TechnicalSummaryModel s = summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName());

            // Check for signal generation
            if ( generatesSignal(s,equityQuotes,equity,etf) ) {
                // Generate signal record
                EquitySignal sortedSignal = new EquitySignal(s);
                // Store signal record in list
                signals.add(sortedSignal);
                // Summarize signal sector
                signalSummary.addSector(equity.getSector());
            }

        }

        // Sort signal list by jdk value
        Collections.sort(signals, new Comparator<EquitySignal>() {
            @Override
            public int compare(EquitySignal sortedSignal, EquitySignal t1) {
                if      ( sortedSignal.getJdk() < t1.getJdk() )
                    return 1;
                else if ( sortedSignal.getJdk() > t1.getJdk() )
                    return -1;
                else
                    return 0;
            }
        });

    }
}

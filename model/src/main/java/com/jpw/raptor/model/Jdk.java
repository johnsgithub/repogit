package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
public class Jdk {

    String symbol;
    Date   date;
    String datestr;
    double rsRatio;
    double rsMomentum;
    double rsRatioMa;
    double macd;
    double signal;
    double histogram;

    public Jdk() {
        symbol      = null;
        date        = null;
        datestr     = null;
        rsRatio     = 0.0;
        rsMomentum  = 0.0;
        rsRatioMa   = 0.0;
        macd        = 0.0;
        signal      = 0.0;
        histogram   = 0.0;
    }

    public Jdk(String symbol, Date date, double ratio, double momentum) {
        this.symbol          = symbol;
        this.date            = date;
        this.rsRatio         = ratio;
        this.rsMomentum      = momentum;
        this.rsRatioMa       = ratio;
        this.macd            = 0.0;
        this.signal          = 0.0;
        this.histogram       = 0.0;

        SimpleDateFormat sdf = new SimpleDateFormat();
        this.datestr         = sdf.format(date);
    }
}

package com.jpw.raptor.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Rp {

    private String  symbol;
    private String  refSymbol;
    private String  equitySymbol;
    private Date    date;
    private String  datestr;
    private double  ref;
    private double  equity;
    private double  val;
    private double  sma;

    protected void init() {
        // Provide default values
        symbol       = null;
        refSymbol    = null;
        equitySymbol = null;
        date         = null;
        datestr      = null;
        ref          = 0.0;
        equity       = 0.0;
        val          = 0.0;
        sma          = 0.0;
    }

    // Constructor
    public Rp () {
        init();
    }
}

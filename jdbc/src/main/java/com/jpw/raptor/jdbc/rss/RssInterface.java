package com.jpw.raptor.jdbc.rss;

import com.jpw.raptor.model.Rss;
import com.jpw.raptor.model.RssEntry;

import java.util.Date;
import java.util.List;

/**
 * Created by john on 5/13/18.
 */
public interface RssInterface {

    public int sqlScript(String script);

    public void add(RssEntry rec);

    public void delete(String hash);

    public void deleteAll();

    public void deletePrior(Date date);

    public RssEntry get(String hash);

    public List<RssEntry> getAll();

    public List<RssEntry> getPrior(Date date);
}

package com.jpw.raptor.algorithm;

import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.RelativePerformanceModel;
import com.jpw.raptor.model.Rp;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * Relative performance is a ratio which increases when the equity is out performing the reference
 * and decreases when the reference is out performing the equity
 *
 */
public class RelativePerformance {

    // Quotes are in descending order by date
    public List<Rp> computeRelativePerformance(
            List<Quote> refQuotes, List<Quote> equityQuotes) {

        // Allocate space for the relative performance list
        int       max    = Math.min(refQuotes.size(), equityQuotes.size());
        List<Rp>  result = new ArrayList<>(max);

        // Used to generate date key
        SimpleDateFormat sdf    = new SimpleDateFormat("yyyy-MM-dd");

        // Create a sorted map to hold the model data
        // data will be stored in ascending order by date string
        TreeMap<String,Rp> map = new TreeMap<>();

        //
        // Populate the map with reference data defaulting the equity value to -9999
        for ( Quote refVal : refQuotes ) {
            Rp rp = new Rp();
            rp.setDate(refVal.getDate());
            rp.setDatestr(sdf.format(refVal.getDate()));
            rp.setRefSymbol(refVal.getSymbol());
            rp.setRef(refVal.getClose());
            rp.setEquity(-9999.0);
            map.put(rp.getDatestr(), rp);
        }

        //
        // update the map with equity data
        for ( Quote equityVal : equityQuotes ) {
            Rp rp = map.get(sdf.format(equityVal.getDate()));

            // if an entry exist then update it
            if (rp != null) {
                rp.setEquitySymbol(equityVal.getSymbol());
                rp.setEquity(equityVal.getClose());
                map.put(rp.getDatestr(), rp);
            }
        }

        //
        // Populate a list with entries that have both ref and equity values
        // traverse the map in sorted order which is ascending order
        for (Map.Entry<String, Rp> entrySetVal : map.entrySet()) {
            Rp entry = entrySetVal.getValue();
            if ( entry.getEquity() > -9999.0 ) {
                entry.setVal( Math.round( (entry.getEquity() / entry.getRef()) * 1000.0 ) / 1000.0 );
                result.add(entry);
            }
        }

        // Reverse the list from ascending to descending order
        Collections.reverse(result);

        // populate list with moving averages
        int days = 21;
        SimpleMovingAverage sma = new SimpleMovingAverage();
        for ( int i=0; i<result.size(); i++) {
            Rp toUpdate = result.get(i);
            toUpdate.setSma( sma.rp(result,i,days));
        }

        return result;
    }

}

package com.jpw.raptor.scrape.yahoo.fields;

import java.util.List;

/**
 * Created by John on 10/13/2017.
 */
public class AssetProfile {

    private String longBusinessSummary;

    public String getLongBusinessSummary() {return longBusinessSummary;}

    public void setLongBusinessSummary(String longBusinessSummary) {
        this.longBusinessSummary = longBusinessSummary;
    }

}

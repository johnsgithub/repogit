package com.jpw.raptor.cmdline.track;

import com.jpw.raptor.model.Quote;
import com.jpw.raptor.model.TrackRec;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by john on 3/29/18.
 */
public class TrackParser {

    public TrackParser() {}

    public List<TrackRec> linesToRecords(String analyst, Date date, List<String> lines ) {
        ArrayList<TrackRec> recs = new ArrayList<>(lines.size());

        for ( String line : lines ) {
            TrackRec rec = parseCsvLine (analyst, date, line);
            if ( rec != null )
                recs.add(rec);
        }

        return recs;
    }

    public TrackRec parseCsvLine (String analyst, Date date, String line) {

        // Parse the string into an array of fields
        // Validate number of fields parsed
        String[] str = line.split(",");
        if ( str.length >= 3 ) {
            // Valid number of columns
        } else {
            // invalid number of fields
            return null;
        }

        // Create the quote
        TrackRec rec = new TrackRec();

        // Symbol
        rec.setSymbol(str[0].toUpperCase());

        // analyst
        rec.setAnalyst(analyst.toUpperCase());

        // name
        rec.setName(str[1]);

        // sector
        rec.setSector(str[2]);

        // date
        rec.setDate(date);

        // delta
        rec.setDelta(0.0);

        return rec;
    }
}

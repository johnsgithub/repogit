package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.algorithm.TechnicalSummaryFactory;
import com.jpw.raptor.cmdline.web.service.ListModelService;
import com.jpw.raptor.jdbc.quote.QuoteDAO;
import com.jpw.raptor.jdbc.stock.StockDAO;
import com.jpw.raptor.model.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Controller
public class StockSignalController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    public QuoteDAO quoteTbl;

    @Autowired
    StockDAO stockTbl;

    protected static final  String REF_SYMBOL = "SPY";
    protected static final  String RECORDS = "tech_model";
    protected static final  String PAGE_TITLE = "pagetitle";
    protected static final  String ENCODING = "UTF-8";
    protected static final  String SIGNAL_PAGE = "signals";
    protected static final String DEFAULT_DAYS = "220";

    @RequestMapping("/stock-signal-all")
    public String stockSignalAllReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("stockSignalAllReq {}", "");

        // Get the Stocks to display
        List<Stock> equities = stockTbl.getAll();

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Stock equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "ALL STOCK Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/stock-signal-relevant")
    public String stockSignalRelevantReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("stockSignalRelevantReq {}", "");

        // Get the Stocks to display
        List<Stock> equities = stockTbl.getRelevant();

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Stock equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "RELEVANT STOCK Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/stock-signal-tracked")
    public String stockSignalTrackedReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("stockSignalTrackedReq {}", "");

        // Get the Stocks to display
        List<Stock> equities = stockTbl.getTracked();

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Stock equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "TRACKED STOCK Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/stock-signal-owned")
    public String stockSignalOwnedReq(
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        //logger.debug("stockSignalOwnedReq {}", "");

        // Get the Stocks to display
        List<Stock>          equities  = stockTbl.getOwned();

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Stock equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute(PAGE_TITLE, "OWNED STOCK Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/stock-signal-sp")
    public String stockSignalSpReq(
            @RequestParam(value="index", required=true) String index,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String indexClean = index.replace('+', ' ');

        //logger.debug("stockSignalSpReq index {} ", indexClean);

        // Get the Stocks to display
        List<Stock> equities = stockTbl.getBySpIndex(indexClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Stock equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute("pagetitle", "S&P " + indexClean.toUpperCase() + " STOCK Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/stock-signal-dow")
    public String stockSignalDowReq(
            @RequestParam(value="index", required=true) String index,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String indexClean = index.replace('+', ' ');

        //logger.debug("stockSignalDowReq index {} ", indexClean);

        // Get the Stocks to display
        List<Stock> equities = stockTbl.getByDowIndex(indexClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Stock equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute("pagetitle", "DOW " + indexClean.toUpperCase() + " STOCK Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/stock-signal-russell")
    public String stockSignalRussellReq(
            @RequestParam(value="index", required=true) String index,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model)
    {
        String indexClean = index.replace('+', ' ');

        //logger.debug("stockSignalRussellReq index {} ", indexClean);

        // Get the Stocks to display
        List<Stock> equities = stockTbl.getByRussellIndex(indexClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Stock equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute("pagetitle", "RUSSELL " + indexClean.toUpperCase() + " STOCK Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/stock-signal-sector")
    public String stockSignalSectorReq(
            @RequestParam(value="sector", required=true) String sector,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model) {

        String sectorClean = sector.replace('+', ' ');

        try {
            sectorClean  = URLDecoder.decode(sector, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("stockSignalSectorReq sector {} ", sectorClean);

        // Get the Stocks to display
        List<Stock> equities = stockTbl.getBySector(sectorClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Stock equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute("pagetitle", "Sector " + sectorClean.toUpperCase() + " STOCK Signals");

        return SIGNAL_PAGE;
    }


    @RequestMapping("/stock-signal-industry")
    public String stockSignalIndustryReq(
            @RequestParam(value="industry", required=true) String industry,
            @RequestParam(value="days", defaultValue=DEFAULT_DAYS) int days,
            Model model) {

        String industryClean = industry.replace('+', ' ');

        try {
            industryClean  = URLDecoder.decode(industry, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("stockSignalSectorReq industry{} ", industryClean);

        // Get the Stocks to display
        List<Stock> equities = stockTbl.getByIndustry(industryClean);

        TechnicalSummaryFactory summaryService = new TechnicalSummaryFactory();

        // Get Reference quotes
        List<Quote> refQuotes = quoteTbl.getAllDesc(REF_SYMBOL, days);

        // Get signals
        List<TechnicalSummaryModel> signals = new ArrayList<>(equities.size());
        for ( Stock equity : equities ) {
            List<Quote> equityQuotes = quoteTbl.getAllDesc(equity.getSymbol(), days);
            signals.add(summaryService.generate(refQuotes, equityQuotes, equity.getSymbol(), equity.getName()));
        }

        model.addAttribute(RECORDS, signals);
        model.addAttribute("pagetitle", "Industry " + industryClean.toUpperCase() + " STOCK Signals");

        return SIGNAL_PAGE;
    }

}

package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.cmdline.web.service.ListModelService;

import com.jpw.raptor.jdbc.stock.StockDAO;

import com.jpw.raptor.model.Stock;
import com.jpw.raptor.model.StockListModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by john on 11/13/18.
 */
@Controller
public class StockListController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    StockDAO stockTbl;

    @Autowired
    ListModelService listService;

    protected final static String DATE_FORMAT = "MM-dd-yyyy";
    protected final static String LIST_MODEL = "list_model";
    protected final static String PAGE_TITLE = "pagetitle";
    protected final static String NO_DATE = "No Date Found";
    protected final static String ENCODING = "UTF-8";
    protected final static String LIST_PAGE = "stock-list";

    @RequestMapping("/stock-list-all")
    public String stockListAllReq(Model model)
    {
        //logger.debug("stockListAllReq {}", "");

        // Get the Stocks to display
        List<Stock> toConvert = stockTbl.getAll();

        // Convert to list format
        List<StockListModel> recs = listService.stockListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 ) {
            if ( recs.get(0).getDate() != null )
                formattedDate = formatter.format(recs.get(0).getDate());
        }

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "ALL Stocks as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/stock-list-relevant")
    public String stockListRelevantReq(Model model)
    {
        //logger.debug("stockListRelevantReq {}", "");

        // Get the Stocks to display
        List<Stock> toConvert = stockTbl.getRelevant();

        // Convert to list format
        List<StockListModel> recs = listService.stockListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 ) {
            if ( recs.get(0).getDate() != null )
                formattedDate = formatter.format(recs.get(0).getDate());
        }

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "RELEVANT Stocks as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/stock-list-tracked")
    public String stockListTrackedReq(Model model)
    {
        //logger.debug("stockListTrackedReq {}", "");

        // Get the Stocks to display
        List<Stock> toConvert = stockTbl.getTracked();

        // Convert to list format
        List<StockListModel> recs = listService.stockListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 ) {
            if ( recs.get(0).getDate() != null )
                formattedDate = formatter.format(recs.get(0).getDate());
        }

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "TRACKED Stocks as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/stock-list-owned")
    public String stockListOwnedReq(Model model)
    {
        //logger.debug("stockListOwnedReq {}", "");

        // Get the Stocks to display
        List<Stock>          toConvert  = stockTbl.getOwned();

        // Convert to list format
        List<StockListModel> recs = listService.stockListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 ) {
            if ( recs.get(0).getDate() != null )
                formattedDate = formatter.format(recs.get(0).getDate());
        }

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "OWNED Stocks as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/stock-list-sp")
    public String stockListSpReq(
            @RequestParam(value="index", required=true) String index,
            Model model)
    {
        String indexClean = index.replace('+', ' ');

        //logger.debug("stockListSpReq  asset class {} ", indexClean);

        // Get the Stocks to display
        List<Stock> toConvert = stockTbl.getBySpIndex(indexClean);

        // Convert to list format
        List<StockListModel> recs = listService.stockListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 ) {
            if ( recs.get(0).getDate() != null )
                formattedDate = formatter.format(recs.get(0).getDate());
        }

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "S&P " + indexClean.toUpperCase() + " Stocks as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/stock-list-dow")
    public String stockListDowReq(
            @RequestParam(value="index", required=true) String index,
            Model model)
    {
        String indexClean = index.replace('+', ' ');

        //logger.debug("stockListDowReq  asset class {} ", indexClean);

        // Get the Stocks to display
        List<Stock> toConvert = stockTbl.getByDowIndex(indexClean);

        // Convert to list format
        List<StockListModel> recs = listService.stockListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 ) {
            if ( recs.get(0).getDate() != null )
                formattedDate = formatter.format(recs.get(0).getDate());
        }

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "DOW " + indexClean.toUpperCase() + " Stocks as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/stock-list-russell")
    public String stockListRussellReq(
            @RequestParam(value="index", required=true) String index,
            Model model)
    {
        String indexClean = index.replace('+', ' ');

        //logger.debug("stockListRussellReq  asset class {} ", indexClean);

        // Get the Stocks to display
        List<Stock> toConvert = stockTbl.getByRussellIndex(indexClean);

        // Convert to list format
        List<StockListModel> recs = listService.stockListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 ) {
            if ( recs.get(0).getDate() != null )
                formattedDate = formatter.format(recs.get(0).getDate());
        }

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "RUSSELL " + indexClean.toUpperCase() + " Stocks as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/stock-list-sector")
    public String stockListSectorReq(
            @RequestParam(value="sector", required=true) String sector,
            Model model) {

        String sectorClean = sector.replace('+', ' ');

        try {
            sectorClean  = URLDecoder.decode(sector, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("stockListSectorReq  asset class {} ", sectorClean);

        // Get the Stocks to display
        List<Stock> toConvert = stockTbl.getBySector(sectorClean);

        // Convert to list format
        List<StockListModel> recs = listService.stockListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 ) {
            if ( recs.get(0).getDate() != null )
                formattedDate = formatter.format(recs.get(0).getDate());
        }

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "Sector " + sectorClean.toUpperCase() + " Stocks as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/stock-list-industry")
    public String stockListIndustryReq(
            @RequestParam(value="industry", required=true) String industry,
            Model model) {

        String industryClean = industry.replace('+', ' ');

        try {
            industryClean  = URLDecoder.decode(industry, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("stockListSectorReq  asset class {} ", industryClean);

        // Get the Stocks to display
        List<Stock> toConvert = stockTbl.getByIndustry(industryClean);

        // Convert to list format
        List<StockListModel> recs = listService.stockListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 ) {
            if ( recs.get(0).getDate() != null )
                formattedDate = formatter.format(recs.get(0).getDate());
        }

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "Industry " + industryClean.toUpperCase() + " Stocks as of " + formattedDate);

        return LIST_PAGE;
    }

}

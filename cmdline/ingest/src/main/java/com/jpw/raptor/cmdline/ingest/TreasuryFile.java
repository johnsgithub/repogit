package com.jpw.raptor.cmdline.ingest;

import com.jpw.raptor.jdbc.treasury.TreasuryDAO;
import com.jpw.raptor.model.FileQualified;
import com.jpw.raptor.model.Treasury;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class TreasuryFile {

    @Autowired
    public TreasuryDAO tbl;

    Date formatDate (String raw ) {
        if ( StringUtils.isEmpty(raw) ) {
            System.out.println(" No date provided ");
            return new Date(-1);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        try {
            return sdf.parse(raw.replace('/','-'));
        } catch (ParseException ex) {
            System.out.println(" Treasury DATE format exception ");
            return new Date(-1);
        }
    }

    double formatNumber ( String raw ) {
        if ( !StringUtils.isEmpty(raw) ) {
            try {
                return Double.parseDouble(raw.replace("N/A","0.0"));
            } catch (NumberFormatException ex) {
                System.out.println(" Treasury number format exception ");
                return 0.0;
            }
        } else {
            return 0.0;
        }
    }

    public Deque<Treasury> readFile (File file) {

        ArrayDeque<Treasury> queue = new ArrayDeque<>();

        // Used to read Eod data file
        BufferedReader br = null;
        String         line;

        try {
            br = new BufferedReader(new FileReader(file));

            // Ignore the first line
            line = br.readLine();

            // Read each record in the file
            while ((line = br.readLine()) != null) {
                // Parse the string into an array of fields
                String[] str = line.split(",");

                if ( str.length == 14 ) {
                    Treasury rec = new Treasury();
                    rec.setDate(formatDate(str[0]));
                    rec.setOneMonth(formatNumber(str[1]));
                    rec.setTwoMonths(formatNumber(str[2]));
                    rec.setThreeMonths(formatNumber(str[3]));
                    rec.setSixMonths(formatNumber(str[5]));
                    rec.setOneYear(formatNumber(str[6]));
                    rec.setTwoYears(formatNumber(str[7]));
                    rec.setThreeYears(formatNumber(str[8]));
                    rec.setFiveYears(formatNumber(str[9]));
                    rec.setSevenYears(formatNumber(str[10]));
                    rec.setTenYears(formatNumber(str[11]));
                    rec.setTwentyYears(formatNumber(str[12]));
                    rec.setThirtyYears(formatNumber(str[13]));
                    queue.push(rec);
                } else if ( str.length == 13 ) {
                    Treasury rec = new Treasury();
                    rec.setDate(formatDate(str[0]));
                    rec.setOneMonth(formatNumber(str[1]));
                    rec.setTwoMonths(formatNumber(str[2]));
                    rec.setThreeMonths(formatNumber(str[3]));
                    rec.setSixMonths(formatNumber(str[4]));
                    rec.setOneYear(formatNumber(str[5]));
                    rec.setTwoYears(formatNumber(str[6]));
                    rec.setThreeYears(formatNumber(str[7]));
                    rec.setFiveYears(formatNumber(str[8]));
                    rec.setSevenYears(formatNumber(str[9]));
                    rec.setTenYears(formatNumber(str[10]));
                    rec.setTwentyYears(formatNumber(str[11]));
                    rec.setThirtyYears(formatNumber(str[12]));
                    queue.push(rec);
                } else {
                    System.out.println(" Treasury not enough fields ");
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if ( br != null ) {
                    br.close();
                }
            }
            catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
        return queue;
    }

    public void processFile(FileQualified fileQualified) {

        // get records from file
        Deque<Treasury> recs = readFile (new File(fileQualified.getName()));

        // Get last date processed
        Treasury lastProcessed = tbl.getLast();
        Date lastDate;
        if ( lastProcessed == null )
            lastDate = new Date(-1);
        else
            lastDate = lastProcessed.getDate();

        for (Treasury t : recs) {
            if ( t.getDate().after(lastDate)) {
                tbl.upsert(t);
            } 
        }
    }

}

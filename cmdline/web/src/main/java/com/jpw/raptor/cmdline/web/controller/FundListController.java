package com.jpw.raptor.cmdline.web.controller;

import com.jpw.raptor.cmdline.web.service.ListModelService;
import com.jpw.raptor.jdbc.fund.FundDAO;
import com.jpw.raptor.lib.properties.FinanceProperties;
import com.jpw.raptor.model.Fund;
import com.jpw.raptor.model.ListModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;


/**
 * Created by john on 4/5/18.
 */
@Controller
public class FundListController {

    //Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    FundDAO fundTbl;

    @Autowired
    ListModelService listService;

    protected final static String DATE_FORMAT = "MM-dd-yyyy";
    protected final static String LIST_MODEL = "list_model";
    protected final static String PAGE_TITLE = "pagetitle";
    protected final static String NO_DATE = "No Date Found";
    protected final static String ENCODING = "UTF-8";
    protected final static String LIST_PAGE = "mf-list";

    @RequestMapping("/mf-list-all")
    public String mfListAllReq(Model model)
    {
        //logger.debug("mfListAllReq {}", "");

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getAll();

        // Convert to list format
        List<ListModel> recs = listService.fundListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "ALL FUNDS as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/mf-list-relevant")
    public String mfListRelevantReq(Model model)
    {
        //logger.debug("mfListRelevantReq {}", "");

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getRelevant();

        // Convert to list format
        List<ListModel> recs = listService.fundListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "RELEVANT FUNDS as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/mf-list-tracked")
    public String mfListTrackedReq(Model model)
    {
        //logger.debug("mfListTrackedReq {}", "");

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getTracked();

        // Convert to list format
        List<ListModel> recs = listService.fundListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;
        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "TRACKED FUNDS as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/mf-list-owned")
    public String mfListOwnedReq(Model model)
    {
        //logger.debug("mfListOwnedReq {}", "");

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getOwned();

        // Convert to list format
        List<ListModel> recs = listService.fundListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;

        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "OWNED FUNDS as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/mf-list-class")
    public String mfListClassReq(
            @RequestParam(value="class", required=true) String assetClass,
            Model model)
    {
        String assetClassClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfListClassReq asset class {} ", assetClassClean);

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getByAssetClass(assetClassClean);

        // Convert to list format
        List<ListModel> recs = listService.fundListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;

        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, assetClassClean.toUpperCase() + " FUNDS as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/mf-list-class-type")
    public String mfListClassTypeReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            Model model)
    {
        String assetClassClean = "";
        String fundTypeClean   = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfListClassTypeReq  asset class {} fund type {}", assetClassClean, fundTypeClean);

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getByAssetClassFundType(assetClassClean, fundTypeClean);

        // Convert to list format
        List<ListModel> recs = listService.fundListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;

        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE,
                fundTypeClean.toUpperCase() + " - " +
                        assetClassClean.toUpperCase() +
                        " FUNDS as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/mf-list-class-type-sub")
    public String mfListClassTypeSubTypeReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            @RequestParam(value="sub", required=true)  String fundSubType,
            Model model)
    {
        String assetClassClean  = "";
        String fundTypeClean    = "";
        String fundSubTypeClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
            fundSubTypeClean = URLDecoder.decode(fundSubType, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfListClassTypeSubTypeReq asset class {} fund type {} sub type {}",
        //        assetClassClean, fundTypeClean, fundSubTypeClean);

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getByAssetClassFundTypeSubType(assetClassClean, fundTypeClean, fundSubTypeClean);

        // Convert to list format
        List<ListModel> recs = listService.fundListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;

        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE,
                fundSubTypeClean.toUpperCase() + " - " +
                        fundTypeClean.toUpperCase() + " - " +
                        assetClassClean.toUpperCase() +
                        " FUNDS as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/mf-list-class-type-sub-factor")
    public String mfListClassTypeSubTypeFactorReq(
            @RequestParam(value="class", required=true) String assetClass,
            @RequestParam(value="type", required=true)  String fundType,
            @RequestParam(value="sub", required=true)  String fundSubType,
            @RequestParam(value="factor", required=true)  String factor,
            Model model)
    {
        String assetClassClean  = "";
        String fundTypeClean    = "";
        String fundSubTypeClean = "";
        String factorClean      = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
            fundTypeClean    = URLDecoder.decode(fundType, ENCODING);
            fundSubTypeClean = URLDecoder.decode(fundSubType, ENCODING);
            factorClean      = URLDecoder.decode(factor, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfListClassTypeSubTypeFactorReq  asset class {} fund type {} sub type {} factor {}",
        //        assetClassClean, fundTypeClean, fundSubTypeClean, factorClean);

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getByAssetClassFundTypeSubTypeFactor(assetClassClean, fundTypeClean, fundSubTypeClean, factorClean);

        // Convert to list format
        List<ListModel> recs = listService.fundListModel(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;

        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE,
                fundSubTypeClean.toUpperCase() + " - " +
                        fundTypeClean.toUpperCase() + " - " +
                        assetClassClean.toUpperCase() + " - " +
                        factorClean.toUpperCase() +
                        " FUNDS as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/mf-list-factors-all")
    public String etfListFactorsAllReq(
            Model model)
    {
        //logger.debug("mfListFactorsAllReq  ");

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getFactorsAll();

        // Convert to list format
        List<ListModel> recs = listService.fundListModelFactors(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;

        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "FACTOR Funds as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/fund-list-domestic-factors-all")
    public String fundListDomesticFactorsAllReq(
            Model model)
    {
        //logger.debug("fundListFactorsAllReq  ");

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getFactorsAll();

        // Convert to list format
        List<ListModel> recs = listService.fundListModelDomesticFactors(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;

        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, "DOMESTIC FACTOR Funds as of " + formattedDate);

        return LIST_PAGE;
    }

    @RequestMapping("/mf-list-factors-class")
    public String mfListFactorClassReq(
            @RequestParam(value="class", required=true) String assetClass,
            Model model)
    {
        String assetClassClean = "";

        try {
            assetClassClean  = URLDecoder.decode(assetClass, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfListFactorClassReq asset class {} ", assetClassClean);

        // Get the Funds to display
        List<Fund>      toConvert   = fundTbl.getFactorsAll();

        // Convert to list format
        List<ListModel> recs = listService.fundListModelDomesticFactors(toConvert);

        SimpleDateFormat formatter    = new SimpleDateFormat(DATE_FORMAT);
        String formattedDate = NO_DATE;

        if ( recs.size() > 0 )
            formattedDate = formatter.format(recs.get(0).getDate());

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE, assetClassClean.toUpperCase() + " FACTORS FUNDS as of " + formattedDate);

        return LIST_PAGE;
    }


    @RequestMapping("/mf-list-by-symbol")
    public String etfListBySymbol(
            @RequestParam(value="file", required=true) String fileName,
            @RequestParam(value="title", required=true)  String pageTitle,
            Model model)
    {
        String fileNameClean  = "";
        String pageTitleClean = "";

        try {
            fileNameClean  = URLDecoder.decode(fileName,  ENCODING);
            pageTitleClean = URLDecoder.decode(pageTitle, ENCODING);
        } catch (UnsupportedEncodingException ignored) {}

        //logger.debug("mfListBySymbol asset class {} fund type {}", fileNameClean, pageTitleClean);

        //
        // Read symbols from file
        FinanceProperties   fp       = new FinanceProperties();
        Properties          prop     = fp.get();
        String              filePath = prop.getProperty("file_dir") + "/" +fileNameClean;

        List<String> symbols = Collections.emptyList();
        try {
            symbols = Files.readAllLines(Paths.get(filePath));
        } catch (IOException ignored) {}

        // Get the Funds to display
        String lineSeparator=System.lineSeparator();

        ArrayList<Fund> toConvert = new ArrayList<>(symbols.size());
        for ( String symbol : symbols) {
            Fund fund = fundTbl.get(symbol.replace(lineSeparator, "").trim().toUpperCase());
            if ( fund != null ) {
                toConvert.add(fund);
            }
        }

        // Convert to list format
        List<ListModel> recs = listService.fundListModel(toConvert);

        model.addAttribute(LIST_MODEL, recs);
        model.addAttribute(PAGE_TITLE,  pageTitleClean);

        return LIST_PAGE;
    }

}

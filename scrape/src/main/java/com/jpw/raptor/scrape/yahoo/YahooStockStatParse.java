package com.jpw.raptor.scrape.yahoo;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class YahooStockStatParse {

    public YahooStockStatistics readPage(String symbol) throws IOException {

        String                  page;
        YahooStockStatistics    results    = new YahooStockStatistics();
        YahooStockStatParseJS   parserJS   = new YahooStockStatParseJS();
        YahooStockStatParseNoJS parserNoJS = new YahooStockStatParseNoJS();

        results.setReadFailed(false);

        // Yahoo replaces . with -
        String ticker = symbol.toUpperCase().replace('.', '-');
        String baseUrl = "https://finance.yahoo.com/quote/" + ticker + "/key-statistics?p=" + ticker;
        try {
            Document doc = Jsoup.connect(baseUrl).get();
            page = doc.toString();
        } catch ( org.jsoup.HttpStatusException ex ) {
            System.out.println("Stock statistics HttpStatusException " + ex.toString());
            results.setReadFailed(true);
            return results;
        }

        // Validate page was found
        if (page.contains("<span>No results for")  || page.contains("<span>Symbols similar to")) {
            System.out.println("Stock statistic Symbol not found " + symbol);
            results.setReadFailed(true);
            return results;
        }

        if ( page.contains("NoJs ") ) {
            try {
                //System.out.println(" No Java Script " );
                parserNoJS.parse(page, results);
            } catch ( Exception ex ) {
                results.setReadFailed(true);
                System.out.println(" **** Stock statistic No Java Script parse failed ***** " );
                System.out.println(ex.toString());
                BufferedWriter writer = new BufferedWriter(new FileWriter("/home/software/page-error-stock-stat-no-js.txt"));
                writer.write(page);
            }
        } else {
            try {
                //System.out.println(" Java Script " );
                parserJS.parse(page, results);
            } catch ( Exception ex ) {
                results.setReadFailed(true);
                System.out.println(" **** Stock statistic Java Script parse failed ***** " );
                System.out.println(ex.toString());
                BufferedWriter writer = new BufferedWriter(new FileWriter("/home/software/page-error-stock-stat-js.txt"));
                writer.write(page);
            }
        }

        return results;
    }
}

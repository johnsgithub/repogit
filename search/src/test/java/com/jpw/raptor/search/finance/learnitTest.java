package com.jpw.raptor.search.finance;


import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.query_dsl.*;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.SourceConfig;
import co.elastic.clients.elasticsearch.indices.*;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.jpw.raptor.model.FinanceKnowledge;
import com.jpw.raptor.model.FinanceKnowledgeModel;
import com.jpw.raptor.model.RaptorConstants;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpHost;





import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import org.elasticsearch.client.RestClient;
import org.elasticsearch.core.TimeValue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.data.elasticsearch.core.query.SourceFilter;

/**
 * Created by john on 4/22/18.
 */
public class learnitTest {

    public ElasticsearchClient client;

    @Before
    public void setup() {

        // Create the low-level client
        RestClient restClient = RestClient.builder(
                new HttpHost("localhost", 9200)).build();

        // Create the transport with a Jackson mapper
        ElasticsearchTransport transport = new RestClientTransport(
                restClient, new JacksonJsonpMapper());

        // And create the API client
        client = new ElasticsearchClient(transport);
    }

    @Ignore
    @Test
    public void aTest() throws IOException {

    }


    public FinanceKnowledge buildRec01() {
        FinanceKnowledge rec   = new FinanceKnowledge();

        StringBuilder   sb          = new StringBuilder();
        String          eol         = System.getProperty("line.separator");

        SimpleDateFormat formatter  = new SimpleDateFormat("yyyy");
        String           year       = formatter.format(new Date());


        rec.setTitle("");
        rec.setTag("");

        sb.append("x").append(eol);
        rec.setBody(sb.toString());

        rec.setId(DigestUtils.md5Hex(rec.getBody()).toUpperCase());
        rec.setLoc( "/doc/" + year + "/" + rec.getId() + ".txt" );
        rec.setUrl("/html/" + year + "/" + rec.getId() + ".html" );
        return rec;
    }

    public void createIndex(String index) throws IOException{

        // note you can only call settings once
        // create index request
        CreateIndexRequest indexReq2 = new CreateIndexRequest.Builder()
                .index(index)
                .settings( builder -> builder
                        .numberOfReplicas("0")
                        .numberOfShards("1") )
                //.mappings
                .build();

        CreateIndexResponse createResponse2 = client.indices().create(indexReq2);
        System.out.println(createResponse2.toString());

        /*
            Add mappings to index

            PUT /index-learn
                {
                    "mappings":{
                        "properties":{
                            "loc":{"type":"text","index":"false"},
                            "url":{"type":"text","index":"false"},
                            "title":{"type":"text","analyzer":"english"},
                            "tag":{"type":"text","analyzer":"english"},
                            "body":{"type":"text","analyzer":"english"}
                        }
                    }
                }
         */
    }

    public void deleteIndex(String index) throws IOException {

        // Time
        // Whenever durations need to be specified, e.g. for a timeout parameter,
        // the duration must specify the unit, like 2d for 2 days. The supported units are:
        //
        // d Days
        // h Hours
        // m Minutes
        // s Seconds
        // ms Milliseconds
        // micros Microseconds
        // nanos  Nanoseconds

        DeleteIndexRequest req = new DeleteIndexRequest.Builder()
                .index(index)
                .timeout(s -> s .time("1s"))
                .build();

        DeleteIndexResponse rsp = client.indices().delete(req);
        System.out.println(rsp.toString());

    }

    public void openIndex(String index) throws IOException {

        OpenRequest req = new OpenRequest.Builder()
                .index(index)
                .build();

        OpenResponse rsp = client.indices().open(req);
        System.out.println(rsp.toString());

    }


    public void closeIndex(String index) throws IOException {

        CloseIndexRequest req = new CloseIndexRequest.Builder()
                .index(index)
                .build();

        CloseIndexResponse rsp = client.indices().close(req);
        System.out.println(rsp.toString());

    }


    public void indexDocument(String index, FinanceKnowledge rec) throws IOException {

        IndexRequest<FinanceKnowledge> request = IndexRequest.of(indexRequestBuilder -> indexRequestBuilder
                .index(index)
                .id(rec.getId())
                .document(rec)
        );

        IndexResponse response = client.index(request);
        System.out.println("Indexed with version " + response.version());

        System.out.println("Index result " + response.result());
/*
 result
    The result of the indexing operation, "created" or "updated".
 */

    }

    public void deleteDocument(String index, String docId) throws IOException {

        DeleteRequest request = DeleteRequest.of(deleteRequestBuilder -> deleteRequestBuilder
                .index(index)
                .id(docId)
                .timeout(s -> s .time("1s"))
        );

        DeleteResponse rsp = client.delete(deleteRequestBuilder -> deleteRequestBuilder
                        .index(index)
                        .id(docId)
                        .timeout(s -> s .time("1s"))
        );

        System.out.println(rsp.toString());
    }

    public void updateDocument(String index, String id, FinanceKnowledge rec) throws IOException {

        UpdateResponse<FinanceKnowledge> rsp = client.update(
                updateRequestBuilder -> updateRequestBuilder
                        .index(index)
                        .id(id)
                        .doc(rec),
                FinanceKnowledge.class);

        System.out.println("Index result " + rsp.result());

    }


    public void getDocument(String index, String docId) throws IOException {

        GetResponse<FinanceKnowledge> rsp = client.get(getRequestBuilder -> getRequestBuilder
                        .index(index)
                        .id(docId),
                        //.sourceIncludes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                        //.sourceExcludes(RaptorConstants.FIELD_BODY),
                        FinanceKnowledge.class
        );

        if (rsp.found()) {
            FinanceKnowledge rec = rsp.source();

        } else {

        }

    }

    public List<FinanceKnowledge> getAll() throws IOException {

        SearchResponse<FinanceKnowledge> responses = client.search(searchReqBuilder -> searchReqBuilder
                        // index to search
                        .index("products")
                        // fields to return from matching documents
                        .source(sourceConfigBuilder -> sourceConfigBuilder
                                .filter(sourceFilterBuilder -> sourceFilterBuilder
                                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                                        .excludes(RaptorConstants.FIELD_BODY)
                                ))
                        // wait time for a shards response
                        .timeout("1s")
                        // this is not needed since the default score sort is desc and match all query every document has a 1 score
                        .sort(sortOptionsBuilder -> sortOptionsBuilder .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)))
                        .query(MatchAllQuery.of(matchAllQueryBuilder -> matchAllQueryBuilder )._toQuery()),
                FinanceKnowledge.class
        );

        // another way

        Query queryMatchAll = new Query.Builder() .matchAll(new MatchAllQuery.Builder().build()).build();

        Query queryMatchAll2 = MatchAllQuery.of(matchAllQueryBuilder -> matchAllQueryBuilder )._toQuery();

        SourceConfig fieldsToReturn = SourceConfig.of(sourceConfigBuilder -> sourceConfigBuilder
                .filter(sourceFilterBuilder -> sourceFilterBuilder
                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                        .excludes(RaptorConstants.FIELD_BODY)
                ));

        SortOptions scoreSort = SortOptions.of(sortOptionsBuilder -> sortOptionsBuilder
                .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)));

        int responseCount = responses.hits().hits().size();

        for (Hit<FinanceKnowledge> hit: responses.hits().hits()) {
            FinanceKnowledge doc = hit.source();
            Double score = hit.score();
            String id = hit.id();
        }

        return null;
    }

    public List<FinanceKnowledge> matchAnyWordInAField() throws IOException {

        String searchText = "bike";

        // match documents with one or more words
        Query queryMatch = MatchQuery.of(matchQueryBuilder -> matchQueryBuilder
                .field("")
                .query("")
                .analyzer("")
                .operator(Operator.Or)
        )._toQuery();

        SourceConfig fieldsToReturn = SourceConfig.of(sourceConfigBuilder -> sourceConfigBuilder
                .filter(sourceFilterBuilder -> sourceFilterBuilder
                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                        .excludes(RaptorConstants.FIELD_BODY)
                ));

        SortOptions scoreSort = SortOptions.of(sortOptionsBuilder -> sortOptionsBuilder
                .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)));

        SearchResponse<FinanceKnowledge> responses = client.search(searchReqBuilder -> searchReqBuilder
                        // index to search
                        .index("products")
                        // fields to return from matching documents
                        .source(sourceConfigBuilder -> sourceConfigBuilder
                                .filter(sourceFilterBuilder -> sourceFilterBuilder
                                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                                        .excludes(RaptorConstants.FIELD_BODY)
                                ))
                        // wait time for a shards response
                        .timeout("1s")
                        // this is not needed since the default score sort is desc and match all query every document has a 1 score
                        .sort(sortOptionsBuilder -> sortOptionsBuilder .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)))
                        .query(queryMatch),
                FinanceKnowledge.class
        );

        int responseCount = responses.hits().hits().size();

        for (Hit<FinanceKnowledge> hit: responses.hits().hits()) {
            FinanceKnowledge doc = hit.source();
            Double score = hit.score();
            String id = hit.id();
        }

        return null;
    }


    public List<FinanceKnowledge> matchAllWordsInAField() throws IOException {

        String searchText = "bike";

        // match documents with one or more words
        Query queryMatch = MatchQuery.of(matchQueryBuilder -> matchQueryBuilder
                        .field("")
                        .query("")
                        .analyzer("")
                        .operator(Operator.And)
        )._toQuery();

        SourceConfig fieldsToReturn = SourceConfig.of(sourceConfigBuilder -> sourceConfigBuilder
                .filter(sourceFilterBuilder -> sourceFilterBuilder
                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                        .excludes(RaptorConstants.FIELD_BODY)
                ));

        SortOptions scoreSort = SortOptions.of(sortOptionsBuilder -> sortOptionsBuilder
                .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)));

        SearchResponse<FinanceKnowledge> responses = client.search(searchReqBuilder -> searchReqBuilder
                        // index to search
                        .index("products")
                        // fields to return from matching documents
                        .source(sourceConfigBuilder -> sourceConfigBuilder
                                .filter(sourceFilterBuilder -> sourceFilterBuilder
                                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                                        .excludes(RaptorConstants.FIELD_BODY)
                                ))
                        // wait time for a shards response
                        .timeout("1s")
                        // this is not needed since the default score sort is desc and match all query every document has a 1 score
                        .sort(sortOptionsBuilder -> sortOptionsBuilder .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)))
                        .query(queryMatch),
                FinanceKnowledge.class
        );

        int responseCount = responses.hits().hits().size();

        for (Hit<FinanceKnowledge> hit: responses.hits().hits()) {
            FinanceKnowledge doc = hit.source();
            Double score = hit.score();
            String id = hit.id();
        }

        return null;
    }


    public List<FinanceKnowledge> matchAnyWordInMultipleFields() throws IOException {

        String searchText = "bike";

        // match documents with one or more words
        Query queryMultiMatch = MultiMatchQuery.of(multiMatchQueryBuilder -> multiMatchQueryBuilder
                        .fields("", "", "")
                        .query("")
                        .analyzer("")
                // all the words must match
                //.operator(Operator.And)
                // one of the words must match
                //.operator(Operator.Or)
        )._toQuery();

        SourceConfig fieldsToReturn = SourceConfig.of(sourceConfigBuilder -> sourceConfigBuilder
                .filter(sourceFilterBuilder -> sourceFilterBuilder
                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                        .excludes(RaptorConstants.FIELD_BODY)
                ));

        SortOptions scoreSort = SortOptions.of(sortOptionsBuilder -> sortOptionsBuilder
                .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)));

        SearchResponse<FinanceKnowledge> responses = client.search(searchReqBuilder -> searchReqBuilder
                        // index to search
                        .index("products")
                        // fields to return from matching documents
                        .source(sourceConfigBuilder -> sourceConfigBuilder
                                .filter(sourceFilterBuilder -> sourceFilterBuilder
                                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                                        .excludes(RaptorConstants.FIELD_BODY)
                                ))
                        // wait time for a shards response
                        .timeout("1s")
                        // this is not needed since the default score sort is desc and match all query every document has a 1 score
                        .sort(sortOptionsBuilder -> sortOptionsBuilder .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)))
                        .query(queryMultiMatch),
                FinanceKnowledge.class
        );

        int responseCount = responses.hits().hits().size();

        for (Hit<FinanceKnowledge> hit: responses.hits().hits()) {
            FinanceKnowledge doc = hit.source();
            Double score = hit.score();
            String id = hit.id();
        }

        return null;
    }


    public List<FinanceKnowledge> matchPhraseInAField() throws IOException {

        String searchText = "bike";

        // match documents with one or more words
        Query queryMatchPhrase = MatchPhraseQuery.of(matchPhraseQueryBuilder -> matchPhraseQueryBuilder
                        .field("")
                        .query("")
                        .analyzer("")
                // slop is the number of other words that may appear between the phrase words
                .slop(0)
        )._toQuery();

        SourceConfig fieldsToReturn = SourceConfig.of(sourceConfigBuilder -> sourceConfigBuilder
                .filter(sourceFilterBuilder -> sourceFilterBuilder
                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                        .excludes(RaptorConstants.FIELD_BODY)
                ));

        SortOptions scoreSort = SortOptions.of(sortOptionsBuilder -> sortOptionsBuilder
                .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)));

        SearchResponse<FinanceKnowledge> responses = client.search(searchReqBuilder -> searchReqBuilder
                        // index to search
                        .index("products")
                        // fields to return from matching documents
                        .source(sourceConfigBuilder -> sourceConfigBuilder
                                .filter(sourceFilterBuilder -> sourceFilterBuilder
                                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                                        .excludes(RaptorConstants.FIELD_BODY)
                                ))
                        // wait time for a shards response
                        .timeout("1s")
                        // this is not needed since the default score sort is desc and match all query every document has a 1 score
                        .sort(sortOptionsBuilder -> sortOptionsBuilder .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)))
                        .query(queryMatchPhrase),
                FinanceKnowledge.class
        );

        int responseCount = responses.hits().hits().size();

        for (Hit<FinanceKnowledge> hit: responses.hits().hits()) {
            FinanceKnowledge doc = hit.source();
            Double score = hit.score();
            String id = hit.id();
        }

        return null;
    }


    public List<FinanceKnowledge> nestedQuery() throws IOException {

        Query queryText = MatchQuery.of(matchQueryBuilder -> matchQueryBuilder
                .field("")
                .query("")
                .analyzer("")
                .operator(Operator.And)
        )._toQuery();

        // match documents with one or more words
        Query queryTitle = MatchQuery.of(matchQueryBuilder -> matchQueryBuilder
                .field("")
                .query("")
                .analyzer("")
                .operator(Operator.Or)
        )._toQuery();

        Query boolQuery = BoolQuery.of(boolQueryBuilder -> boolQueryBuilder
                .should(queryText)
                .must(queryTitle)
        )._toQuery();

        SourceConfig fieldsToReturn = SourceConfig.of(sourceConfigBuilder -> sourceConfigBuilder
                .filter(sourceFilterBuilder -> sourceFilterBuilder
                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                        .excludes(RaptorConstants.FIELD_BODY)
                ));

        SortOptions scoreSort = SortOptions.of(sortOptionsBuilder -> sortOptionsBuilder
                .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)));

        SearchResponse<FinanceKnowledge> responses = client.search(searchReqBuilder -> searchReqBuilder
                        // index to search
                        .index("products")
                        // fields to return from matching documents
                        .source(sourceConfigBuilder -> sourceConfigBuilder
                                .filter(sourceFilterBuilder -> sourceFilterBuilder
                                        .includes(RaptorConstants.FIELD_TITLE, RaptorConstants.FIELD_URL, RaptorConstants.FIELD_LOC, RaptorConstants.FIELD_TAG)
                                        .excludes(RaptorConstants.FIELD_BODY)
                                ))
                        // wait time for a shards response
                        .timeout("1s")
                        // this is not needed since the default score sort is desc and match all query every document has a 1 score
                        .sort(sortOptionsBuilder -> sortOptionsBuilder .score(scoreSortBuilder -> scoreSortBuilder .order(SortOrder.Desc)))
                        .query(boolQuery),
                FinanceKnowledge.class
        );

        int responseCount = responses.hits().hits().size();

        for (Hit<FinanceKnowledge> hit: responses.hits().hits()) {
            FinanceKnowledge doc = hit.source();
            Double score = hit.score();
            String id = hit.id();
        }

        return null;
    }

    public List<FinanceKnowledge> learnQueries(String phrase) throws IOException {

        return null;
    }


    @Ignore
    @Test
    public void search() throws IOException {
/*

        //
        // enable fuzzy search
        // which is the number of one character changes that need to be made to one string to make it the same as another string.
        // AUTO generates an edit distance based on the length of the term.
        //      term length 0..2 must match exactly
        //      term length 3..5 one edit allowed
        //      term length >5 two edits allowed
        // prefix_length The number of initial characters which will not be “fuzzified”
        // max_expansions The maximum number of terms that the fuzzy query will expand to.
        QueryBuilders.matchQuery("user", "john doe").fuzziness(Fuzziness.AUTO).prefixLength(3).maxExpansions(10);

        //
        // term query exact match for kimchy in the user field (includes spelling and capitalization)


        //
        // matches all documents where john or doe appear in any of the specified fields
        searchSourceBuilder.query(QueryBuilders.multiMatchQuery("john doe", "field_1", "field_2", "*_field_wildcard"));


        // boolean query
        searchSourceBuilder.query(
                QueryBuilders.boolQuery()
                        .must(QueryBuilders.termQuery("content", "test1"))
                        .must(QueryBuilders.termQuery("content", "test4"))
                        .mustNot(QueryBuilders.termQuery("content", "test2"))
                        .should(QueryBuilders.termQuery("content", "test3"))
                        .filter(QueryBuilders.termQuery("content", "test5"))
        );

        //
        // Search response
        RestStatus  status = searchResponse.status();
        TimeValue   took = searchResponse.getTook();
        Boolean     terminatedEarly = searchResponse.isTerminatedEarly();
        boolean     timedOut = searchResponse.isTimedOut();

        //
        // Search hits
        SearchHits hits = searchResponse.getHits();
        long totalHits = hits.getTotalHits();
        float maxScore = hits.getMaxScore();

 */
    }


    @Ignore
    @Test
    public void searchWithScroll() throws IOException {
    }


}

package com.jpw.raptor.analytic;

import com.jpw.raptor.model.*;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by john on 6/26/18.
 */
public class SpreadSheet {

    protected Sheet           tab;
    protected CreationHelper  helper;
    protected CellStyle       center;
    protected CellStyle       left;
    protected short           rowNo;
    protected Workbook        wb;

    public SpreadSheet() {

        wb      = new XSSFWorkbook();
        helper  = wb.getCreationHelper();
        tab     = wb.createSheet("Signals");

        center  = wb.createCellStyle();
        center.setAlignment(HorizontalAlignment.CENTER);

        left    = wb.createCellStyle();
        left.setAlignment(HorizontalAlignment.LEFT);

        rowNo   = 0;

        addColumnHeaders();
    }


    public void writeToFile(String dir, String name) throws IOException {

        FileOutputStream fileOut = new FileOutputStream(dir + "/" + name + ".xlsx" );
        wb.write(fileOut);
        fileOut.close();
    }


    protected void addColumnHeaders() {

        // column number in the row
        int col = 0;

        // Cell object
        Cell cell;

        // Create row for the column headers
        Row row = tab.createRow(rowNo);

        // Symbol column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("Symbol"));
        cell.setCellStyle(center);

        // Date column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("Date"));
        cell.setCellStyle(center);

        // JDK column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("JDK"));
        cell.setCellStyle(center);

        // Moving Average column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("Moving Avg"));
        cell.setCellStyle(center);

        // MACD column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("MACD"));
        cell.setCellStyle(center);

        // RSI column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("RSI"));
        cell.setCellStyle(center);

        // ADX column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("ADX"));
        cell.setCellStyle(center);

        // ATR column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("ATR"));
        cell.setCellStyle(center);

        // Avgs column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("Close"));
        cell.setCellStyle(center);

        // Avgs column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("5"));
        cell.setCellStyle(center);

        // Avgs column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("10"));
        cell.setCellStyle(center);

        // Avgs column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("20"));
        cell.setCellStyle(center);

        // Avgs column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("50"));
        cell.setCellStyle(center);

        // Avgs column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("100"));
        cell.setCellStyle(center);

        // Avgs column header
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString("200"));
        cell.setCellStyle(center);
        // update for next row entry
        rowNo++;
    }

    public void addRow(TechnicalAnalysisModel rec) {

        // column number in the row
        int col = 0;

        // Cell object
        Cell cell;

        // format percentage change
        DecimalFormat df = new DecimalFormat("#.##");

        double val = 0.0;
        double fmt = 0.0;
        String str;

        // Create row
        Row row = tab.createRow(rowNo);

        // symbol
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString(rec.getSymbol()));
        cell.setCellStyle(center);

        // date
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fromatedDate = formatter.format(rec.getDate());
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString(fromatedDate));
        cell.setCellStyle(center);

        // JDX
        cell = row.createCell(col++);
        cell.setCellValue(rec.getJdkSignal());
        cell.setCellStyle(center);

        // Moving Average
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString(rec.getMAvgSignal()));
        cell.setCellStyle(center);

        // MACD
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString(rec.getMacdSignal()));
        cell.setCellStyle(center);

        // RSI
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString(rec.getRsiSignal()));
        cell.setCellStyle(center);

        // ADX
        cell = row.createCell(col++);
        cell.setCellValue(helper.createRichTextString(rec.getAdxSignal()));
        cell.setCellStyle(center);

        // ATR
        cell = row.createCell(col++);
        cell.setCellValue(rec.getAtrSignal());
        cell.setCellStyle(center);

        // avgs
        cell = row.createCell(col++);
        cell.setCellValue(rec.getAvgs().getClose());
        cell.setCellStyle(center);

        // avgs
        cell = row.createCell(col++);
        cell.setCellValue(rec.getAvgs().getSimple5());
        cell.setCellStyle(center);

        // avgs
        cell = row.createCell(col++);
        cell.setCellValue(rec.getAvgs().getSimple10());
        cell.setCellStyle(center);

        // avgs
        cell = row.createCell(col++);
        cell.setCellValue(rec.getAvgs().getSimple20());
        cell.setCellStyle(center);

        // avgs
        cell = row.createCell(col++);
        cell.setCellValue(rec.getAvgs().getSimple50());
        cell.setCellStyle(center);

        // avgs
        cell = row.createCell(col++);
        cell.setCellValue(rec.getAvgs().getSimple100());
        cell.setCellStyle(center);

        // avgs
        cell = row.createCell(col++);
        cell.setCellValue(rec.getAvgs().getSimple200());
        cell.setCellStyle(center);

        // Increment for next row
        rowNo++;
    }

}

package com.jpw.raptor.cmdline.reports.track;

import com.jpw.raptor.model.TrackRec;

import java.text.SimpleDateFormat;
import java.util.*;

public class TrackMap {

    protected TreeMap<String, TrackRec> map;

    public TrackMap() {
        Date formatted = null;
        map = new TreeMap<String, TrackRec>();
        addRec(new TrackRec ("XLC", "", "Etf", "Communication Services", formatted, 0.0));
        addRec(new TrackRec ("XLY", "", "Etf", "Consumer Discretionary", formatted, 0.0));
        addRec(new TrackRec ("XLP", "", "Etf", "Consumer Staples", formatted, 0.0));
        addRec(new TrackRec ("XLE", "", "Etf", "Energy", formatted, 0.0));
        addRec(new TrackRec ("XLF", "", "Etf", "Financials", formatted, 0.0));
        addRec(new TrackRec ("XLV", "", "Etf", "Health Care", formatted, 0.0));
        addRec(new TrackRec ("XLI", "", "Etf", "Industrials", formatted, 0.0));
        addRec(new TrackRec ("XLK", "", "Etf", "Information Technology", formatted, 0.0));
        addRec(new TrackRec ("XLB", "", "Etf", "Materials", formatted, 0.0));
        addRec(new TrackRec ("XLRE", "", "Etf", "Real Estate", formatted, 0.0));
        addRec(new TrackRec ("XLU", "", "Etf", "Utilities", formatted, 0.0));
        addRec(new TrackRec ("SPY", "", "Etf", "SP 500", formatted, 0.0));
    }

    public void addRecs(List<TrackRec> recs) {
        for ( TrackRec rec : recs ) {
            addRec(rec);
        }
    }

    public void addRec(TrackRec rec) {

        if ( map.containsKey(rec.getSymbol()) ) {
            // Update analyst count and names
            TrackRec newRec = map.get(rec.getSymbol());
            newRec.setCount(newRec.getCount() + 1);
            newRec.setAnalyst(newRec.getAnalyst() + ":" + rec.getAnalyst());
            map.replace(rec.getSymbol(), newRec);
        } else {
            // add record to map
            rec.setCount(1);
            map.put(rec.getSymbol(), rec);
        }
    }

    public List<TrackRec> getRecs() {
        // any entries remaining in the map are to be deleted from
        // the database since they are no longer recommended
        List<TrackRec> list = new ArrayList<TrackRec>(map.size());
        for (Map.Entry<String, TrackRec> entrySetVal : map.entrySet()) {
            TrackRec entry = entrySetVal.getValue();
            list.add(entry);
        }

        return list;
    }
}

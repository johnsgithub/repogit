package com.jpw.raptor.onetime.updateindex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by john on 10/22/18.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Entry {
    private String symbol;
    private String name;
    private String sector;
}

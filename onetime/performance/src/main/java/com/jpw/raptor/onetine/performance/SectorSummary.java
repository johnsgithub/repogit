package com.jpw.raptor.onetine.performance;


import javafx.util.Pair;

import java.util.*;

public class SectorSummary {

    private TreeMap<String,Integer> map;

    public SectorSummary() {
        map = new TreeMap<>();
    }

    public void addSector(String sector) {
        Integer objFound = map.get(sector);

        if ( objFound == null ) {
            map.put(sector, 1);
        } else {
            Integer newValue = objFound + 1;
            map.put(sector, newValue);
        }
    }

    // Quotes are in descending order by date
    public List<Pair<String,Integer>> getSectors() {

        // Allocate space for the list
        List<Pair<String, Integer>>  result = new ArrayList<>();

        //
        // Populate a list
        // traverse the map in sorted order which is ascending order
        for (Map.Entry<String, Integer> entrySetVal : map.entrySet()) {
            result.add(new Pair<>(entrySetVal.getKey(), entrySetVal.getValue()));
        }

        // Reverse the list from ascending to descending order
        //Collections.reverse(result)

        return result;
    }

    public String formatDouble(Double d) {
        return String.format("%.1f",d * 100.0) + "%";
    }

    // Quotes are in descending order by date
    public Integer getSectorCount() {

        Integer count = 0;

        //
        // Populate a list
        // traverse the map in sorted order which is ascending order
        for (Map.Entry<String, Integer> entrySetVal : map.entrySet()) {
            count += entrySetVal.getValue();
        }

        return count;
    }

    public List<Pair<String,Pair<String,String>>> combineSummaries(SectorSummary a, SectorSummary b) {

        TreeMap<String,Pair<String,String>> combinedMap = new TreeMap<>();

        // Get the values for the first summary
        List<Pair<String,Integer>> aList = a.getSectors();

        // Compute percentage and add to map
        Double aCount = Double.valueOf( a.getSectorCount() );

        for ( Pair<String,Integer> aRecord : aList ) {
            Double aPercent = Double.valueOf( aRecord.getValue() ) / aCount;
            combinedMap.put(aRecord.getKey(), new Pair<String,String>(formatDouble(aPercent),"0.0%"));
        }

        // Get the values for the second summary
        List<Pair<String,Integer>> bList = b.getSectors();

        // Compute percentage and update map
        Double bCount = Double.valueOf( b.getSectorCount() );

        for ( Pair<String,Integer> bRecord : bList ) {

            Pair<String,String> sectorFound = combinedMap.get(bRecord.getKey());

            if ( sectorFound != null ) {
                Double bPercent = Double.valueOf( bRecord.getValue() ) / bCount;
                combinedMap.put(bRecord.getKey(), new Pair<String,String>(sectorFound.getKey(), formatDouble(bPercent)));
            }
        }

        // generate the result list
        // Allocate space for the list
        List<Pair<String,Pair<String,String>>>  result = new ArrayList<>();

        //
        // Populate a list
        // traverse the map in sorted order which is ascending order
        for (Map.Entry<String, Pair<String,String>> entrySetVal : combinedMap.entrySet()) {
            result.add(new Pair<>(entrySetVal.getKey(), entrySetVal.getValue()));
        }

        // Reverse the list from ascending to descending order
        //Collections.reverse(result)

        return result;
    }

}

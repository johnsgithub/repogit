package com.jpw.raptor.onetime.generic;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {

    public String firstLine;
    private static final DecimalFormat df = new DecimalFormat("0.00");

    public List<Entry> getRecords(String fileName) throws IOException {

        ArrayList<Entry> list = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader(fileName));

        String line = null;

        // Ignore first line
        firstLine = br.readLine();

        // Read each record in the file
        while ( (line = br.readLine()) != null ) {
            String[] str = line.split(",");
            if ( str.length >=3 ) {
                // Valid number of columns
                Entry entry = new Entry();
                entry.setSymbol(str[0]);
                entry.setName(str[1]);
                entry.setSector(str[2]);
                entry.setIndustry(str[3]);
                entry.setYtd(Double.parseDouble(str[4]));
                entry.setOneYear(Double.parseDouble(str[5]));
                list.add(entry);
            }
        }

        br.close();
        return list;
    }

    public String formatEntry (Entry e) {
        return  e.getSymbol() + "," +
                e.getName() + "," +
                e.getSector() + "," +
                e.getIndustry() + "," +
                df.format(e.getYtd()) + "," +
                df.format(e.getOneYear()) + "\n";

    }

    public void writeRecords(String fileName, List<Entry> list) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));

        writer.write(firstLine + "\n");

        for (Entry e : list) {
            writer.write(formatEntry(e));
        }

        writer.close();

    }


    public List<Entry> selectRecords(List<Entry> list, double ytdMin, double oneYearMin, double mult) throws IOException {

        ArrayList<Entry> newList = new ArrayList<>();

        for (Entry e : list) {
            if ( e.getOneYear() >= oneYearMin && e.getYtd() >= ytdMin && ( e.getOneYear() > (e.getYtd() * mult)) ) {
                newList.add(e);
            }
        }

        return newList;
    }

}
